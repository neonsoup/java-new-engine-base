#version 150

in vec2 in_Position;

uniform mat4 u_ModelMatrix;
uniform mat4 u_ProjectionMatrix;

void main() {
    gl_Position = u_ProjectionMatrix * u_ModelMatrix * vec4(in_Position, 0.0, 1.0);
}
