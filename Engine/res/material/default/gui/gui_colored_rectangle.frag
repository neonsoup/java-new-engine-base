#version 150

uniform vec4 u_Color;

out vec4 result;

void main() {
    result = u_Color;
}