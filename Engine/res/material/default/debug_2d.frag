#version 150

in vec4 var_Color;

out vec4 result;

void main() {
    result = var_Color;
}