#version 150

in vec2 in_Position;
in vec4 in_Color;

uniform mat4 u_ModelMatrix;
uniform mat4 u_ViewMatrix;
uniform mat4 u_ProjectionMatrix;

out vec4 var_Color;

void main() {
    var_Color = in_Color;
    
    gl_Position = u_ProjectionMatrix * u_ViewMatrix * u_ModelMatrix * vec4(in_Position, 0.0f, 1.0);
}
