#version 150

in vec2 var_TexCoord;

in vec3 var_Normal;

out vec4 result;

void main() {
    result = vec4((normalize(var_Normal) * vec3(0.5)) + vec3(0.5), 1.0);
}