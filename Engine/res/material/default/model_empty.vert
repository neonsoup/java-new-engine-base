#version 150

in vec3 in_Position;
in vec3 in_Normal;

uniform mat4 u_ModelMatrix;
uniform mat4 u_ViewMatrix;
uniform mat4 u_ProjectionMatrix;

#ifdef FEATURE_SKINNING

#define MAX_BONES 16

in uint in_BoneCount;
in uvec4 in_BoneIndices;
in vec4 in_BoneWeights;

uniform Bone {
    mat4 matrix[MAX_BONES];
} bones;

#endif

out vec3 var_Normal;

void main() {
    var_Normal = (transpose(inverse(u_ModelMatrix)) * vec4(in_Normal, 1.0)).xyz;
    
    vec4 pos;
    
    #ifdef FEATURE_SKINNING
        
        pos = vec4(0.0, 0.0, 0.0, 0.0);
    
        for(uint i = 0u; i < in_BoneCount; i++) {
            mat4 armMat = bones.matrix[in_BoneIndices[i]];
            mat4 objMat = u_ProjectionMatrix * u_ViewMatrix * u_ModelMatrix;
            
            pos += in_BoneWeights[i] * (objMat * armMat * vec4(in_Position, 1.0));
        }
        
    #else
        
        pos = u_ProjectionMatrix * u_ViewMatrix * u_ModelMatrix * vec4(in_Position, 1.0);
        
    #endif
    
    gl_Position = pos;
}
