package com.neonsoup.engine3d.graphics.model;

import java.util.ArrayList;
import java.util.List;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;

import com.neonsoup.engine.scene.SceneConfiguration;

public class ArmatureModelPartNode extends ModelPartNode {

    protected final ModelArmatureData armatureData;

    protected final Vector3f[] boneTranslations;
    protected final Quaternionf[] boneRotations;
    protected final Vector3f[] boneScales;

    protected final Matrix4f[] armatureToPoseMatrices;
    protected final Matrix4f[] bindPoseToArmatureMatrices;

    protected final List<MeshArmatureBinding> meshArmatureBindings = new ArrayList<>(1);

    protected boolean armatureTransformDirty = false;


    public ArmatureModelPartNode(ModelArmatureData armatureData, SceneConfiguration config) {
        super(config);

        this.armatureData = armatureData;

        this.boneTranslations = new Vector3f[armatureData.bones.length];
        this.boneRotations = new Quaternionf[armatureData.bones.length];
        this.boneScales = new Vector3f[armatureData.bones.length];

        this.armatureToPoseMatrices = new Matrix4f[this.boneTranslations.length];
        this.bindPoseToArmatureMatrices = new Matrix4f[this.boneTranslations.length];

        for (int i = 0; i < boneTranslations.length; i++) {
            boneTranslations[i] = new Vector3f();
            boneRotations[i] = new Quaternionf();
            boneScales[i] = new Vector3f(1.0f, 1.0f, 1.0f);

            armatureToPoseMatrices[i] = new Matrix4f();
            bindPoseToArmatureMatrices[i] = new Matrix4f();
        }

        calculatePoseMatrices();
    }


    public void markTransformsDirty() {
        armatureTransformDirty = true;
    }


    public void calculatePoseMatrices() {
        for (ModelBone bone : armatureData.rootBones) {
            calculateBindTransformMatricesRecursive(bone);
        }

        for (int i = 0; i < bindPoseToArmatureMatrices.length; i++) {
            bindPoseToArmatureMatrices[i].set(armatureData.bones[i].invBindPoseMatrix);
        }

        for (MeshArmatureBinding meshArmatureBinding : meshArmatureBindings) {
            meshArmatureBinding.calculatePoseMatrices();
        }

        armatureTransformDirty = false;
    }


    public void setBoneTranslation(String boneName, float x, float y, float z) {
        int boneIndex = armatureData.bonesByName.get(boneName).index;
        boneTranslations[boneIndex].set(x, y, z);
        markTransformsDirty();
    }


    public void setBoneTranslation(String boneName, Vector3f vec) {
        int boneIndex = armatureData.bonesByName.get(boneName).index;
        boneTranslations[boneIndex].set(vec);
        markTransformsDirty();
    }


    public void setBoneRotationQuat(String boneName, float x, float y, float z, float w) {
        int boneIndex = armatureData.bonesByName.get(boneName).index;
        boneRotations[boneIndex].set(x, y, z, w);
        markTransformsDirty();
    }


    public void setBoneRotation(String boneName, Quaternionf rot) {
        int boneIndex = armatureData.bonesByName.get(boneName).index;
        boneRotations[boneIndex].set(rot);
        markTransformsDirty();
    }


    public void setBoneScale(String boneName, float sx, float sy, float sz) {
        int boneIndex = armatureData.bonesByName.get(boneName).index;
        boneScales[boneIndex].set(sx, sy, sz);
        markTransformsDirty();
    }


    public void setBoneScale(String boneName, Vector3f vec) {
        int boneIndex = armatureData.bonesByName.get(boneName).index;
        boneScales[boneIndex].set(vec);
        markTransformsDirty();
    }


    protected void calculateBindTransformMatricesRecursive(ModelBone bone) {
        int index = bone.index;

        Matrix4f mat = armatureToPoseMatrices[index];

        if (bone.parent == null) {
            mat.set(bone.bindPoseParentToThisMatrix);
        } else {
            mat.set(armatureToPoseMatrices[bone.parent.index]);
            mat.mul(bone.bindPoseParentToThisMatrix);
        }

        mat.translate(boneTranslations[index]);
        mat.rotate(boneRotations[index]);
        mat.scale(boneScales[index]);

        for (ModelBone child : bone.children) {
            calculateBindTransformMatricesRecursive(child);
        }
    }


    @Override
    public void updateTransform() {
        if (armatureTransformDirty) {
            calculatePoseMatrices();
        }

        super.updateTransform();
    }

}
