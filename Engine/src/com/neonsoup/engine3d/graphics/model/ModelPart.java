package com.neonsoup.engine3d.graphics.model;

import java.util.List;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;

import com.neonsoup.engine.AbstractDisposable;

final class ModelPart extends AbstractDisposable {

    public static enum Type {
        UNKNOWN, MESH, ARMATURE;
    }


    protected final Vector3f location = new Vector3f();
    protected final Quaternionf rotation = new Quaternionf();
    protected final Vector3f scale = new Vector3f();

    protected final Matrix4f localTransformMatrix = new Matrix4f();
    protected Matrix4f globalTransformMatrix = new Matrix4f();

    protected final int index;

    protected final String name;

    protected final Type type;

    protected ModelPart parent;
    protected final ModelPart[] children;

    protected ModelMeshData meshData;

    protected ModelArmatureData armatureData;


    protected ModelPart(int index,
                        String name,
                        Type type,
                        int childCount,
                        Vector3f location,
                        Quaternionf rotation,
                        Vector3f scale) {

        this.index = index;
        this.name = name;

        this.type = type;

        this.children = new ModelPart[childCount];

        this.location.set(location);
        this.rotation.set(rotation);
        this.scale.set(scale);

        this.localTransformMatrix.identity();

        this.localTransformMatrix.translate(location);
        this.localTransformMatrix.rotate(rotation);
        this.localTransformMatrix.scale(scale);
    }


    protected void setParentAndChildren(ModelPart parent, List<ModelPart> children) {
        this.parent = parent;
        children.toArray(this.children);
    }


    protected void calculateGlobalTransform() {
        if (parent != null) {
            globalTransformMatrix.set(parent.globalTransformMatrix);
        } else {
            globalTransformMatrix.identity();
        }

        globalTransformMatrix.mul(localTransformMatrix);
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        for (ModelPart child : children) {
            child.dispose();
        }

        if (this.meshData != null) {
            this.meshData.dispose();
        }

        disposed = true;
    }

}