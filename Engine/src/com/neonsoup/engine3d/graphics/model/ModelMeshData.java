package com.neonsoup.engine3d.graphics.model;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.graphics.gl.GLMesh;
import com.neonsoup.engine.graphics.material.Material;
import com.neonsoup.engine.graphics.material.MaterialProperties;

import gnu.trove.map.TObjectShortMap;

public class ModelMeshData extends AbstractDisposable {

    protected GLMesh[] meshes;
    protected Material[] materials;
    protected MaterialProperties[][] materialProperties;

    protected ModelArmatureData armature;
    protected TObjectShortMap<String> boneNameToIndexMap;


    public ModelMeshData(GLMesh[] meshes,
                         Material[] materials,
                         MaterialProperties[][] materialProperties,
                         TObjectShortMap<String> boneNameToIndexMap) {

        this.meshes = meshes;
        this.materials = materials;
        this.materialProperties = materialProperties;

        this.boneNameToIndexMap = boneNameToIndexMap;
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        for (GLMesh mesh : meshes) {
            mesh.disposeAllAttachedData();
        }

        disposed = true;
    }

}
