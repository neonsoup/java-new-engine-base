package com.neonsoup.engine3d.graphics.model;

import com.neonsoup.engine.Disposable;
import com.neonsoup.engine.graphics.MaterialMesh;
import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;
import com.neonsoup.engine.graphics.material.Material;
import com.neonsoup.engine.graphics.material.MaterialProperties;
import com.neonsoup.engine.scene.SceneConfiguration;

public class MeshModelPartNode extends ModelPartNode implements Disposable {

    protected boolean disposed = false;

    protected final ModelMeshData modelMeshData;
    protected final MaterialMesh[] matMeshes;

    protected ArmatureModelPartNode armatureInstance = null;
    protected final BoneMaterialProperties boneProps[];

    protected MeshArmatureBinding meshArmatureBinding = null;


    public MeshModelPartNode(ModelMeshData modelMeshData, SceneConfiguration config) {
        super(config);

        this.modelMeshData = modelMeshData;

        int meshCount = modelMeshData.meshes.length;

        this.matMeshes = new MaterialMesh[meshCount];

        this.boneProps = new BoneMaterialProperties[meshCount];

        for (int i = 0; i < meshCount; i++) {
            MaterialProperties[] props = modelMeshData.materialProperties[i];

            this.matMeshes[i] = new MaterialMesh(modelMeshData.meshes[i], modelMeshData.materials[i], props);
        }
    }


    public void bindArmatureInstance(ArmatureModelPartNode armatureInstance) {
        if (this.armatureInstance != null) {
            this.armatureInstance.meshArmatureBindings.remove(meshArmatureBinding);

            for (int i = 0; i < matMeshes.length; i++) {
                BoneMaterialProperties oldProps = boneProps[i];

                matMeshes[i].removeProperties(oldProps);

                oldProps.dispose();

                boneProps[i] = null;

                meshArmatureBinding = null;
            }
        }

        this.armatureInstance = armatureInstance;

        if (armatureInstance != null) {
            meshArmatureBinding = new MeshArmatureBinding(this, armatureInstance);

            this.armatureInstance.meshArmatureBindings.add(meshArmatureBinding);

            this.armatureInstance.markTransformsDirty();

            for (int i = 0; i < matMeshes.length; i++) {
                Material mat = matMeshes[i].getMaterial();

                BoneMaterialProperties newProps = new BoneMaterialProperties(mat,
                                                                             mat.getTechniqueBits(),
                                                                             this,
                                                                             armatureInstance);

                matMeshes[i].addPropeties(newProps);

                boneProps[i] = newProps;
            }
        }
    }


    @Override
    public void draw(SceneRenderer renderer, RenderTechnique technique) {
        for (MaterialMesh mesh : matMeshes) {
            if (armatureInstance != null) {
                mesh.draw(renderer, technique, scene, modelInstance.rootNode);
            } else {
                mesh.draw(renderer, technique, scene, this);
            }
        }
    }


    @Override
    public boolean isDisposed() {
        return disposed;
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        for (int i = 0; i < matMeshes.length; i++) {
            if (this.armatureInstance != null) {
                BoneMaterialProperties oldProps = boneProps[i];

                matMeshes[i].removeProperties(oldProps);

                oldProps.dispose();

                boneProps[i] = null;
            }

            matMeshes[i].dispose();
        }

        this.disposed = true;
    }

}
