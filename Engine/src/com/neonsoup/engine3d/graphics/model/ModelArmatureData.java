package com.neonsoup.engine3d.graphics.model;

import java.util.HashMap;
import java.util.Map;

public class ModelArmatureData {

    protected final ModelPart modelPart;

    protected final ModelBone[] bones;
    protected final ModelBone[] rootBones;

    protected final Map<String, ModelBone> bonesByName;


    public ModelArmatureData(ModelPart modelPart, ModelBone[] bones, ModelBone[] rootBones) {
        this.modelPart = modelPart;

        this.bones = bones;
        this.rootBones = rootBones;

        this.bonesByName = new HashMap<>(bones.length);
        for (ModelBone bone : bones) {
            this.bonesByName.put(bone.name, bone);
        }
    }

}
