package com.neonsoup.engine3d.graphics.model;

import com.neonsoup.engine.scene.SceneConfiguration;
import com.neonsoup.engine.scene.SceneNode;

public class ModelPartNode extends SceneNode {

    protected ModelInstance modelInstance;
    protected int modelPartIndex = -1;


    public ModelPartNode(SceneConfiguration config) {
        super(config);
    }


    public ModelInstance getModelInstance() {
        return modelInstance;
    }


    public void setModelInstance(ModelInstance modelInstance) {
        this.modelInstance = modelInstance;
    }


    public int getModelPartIndex() {
        return modelPartIndex;
    }


    public void setModelPartIndex(int modelInstanceIndex) {
        this.modelPartIndex = modelInstanceIndex;
    }

}
