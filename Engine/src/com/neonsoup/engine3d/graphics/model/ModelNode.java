package com.neonsoup.engine3d.graphics.model;

import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;
import com.neonsoup.engine.scene.SceneConfiguration;
import com.neonsoup.engine.scene.SceneNode;

public class ModelNode extends SceneNode {

    protected ModelInstance modelInstance;


    public ModelNode(SceneConfiguration config) {
        super(config);
    }


    public ModelInstance getModelInstance() {
        return modelInstance;
    }


    public void setModelInstance(ModelInstance modelInstance) {
        this.modelInstance = modelInstance;
    }


    @Override
    public void draw(SceneRenderer renderer, RenderTechnique technique) {
        if (modelInstance.isDisposed()) {
            throw new IllegalStateException("Model instance is disposed");
        }
    }

}
