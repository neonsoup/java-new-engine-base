package com.neonsoup.engine3d.graphics.model;

import static org.lwjgl.opengl.GL30.*;

import java.nio.ByteBuffer;

import org.joml.Matrix4f;

import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;
import com.neonsoup.engine.graphics.gl.GLBuffer;
import com.neonsoup.engine.graphics.gl.GLShaderProgram;
import com.neonsoup.engine.graphics.material.Material;
import com.neonsoup.engine.graphics.material.MaterialProperties;

public class BoneMaterialProperties extends MaterialProperties {

    private final MeshModelPartNode meshInstance;
    private final ArmatureModelPartNode armatureInstance;

    private final GLShaderProgram.UniformBlock[] boneBlocks;

    private final int[] boneMatrixUniformArrayStrides;
    private final int[] boneMatrixUniformMatrixStrides;

    private final GLBuffer armatureMatrixBoneBuffer;

    private final int[] localBoneIndices;


    public BoneMaterialProperties(Material material,
                                  int techniques,
                                  MeshModelPartNode meshInstance,
                                  ArmatureModelPartNode armatureInstance) {

        super(material, techniques);

        this.meshInstance = meshInstance;
        this.armatureInstance = armatureInstance;

        int minTechCount = Math.min(material.getTechniqueCount(), 32 - Integer.numberOfLeadingZeros(techniques));

        this.boneBlocks = new GLShaderProgram.UniformBlock[minTechCount];

        this.boneMatrixUniformArrayStrides = new int[minTechCount];
        this.boneMatrixUniformMatrixStrides = new int[minTechCount];

        this.localBoneIndices = new int[armatureInstance.armatureData.bones.length];

        for (ModelBone bone : armatureInstance.armatureData.bones) {
            localBoneIndices[bone.index] = meshInstance.modelMeshData.boneNameToIndexMap.get(bone.name);
        }

        int maxSize = -1;

        for (int i = 0; i < minTechCount; i++) {
            if ((techniques & (1 << i)) != 0) {
                GLShaderProgram shader = material.getShader(i);

                GLShaderProgram.UniformBlock block = shader.getUniformBlock("Bone");

                boneBlocks[i] = block;

                int uniformIndex = shader.getUniformIndex("Bone.matrix[0]");

                boneMatrixUniformArrayStrides[i] = block.getUniformArrayStride(uniformIndex);
                boneMatrixUniformMatrixStrides[i] = block.getUniformMatrixStride(uniformIndex);

                if (block != null) {
                    maxSize = Math.max(maxSize, block.getSize());
                }
            }
        }

        if (maxSize > 0) {
            armatureMatrixBoneBuffer = new GLBuffer(maxSize,
                                                    GLBuffer.Target.UNIFORM_BUFFER,
                                                    GLBuffer.Usage.DYNAMIC_DRAW);
        } else {
            armatureMatrixBoneBuffer = null;
        }
    }


    @Override
    public void setProperties(SceneRenderer renderer, RenderTechnique technique) {
        if (armatureMatrixBoneBuffer != null) {
            int techId = technique.getId();

            GLShaderProgram.UniformBlock block = boneBlocks[techId];

            armatureMatrixBoneBuffer.bind();

            ByteBuffer bufMap = armatureMatrixBoneBuffer.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY);

            for (ModelBone bone : armatureInstance.armatureData.bones) {
                Matrix4f mat = meshInstance.meshArmatureBinding.poseTransformMatrices[bone.index];

                int idx = localBoneIndices[bone.index];

                int arrayStride = boneMatrixUniformArrayStrides[techId];
                int matrixStride = boneMatrixUniformMatrixStrides[techId];

                if (arrayStride == 64 && matrixStride == 16) {
                    mat.get(idx * 64, bufMap);
                } else {
                    throw new UnsupportedOperationException();
                }
            }

            armatureMatrixBoneBuffer.unmapBuffer();

            armatureMatrixBoneBuffer.bindBase(block.getBinding());

            armatureMatrixBoneBuffer.unbind();
        }
    }


    @Override
    public void unsetProperties(SceneRenderer renderer, RenderTechnique technique) {
        if (armatureMatrixBoneBuffer != null) {
            int techId = technique.getId();

            GLShaderProgram.UniformBlock block = boneBlocks[techId];

            glBindBufferBase(GLBuffer.Target.UNIFORM_BUFFER.glTarget, block.getBinding(), 0);
        }
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        if (armatureMatrixBoneBuffer != null) {
            armatureMatrixBoneBuffer.dispose();
        }

        this.disposed = true;
    }

}
