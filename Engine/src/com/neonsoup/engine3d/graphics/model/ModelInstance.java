package com.neonsoup.engine3d.graphics.model;

import java.util.HashMap;
import java.util.Map;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.res.Resource;
import com.neonsoup.engine.res.ResourceHandle;
import com.neonsoup.engine.scene.SceneConfiguration;
import com.neonsoup.engine3d.graphics.model.ModelPart.Type;

public class ModelInstance extends AbstractDisposable {

    protected final ResourceHandle<Model> modelHandle;

    protected final Model model;
    protected final ModelNode rootNode;

    protected final ModelPartNode[] partNodes;
    protected final Map<String, ModelPartNode> partNodesByName;


    protected ModelInstance(Resource<Model> modelHandle, SceneConfiguration sceneConfig) {
        this.model = modelHandle.getResourceObject();

        this.rootNode = new ModelNode(sceneConfig);

        this.partNodesByName = new HashMap<>(model.parts.length);

        for (ModelPart modelPart : model.rootParts) {
            rootNode.attachChild(fillModelInstanceSceneNodesMap(modelHandle,
                                                                null,
                                                                modelPart,
                                                                sceneConfig,
                                                                partNodesByName));
        }

        this.modelHandle = new ResourceHandle<>(modelHandle);

        rootNode.setModelInstance(this);

        this.partNodes = new ModelPartNode[partNodesByName.size()];
        for (ModelPartNode node : partNodesByName.values()) {
            node.setModelInstance(this);
            this.partNodes[node.getModelPartIndex()] = node;

            if (model.parts[node.modelPartIndex].type == Type.MESH) {
                MeshModelPartNode meshNode = (MeshModelPartNode) node;

                ModelArmatureData armData = meshNode.modelMeshData.armature;

                if (armData != null) {
                    ArmatureModelPartNode armNode = (ArmatureModelPartNode) partNodesByName.get(armData.modelPart.name);

                    meshNode.bindArmatureInstance(armNode);
                }
            }
        }
    }


    protected static ModelPartNode fillModelInstanceSceneNodesMap(Resource<Model> modelRes,
                                                                  ModelPartNode parent,
                                                                  ModelPart modelPart,
                                                                  SceneConfiguration sceneConfig,
                                                                  Map<String, ModelPartNode> dest) {

        ModelPartNode modelPartNode;

        switch (modelPart.type) {
            case ARMATURE: {
                modelPartNode = new ArmatureModelPartNode(modelPart.armatureData, sceneConfig);
                break;
            }

            case MESH: {
                modelPartNode = new MeshModelPartNode(modelPart.meshData, sceneConfig);
                break;
            }

            case UNKNOWN:
                modelPartNode = new ModelPartNode(sceneConfig);
                break;

            default:
                throw new RuntimeException();
        }

        modelPartNode.setModelPartIndex(modelPart.index);

        modelPartNode.setTranslation(modelPart.location);
        modelPartNode.setRotation(modelPart.rotation);
        modelPartNode.setScale(modelPart.scale);

        dest.put(modelPart.name, modelPartNode);

        for (ModelPart child : modelPart.children) {
            modelPartNode
                    .attachChild(fillModelInstanceSceneNodesMap(modelRes, modelPartNode, child, sceneConfig, dest));
        }

        return modelPartNode;
    }


    public ModelNode getRootNode() {
        return rootNode;
    }


    public ModelPartNode getPartNode(String name) {
        return partNodes[model.partsByName.get(name).index];
    }


    public ModelPartNode getModelPartNodeByIndex(int idx) {
        return partNodes[idx];
    }


    public ModelPartNode getModelPartNodeByName(String name) {
        return partNodesByName.get(name);
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        for (ModelPartNode partNode : partNodes) {
            switch (model.parts[partNode.modelPartIndex].type) {
                case ARMATURE:
                    break;

                case MESH: {
                    ((MeshModelPartNode) partNode).dispose();
                    break;
                }

                case UNKNOWN:
                    break;

                default:
                    break;
            }
        }

        modelHandle.release();

        disposed = true;
    }

}
