package com.neonsoup.engine3d.graphics.model;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joml.Quaternionf;
import org.joml.Vector3f;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.graphics.Texture2D;
import com.neonsoup.engine.graphics.gl.GLBuffer;
import com.neonsoup.engine.graphics.gl.GLMesh;
import com.neonsoup.engine.graphics.gl.GLVertexAttributeData;
import com.neonsoup.engine.graphics.gl.GLVertexAttributeSignature;
import com.neonsoup.engine.graphics.gl.GLVertexIndexData;
import com.neonsoup.engine.graphics.material.Material;
import com.neonsoup.engine.graphics.material.MaterialProperties;
import com.neonsoup.engine.graphics.material.StringMaterialFeatureKey;
import com.neonsoup.engine.res.ResourceHandle;
import com.neonsoup.engine.res.material.MaterialKey;
import com.neonsoup.engine.res.texture.Texture2DKey;
import com.neonsoup.engine.scene.SceneConfiguration;

import gnu.trove.map.TObjectShortMap;
import gnu.trove.map.hash.TObjectShortHashMap;

public class Model extends AbstractDisposable {

    protected static final Map<String, String> NMD_MESH_ATTRIB_NAMES_TO_SHADER_NAMES = new HashMap<>();

    static {
        NMD_MESH_ATTRIB_NAMES_TO_SHADER_NAMES.put("position", "in_Position");
        NMD_MESH_ATTRIB_NAMES_TO_SHADER_NAMES.put("normal", "in_Normal");
        NMD_MESH_ATTRIB_NAMES_TO_SHADER_NAMES.put("bone_count", "in_BoneCount");
        NMD_MESH_ATTRIB_NAMES_TO_SHADER_NAMES.put("bone_indices", "in_BoneIndices");
        NMD_MESH_ATTRIB_NAMES_TO_SHADER_NAMES.put("bone_weights", "in_BoneWeights");
    }

    protected final ModelPart[] parts;
    protected final ModelPart[] rootParts;

    protected final Map<String, ModelPart> partsByName;

    protected final ResourceHandle<Material>[] materialHandles;

    protected final ResourceHandle<Texture2D>[] textureHandles;
    protected final Texture2D[] textures;


    protected Model(ModelPart[] parts,
                    ModelPart[] rootParts,
                    ResourceHandle<Material>[] materials,
                    ResourceHandle<Texture2D>[] textureHandles,
                    Texture2D[] textures) {

        this.parts = parts;
        this.rootParts = rootParts;

        this.partsByName = new HashMap<>(parts.length);
        for (ModelPart part : parts) {
            partsByName.put(part.name, part);
        }

        this.materialHandles = materials;
        this.textureHandles = textureHandles;
        this.textures = textures;
    }


    public static ModelInstance createIntance(ResourceHandle<Model> modelRes, SceneConfiguration sceneConfig) {
        return new ModelInstance(modelRes.getResource(), sceneConfig);
    }


    protected static final short TAG_VERSION = 1;

    protected static final short TAG_CHILDREN = 12;
    protected static final short TAG_OBJECT_NAME = 13;
    protected static final short TAG_PARENT = 14;

    protected static final short TAG_MATERIALS = 40;

    protected static final short TAG_MATERIAL_NAME = 41;
    protected static final short TAG_MATERIAL_TYPE_EMPTY = 42;
    protected static final short TAG_MATERIAL_TYPE_UV_TEXTURE = 43;
    protected static final short TAG_MATERIAL_TYPE_EMPTY_SKINNED = 44;
    protected static final short TAG_TEXTURE_NAME = 25;

    protected static final short TAG_OBJECTS = 80;

    protected static final short TAG_OBJECT_TRANSFORM = 81;
    protected static final short TAG_OBJECT_TYPE_UNKNOWN = 82;
    protected static final short TAG_OBJECT_TYPE_MESH = 83;
    protected static final short TAG_OBJECT_TYPE_ARMATURE = 84;

    protected static final short TAG_MESHES = 120;

    protected static final short TAG_MESH_HAS_ARMATURE = 121;
    protected static final short TAG_MESH_ARMATURE_NAME = 122;
    protected static final short TAG_MESH_BONE_NAME_TO_INDEX_MAP = 123;

    protected static final short TAG_MESH_MATERIAL = 124;

    protected static final short TAG_MESH_ATTRIBS = 160;
    protected static final short TAG_MESH_ATTRIB_NAME = 161;
    protected static final short TAG_MESH_ATTRIB_TYPE = 162;
    protected static final short TAG_MESH_ATTRIB_ELEMENT_COUNT = 163;
    protected static final short TAG_MESH_ATTRIB_VALUES = 164;
    protected static final short TAG_MESH_INDICES = 165;
    protected static final short TAG_MESH_INDEX_TYPE = 166;
    protected static final short TAG_MESH_INDEX_VALUES = 167;

    protected static final short TAG_TYPE_UBYTE = 200;
    protected static final short TAG_TYPE_SBYTE = 201;
    protected static final short TAG_TYPE_USHORT = 202;
    protected static final short TAG_TYPE_SSHORT = 203;
    protected static final short TAG_TYPE_UINT = 204;
    protected static final short TAG_TYPE_SINT = 205;
    protected static final short TAG_TYPE_FLOAT = 206;
    protected static final short TAG_TYPE_DOUBLE = 207;

    protected static final short TAG_ARMATURE_BONES = 240;

    protected static final short TAG_BONE_NAME = 241;
    protected static final short TAG_BONE_INV_BIND_POSE_MATRIX = 242;
    protected static final short TAG_BONE_BIND_POSE_PARENT_TO_CHILD_MATRIX = 243;


    protected static void ensureTag(DataInputStream in, short tag) throws IOException {
        if (in.readShort() != tag) {
            throw new IOException();
        }
    }


    protected static String readString(DataInputStream in) throws IOException {
        int len = in.readInt();

        byte[] data = new byte[len];
        in.read(data, 0, len);

        return new String(data, "UTF-8");
    }


    public static Model loadModel(Application app, InputStream in) throws IOException {
        try (DataInputStream dataIn = new DataInputStream(in)) {

            ensureTag(dataIn, TAG_VERSION);

            dataIn.readInt(); // version

            ensureTag(dataIn, TAG_MATERIALS);

            int materialCount = dataIn.readInt();

            Map<String, ResourceHandle<Texture2D>> textureHandles = new HashMap<>(materialCount);

            Map<String, ResourceHandle<Material>> materialsHandles = new HashMap<>(materialCount);
            // Map<String, MaterialProperties> materialProps = new
            // HashMap<>(materialCount);

            for (int i = 0; i < materialCount; i++) {
                ensureTag(dataIn, TAG_MATERIAL_NAME);

                String name = readString(dataIn);

                short type = dataIn.readShort();

                switch (type) {
                    case TAG_MATERIAL_TYPE_EMPTY: {

                        ResourceHandle<Material> materialHandle = app.getGraphics().getMaterialManager()
                                .loadResourceDependency(MaterialKey.fromDefaultName("model_empty"));

                        materialsHandles.put(name, materialHandle);

                        break;
                    }

                    case TAG_MATERIAL_TYPE_EMPTY_SKINNED: {

                        ResourceHandle<Material> materialHandle = app.getGraphics().getMaterialManager()
                                .loadResourceDependency(MaterialKey
                                        .fromDefaultName("model_empty", new StringMaterialFeatureKey("SKINNING")));

                        materialsHandles.put(name, materialHandle);

                        break;
                    }

                    case TAG_MATERIAL_TYPE_UV_TEXTURE: {
                        ensureTag(dataIn, TAG_TEXTURE_NAME);

                        String texName = readString(dataIn);

                        @SuppressWarnings("unused")
                        final Texture2D tex;

                        if (!textureHandles.containsKey(texName)) {
                            ResourceHandle<Texture2D> texHandle = app.getGraphics().getTexture2DManager()
                                    .loadResourceDependency(new Texture2DKey(texName));

                            textureHandles.put(texName, texHandle);

                            tex = texHandle.getResourceObject();
                        } else {
                            tex = textureHandles.get(texName).getResourceObject();
                        }

                        ResourceHandle<Material> materialHandle = app.getGraphics().getMaterialManager()
                                .loadResourceDependency(MaterialKey.fromDefaultName("model_uv"));

                        materialsHandles.put(name, materialHandle);

                        break;
                    }

                    default:
                        throw new IOException();
                }
            }

            ensureTag(dataIn, TAG_CHILDREN);

            int childCount = dataIn.readInt();

            Set<String> rootPartNames = new HashSet<>(childCount);

            for (int i = 0; i < childCount; i++) {
                ensureTag(dataIn, TAG_OBJECT_NAME);
                rootPartNames.add(readString(dataIn));
            }

            ensureTag(dataIn, TAG_OBJECTS);
            int objectCount = dataIn.readInt();

            Map<String, ModelPart> parts = new HashMap<>(objectCount);
            Map<ModelPart, String> partParents = new HashMap<>(objectCount);
            Map<ModelPart, List<String>> partChildren = new HashMap<>(objectCount);

            Map<ModelPart, String> partArmatures = new HashMap<>();

            int objIndex = 0;

            for (int i = 0; i < objectCount; i++) {
                ensureTag(dataIn, TAG_OBJECT_NAME);
                String name = readString(dataIn);

                ensureTag(dataIn, TAG_OBJECT_NAME);
                String parentName = readString(dataIn);

                ensureTag(dataIn, TAG_CHILDREN);
                childCount = dataIn.readInt();

                List<String> childNameList = new ArrayList<>();

                for (int c = 0; c < childCount; c++) {
                    ensureTag(dataIn, TAG_OBJECT_NAME);
                    String childName = readString(dataIn);

                    childNameList.add(childName);
                }

                ensureTag(dataIn, TAG_OBJECT_TRANSFORM);

                float tx = dataIn.readFloat();
                float ty = dataIn.readFloat();
                float tz = dataIn.readFloat();

                float rx = dataIn.readFloat();
                float ry = dataIn.readFloat();
                float rz = dataIn.readFloat();
                float rw = dataIn.readFloat();

                float sx = dataIn.readFloat();
                float sy = dataIn.readFloat();
                float sz = dataIn.readFloat();

                short objType = dataIn.readShort();

                ModelPart.Type objTypeEnum;

                switch (objType) {
                    case TAG_OBJECT_TYPE_MESH:
                        objTypeEnum = ModelPart.Type.MESH;
                        break;

                    case TAG_OBJECT_TYPE_ARMATURE:
                        objTypeEnum = ModelPart.Type.ARMATURE;
                        break;

                    case TAG_OBJECT_TYPE_UNKNOWN:
                        objTypeEnum = ModelPart.Type.UNKNOWN;
                        break;

                    default:
                        throw new IOException();
                }

                ModelPart part = new ModelPart(objIndex,
                                               name,
                                               objTypeEnum,
                                               childCount,
                                               new Vector3f(tx, ty, tz),
                                               new Quaternionf(rx, ry, rz, rw),
                                               new Vector3f(sx, sy, sz));

                parts.put(name, part);
                partParents.put(part, parentName);

                partChildren.put(part, childNameList);

                switch (objType) {
                    case TAG_OBJECT_TYPE_MESH: {

                        ensureTag(dataIn, TAG_MESH_HAS_ARMATURE);
                        int hasArmature = dataIn.readByte();

                        TObjectShortMap<String> boneNameToIndexMap = null;
                        String armatureName = null;

                        if (hasArmature != 0) {
                            ensureTag(dataIn, TAG_MESH_ARMATURE_NAME);
                            armatureName = readString(dataIn);

                            partArmatures.put(part, armatureName);

                            ensureTag(dataIn, TAG_MESH_BONE_NAME_TO_INDEX_MAP);

                            int mapCount = dataIn.readInt();

                            boneNameToIndexMap = new TObjectShortHashMap<>(mapCount);

                            for (int map = 0; map < mapCount; map++) {
                                String boneName = readString(dataIn);
                                short boneIndex = dataIn.readShort();

                                boneNameToIndexMap.put(boneName, boneIndex);
                            }
                        }

                        ensureTag(dataIn, TAG_MESHES);
                        int meshCount = dataIn.readInt();

                        GLMesh[] glMeshes = new GLMesh[meshCount];
                        Material[] materials = new Material[meshCount];
                        MaterialProperties[][] materialProperties = new MaterialProperties[meshCount][];

                        for (int m = 0; m < meshCount; m++) {
                            ensureTag(dataIn, TAG_MESH_MATERIAL);

                            String meshMaterialName = readString(dataIn);
                            Material meshMaterial = materialsHandles.get(meshMaterialName).getResourceObject();
                            // MaterialProperties meshMaterialProps =
                            // materialProps.get(meshMaterialName);

                            ensureTag(dataIn, TAG_MESH_ATTRIBS);
                            int attribCount = dataIn.readInt();

                            int vertexCount = dataIn.readInt();

                            Map<String, GLBuffer> meshAttribs = new HashMap<>(attribCount);

                            GLVertexAttributeData[] attribData = new GLVertexAttributeData[attribCount];

                            int attribIdx = 0;

                            for (int a = 0; a < attribCount; a++) {
                                ensureTag(dataIn, TAG_MESH_ATTRIB_NAME);
                                String attribName = readString(dataIn);

                                ensureTag(dataIn, TAG_MESH_ATTRIB_TYPE);
                                short attribType = dataIn.readShort();
                                GLVertexAttributeSignature.Type attribTypeEnum;

                                ensureTag(dataIn, TAG_MESH_ATTRIB_ELEMENT_COUNT);
                                int attribElementCount = dataIn.readShort();

                                ensureTag(dataIn, TAG_MESH_ATTRIB_VALUES);

                                GLBuffer buf;

                                switch (attribType) {
                                    case TAG_TYPE_FLOAT: {
                                        buf = new GLBuffer(4 * attribElementCount * vertexCount,
                                                           GLBuffer.Target.ARRAY_BUFFER,
                                                           GLBuffer.Usage.STATIC_DRAW);

                                        buf.bind();
                                        try {
                                            ByteBuffer map = buf.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY);

                                            try {
                                                FloatBuffer fmap = map.asFloatBuffer();

                                                int fcount = vertexCount * attribElementCount;

                                                for (int f = 0; f < fcount; f++) {
                                                    fmap.put(dataIn.readFloat());
                                                }
                                            } finally {
                                                buf.unmapBuffer();
                                            }
                                        } finally {
                                            buf.unbind();
                                        }

                                        meshAttribs.put(attribName, buf);

                                        attribTypeEnum = GLVertexAttributeSignature.Type.FLOAT;

                                        break;
                                    }

                                    case TAG_TYPE_USHORT:
                                    case TAG_TYPE_SSHORT: {
                                        buf = new GLBuffer(2 * attribElementCount * vertexCount,
                                                           GLBuffer.Target.ARRAY_BUFFER,
                                                           GLBuffer.Usage.STATIC_DRAW);

                                        buf.bind();
                                        try {
                                            ByteBuffer map = buf.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY);

                                            try {
                                                ShortBuffer smap = map.asShortBuffer();

                                                int scount = vertexCount * attribElementCount;

                                                for (int s = 0; s < scount; s++) {
                                                    smap.put(dataIn.readShort());
                                                }
                                            } finally {
                                                buf.unmapBuffer();
                                            }
                                        } finally {
                                            buf.unbind();
                                        }

                                        meshAttribs.put(attribName, buf);

                                        if (attribType == TAG_TYPE_USHORT) {
                                            attribTypeEnum = GLVertexAttributeSignature.Type.UNSIGNED_SHORT;
                                        } else {
                                            attribTypeEnum = GLVertexAttributeSignature.Type.SHORT;
                                        }

                                        break;
                                    }

                                    default:
                                        throw new IOException();
                                }

                                String shaderAttribName = NMD_MESH_ATTRIB_NAMES_TO_SHADER_NAMES.get(attribName);

                                GLVertexAttributeSignature attribSig = new GLVertexAttributeSignature(shaderAttribName,
                                                                                                      attribTypeEnum,
                                                                                                      attribElementCount);

                                attribData[attribIdx++] = new GLVertexAttributeData(buf, attribSig);
                            }

                            ensureTag(dataIn, TAG_MESH_INDICES);

                            ensureTag(dataIn, TAG_MESH_INDEX_TYPE);

                            short indexType = dataIn.readShort();
                            GLVertexIndexData.Type indexTypeEnum;

                            ensureTag(dataIn, TAG_MESH_INDEX_VALUES);

                            int indexCount = dataIn.readInt();

                            GLBuffer indexBuffer;

                            switch (indexType) {
                                case TAG_TYPE_UBYTE: {
                                    indexBuffer = new GLBuffer(indexCount,
                                                               GLBuffer.Target.ELEMENT_ARRAY_BUFFER,
                                                               GLBuffer.Usage.STATIC_DRAW);

                                    indexBuffer.bind();
                                    try {
                                        ByteBuffer map = indexBuffer.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY);

                                        try {
                                            for (int idx = 0; idx < indexCount; idx++) {
                                                map.put(dataIn.readByte());
                                            }
                                        } finally {
                                            indexBuffer.unmapBuffer();
                                        }
                                    } finally {
                                        indexBuffer.unbind();
                                    }

                                    indexTypeEnum = GLVertexIndexData.Type.UNSIGNED_BYTE;

                                    break;
                                }

                                case TAG_TYPE_USHORT: {
                                    indexBuffer = new GLBuffer(indexCount * 2,
                                                               GLBuffer.Target.ELEMENT_ARRAY_BUFFER,
                                                               GLBuffer.Usage.STATIC_DRAW);

                                    indexBuffer.bind();
                                    try {
                                        ByteBuffer map = indexBuffer.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY);

                                        try {
                                            for (int idx = 0; idx < indexCount; idx++) {
                                                map.putShort(dataIn.readShort());
                                            }
                                        } finally {
                                            indexBuffer.unmapBuffer();
                                        }
                                    } finally {
                                        indexBuffer.unbind();
                                    }

                                    indexTypeEnum = GLVertexIndexData.Type.UNSIGNED_SHORT;

                                    break;
                                }

                                case TAG_TYPE_UINT: {
                                    indexBuffer = new GLBuffer(indexCount * 4,
                                                               GLBuffer.Target.ELEMENT_ARRAY_BUFFER,
                                                               GLBuffer.Usage.STATIC_DRAW);

                                    indexBuffer.bind();
                                    try {
                                        ByteBuffer map = indexBuffer.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY);

                                        try {
                                            for (int idx = 0; idx < indexCount; idx++) {
                                                map.putInt(dataIn.readInt());
                                            }
                                        } finally {
                                            indexBuffer.unmapBuffer();
                                        }
                                    } finally {
                                        indexBuffer.unbind();
                                    }

                                    indexTypeEnum = GLVertexIndexData.Type.UNSIGNED_INT;

                                    break;
                                }

                                default:
                                    throw new IOException();
                            }

                            GLVertexIndexData indexData = new GLVertexIndexData(indexBuffer, indexTypeEnum);

                            glMeshes[m] = new GLMesh(GLMesh.Mode.TRIANGLES, indexCount, attribData, indexData);
                            materials[m] = meshMaterial;
                            materialProperties[m] = new MaterialProperties[] { /*
                                                                                * meshMaterialProps
                                                                                */ };
                        }

                        part.meshData = new ModelMeshData(glMeshes, materials, materialProperties, boneNameToIndexMap);

                        break;
                    }

                    case TAG_OBJECT_TYPE_ARMATURE: {
                        ensureTag(dataIn, TAG_CHILDREN);

                        int rootBoneCount = dataIn.readInt();

                        List<String> rootBoneNames = new ArrayList<>(rootBoneCount);

                        for (int b = 0; b < rootBoneCount; b++) {
                            ensureTag(dataIn, TAG_BONE_NAME);
                            rootBoneNames.add(readString(dataIn));
                        }

                        ensureTag(dataIn, TAG_ARMATURE_BONES);

                        int boneCount = dataIn.readInt();

                        Map<String, ModelBone> bones = new HashMap<>(boneCount);
                        Map<String, List<String>> boneChildrenMap = new HashMap<>(boneCount);

                        for (int b = 0; b < boneCount; b++) {
                            ensureTag(dataIn, TAG_BONE_NAME);
                            String boneName = readString(dataIn);

                            ensureTag(dataIn, TAG_BONE_INV_BIND_POSE_MATRIX);
                            float[] invBindPoseMatrix = new float[16];
                            for (int f = 0; f < 16; f++) {
                                invBindPoseMatrix[f] = dataIn.readFloat();
                            }

                            ensureTag(dataIn, TAG_BONE_BIND_POSE_PARENT_TO_CHILD_MATRIX);
                            float[] bindPoseParentToChildMatrix = new float[16];
                            for (int f = 0; f < 16; f++) {
                                bindPoseParentToChildMatrix[f] = dataIn.readFloat();
                            }

                            ensureTag(dataIn, TAG_CHILDREN);

                            int boneChildCount = dataIn.readInt();

                            List<String> boneChildren = new ArrayList<>(boneChildCount);

                            for (int c = 0; c < boneChildCount; c++) {
                                ensureTag(dataIn, TAG_BONE_NAME);
                                boneChildren.add(readString(dataIn));
                            }

                            boneChildrenMap.put(boneName, boneChildren);

                            ModelBone bone = new ModelBone(boneName, b, boneChildCount);

                            bone.bindPoseParentToThisMatrix.set(bindPoseParentToChildMatrix);
                            bone.invBindPoseMatrix.set(invBindPoseMatrix);

                            bones.put(boneName, bone);
                        }

                        for (ModelBone bone : bones.values()) {
                            List<String> children = boneChildrenMap.get(bone.name);

                            for (int c = 0; c < children.size(); c++) {
                                ModelBone child = bones.get(children.get(c));

                                bone.children[c] = child;
                                child.parent = bone;
                            }
                        }

                        ModelBone[] boneArr = new ModelBone[bones.size()];

                        for (ModelBone bone : bones.values()) {
                            boneArr[bone.index] = bone;
                        }

                        ModelBone[] rootBoneArr = new ModelBone[rootBoneNames.size()];

                        for (int b = 0; b < rootBoneNames.size(); b++) {
                            rootBoneArr[b] = bones.get(rootBoneNames.get(b));
                        }

                        ModelArmatureData armature = new ModelArmatureData(part, boneArr, rootBoneArr);

                        part.armatureData = armature;

                        break;
                    }

                    case TAG_OBJECT_TYPE_UNKNOWN: {
                        break;
                    }

                    default:
                        throw new IOException();
                }

                objIndex++;
            }

            for (Map.Entry<ModelPart, String> ent : partArmatures.entrySet()) {
                ModelPart part = ent.getKey();
                String armName = ent.getValue();

                part.meshData.armature = parts.get(armName).armatureData;
            }

            ModelPart[] partArr = new ModelPart[parts.size()];

            for (ModelPart part : parts.values()) {
                partArr[part.index] = part;

                String parentName = partParents.get(part);
                ModelPart parent = parts.get(parentName);

                List<String> childNames = partChildren.get(part);
                List<ModelPart> children = new ArrayList<>(childNames.size());

                for (String childName : childNames) {
                    children.add(parts.get(childName));
                }

                part.setParentAndChildren(parent, children);
            }

            for (ModelPart part : partArr) {
                part.calculateGlobalTransform();
            }

            ModelPart[] rootPartArr = new ModelPart[rootPartNames.size()];

            int rootNodeIdx = 0;
            for (String rootNodeName : rootPartNames) {
                rootPartArr[rootNodeIdx++] = parts.get(rootNodeName);
            }

            @SuppressWarnings("unchecked")
            ResourceHandle<Material>[] materialArr = (ResourceHandle<Material>[]) new ResourceHandle[materialsHandles
                    .size()];

            int matIdx = 0;
            for (ResourceHandle<Material> mat : materialsHandles.values()) {
                materialArr[matIdx++] = mat;
            }

            @SuppressWarnings("unchecked")
            ResourceHandle<Texture2D>[] texHandleArr = new ResourceHandle[textureHandles.size()];

            Texture2D[] texArr = new Texture2D[textureHandles.size()];

            int texIdx = 0;
            for (Map.Entry<String, ResourceHandle<Texture2D>> ent : textureHandles.entrySet()) {
                texHandleArr[texIdx] = ent.getValue();
                texArr[texIdx] = ent.getValue().getResourceObject();

                texIdx++;
            }

            return new Model(partArr, rootPartArr, materialArr, texHandleArr, texArr);
        }
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        for (ModelPart node : rootParts) {
            node.dispose();
        }

        for (ResourceHandle<Material> matHandle : materialHandles) {
            matHandle.release();
        }

        for (ResourceHandle<Texture2D> texHandle : textureHandles) {
            texHandle.release();
        }

        disposed = true;
    }

}
