package com.neonsoup.engine3d.graphics.model;

import org.joml.Matrix4f;

public class ModelBone {

    protected final String name;
    protected final int index;

    protected final Matrix4f bindPoseParentToThisMatrix = new Matrix4f().identity();
    protected final Matrix4f invBindPoseMatrix = new Matrix4f().identity();

    protected ModelBone parent;
    protected final ModelBone[] children;


    public ModelBone(String name, int index, int childCount) {
        this.name = name;
        this.index = index;

        this.children = new ModelBone[childCount];
    }

}
