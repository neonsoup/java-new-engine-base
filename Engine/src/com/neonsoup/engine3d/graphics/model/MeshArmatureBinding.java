package com.neonsoup.engine3d.graphics.model;

import org.joml.Matrix4f;
import org.joml.Matrix4fc;

public class MeshArmatureBinding {

    protected final MeshModelPartNode meshInstance;
    protected final ArmatureModelPartNode armatureInstance;

    protected final Matrix4fc armMat;
    protected final Matrix4fc meshMat;

    protected final Matrix4f invArmMat;

    protected final Matrix4f[] poseTransformMatrices;


    public MeshArmatureBinding(MeshModelPartNode mesh, ArmatureModelPartNode arm) {
        this.meshInstance = mesh;
        this.armatureInstance = arm;

        armMat = armatureInstance.modelInstance.model.parts[armatureInstance.modelPartIndex].globalTransformMatrix;
        meshMat = meshInstance.modelInstance.model.parts[meshInstance.modelPartIndex].globalTransformMatrix;

        invArmMat = new Matrix4f(armMat);
        invArmMat.invert();

        int boneCount = arm.boneTranslations.length;

        this.poseTransformMatrices = new Matrix4f[boneCount];

        for (int i = 0; i < boneCount; i++) {
            this.poseTransformMatrices[i] = new Matrix4f();
        }
    }


    public void calculatePoseMatrices() {
        for (int i = 0; i < poseTransformMatrices.length; i++) {
            Matrix4f mat = poseTransformMatrices[i];

            mat.set(armMat);
            mat.mul(armatureInstance.armatureToPoseMatrices[i]);
            mat.mul(armatureInstance.bindPoseToArmatureMatrices[i]);
            mat.mul(invArmMat);
            mat.mul(meshMat);
        }
    }

}
