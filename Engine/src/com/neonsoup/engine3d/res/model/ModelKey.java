package com.neonsoup.engine3d.res.model;

import com.neonsoup.engine.res.ResourceKey;
import com.neonsoup.engine.res.ResourceName;
import com.neonsoup.engine3d.graphics.model.Model;

public class ModelKey implements ResourceKey<Model> {

    protected final ResourceName name;


    public ModelKey(String name) {
        this.name = new ResourceName(name);
    }


    @Override
    public int hashCode() {
        return name.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ModelKey)) {
            return false;
        }

        ModelKey other = (ModelKey) obj;

        return other.name.equals(this.name);
    }


    @Override
    public String toString() {
        return "ModelKey [" + name + "]";
    }

}
