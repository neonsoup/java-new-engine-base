package com.neonsoup.engine3d.res.model;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.async.AsyncWorker;
import com.neonsoup.engine.graphics.gl.GL;
import com.neonsoup.engine.res.ReadyResource;
import com.neonsoup.engine.res.Resource;
import com.neonsoup.engine.res.ResourceInfoLocator;
import com.neonsoup.engine.res.ResourceKey;
import com.neonsoup.engine.res.ResourceLoadingTask;
import com.neonsoup.engine.res.ResourceManager;
import com.neonsoup.engine3d.graphics.model.Model;

public class ModelManager extends ResourceManager<Model> {

    private static final Logger log = Logger.getLogger(ModelManager.class.getName());


    public ModelManager(Application app, int cacheSize) {
        super(app, cacheSize);
    }


    @Override
    protected AsyncWorker getLoadingWorker(Application app) {
        return app.getGraphics().getResourceLoadingWorker();
    }


    @Override
    protected Logger getLogger() {
        return log;
    }


    @Override
    protected Resource<Model> createDependencyResource(ResourceKey<Model> key) throws Exception {
        Resource<Model> res = createReadyResource(key);

        GL.syncGpuCommandsComplete();

        return res;
    }


    @Override
    protected Resource<Model> createReadyResource(ResourceKey<Model> key) throws Exception {
        ModelKey modelKey = (ModelKey) key;

        ResourceInfoLocator streamLocator = app.getResourceInfoLocator();

        try (InputStream in = streamLocator.locateResourceInfo(modelKey.name.canonicalName).getResourceStream()) {

            Model model = Model.loadModel(app, in);

            return new ReadyResource<Model>(this, key, model);
        }
    }


    @Override
    protected ResourceLoadingTask<Model> createResourceLoadingTask(ResourceKey<Model> key, Resource<Model> res) {
        return new ResourceLoadingTask<Model>(this, key, res) {

            @Override
            public void execute() {
                ModelKey modelKey = (ModelKey) key;

                ResourceInfoLocator streamLocator = app.getResourceInfoLocator();

                try (InputStream in = streamLocator.locateResourceInfo(modelKey.name.canonicalName)
                        .getResourceStream()) {

                    Model model = Model.loadModel(app, in);

                    try {
                        GL.syncGpuCommandsComplete();

                        this.resourceObject = model;

                        addLoadedResource(res);
                    } catch (Exception e) {
                        model.dispose();

                        throw new RuntimeException(e);
                    }
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Error loading model by key \"" + key + "\"", e);

                    addLoadedResource(res);
                }
            }

        };
    }

}
