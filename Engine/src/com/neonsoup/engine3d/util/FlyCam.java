package com.neonsoup.engine3d.util;

import org.joml.Matrix4f;
import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.joml.Vector4f;

import com.neonsoup.engine.app.input.KeyboardEvent;
import com.neonsoup.engine.app.input.KeyboardKey;
import com.neonsoup.engine.app.input.KeyboardKeyAction;
import com.neonsoup.engine.app.input.MouseMotionEvent;
import com.neonsoup.engine.math.MathUtil;
import com.neonsoup.engine.scene.SceneNode;

public class FlyCam {

    protected final SceneNode cameraNode;

    protected final float flySpeed;
    protected final float mouseSens;

    protected float yaw;
    protected float pitch;

    protected boolean forward = false;
    protected boolean backward = false;
    protected boolean left = false;
    protected boolean right = false;


    public FlyCam(SceneNode cameraNode, float cameraSpeed, float mouseSensitivity) {
        this.cameraNode = cameraNode;
        this.flySpeed = cameraSpeed;
        this.mouseSens = mouseSensitivity;
    }


    public float getYaw() {
        return yaw;
    }


    public void setYaw(float yaw) {
        this.yaw = yaw;
    }


    public float getPitch() {
        return pitch;
    }


    public void setPitch(float pitch) {
        if (pitch > MathUtil.HALF_PI) {
            pitch = MathUtil.HALF_PI;
        } else if (pitch < -MathUtil.HALF_PI) {
            pitch = -MathUtil.HALF_PI;
        }

        this.pitch = pitch;
    }


    private final Matrix4f tempRot = new Matrix4f();
    private final Quaternionf tempRotQuat = new Quaternionf();


    public void update(float dt, long dtNanos) {

        tempRot.rotationY(yaw);
        tempRot.rotateX(pitch);

        Vector3f delta3 = new Vector3f(0.0f, 0.0f, 0.0f);

        if (forward) {
            delta3.z = -1.0f;
        } else if (backward) {
            delta3.z = 1.0f;
        }

        if (left) {
            delta3.x = -1.0f;
        } else if (right) {
            delta3.x = 1.0f;
        }

        if (delta3.lengthSquared() != 0.0f) {
            delta3.normalize();
        }

        delta3.mul(dt * flySpeed);

        Vector4f delta = new Vector4f(delta3.x, delta3.y, delta3.z, 1.0f);

        tempRot.transform(delta);

        Vector3f pos = new Vector3f(cameraNode.getTranslation());

        pos.add(delta.x, delta.y, delta.z);

        cameraNode.setTranslation(pos);

        tempRot.getUnnormalizedRotation(tempRotQuat);

        cameraNode.setRotation(tempRotQuat);

        cameraNode.markTransformDirty();
    }


    public void keyboardEvent(KeyboardEvent event) {
        if (event.key == KeyboardKey.KEY_W) {
            if (event.keyAction == KeyboardKeyAction.PRESS) {
                forward = true;
            } else if (event.keyAction == KeyboardKeyAction.RELEASE) {
                forward = false;
            }
        }
        if (event.key == KeyboardKey.KEY_S) {
            if (event.keyAction == KeyboardKeyAction.PRESS) {
                backward = true;
            } else if (event.keyAction == KeyboardKeyAction.RELEASE) {
                backward = false;
            }
        }
        if (event.key == KeyboardKey.KEY_A) {
            if (event.keyAction == KeyboardKeyAction.PRESS) {
                left = true;
            } else if (event.keyAction == KeyboardKeyAction.RELEASE) {
                left = false;
            }
        }
        if (event.key == KeyboardKey.KEY_D) {
            if (event.keyAction == KeyboardKeyAction.PRESS) {
                right = true;
            } else if (event.keyAction == KeyboardKeyAction.RELEASE) {
                right = false;
            }
        }
    }


    public void mouseMoved(MouseMotionEvent event) {
        yaw -= event.deltaX * mouseSens;

        pitch -= event.deltaY * mouseSens;

        if (pitch > MathUtil.HALF_PI) {
            pitch = MathUtil.HALF_PI;
        } else if (pitch < -MathUtil.HALF_PI) {
            pitch = -MathUtil.HALF_PI;
        }
    }

}
