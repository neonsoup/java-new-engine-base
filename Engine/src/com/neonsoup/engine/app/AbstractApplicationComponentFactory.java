package com.neonsoup.engine.app;

public abstract class AbstractApplicationComponentFactory implements ApplicationComponentFactory {

    protected Application app;


    public AbstractApplicationComponentFactory() {
    }


    @Override
    public void setApplication(Application app) {
        this.app = app;
    }

}
