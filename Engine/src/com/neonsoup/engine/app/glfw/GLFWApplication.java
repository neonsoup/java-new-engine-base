package com.neonsoup.engine.app.glfw;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.app.RealTimeApplicationLoopListener;
import com.neonsoup.engine.app.config.ApplicationConfigProvider;
import com.neonsoup.engine.app.config.RealTimeLoopConfig;

public class GLFWApplication extends Application {

    protected class GLFWApplicationRealTimeLoopListener extends ApplicationRealTimeLoopListener {

        @Override
        public void onLoopStart() {
            super.onLoopStart();
        }


        @Override
        public void onLoopPauseRequested() {
            super.onLoopPauseRequested();
        }


        @Override
        public void onPreLoopUpdate() {
            super.onPreLoopUpdate();
        }


        @Override
        public void onLoopUpdate(float dt, long dtNanos) {
            super.onLoopUpdate(dt, dtNanos);
        }


        @Override
        public void onPostLoopUpdate() {
            super.onPostLoopUpdate();
        }


        @Override
        public void onLoopRender(float dtSinceLastUpdate, long dtSinceLastUpdateNanos) {
            super.onLoopRender(dtSinceLastUpdate, dtSinceLastUpdateNanos);
        }


        @Override
        public void onLoopPause() {
            super.onLoopPause();
        }


        @Override
        public void onLoopResume() {
            super.onLoopResume();
        }


        @Override
        public void onLoopStop() {
            super.onLoopStop();
        }

    }


    public GLFWApplication(ApplicationConfigProvider configProvider) {
        super(configProvider, new GLFWApplicationComponentFactory());

        {
            RealTimeLoopConfig realTimeLoopConfig = configProvider.getRealTimeLoopConfig();

            if (realTimeLoopConfig == null) {
                throw new IllegalArgumentException(ApplicationConfigProvider.class.getSimpleName()
                                                   + " returned null for real time loop config");
            }

            this.realTimeLoop = componentFactory.createRealTimeLoop(realTimeLoopConfig);
        }
    }


    @Override
    public RealTimeApplicationLoopListener createApplicationRealTimeLoopListener() {
        return new GLFWApplicationRealTimeLoopListener();
    }


    @Override
    public void initInMainThread() {
        super.initInMainThread();
    }


    public void windowCloseRequested() {
        if (hasState()) {
            if (getState().onStateWindowCloseRequested()) {
                getRealTimeLoop().stop();
            }
        } else {
            getRealTimeLoop().stop();
        }
    }


    @Override
    public void disposeInMainThread() {
        super.disposeInMainThread();
    }


    @Override
    public void start() {
        this.realTimeLoop.start();
    }


    @Override
    public void close() {
        this.realTimeLoop.stop();
    }

}
