package com.neonsoup.engine.app.glfw;

import static org.lwjgl.glfw.GLFW.*;

import org.lwjgl.opengl.GL;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.graphics.gl.GLSharedContext;

public class GLFWGLSharedContext extends AbstractDisposable implements GLSharedContext {

    protected final GLFWApplicationGraphics graphics;
    protected final long glfwHandle;


    public GLFWGLSharedContext(GLFWApplicationGraphics graphics, long handle) {
        this.graphics = graphics;
        this.glfwHandle = handle;
    }


    @Override
    public void makeCurrent() {
        glfwMakeContextCurrent(glfwHandle);
        GL.createCapabilities();
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        graphics.disposeSharedGLContext(this);

        disposed = true;
    }

}
