package com.neonsoup.engine.app.glfw;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.app.ApplicationCanvas;
import com.neonsoup.engine.app.config.CanvasConfig;

public class GLFWApplicationCanvas extends ApplicationCanvas {

    public GLFWApplicationCanvas(Application app, CanvasConfig config) {
        super(app, config);
    }


    @Override
    public void initInMainThread() {
    }


    @Override
    public boolean isDoubleBuffered() {
        return true;
    }


    @Override
    public void swapBuffers() {
        ((GLFWApplicationWindow) app.getWindow()).swapBuffers();
    }


    @Override
    public void disposeInMainThread() {
    }

}
