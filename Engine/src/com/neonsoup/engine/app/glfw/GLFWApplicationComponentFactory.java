package com.neonsoup.engine.app.glfw;

import com.neonsoup.engine.app.AbstractApplicationComponentFactory;
import com.neonsoup.engine.app.ApplicationCanvas;
import com.neonsoup.engine.app.ApplicationGraphics;
import com.neonsoup.engine.app.ApplicationWindow;
import com.neonsoup.engine.app.RealTimeApplicationLoop;
import com.neonsoup.engine.app.config.CanvasConfig;
import com.neonsoup.engine.app.config.GraphicsConfig;
import com.neonsoup.engine.app.config.InputConfig;
import com.neonsoup.engine.app.config.RealTimeLoopConfig;
import com.neonsoup.engine.app.config.WindowConfig;
import com.neonsoup.engine.app.input.ApplicationInput;
import com.neonsoup.engine.app.input.glfw.GLFWApplicationInput;

public class GLFWApplicationComponentFactory extends AbstractApplicationComponentFactory {

    public GLFWApplicationComponentFactory() {
    }


    @Override
    public boolean hasGraphics() {
        return true;
    }


    @Override
    public ApplicationGraphics createGraphics(GraphicsConfig config) {
        return new GLFWApplicationGraphics(app, config);
    }


    @Override
    public boolean hasCanvas() {
        return true;
    }


    @Override
    public ApplicationCanvas createCanvas(CanvasConfig config) {
        return new GLFWApplicationCanvas(app, config);
    }


    @Override
    public boolean hasWindow() {
        return true;
    }


    @Override
    public ApplicationWindow createWindow(WindowConfig config) {
        return new GLFWApplicationWindow(app, config);
    }


    @Override
    public boolean hasRealTimeLoop() {
        return true;
    }


    @Override
    public RealTimeApplicationLoop createRealTimeLoop(RealTimeLoopConfig config) {
        return new RealTimeApplicationLoop(app, app.createApplicationRealTimeLoopListener(), config);
    }


    @Override
    public boolean hasInput() {
        return true;
    }


    @Override
    public ApplicationInput createInput(InputConfig config) {
        return new GLFWApplicationInput(app, config);
    }

}
