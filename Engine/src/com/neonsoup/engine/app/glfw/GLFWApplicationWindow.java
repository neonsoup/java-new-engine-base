package com.neonsoup.engine.app.glfw;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import java.nio.IntBuffer;

import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowCloseCallback;
import org.lwjgl.system.MemoryUtil;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.app.ApplicationGraphics;
import com.neonsoup.engine.app.ApplicationWindow;
import com.neonsoup.engine.app.config.CanvasConfig;
import com.neonsoup.engine.app.config.WindowConfig;
import com.sun.prism.impl.BufferUtil;

public class GLFWApplicationWindow extends ApplicationWindow {

    protected long glfwWindowHandle = MemoryUtil.NULL;


    public GLFWApplicationWindow(Application app, WindowConfig config) {
        super(app, config);
    }


    public long getGLFWWindowHandle() {
        return glfwWindowHandle;
    }


    @Override
    public void initInMainThread() {
        glfwDefaultWindowHints();

        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

        glfwWindowHint(GLFW_STEREO, GLFW_FALSE);
        glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
        glfwWindowHint(GLFW_SAMPLES, 0);

        boolean fullscreen = config.getFullscreen();

        if (fullscreen) {
            glfwWindowHint(GLFW_AUTO_ICONIFY, GLFW_TRUE);

            int refreshRate = config.getRefreshRate();

            if (refreshRate == WindowConfig.DEFAULT_REFRESH_RATE) {
                glfwWindowHint(GLFW_REFRESH_RATE, GLFW_DONT_CARE);
            } else {
                glfwWindowHint(GLFW_REFRESH_RATE, refreshRate);
            }
        } else {
            glfwWindowHint(GLFW_DECORATED, GLFW_TRUE);
            glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
            glfwWindowHint(GLFW_FLOATING, GLFW_FALSE);
            glfwWindowHint(GLFW_MAXIMIZED, GLFW_FALSE);
        }

        glfwWindowHint(GLFW_SRGB_CAPABLE, GLFW_TRUE);

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, ApplicationGraphics.GL_VERSION_MAJOR);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, ApplicationGraphics.GL_VERSION_MINOR);

        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        CanvasConfig canvasConfig = app.getCanvas().getCurrentConfig();

        glfwWindowHint(GLFW_RED_BITS, canvasConfig.getRedBits());
        glfwWindowHint(GLFW_GREEN_BITS, canvasConfig.getGreenBits());
        glfwWindowHint(GLFW_BLUE_BITS, canvasConfig.getBlueBits());

        glfwWindowHint(GLFW_ALPHA_BITS, canvasConfig.getAlphaBits());

        glfwWindowHint(GLFW_DEPTH_BITS, canvasConfig.getDepthBits());

        glfwWindowHint(GLFW_STENCIL_BITS, canvasConfig.getStencilBits());

        {

            GLFWApplicationGraphics graphics = (GLFWApplicationGraphics) app.getGraphics();

            int monitorIndex = config.getMonitorIndex();

            long monitorHandle;
            if (monitorIndex == WindowConfig.PRIMARY_MONITOR_INDEX) {
                monitorHandle = glfwGetPrimaryMonitor();
            } else {
                monitorHandle = graphics.getGLFWMonitorHandleForIndex(monitorIndex);
            }

            int width = canvasConfig.getWidth();
            int height = canvasConfig.getHeight();

            String name = config.getName();

            if (fullscreen) {
                glfwWindowHandle = glfwCreateWindow(width, height, name, monitorHandle, MemoryUtil.NULL);

                if (glfwWindowHandle == MemoryUtil.NULL) {
                    throw new RuntimeException("Unable to create GLFW window");
                }
            } else {
                glfwWindowHandle = glfwCreateWindow(width, height, name, MemoryUtil.NULL, MemoryUtil.NULL);

                if (glfwWindowHandle == MemoryUtil.NULL) {
                    throw new RuntimeException("Unable to create GLFW window");
                }

                IntBuffer monitorPositionBuffer = BufferUtil.newIntBuffer(2);
                {
                    glfwGetMonitorPos(monitorHandle, monitorPositionBuffer, null);
                    monitorPositionBuffer.position(1);
                    glfwGetMonitorPos(monitorHandle, null, monitorPositionBuffer);
                    monitorPositionBuffer.rewind();
                }

                GLFWVidMode vidMode;
                if (monitorIndex == WindowConfig.PRIMARY_MONITOR_INDEX) {
                    vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
                } else {
                    vidMode = glfwGetVideoMode(graphics.getGLFWMonitorHandleForIndex(monitorIndex));
                }

                glfwSetWindowPos(glfwWindowHandle,
                                 monitorPositionBuffer.get(0) + (vidMode.width() - width) / 2,
                                 monitorPositionBuffer.get(1) + (vidMode.height() - height) / 2);
            }
        }

        glfwMakeContextCurrent(glfwWindowHandle);

        boolean vsync = config.getVSync();

        if (vsync) {
            glfwSwapInterval(1);
            app.getRealTimeLoop().setVsync(true);
        } else {
            glfwSwapInterval(0);
            app.getRealTimeLoop().setVsync(false);
        }

        app.getGraphics().initMainContext();

        glfwShowWindow(glfwWindowHandle);

        {
            glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT);
            app.getCanvas().swapBuffers();
        }

        glfwSetWindowCloseCallback(glfwWindowHandle, new GLFWWindowCloseCallback() {

            @Override
            public void invoke(long window) {
                if (glfwWindowShouldClose(glfwWindowHandle)) {
                    ((GLFWApplication) app).windowCloseRequested();
                }
            }

        });

    }


    public void swapBuffers() {
        glfwSwapBuffers(glfwWindowHandle);
    }


    @Override
    public void disposeInMainThread() {
        if (glfwWindowHandle != MemoryUtil.NULL) {
            {
                GLFWWindowCloseCallback closeCallback = glfwSetWindowCloseCallback(glfwWindowHandle, null);
                if (closeCallback != null) {
                    closeCallback.free();
                }
            }

            glfwFreeCallbacks(glfwWindowHandle);
        }
    }

}
