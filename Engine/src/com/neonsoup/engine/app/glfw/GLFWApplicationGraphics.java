package com.neonsoup.engine.app.glfw;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.system.MemoryUtil;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.app.ApplicationGraphics;
import com.neonsoup.engine.app.RealTimeApplicationLoopListener;
import com.neonsoup.engine.app.config.CanvasConfig;
import com.neonsoup.engine.app.config.GraphicsConfig;
import com.neonsoup.engine.graphics.gl.GLSharedContext;

public class GLFWApplicationGraphics extends ApplicationGraphics {

    private final List<GLFWGLSharedContext> sharedContexts = new ArrayList<>();
    private final Queue<GLFWGLSharedContext> contextToDelete = new LinkedBlockingQueue<>();

    private boolean inited = false;

    private final RealTimeApplicationLoopListener loopListener = new RealTimeApplicationLoopListener() {

        @Override
        public void onPreLoopUpdate() {
        }


        @Override
        public void onLoopUpdate(float dt, long dtNanos) {
        }


        @Override
        public void onPostLoopUpdate() {
            synchronized (contextToDelete) {
                GLFWGLSharedContext context = null;

                while ((context = contextToDelete.poll()) != null) {
                    disposeSharedContextInMainThread(context);
                }
            }
        }


        @Override
        public void onLoopStop() {
        }


        @Override
        public void onLoopStart() {
        }


        @Override
        public void onLoopResume() {
        }


        @Override
        public void onLoopRender(float dtSinceLastUpdate, long dtSinceLastUpdateNanos) {
        }


        @Override
        public void onLoopPauseRequested() {
        }


        @Override
        public void onLoopPause() {
        }

    };


    public GLFWApplicationGraphics(Application app, GraphicsConfig config) {
        super(app, config);
    }


    @Override
    public void initInMainThread() {
        GLFWErrorCallback.createPrint(System.err).set();

        if (!(inited = glfwInit())) {
            throw new RuntimeException("GLFW failed to init");
        }

        app.getRealTimeLoop().addListener(loopListener);

        super.initInMainThread();
    }


    public long getGLFWMonitorHandleForIndex(int index) {
        PointerBuffer monitors = glfwGetMonitors();

        if (index < 0 || index >= monitors.limit()) {
            throw new IllegalArgumentException("index is out of monitor index range");
        }

        return glfwGetMonitors().get(index);
    }


    @Override
    public void disposeInMainThread() {
        super.disposeInMainThread();
    }


    protected void disposeSharedContextInMainThread(GLFWGLSharedContext sharedContext) {
        glfwFreeCallbacks(sharedContext.glfwHandle);
        glfwDestroyWindow(sharedContext.glfwHandle);

        sharedContexts.remove(sharedContext);
    }


    @Override
    public void disposeMainContext() {
        super.disposeMainContext();

        glfwDestroyWindow(((GLFWApplicationWindow) app.getWindow()).glfwWindowHandle);

        if (inited) {
            glfwTerminate();
        }

        GLFWErrorCallback errCallback = glfwSetErrorCallback(null);
        if (errCallback != null) {
            errCallback.free();
        }
    }


    @Override
    protected void disposeSharedContexts() {
        if (inited) {
            synchronized (contextToDelete) {
                GLFWGLSharedContext context = null;

                while ((context = contextToDelete.poll()) != null) {
                    disposeSharedContextInMainThread(context);
                }
            }

            for (GLFWGLSharedContext context : new ArrayList<>(sharedContexts)) {
                disposeSharedContextInMainThread(context);
            }
        }
    }


    @Override
    public GLSharedContext createGLSharedContext() {
        glfwDefaultWindowHints();

        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

        glfwWindowHint(GLFW_STEREO, GLFW_FALSE);
        glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_FALSE);
        glfwWindowHint(GLFW_SAMPLES, 0);

        glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
        glfwWindowHint(GLFW_FLOATING, GLFW_FALSE);
        glfwWindowHint(GLFW_MAXIMIZED, GLFW_FALSE);
        glfwWindowHint(GLFW_SRGB_CAPABLE, GLFW_TRUE);

        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, ApplicationGraphics.GL_VERSION_MAJOR);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, ApplicationGraphics.GL_VERSION_MINOR);

        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        CanvasConfig canvasConfig = app.getCanvas().getCurrentConfig();

        glfwWindowHint(GLFW_RED_BITS, canvasConfig.getRedBits());
        glfwWindowHint(GLFW_GREEN_BITS, canvasConfig.getGreenBits());
        glfwWindowHint(GLFW_BLUE_BITS, canvasConfig.getBlueBits());

        glfwWindowHint(GLFW_ALPHA_BITS, canvasConfig.getAlphaBits());

        glfwWindowHint(GLFW_DEPTH_BITS, canvasConfig.getDepthBits());

        glfwWindowHint(GLFW_STENCIL_BITS, canvasConfig.getStencilBits());

        int width = canvasConfig.getWidth();
        int height = canvasConfig.getHeight();

        long newContextHandle = glfwCreateWindow(width,
                                                 height,
                                                 "shared context",
                                                 MemoryUtil.NULL,
                                                 ((GLFWApplicationWindow) getApplication()
                                                         .getWindow()).glfwWindowHandle);

        if (newContextHandle == MemoryUtil.NULL) {
            throw new RuntimeException("Unable to create shared GLFW window");
        }

        GLFWGLSharedContext newContext = new GLFWGLSharedContext(this, newContextHandle);

        sharedContexts.add(newContext);

        return newContext;
    }


    public void disposeSharedGLContext(GLFWGLSharedContext context) {
        synchronized (contextToDelete) {
            contextToDelete.add(context);
        }
    }

}
