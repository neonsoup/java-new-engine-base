package com.neonsoup.engine.app;

public interface RealTimeApplicationLoopListener {

    void onLoopStart();


    void onLoopPauseRequested();


    void onPreLoopUpdate();


    void onLoopUpdate(float dt, long dtNanos);


    void onPostLoopUpdate();


    void onLoopRender(float dtSinceLastUpdate, long dtSinceLastUpdateNanos);


    void onLoopPause();


    void onLoopResume();


    void onLoopStop();

}
