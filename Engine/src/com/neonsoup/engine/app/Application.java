package com.neonsoup.engine.app;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonReader;

import org.apache.commons.io.IOUtils;

import com.neonsoup.common.util.NoModCheckLinkedList;
import com.neonsoup.engine.Disposable;
import com.neonsoup.engine.app.config.ApplicationConfig;
import com.neonsoup.engine.app.config.ApplicationConfigBuilder;
import com.neonsoup.engine.app.config.ApplicationConfigProvider;
import com.neonsoup.engine.app.config.CanvasConfig;
import com.neonsoup.engine.app.config.GraphicsConfig;
import com.neonsoup.engine.app.config.InputConfig;
import com.neonsoup.engine.app.config.WindowConfig;
import com.neonsoup.engine.app.input.ApplicationInput;
import com.neonsoup.engine.app.input.KeyboardEvent;
import com.neonsoup.engine.app.input.KeyboardListener;
import com.neonsoup.engine.app.state.ApplicationState;
import com.neonsoup.engine.app.state.ApplicationStateFactory;
import com.neonsoup.engine.res.ApplicationResourceInfoLocator;
import com.neonsoup.engine.res.ResourceInfo;
import com.neonsoup.engine.res.ResourceInfoLocator;
import com.neonsoup.engine.res.ResourceManager;

public abstract class Application {

    private static final Logger log = Logger.getLogger(Application.class.getName());


    protected class ApplicationRealTimeLoopListener implements RealTimeApplicationLoopListener {

        protected ApplicationRealTimeLoopListener() {
        }


        @Override
        public void onLoopStart() {
            Application.this.initInMainThread();

            for (ApplicationLifecycleListener listener : appLifecycleListeners) {
                listener.onStart();
            }

            if (hasState()) {
                getState().onApplicationStart();
            }
        }


        @Override
        public void onLoopPauseRequested() {
            if (hasState()) {
                getState().onApplicationPauseRequested();
            }

            for (ApplicationLifecycleListener listener : appLifecycleListeners) {
                listener.onPauseRequested();
            }
        }


        @Override
        public void onPreLoopUpdate() {
        }


        @Override
        public void onLoopUpdate(float dt, long dtNanos) {
            if (hasState()) {
                getState().onStateUpdate(dt, dtNanos);
            }

            if (hasInput()) {
                input.pollEvents();
            }

            if (switchToState != null) {
                setStateObject(switchToState);

                switchToState = null;
            }
        }


        @Override
        public void onPostLoopUpdate() {
        }


        @Override
        public void onLoopRender(float dtSinceLastUpdate, long dtSinceLastUpdateNanos) {
            if (hasGraphics() && hasState()) {
                ApplicationState state = getState();

                graphics.getDefaultGLStateProperties().beginDraw(null);

                state.onStateRender(dtSinceLastUpdate, dtSinceLastUpdateNanos);

                graphics.getGUI().draw();

                graphics.getDefaultGLStateProperties().endDraw();

                getCanvas().swapBuffers();
            }
        }


        @Override
        public void onLoopPause() {
            if (hasState()) {
                getState().onApplicationPause();
            }

            for (ApplicationLifecycleListener listener : appLifecycleListeners) {
                listener.onPause();
            }
        }


        @Override
        public void onLoopResume() {
            for (ApplicationLifecycleListener listener : appLifecycleListeners) {
                listener.onResume();
            }

            if (hasState()) {
                getState().onApplicationResume();
            }
        }


        @Override
        public void onLoopStop() {
            if (hasState()) {
                getState().onStateLeave();

                getState().onApplicationStop();
            }

            for (ApplicationLifecycleListener listener : appLifecycleListeners) {
                listener.onStop();
            }

            Application.this.disposeInMainThread();
        }

    }


    protected class ApplicationInputListener implements KeyboardListener {

        @Override
        public void onKeyboardEvent(KeyboardEvent event) {
            if (hasState()) {
                getState().onKeyboardEvent(event);
            }
        }

    }


    protected final ApplicationConfigProvider configProvider;

    protected ApplicationConfig config;

    protected final ApplicationComponentFactory componentFactory;

    protected ApplicationGraphics graphics;
    protected ApplicationCanvas canvas;
    protected ApplicationWindow window;
    protected RealTimeApplicationLoop realTimeLoop;
    protected ApplicationInput input;

    protected File baseDirectory;

    protected ApplicationResourceInfoLocator resourceStreamLocator;

    protected ApplicationStateFactory stateFactory;

    protected ApplicationInputListener inputListener;

    protected ApplicationState currentState;

    protected ApplicationState switchToState;

    protected Map<Class<? extends Disposable>, ResourceManager<?>> registeredResourceManagers = new HashMap<>(8);

    protected final List<ApplicationLifecycleListener> appLifecycleListeners = new NoModCheckLinkedList<>();


    protected Application(ApplicationConfigProvider configProvider, ApplicationComponentFactory componentFactory) {
        if (configProvider == null) {
            throw new IllegalArgumentException("Application config provider must not be null");
        }
        if (componentFactory == null) {
            throw new IllegalArgumentException("Application component factory must not be null");
        }

        ApplicationConfig config = configProvider.getApplicationConfig();
        if (config == null) {
            throw new IllegalArgumentException(ApplicationConfigProvider.class.getSimpleName()
                                               + " returned null for application config");
        }
        this.config = new ApplicationConfigBuilder(config);

        this.configProvider = configProvider;

        this.componentFactory = componentFactory;
        componentFactory.setApplication(this);
    }


    public void addApplicationLifecycleListener(ApplicationLifecycleListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException("listener must not be null");
        }

        if (!appLifecycleListeners.contains(listener)) {
            appLifecycleListeners.add(0, listener);
        }
    }


    public void removeApplicationLifecycleListener(ApplicationLifecycleListener listener) {
        if (listener == null) {
            return;
        }

        appLifecycleListeners.remove(listener);
    }


    public Thread getMainThread() {
        return realTimeLoop.getThread();
    }


    public boolean hasState() {
        return currentState != null;
    }


    public ApplicationState getState() {
        return currentState;
    }


    public void setState(Object stateKey) {
        this.switchToState = stateFactory.createState(stateKey);
    }


    protected void setStateObject(ApplicationState state) {
        if (this.currentState != null) {
            log.log(Level.INFO, "Leaving state \"" + this.currentState + "\"");
            this.currentState.onStateLeave();
        }

        log.log(Level.INFO, "Entering state \"" + state + "\"");

        if (state != null && state.getApplication() != this) {
            state.setApplication(this);
        }

        ApplicationState oldState = this.currentState;

        this.currentState = state;

        if (oldState != null) {
            if (input != null) {
                input.requestMouseInputFocusRelease();
            }
        }

        if (state != null) {
            state.onStateEnter();
        }
    }


    public boolean hasGraphics() {
        return graphics != null;
    }


    public ApplicationGraphics getGraphics() {
        if (graphics == null) {
            throw new IllegalStateException("This application has no graphics");
        }

        return graphics;
    }


    public boolean hasWindow() {
        return window != null;
    }


    public ApplicationWindow getWindow() {
        if (window == null) {
            throw new IllegalStateException("This application has no window");
        }

        return window;
    }


    public boolean hasCanvas() {
        return canvas != null;
    }


    public ApplicationCanvas getCanvas() {
        if (canvas == null) {
            throw new IllegalStateException("This application has no viewport");
        }

        return canvas;
    }


    public boolean hasLoop() {
        return realTimeLoop != null;
    }


    public RealTimeApplicationLoop getRealTimeLoop() {
        if (realTimeLoop == null) {
            throw new IllegalStateException("This application has no real time loop");
        }

        return realTimeLoop;
    }


    public boolean hasInput() {
        return input != null;
    }


    public ApplicationInput getInput() {
        if (input == null) {
            throw new IllegalStateException("This application has no input");
        }

        return input;
    }


    public File getBaseDirectory() {
        return baseDirectory;
    }


    public ResourceInfoLocator getResourceInfoLocator() {
        return resourceStreamLocator;
    }


    public <O extends Disposable> void registerResourceManager(Class<O> resClass, ResourceManager<O> resManager) {
        registeredResourceManagers.put(resClass, resManager);
    }


    @SuppressWarnings("unchecked")
    public <O extends Disposable> ResourceManager<O> getResourceManagerByResourceClass(Class<? extends O> resClass) {

        ResourceManager<O> manager = (ResourceManager<O>) registeredResourceManagers.get(resClass);

        if (manager != null) {
            return manager;
        }

        for (Map.Entry<Class<? extends Disposable>, ResourceManager<?>> ent : registeredResourceManagers.entrySet()) {
            if (ent.getKey().isAssignableFrom(resClass)) {
                return (ResourceManager<O>) ent.getValue();
            }
        }

        throw new RuntimeException("No resource manager of resource object class \""
                                   + resClass.getName()
                                   + "\" was found");
    }


    public RealTimeApplicationLoopListener createApplicationRealTimeLoopListener() {
        return new ApplicationRealTimeLoopListener();
    }


    public String loadTextUTF8(String path) throws IOException {
        ResourceInfoLocator streamLocator = getResourceInfoLocator();

        ResourceInfo textInfo = streamLocator.locateResourceInfo(path);

        if (textInfo != null) {
            try (InputStream in = textInfo.getResourceStream()) {
                return IOUtils.toString(in, "UTF-8");
            }
        }

        return null;
    }


    public JsonReader createJsonReader(String path) throws IOException {
        ResourceInfoLocator streamLocator = getResourceInfoLocator();

        ResourceInfo jsonInfo = streamLocator.locateResourceInfo(path);

        if (jsonInfo != null) {
            try (InputStream in = jsonInfo.getResourceStream();
                 BufferedInputStream bufIn = new BufferedInputStream(in)) {

                return Json.createReader(bufIn);
            }
        }

        return null;
    }


    public BufferedImage loadBufferedImage(String path) throws IOException {
        ResourceInfoLocator streamLocator = getResourceInfoLocator();

        ResourceInfo textInfo = streamLocator.locateResourceInfo(path);

        if (textInfo != null) {
            try (InputStream in = textInfo.getResourceStream()) {
                return ImageIO.read(in);
            }
        }

        return null;
    }


    public void initInMainThread() {
        baseDirectory = config.getBaseDirectory();

        if (baseDirectory == null) {
            baseDirectory = new File(System.getProperty("user.dir"));
        }

        resourceStreamLocator = new ApplicationResourceInfoLocator(this);

        if (componentFactory.hasGraphics()) {
            {
                GraphicsConfig graphicsConfig = configProvider.getGraphicsConfig();

                if (graphicsConfig == null) {
                    throw new RuntimeException(configProvider.getClass() + " returned null for graphics config");
                }

                graphics = componentFactory.createGraphics(graphicsConfig);
            }

            if (componentFactory.hasWindow()) {
                WindowConfig windowConfig = configProvider.getWindowConfig();

                if (windowConfig == null) {
                    throw new RuntimeException(configProvider.getClass() + " returned null for window config");
                }

                window = componentFactory.createWindow(windowConfig);
            }

            if (componentFactory.hasCanvas()) {
                CanvasConfig viewporConfig = configProvider.getCanvasConfig();

                if (viewporConfig == null) {
                    throw new RuntimeException(configProvider.getClass() + " returned null for viewport config");
                }

                canvas = componentFactory.createCanvas(viewporConfig);
            }

            graphics.initInMainThread();
        }

        if (componentFactory.hasInput()) {
            InputConfig inputConfig = configProvider.getInputConfig();

            if (inputConfig == null) {
                throw new RuntimeException(configProvider.getClass() + " returned null for input config");
            }

            input = componentFactory.createInput(inputConfig);

            input.initInMainThread();

            {
                this.inputListener = new ApplicationInputListener();

                input.addKeyboardListener(this.inputListener);
            }

            if (graphics != null && graphics.gui != null) {
                input.addMouseListener(graphics.gui);
                input.addWindowFocusEventListener(graphics.gui);
            }
        }

        stateFactory = config.getStateFactory();

        if (stateFactory != null) {
            stateFactory.setApplication(this);

            setStateObject(stateFactory.createState(config.getInitialStateKey()));
        } else {
            throw new RuntimeException(config.getClass() + " returned null for state factory");
        }
    }


    public void disposeInMainThread() {
        if (input != null) {
            input.disposeInMainThread();
        }

        if (graphics != null) {
            graphics.disposeInMainThread();
        }
    }


    public abstract void start();


    public abstract void close();

}
