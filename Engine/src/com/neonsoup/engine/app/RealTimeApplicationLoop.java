package com.neonsoup.engine.app;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.neonsoup.common.ThreadUtil;
import com.neonsoup.common.util.NoModCheckLinkedList;
import com.neonsoup.engine.app.config.RealTimeLoopConfig;

public class RealTimeApplicationLoop implements Runnable {

    private static final Logger log = Logger.getLogger(RealTimeApplicationLoop.class.getName());

    private static final double FPS_MEASURE_FACTOR = 0.8;

    private static final double NANOS_PER_SECOND_D = 1_000_000_000d;

    private final Thread thread;

    private volatile float targetUpdateFPS;
    private volatile long nanosPerUpdateFrame;

    private volatile float targetRenderFPS;
    private volatile long nanosPerRenderFrame;

    private volatile float measuredFPS = 0.0f;

    private volatile float targetPauseFPS;

    private volatile boolean pauseRequested = false;
    private volatile boolean paused = false;

    private volatile boolean stopRequested = false;

    private volatile boolean vsync = false; // determines whether the game loop
                                            // should handle render delay on
                                            // itself or rely on vsync delay

    private final Object renderFPSParamsLock = new Object();

    private final RealTimeApplicationLoopListener appListener;

    private final NoModCheckLinkedList<RealTimeApplicationLoopListener> listeners = new NoModCheckLinkedList<>();

    private final Application app;

    private final ThreadUtil.SleepInterrupter sleepInterrupter = new ThreadUtil.SleepInterrupter() {

        @Override
        public boolean interruptRequested() {
            return stopRequested;
        }

    };


    public RealTimeApplicationLoop(Application app,
                                   RealTimeApplicationLoopListener appListener,
                                   RealTimeLoopConfig config) {

        this.app = app;

        this.appListener = appListener;

        this.thread = new Thread(this, "Application Loop thread");

        targetUpdateFPS = config.getTargetUpdateFPS();
        nanosPerUpdateFrame = (long) (NANOS_PER_SECOND_D / targetUpdateFPS);

        targetRenderFPS = config.getTargetRenderFPS();
        nanosPerRenderFrame = (long) (NANOS_PER_SECOND_D / targetRenderFPS);

        targetPauseFPS = config.getTargetPauseFPS();
    }


    public void addListener(RealTimeApplicationLoopListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException("listener must not be null");
        }

        synchronized (listeners) {
            listeners.add(listener);
        }
    }


    public void removeListener(RealTimeApplicationLoopListener listener) {
        if (listener == null) {
            return;
        }

        synchronized (listeners) {
            listeners.remove(listener);
        }
    }


    public Application getApplication() {
        return app;
    }


    public float getTargetUpdateFPS() {
        return targetUpdateFPS;
    }


    public void setTargetUpdateFPS(float fps) {
        if (fps <= 0.0f) {
            throw new IllegalArgumentException("fps must be greater than zero");
        }

        targetUpdateFPS = fps;
        nanosPerUpdateFrame = (long) (NANOS_PER_SECOND_D / fps);
    }


    public float getTargetRenderFPS() {
        return targetRenderFPS;
    }


    public void setTargetRenderFPS(float fps) {
        if (fps < 0.0f) {
            throw new IllegalArgumentException("fps must be greater than or equal to zero");
        }

        synchronized (renderFPSParamsLock) {
            targetRenderFPS = fps;
            nanosPerRenderFrame = (long) (NANOS_PER_SECOND_D / fps);
        }
    }


    public float getTargetPauseFPS() {
        return targetPauseFPS;
    }


    public void setTargetPauseFPS(float fps) {
        if (fps < 0.0f) {
            throw new IllegalArgumentException("fps must be greater than zero");
        }

        targetPauseFPS = fps;
    }


    public boolean getVsync() {
        return vsync;
    }


    public void setVsync(boolean vsync) {
        this.vsync = vsync;
    }


    public float getMeasuredFPS() {
        return measuredFPS;
    }


    public Thread getThread() {
        return thread;
    }


    public boolean isPaused() {
        return paused;
    }


    @Override
    public void run() {

        try {
            // StartEvent
            if (appListener != null) {
                appListener.onLoopStart();
            }
            synchronized (listeners) {
                for (RealTimeApplicationLoopListener listener : listeners) {
                    listener.onLoopStart();
                }
            }

            long prevFrameBeginTime = System.nanoTime();
            long lag = 0;

            boolean stopRequestedLocal = this.stopRequested;

            while (!stopRequestedLocal) {
                long frameBeginTime = System.nanoTime();

                boolean pauseRequestedLocal;
                boolean pausedLocal = this.paused;

                if (!pausedLocal) {
                    // calculate FPS
                    {
                        double instantRate = NANOS_PER_SECOND_D / (frameBeginTime - prevFrameBeginTime);

                        float measuredFPSLocal = (float) (measuredFPS * FPS_MEASURE_FACTOR
                                                          + instantRate * (1.0 - FPS_MEASURE_FACTOR));

                        if (measuredFPSLocal == Float.POSITIVE_INFINITY) {
                            measuredFPSLocal = Float.MAX_VALUE;
                        }

                        measuredFPS = measuredFPSLocal;
                    }

                    long elapsed = frameBeginTime - prevFrameBeginTime;

                    lag += elapsed;

                    pauseRequestedLocal = this.pauseRequested;

                    // PauseRequestedEvent
                    if (appListener != null && pauseRequestedLocal) {
                        appListener.onLoopPauseRequested();
                    }
                    synchronized (listeners) {
                        if (pauseRequestedLocal) {
                            for (RealTimeApplicationLoopListener listener : listeners) {
                                listener.onLoopPauseRequested();
                            }
                        }
                    }

                    // update
                    {
                        long nanosPerUpdateFrameLocal = this.nanosPerUpdateFrame;

                        int maxUpdates = 4;

                        while (lag >= nanosPerUpdateFrameLocal && maxUpdates-- > 0) {

                            // UpdateEvent
                            synchronized (listeners) {
                                if (appListener != null) {
                                    appListener.onPreLoopUpdate();
                                }
                                for (RealTimeApplicationLoopListener listener : listeners) {
                                    listener.onPreLoopUpdate();
                                }

                                float dt = (float) (nanosPerUpdateFrameLocal / NANOS_PER_SECOND_D);
                                long dtNanos = nanosPerUpdateFrameLocal;

                                if (appListener != null) {
                                    appListener.onLoopUpdate(dt, dtNanos);
                                }
                                for (RealTimeApplicationLoopListener listener : listeners) {
                                    listener.onLoopUpdate(dt, dtNanos);
                                }

                                if (appListener != null) {
                                    appListener.onPostLoopUpdate();
                                }
                                for (RealTimeApplicationLoopListener listener : listeners) {
                                    listener.onPostLoopUpdate();
                                }
                            }

                            lag -= nanosPerUpdateFrameLocal;
                        }
                    }

                    if (stopRequested) {
                        break;
                    }

                    // RenderEvent
                    {
                        float floatLag = (float) (lag / NANOS_PER_SECOND_D);
                        if (appListener != null) {
                            appListener.onLoopRender(floatLag, lag);
                        }
                        synchronized (listeners) {
                            for (RealTimeApplicationLoopListener listener : listeners) {
                                listener.onLoopRender(floatLag, lag);
                            }
                        }
                    }

                    // pause
                    if (pauseRequestedLocal) {
                        // PauseEvent
                        if (appListener != null) {
                            appListener.onLoopPause();
                        }
                        synchronized (listeners) {
                            for (RealTimeApplicationLoopListener listener : listeners) {
                                listener.onLoopPause();
                            }
                        }

                        this.paused = true;
                        pausedLocal = true;
                    }

                }

                pauseRequestedLocal = this.pauseRequested;
                stopRequestedLocal = this.stopRequested;

                if (pausedLocal && !pauseRequestedLocal && !stopRequestedLocal) {
                    // ResumeEvent
                    if (appListener != null) {
                        appListener.onLoopResume();
                    }
                    synchronized (listeners) {
                        for (RealTimeApplicationLoopListener listener : listeners) {
                            listener.onLoopResume();
                        }
                    }

                    this.paused = false;
                    pausedLocal = false;
                }

                stopRequestedLocal = this.stopRequested;

                if (!stopRequestedLocal) {
                    if (pausedLocal) {
                        if (targetPauseFPS > 0.0f) {
                            float targetPauseFPS = this.targetPauseFPS;

                            int sleep = (int) (1000.0f / targetPauseFPS);

                            if (sleep > 0) {
                                try {
                                    Thread.sleep(sleep);
                                } catch (InterruptedException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        }
                    } else {
                        if (!this.vsync) {
                            float targetRenderFPSLocal;
                            long nanosPerRenderFrameLocal;

                            synchronized (renderFPSParamsLock) {
                                targetRenderFPSLocal = this.targetRenderFPS;
                                nanosPerRenderFrameLocal = this.nanosPerRenderFrame;
                            }

                            if (targetRenderFPSLocal > 0.0) {
                                long timeAfterFrame = System.nanoTime();

                                long sleep = nanosPerRenderFrameLocal - (timeAfterFrame - frameBeginTime);

                                if (sleep > 0) {
                                    try {
                                        ThreadUtil.sleepNanos(sleep, sleepInterrupter);
                                    } catch (InterruptedException e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                            }
                        }
                    }

                    stopRequestedLocal = this.stopRequested;
                }

                prevFrameBeginTime = frameBeginTime;
            }

            // StopEvent
            if (appListener != null) {
                appListener.onLoopStop();
            }
            synchronized (listeners) {
                for (RealTimeApplicationLoopListener listener : listeners) {
                    listener.onLoopStop();
                }
            }
        } catch (Exception ex) {
            log.log(Level.SEVERE, "Exception in Application Loop thread", ex);
        }
    }


    public void start() {
        thread.start();
    }


    public void pause() {
        pauseRequested = true;
    }


    public void resume() {
        pauseRequested = false;
    }


    public void stop() {
        stopRequested = true;
    }

}
