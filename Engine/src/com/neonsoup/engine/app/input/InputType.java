package com.neonsoup.engine.app.input;

public final class InputType {

    public static final int INPUT_TYPE_BIT_MOUSE = 0b1;
    public static final int INPUT_TYPE_BIT_KEYBOARD = 0b10;
    public static final int INPUT_TYPE_BIT_GAMEPAD = 0b100;

    public static final int INPUT_TYPE_BITS_ALL = 0b111;


    private InputType() {
    }

}
