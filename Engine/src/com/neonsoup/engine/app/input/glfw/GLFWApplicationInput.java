package com.neonsoup.engine.app.input.glfw;

import static org.lwjgl.glfw.GLFW.*;

import java.nio.DoubleBuffer;
import java.util.LinkedList;
import java.util.List;

import org.lwjgl.glfw.GLFWCursorEnterCallback;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.glfw.GLFWWindowFocusCallback;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.app.config.InputConfig;
import com.neonsoup.engine.app.glfw.GLFWApplicationWindow;
import com.neonsoup.engine.app.input.ApplicationInput;
import com.neonsoup.engine.app.input.KeyboardEvent;
import com.neonsoup.engine.app.input.KeyboardKey;
import com.neonsoup.engine.app.input.KeyboardKeyAction;
import com.neonsoup.engine.app.input.KeyboardListener;
import com.neonsoup.engine.app.input.MouseCursorMode;
import com.neonsoup.engine.app.input.MouseEnterEvent;
import com.neonsoup.engine.app.input.MouseKey;
import com.neonsoup.engine.app.input.MouseKeyAction;
import com.neonsoup.engine.app.input.MouseKeyEvent;
import com.neonsoup.engine.app.input.MouseListener;
import com.neonsoup.engine.app.input.MouseMotionEvent;
import com.neonsoup.engine.app.input.MouseScrollEvent;
import com.neonsoup.engine.app.input.WindowFocusEvent;
import com.neonsoup.engine.app.input.WindowFocusListener;

import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;

public class GLFWApplicationInput extends ApplicationInput {

    protected static final TIntObjectMap<KeyboardKey> GLFW_CODE_TO_KEYBOARD_KEY_MAP;
    protected static final TObjectIntMap<KeyboardKey> KEYBOARD_KEY_TO_GLFW_CODE_MAP;

    static {

        final int COUNT = 121;

        {
            GLFW_CODE_TO_KEYBOARD_KEY_MAP = new TIntObjectHashMap<>(COUNT);

            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_UNKNOWN, KeyboardKey.KEY_UNKNOWN);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_SPACE, KeyboardKey.KEY_SPACE);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_APOSTROPHE, KeyboardKey.KEY_APOSTROPHE);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_COMMA, KeyboardKey.KEY_COMMA);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_MINUS, KeyboardKey.KEY_MINUS);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_PERIOD, KeyboardKey.KEY_PERIOD);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_SLASH, KeyboardKey.KEY_SLASH);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_0, KeyboardKey.KEY_0);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_1, KeyboardKey.KEY_1);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_2, KeyboardKey.KEY_2);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_3, KeyboardKey.KEY_3);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_4, KeyboardKey.KEY_4);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_5, KeyboardKey.KEY_5);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_6, KeyboardKey.KEY_6);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_7, KeyboardKey.KEY_7);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_8, KeyboardKey.KEY_8);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_9, KeyboardKey.KEY_9);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_SEMICOLON, KeyboardKey.KEY_SEMICOLON);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_EQUAL, KeyboardKey.KEY_EQUAL);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_A, KeyboardKey.KEY_A);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_B, KeyboardKey.KEY_B);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_C, KeyboardKey.KEY_C);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_D, KeyboardKey.KEY_D);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_E, KeyboardKey.KEY_E);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F, KeyboardKey.KEY_F);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_G, KeyboardKey.KEY_G);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_H, KeyboardKey.KEY_H);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_I, KeyboardKey.KEY_I);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_J, KeyboardKey.KEY_J);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_K, KeyboardKey.KEY_K);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_L, KeyboardKey.KEY_L);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_M, KeyboardKey.KEY_M);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_N, KeyboardKey.KEY_N);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_O, KeyboardKey.KEY_O);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_P, KeyboardKey.KEY_P);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_Q, KeyboardKey.KEY_Q);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_R, KeyboardKey.KEY_R);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_S, KeyboardKey.KEY_S);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_T, KeyboardKey.KEY_T);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_U, KeyboardKey.KEY_U);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_V, KeyboardKey.KEY_V);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_W, KeyboardKey.KEY_W);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_X, KeyboardKey.KEY_X);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_Y, KeyboardKey.KEY_Y);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_Z, KeyboardKey.KEY_Z);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_LEFT_BRACKET, KeyboardKey.KEY_LEFT_BRACKET);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_BACKSLASH, KeyboardKey.KEY_BACKSLASH);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_RIGHT_BRACKET, KeyboardKey.KEY_RIGHT_BRACKET);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_GRAVE_ACCENT, KeyboardKey.KEY_GRAVE_ACCENT);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_WORLD_1, KeyboardKey.KEY_WORLD_1);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_WORLD_2, KeyboardKey.KEY_WORLD_2);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_ESCAPE, KeyboardKey.KEY_ESCAPE);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_ENTER, KeyboardKey.KEY_ENTER);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_TAB, KeyboardKey.KEY_TAB);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_BACKSPACE, KeyboardKey.KEY_BACKSPACE);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_INSERT, KeyboardKey.KEY_INSERT);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_DELETE, KeyboardKey.KEY_DELETE);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_RIGHT, KeyboardKey.KEY_RIGHT);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_LEFT, KeyboardKey.KEY_LEFT);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_DOWN, KeyboardKey.KEY_DOWN);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_UP, KeyboardKey.KEY_UP);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_PAGE_UP, KeyboardKey.KEY_PAGE_UP);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_PAGE_DOWN, KeyboardKey.KEY_PAGE_DOWN);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_HOME, KeyboardKey.KEY_HOME);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_END, KeyboardKey.KEY_END);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_CAPS_LOCK, KeyboardKey.KEY_CAPS_LOCK);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_SCROLL_LOCK, KeyboardKey.KEY_SCROLL_LOCK);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_NUM_LOCK, KeyboardKey.KEY_NUM_LOCK);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_PRINT_SCREEN, KeyboardKey.KEY_PRINT_SCREEN);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_PAUSE, KeyboardKey.KEY_PAUSE);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F1, KeyboardKey.KEY_F1);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F2, KeyboardKey.KEY_F2);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F3, KeyboardKey.KEY_F3);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F4, KeyboardKey.KEY_F4);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F5, KeyboardKey.KEY_F5);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F6, KeyboardKey.KEY_F6);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F7, KeyboardKey.KEY_F7);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F8, KeyboardKey.KEY_F8);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F9, KeyboardKey.KEY_F9);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F10, KeyboardKey.KEY_F10);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F11, KeyboardKey.KEY_F11);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F12, KeyboardKey.KEY_F12);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F13, KeyboardKey.KEY_F13);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F14, KeyboardKey.KEY_F14);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F15, KeyboardKey.KEY_F15);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F16, KeyboardKey.KEY_F16);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F17, KeyboardKey.KEY_F17);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F18, KeyboardKey.KEY_F18);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F19, KeyboardKey.KEY_F19);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F20, KeyboardKey.KEY_F20);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F21, KeyboardKey.KEY_F21);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F22, KeyboardKey.KEY_F22);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F23, KeyboardKey.KEY_F23);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F24, KeyboardKey.KEY_F24);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_F25, KeyboardKey.KEY_F25);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_0, KeyboardKey.KEY_KP_0);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_1, KeyboardKey.KEY_KP_1);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_2, KeyboardKey.KEY_KP_2);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_3, KeyboardKey.KEY_KP_3);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_4, KeyboardKey.KEY_KP_4);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_5, KeyboardKey.KEY_KP_5);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_6, KeyboardKey.KEY_KP_6);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_7, KeyboardKey.KEY_KP_7);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_8, KeyboardKey.KEY_KP_8);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_9, KeyboardKey.KEY_KP_9);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_DECIMAL, KeyboardKey.KEY_KP_DECIMAL);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_DIVIDE, KeyboardKey.KEY_KP_DIVIDE);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_MULTIPLY, KeyboardKey.KEY_KP_MULTIPLY);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_SUBTRACT, KeyboardKey.KEY_KP_SUBTRACT);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_ADD, KeyboardKey.KEY_KP_ADD);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_ENTER, KeyboardKey.KEY_KP_ENTER);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_KP_EQUAL, KeyboardKey.KEY_KP_EQUAL);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_LEFT_SHIFT, KeyboardKey.KEY_LEFT_SHIFT);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_LEFT_CONTROL, KeyboardKey.KEY_LEFT_CONTROL);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_LEFT_ALT, KeyboardKey.KEY_LEFT_ALT);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_LEFT_SUPER, KeyboardKey.KEY_LEFT_SUPER);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_RIGHT_SHIFT, KeyboardKey.KEY_RIGHT_SHIFT);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_RIGHT_CONTROL, KeyboardKey.KEY_RIGHT_CONTROL);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_RIGHT_ALT, KeyboardKey.KEY_RIGHT_ALT);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_RIGHT_SUPER, KeyboardKey.KEY_RIGHT_SUPER);
            GLFW_CODE_TO_KEYBOARD_KEY_MAP.put(GLFW_KEY_MENU, KeyboardKey.KEY_MENU);
        }

        {
            KEYBOARD_KEY_TO_GLFW_CODE_MAP = new TObjectIntHashMap<>(COUNT);

            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_UNKNOWN, GLFW_KEY_UNKNOWN);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_SPACE, GLFW_KEY_SPACE);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_APOSTROPHE, GLFW_KEY_APOSTROPHE);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_COMMA, GLFW_KEY_COMMA);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_MINUS, GLFW_KEY_MINUS);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_PERIOD, GLFW_KEY_PERIOD);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_SLASH, GLFW_KEY_SLASH);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_0, GLFW_KEY_0);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_1, GLFW_KEY_1);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_2, GLFW_KEY_2);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_3, GLFW_KEY_3);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_4, GLFW_KEY_4);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_5, GLFW_KEY_5);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_6, GLFW_KEY_6);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_7, GLFW_KEY_7);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_8, GLFW_KEY_8);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_9, GLFW_KEY_9);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_SEMICOLON, GLFW_KEY_SEMICOLON);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_EQUAL, GLFW_KEY_EQUAL);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_A, GLFW_KEY_A);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_B, GLFW_KEY_B);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_C, GLFW_KEY_C);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_D, GLFW_KEY_D);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_E, GLFW_KEY_E);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F, GLFW_KEY_F);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_G, GLFW_KEY_G);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_H, GLFW_KEY_H);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_I, GLFW_KEY_I);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_J, GLFW_KEY_J);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_K, GLFW_KEY_K);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_L, GLFW_KEY_L);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_M, GLFW_KEY_M);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_N, GLFW_KEY_N);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_O, GLFW_KEY_O);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_P, GLFW_KEY_P);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_Q, GLFW_KEY_Q);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_R, GLFW_KEY_R);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_S, GLFW_KEY_S);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_T, GLFW_KEY_T);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_U, GLFW_KEY_U);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_V, GLFW_KEY_V);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_W, GLFW_KEY_W);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_X, GLFW_KEY_X);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_Y, GLFW_KEY_Y);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_Z, GLFW_KEY_Z);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_LEFT_BRACKET, GLFW_KEY_LEFT_BRACKET);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_BACKSLASH, GLFW_KEY_BACKSLASH);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_RIGHT_BRACKET, GLFW_KEY_RIGHT_BRACKET);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_GRAVE_ACCENT, GLFW_KEY_GRAVE_ACCENT);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_WORLD_1, GLFW_KEY_WORLD_1);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_WORLD_2, GLFW_KEY_WORLD_2);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_ESCAPE, GLFW_KEY_ESCAPE);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_ENTER, GLFW_KEY_ENTER);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_TAB, GLFW_KEY_TAB);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_BACKSPACE, GLFW_KEY_BACKSPACE);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_INSERT, GLFW_KEY_INSERT);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_DELETE, GLFW_KEY_DELETE);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_RIGHT, GLFW_KEY_RIGHT);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_LEFT, GLFW_KEY_LEFT);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_DOWN, GLFW_KEY_DOWN);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_UP, GLFW_KEY_UP);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_PAGE_UP, GLFW_KEY_PAGE_UP);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_PAGE_DOWN, GLFW_KEY_PAGE_DOWN);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_HOME, GLFW_KEY_HOME);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_END, GLFW_KEY_END);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_CAPS_LOCK, GLFW_KEY_CAPS_LOCK);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_SCROLL_LOCK, GLFW_KEY_SCROLL_LOCK);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_NUM_LOCK, GLFW_KEY_NUM_LOCK);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_PRINT_SCREEN, GLFW_KEY_PRINT_SCREEN);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_PAUSE, GLFW_KEY_PAUSE);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F1, GLFW_KEY_F1);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F2, GLFW_KEY_F2);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F3, GLFW_KEY_F3);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F4, GLFW_KEY_F4);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F5, GLFW_KEY_F5);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F6, GLFW_KEY_F6);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F7, GLFW_KEY_F7);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F8, GLFW_KEY_F8);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F9, GLFW_KEY_F9);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F10, GLFW_KEY_F10);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F11, GLFW_KEY_F11);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F12, GLFW_KEY_F12);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F13, GLFW_KEY_F13);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F14, GLFW_KEY_F14);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F15, GLFW_KEY_F15);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F16, GLFW_KEY_F16);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F17, GLFW_KEY_F17);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F18, GLFW_KEY_F18);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F19, GLFW_KEY_F19);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F20, GLFW_KEY_F20);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F21, GLFW_KEY_F21);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F22, GLFW_KEY_F22);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F23, GLFW_KEY_F23);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F24, GLFW_KEY_F24);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_F25, GLFW_KEY_F25);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_0, GLFW_KEY_KP_0);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_1, GLFW_KEY_KP_1);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_2, GLFW_KEY_KP_2);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_3, GLFW_KEY_KP_3);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_4, GLFW_KEY_KP_4);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_5, GLFW_KEY_KP_5);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_6, GLFW_KEY_KP_6);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_7, GLFW_KEY_KP_7);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_8, GLFW_KEY_KP_8);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_9, GLFW_KEY_KP_9);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_DECIMAL, GLFW_KEY_KP_DECIMAL);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_DIVIDE, GLFW_KEY_KP_DIVIDE);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_MULTIPLY, GLFW_KEY_KP_MULTIPLY);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_SUBTRACT, GLFW_KEY_KP_SUBTRACT);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_ADD, GLFW_KEY_KP_ADD);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_ENTER, GLFW_KEY_KP_ENTER);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_KP_EQUAL, GLFW_KEY_KP_EQUAL);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_LEFT_SHIFT, GLFW_KEY_LEFT_SHIFT);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_LEFT_CONTROL, GLFW_KEY_LEFT_CONTROL);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_LEFT_ALT, GLFW_KEY_LEFT_ALT);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_LEFT_SUPER, GLFW_KEY_LEFT_SUPER);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_RIGHT_SHIFT, GLFW_KEY_RIGHT_SHIFT);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_RIGHT_CONTROL, GLFW_KEY_RIGHT_CONTROL);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_RIGHT_ALT, GLFW_KEY_RIGHT_ALT);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_RIGHT_SUPER, GLFW_KEY_RIGHT_SUPER);
            KEYBOARD_KEY_TO_GLFW_CODE_MAP.put(KeyboardKey.KEY_MENU, GLFW_KEY_MENU);
        }
    }


    protected static final class InputEventRecord {

        protected static final int TYPE_KEYBOARD = 1;
        protected static final int TYPE_MOUSE_MOVE = 2;
        protected static final int TYPE_MOUSE_ENTER = 3;
        protected static final int TYPE_MOUSE_KEY = 4;
        protected static final int TYPE_WINDOW_FOCUS = 5;
        protected static final int TYPE_SCROLL = 6;

        protected int type;

        protected KeyboardEvent keyboardEvent;
        protected MouseMotionEvent mouseMotionEvent;
        protected MouseEnterEvent mouseEnterEvent;
        protected MouseKeyEvent mouseKeyEvent;
        protected MouseScrollEvent mouseScrollEvent;
        protected WindowFocusEvent windowFocusEvent;

    }


    protected final Object eventLock = new Object();

    protected GLFWKeyCallback glfwKeyCallback;

    protected GLFWCursorPosCallback glfwCursorPositionCallback;
    protected GLFWCursorEnterCallback glfwCursorEnterCallback;
    protected GLFWMouseButtonCallback glfwMouseButtonCallback;
    protected GLFWScrollCallback glfwScrollCallback;

    protected GLFWWindowFocusCallback glfwWindowFocusCallback;

    protected final List<InputEventRecord> eventQueue = new LinkedList<>();

    protected boolean hasWindowFocus = false;

    protected MouseCursorMode mouseCursorMode = MouseCursorMode.NORMAL;

    protected float prevMouseX, prevMouseY;
    protected float mouseX, mouseY;


    public GLFWApplicationInput(Application app, InputConfig config) {
        super(app, config);
    }


    @Override
    public boolean hasWindowFocus() {
        return hasWindowFocus;
    }


    @Override
    public MouseCursorMode getMouseCursorMode() {
        return mouseCursorMode;
    }


    @Override
    public void setMouseCursorMode(MouseCursorMode mode) {
        if (mode == null) {
            throw new IllegalArgumentException("mode must not be null");
        }

        long windowHandle = ((GLFWApplicationWindow) app.getWindow()).getGLFWWindowHandle();

        switch (mode) {
            case HIDDEN:
                glfwSetInputMode(windowHandle, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
                break;

            case NORMAL:
                glfwSetInputMode(windowHandle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
                break;

            case UNLIMITED_MOTION:
                glfwSetInputMode(windowHandle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
                break;
        }

        this.mouseCursorMode = mode;
    }


    @Override
    public float getMouseCursorX() {
        return mouseX;
    }


    @Override
    public float getMouseCursorY() {
        return mouseY;
    }


    @Override
    public float getPrevMouseCursorX() {
        return prevMouseX;
    }


    @Override
    public float getPrevMouseCursorY() {
        return prevMouseY;
    }


    @Override
    public void pollEvents() {
        glfwPollEvents();

        synchronized (eventLock) {
            while (!eventQueue.isEmpty()) {
                InputEventRecord rec = eventQueue.remove(0);

                switch (rec.type) {
                    case InputEventRecord.TYPE_KEYBOARD:
                        for (KeyboardListener listener : keyboardListeners) {
                            listener.onKeyboardEvent(rec.keyboardEvent);
                        }
                        break;

                    case InputEventRecord.TYPE_MOUSE_MOVE:
                        prevMouseX = mouseX;
                        prevMouseY = mouseY;

                        mouseX = rec.mouseMotionEvent.cursorX;
                        mouseY = rec.mouseMotionEvent.cursorY;

                        rec.mouseMotionEvent.deltaX = mouseX - prevMouseX;
                        rec.mouseMotionEvent.deltaY = mouseY - prevMouseY;

                        rec.mouseMotionEvent.prevCursorX = prevMouseX;
                        rec.mouseMotionEvent.prevCursorY = prevMouseY;

                        for (MouseListener listener : mouseListeners) {
                            listener.onMouseMoved(rec.mouseMotionEvent);
                        }

                        if (mouseInputFocus != null) {
                            mouseInputFocus.onMouseMoved(rec.mouseMotionEvent);
                        }

                        break;

                    case InputEventRecord.TYPE_MOUSE_ENTER:
                        for (MouseListener listener : mouseListeners) {
                            if (rec.mouseEnterEvent.entered) {
                                listener.onMouseEnteredWindow(rec.mouseEnterEvent);
                            } else {
                                listener.onMouseLeftWindow(rec.mouseEnterEvent);
                            }
                        }

                        if (mouseInputFocus != null) {
                            if (rec.mouseEnterEvent.entered) {
                                mouseInputFocus.onMouseEnteredWindow(rec.mouseEnterEvent);
                            } else {
                                mouseInputFocus.onMouseLeftWindow(rec.mouseEnterEvent);
                            }
                        }

                        break;

                    case InputEventRecord.TYPE_MOUSE_KEY:
                        for (MouseListener listener : mouseListeners) {
                            listener.onMouseKeyEvent(rec.mouseKeyEvent);
                        }

                        if (mouseInputFocus != null) {
                            mouseInputFocus.onMouseKeyEvent(rec.mouseKeyEvent);
                        }

                        break;

                    case InputEventRecord.TYPE_SCROLL:
                        for (MouseListener listener : mouseListeners) {
                            listener.onMouseScrollEvent(rec.mouseScrollEvent);
                        }

                        if (mouseInputFocus != null) {
                            mouseInputFocus.onMouseScrollEvent(rec.mouseScrollEvent);
                        }

                        break;

                    case InputEventRecord.TYPE_WINDOW_FOCUS:
                        hasWindowFocus = rec.windowFocusEvent.focused;

                        for (WindowFocusListener listener : windowFocusListeners) {
                            if (rec.windowFocusEvent.focused) {
                                listener.onWindowFocusAcquired(rec.windowFocusEvent);
                            } else {
                                listener.onWindowFocusLost(rec.windowFocusEvent);
                            }
                        }

                        break;

                    default:
                        throw new RuntimeException("Unknown input event record type: " + rec.type);
                }
            }
        }
    }


    @Override
    public void initInMainThread() {
        long windowHandle = ((GLFWApplicationWindow) app.getWindow()).getGLFWWindowHandle();

        glfwWindowFocusCallback = new GLFWWindowFocusCallback() {

            @Override
            public void invoke(long window, boolean focused) {
                synchronized (eventLock) {
                    InputEventRecord record = new InputEventRecord();

                    record.type = InputEventRecord.TYPE_WINDOW_FOCUS;

                    record.windowFocusEvent = new WindowFocusEvent();

                    record.windowFocusEvent.focused = focused;

                    eventQueue.add(record);
                }
            }

        };

        glfwSetWindowFocusCallback(windowHandle, glfwWindowFocusCallback);

        glfwKeyCallback = new GLFWKeyCallback() {

            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
                synchronized (eventLock) {
                    InputEventRecord record = new InputEventRecord();

                    record.type = InputEventRecord.TYPE_KEYBOARD;

                    KeyboardEvent event = new KeyboardEvent();
                    record.keyboardEvent = event;

                    event.key = GLFW_CODE_TO_KEYBOARD_KEY_MAP.get(key);

                    if (event.key == null) {
                        event.key = KeyboardKey.KEY_UNKNOWN;
                    }

                    event.scancode = scancode;

                    switch (action) {
                        case GLFW_PRESS:
                            event.keyAction = KeyboardKeyAction.PRESS;
                            break;

                        case GLFW_REPEAT:
                            event.keyAction = KeyboardKeyAction.REPEAT;
                            break;

                        case GLFW_RELEASE:
                            event.keyAction = KeyboardKeyAction.RELEASE;
                            break;
                    }

                    event.modifiers = mods;

                    eventQueue.add(record);
                }
            }

        };

        glfwSetKeyCallback(windowHandle, glfwKeyCallback);

        glfwCursorPositionCallback = new GLFWCursorPosCallback() {

            @Override
            public void invoke(long window, double xpos, double ypos) {
                synchronized (eventLock) {
                    InputEventRecord record = new InputEventRecord();

                    record.type = InputEventRecord.TYPE_MOUSE_MOVE;

                    MouseMotionEvent event = new MouseMotionEvent();
                    record.mouseMotionEvent = event;

                    event.cursorX = (float) xpos;
                    event.cursorY = (float) ypos;

                    eventQueue.add(record);
                }
            }

        };

        glfwSetCursorPosCallback(windowHandle, glfwCursorPositionCallback);

        glfwCursorEnterCallback = new GLFWCursorEnterCallback() {

            @Override
            public void invoke(long window, boolean entered) {
                synchronized (eventLock) {
                    InputEventRecord record = new InputEventRecord();

                    record.type = InputEventRecord.TYPE_MOUSE_ENTER;

                    MouseEnterEvent event = new MouseEnterEvent();
                    record.mouseEnterEvent = event;

                    event.entered = entered;

                    eventQueue.add(record);
                }
            }

        };

        glfwSetCursorEnterCallback(windowHandle, glfwCursorEnterCallback);

        glfwMouseButtonCallback = new GLFWMouseButtonCallback() {

            @Override
            public void invoke(long window, int button, int action, int mods) {
                synchronized (eventLock) {
                    switch (button) {
                        case GLFW_MOUSE_BUTTON_LEFT:
                        case GLFW_MOUSE_BUTTON_RIGHT:
                        case GLFW_MOUSE_BUTTON_MIDDLE:
                            break;

                        default:
                            return;
                    }

                    InputEventRecord record = new InputEventRecord();

                    record.type = InputEventRecord.TYPE_MOUSE_KEY;

                    MouseKeyEvent event = new MouseKeyEvent();
                    record.mouseKeyEvent = event;

                    switch (button) {
                        case GLFW_MOUSE_BUTTON_LEFT:
                            event.key = MouseKey.LEFT;
                            break;

                        case GLFW_MOUSE_BUTTON_RIGHT:
                            event.key = MouseKey.RIGHT;
                            break;

                        case GLFW_MOUSE_BUTTON_MIDDLE:
                            event.key = MouseKey.MIDDLE;
                            break;

                        default:
                            break;
                    }

                    event.keyAction = action == GLFW_PRESS ? MouseKeyAction.PRESS : MouseKeyAction.RELEASE;

                    eventQueue.add(record);
                }
            }

        };

        glfwSetMouseButtonCallback(windowHandle, glfwMouseButtonCallback);

        glfwScrollCallback = new GLFWScrollCallback() {

            @Override
            public void invoke(long window, double xoffset, double yoffset) {
                synchronized (eventLock) {
                    InputEventRecord record = new InputEventRecord();

                    record.type = InputEventRecord.TYPE_SCROLL;

                    MouseScrollEvent event = new MouseScrollEvent();
                    record.mouseScrollEvent = event;

                    event.verticalScroll = (float) yoffset;

                    eventQueue.add(record);
                }
            }

        };

        glfwSetScrollCallback(windowHandle, glfwScrollCallback);

        try (MemoryStack stack = MemoryStack.stackPush()) {
            DoubleBuffer x = stack.mallocDouble(1);
            DoubleBuffer y = stack.mallocDouble(1);

            glfwGetCursorPos(windowHandle, x, y);

            mouseX = prevMouseX = (float) x.get(0);
            mouseY = prevMouseY = (float) y.get(0);
        }

        hasWindowFocus = glfwGetWindowAttrib(windowHandle, GLFW_FOCUSED) == GLFW_TRUE ? true : false;
    }


    @Override
    public void disposeInMainThread() {
        long glfwWindowHandle = ((GLFWApplicationWindow) app.getWindow()).getGLFWWindowHandle();

        if (glfwWindowHandle != MemoryUtil.NULL) {
            GLFWKeyCallback keyCallback = glfwSetKeyCallback(glfwWindowHandle, null);

            if (keyCallback != null) {
                keyCallback.free();
            }

            GLFWCursorPosCallback cursorPosCallback = glfwSetCursorPosCallback(glfwWindowHandle, null);

            if (cursorPosCallback != null) {
                cursorPosCallback.free();
            }

            GLFWCursorEnterCallback cursorEnterCallback = glfwSetCursorEnterCallback(glfwWindowHandle, null);

            if (cursorEnterCallback != null) {
                cursorEnterCallback.free();
            }

            GLFWMouseButtonCallback mouseButtonCallback = glfwSetMouseButtonCallback(glfwWindowHandle, null);

            if (mouseButtonCallback != null) {
                mouseButtonCallback.free();
            }

            GLFWWindowFocusCallback windowFocusCallback = glfwSetWindowFocusCallback(glfwWindowHandle, null);

            if (windowFocusCallback != null) {
                windowFocusCallback.free();
            }

            GLFWScrollCallback scrollCallback = glfwSetScrollCallback(glfwWindowHandle, null);

            if (scrollCallback != null) {
                scrollCallback.free();
            }
        }
    }

}
