package com.neonsoup.engine.app.input;

public interface MouseInputFocusHolder extends MouseListener {

    void onMouseInputFocusAcquired();


    void onMouseInputFocusLost();

}
