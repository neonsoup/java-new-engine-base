package com.neonsoup.engine.app.input;

public enum MouseCursorMode {
    NORMAL, HIDDEN, UNLIMITED_MOTION;
}
