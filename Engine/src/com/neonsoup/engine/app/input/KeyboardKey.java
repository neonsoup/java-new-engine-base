package com.neonsoup.engine.app.input;

public enum KeyboardKey {

    KEY_UNKNOWN("UNKNOWN"),

    KEY_SPACE("SPACE"),

    KEY_APOSTROPHE("APOSTROPHE"),

    KEY_COMMA("COMMA"),

    KEY_MINUS("MINUS"),

    KEY_PERIOD("PERIOD"),

    KEY_SLASH("SLASH"),

    KEY_0("0"),

    KEY_1("1"),

    KEY_2("2"),

    KEY_3("3"),

    KEY_4("4"),

    KEY_5("5"),

    KEY_6("6"),

    KEY_7("7"),

    KEY_8("8"),

    KEY_9("9"),

    KEY_SEMICOLON("SEMICOLON"),

    KEY_EQUAL("EQUAL"),

    KEY_A("A"),

    KEY_B("B"),

    KEY_C("C"),

    KEY_D("D"),

    KEY_E("E"),

    KEY_F("F"),

    KEY_G("G"),

    KEY_H("H"),

    KEY_I("I"),

    KEY_J("J"),

    KEY_K("K"),

    KEY_L("L"),

    KEY_M("M"),

    KEY_N("N"),

    KEY_O("O"),

    KEY_P("P"),

    KEY_Q("Q"),

    KEY_R("R"),

    KEY_S("S"),

    KEY_T("T"),

    KEY_U("U"),

    KEY_V("V"),

    KEY_W("W"),

    KEY_X("X"),

    KEY_Y("Y"),

    KEY_Z("Z"),

    KEY_LEFT_BRACKET("LEFT_BRACKET"),

    KEY_BACKSLASH("BACKSLASH"),

    KEY_RIGHT_BRACKET("RIGHT_BRACKET"),

    KEY_GRAVE_ACCENT("GRAVE_ACCENT"),

    KEY_WORLD_1("WORLD_1"),

    KEY_WORLD_2("WORLD_2"),

    KEY_ESCAPE("ESCAPE"),

    KEY_ENTER("ENTER"),

    KEY_TAB("TAB"),

    KEY_BACKSPACE("BACKSPACE"),

    KEY_INSERT("INSERT"),

    KEY_DELETE("DELETE"),

    KEY_RIGHT("RIGHT"),

    KEY_LEFT("LEFT"),

    KEY_DOWN("DOWN"),

    KEY_UP("UP"),

    KEY_PAGE_UP("PAGE_UP"),

    KEY_PAGE_DOWN("PAGE_DOWN"),

    KEY_HOME("HOME"),

    KEY_END("END"),

    KEY_CAPS_LOCK("CAPS_LOCK"),

    KEY_SCROLL_LOCK("SCROLL_LOCK"),

    KEY_NUM_LOCK("NUM_LOCK"),

    KEY_PRINT_SCREEN("PRINT_SCREEN"),

    KEY_PAUSE("PAUSE"),

    KEY_F1("F1"),

    KEY_F2("F2"),

    KEY_F3("F3"),

    KEY_F4("F4"),

    KEY_F5("F5"),

    KEY_F6("F6"),

    KEY_F7("F7"),

    KEY_F8("F8"),

    KEY_F9("F9"),

    KEY_F10("F10"),

    KEY_F11("F11"),

    KEY_F12("F12"),

    KEY_F13("F13"),

    KEY_F14("F14"),

    KEY_F15("F15"),

    KEY_F16("F16"),

    KEY_F17("F17"),

    KEY_F18("F18"),

    KEY_F19("F19"),

    KEY_F20("F20"),

    KEY_F21("F21"),

    KEY_F22("F22"),

    KEY_F23("F23"),

    KEY_F24("F24"),

    KEY_F25("F25"),

    KEY_KP_0("KP_0"),

    KEY_KP_1("KP_1"),

    KEY_KP_2("KP_2"),

    KEY_KP_3("KP_3"),

    KEY_KP_4("KP_4"),

    KEY_KP_5("KP_5"),

    KEY_KP_6("KP_6"),

    KEY_KP_7("KP_7"),

    KEY_KP_8("KP_8"),

    KEY_KP_9("KP_9"),

    KEY_KP_DECIMAL("KP_DECIMAL"),

    KEY_KP_DIVIDE("KP_DIVIDE"),

    KEY_KP_MULTIPLY("KP_MULTIPLY"),

    KEY_KP_SUBTRACT("KP_SUBTRACT"),

    KEY_KP_ADD("KP_ADD"),

    KEY_KP_ENTER("KP_ENTER"),

    KEY_KP_EQUAL("KP_EQUAL"),

    KEY_LEFT_SHIFT("LEFT_SHIFT"),

    KEY_LEFT_CONTROL("LEFT_CONTROL"),

    KEY_LEFT_ALT("LEFT_ALT"),

    KEY_LEFT_SUPER("LEFT_SUPER"),

    KEY_RIGHT_SHIFT("RIGHT_SHIFT"),

    KEY_RIGHT_CONTROL("RIGHT_CONTROL"),

    KEY_RIGHT_ALT("RIGHT_ALT"),

    KEY_RIGHT_SUPER("RIGHT_SUPER"),

    KEY_MENU("MENU");

    public final String name;


    private KeyboardKey(String name) {
        this.name = name;
    }

}
