package com.neonsoup.engine.app.input;

public interface KeyboardListener {

    void onKeyboardEvent(KeyboardEvent event);

}
