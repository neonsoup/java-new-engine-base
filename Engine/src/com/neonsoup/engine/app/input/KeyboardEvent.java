package com.neonsoup.engine.app.input;

public class KeyboardEvent {

    public KeyboardKey key;
    public int scancode;
    public KeyboardKeyAction keyAction;
    public int modifiers;


    public KeyboardEvent() {
    }


    @Override
    public String toString() {
        return "KeyboardEvent [Key: "
               + key
               + "; Scancode: "
               + scancode
               + "; Key action: "
               + keyAction
               + "; Modifiers: "
               + KeyModifier.toString(modifiers)
               + "]";
    }

}
