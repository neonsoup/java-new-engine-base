package com.neonsoup.engine.app.input;

public interface MouseListener {

    void onMouseMoved(MouseMotionEvent event);


    void onMouseEnteredWindow(MouseEnterEvent event);


    void onMouseLeftWindow(MouseEnterEvent event);


    void onMouseKeyEvent(MouseKeyEvent event);


    void onMouseScrollEvent(MouseScrollEvent event);

}
