package com.neonsoup.engine.app.input;

import java.util.ArrayList;
import java.util.List;

public class KeyModifier {

    public static final int SHIFT = 1 << 0;
    public static final int CONTROL = 1 << 1;
    public static final int ALT = 1 << 2;
    public static final int SUPER = 1 << 3;


    private KeyModifier() {
    }


    public static String toString(int mods) {
        StringBuilder str = new StringBuilder();

        List<String> modStrings = new ArrayList<>(4);

        if ((mods & SHIFT) != 0) {
            modStrings.add("SHIFT");
        }

        if ((mods & CONTROL) != 0) {
            modStrings.add("CONTROL");
        }

        if ((mods & ALT) != 0) {
            modStrings.add("ALT");
        }

        if ((mods & SUPER) != 0) {
            modStrings.add("SUPER");
        }

        str.append("(");

        for (int i = 0; i < modStrings.size(); i++) {
            str.append(modStrings.get(i));

            if (i < modStrings.size() - 1) {
                str.append(", ");
            }
        }

        str.append(")");

        return str.toString();
    }

}
