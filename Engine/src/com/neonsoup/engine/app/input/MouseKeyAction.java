package com.neonsoup.engine.app.input;

public enum MouseKeyAction {
    PRESS, RELEASE;
}
