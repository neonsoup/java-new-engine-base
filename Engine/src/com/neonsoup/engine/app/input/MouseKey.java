package com.neonsoup.engine.app.input;

public enum MouseKey {
    LEFT, RIGHT, MIDDLE;
}
