package com.neonsoup.engine.app.input;

public class WindowFocusEvent {

    public boolean focused;


    public WindowFocusEvent() {
    }


    @Override
    public String toString() {
        return "WindowFocusEvent [Focused: " + focused + "]";
    }

}
