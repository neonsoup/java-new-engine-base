package com.neonsoup.engine.app.input;

public enum KeyboardKeyAction {
    PRESS, REPEAT, RELEASE;
}
