package com.neonsoup.engine.app.input;

import java.util.Collection;

import com.neonsoup.common.util.NoModCheckLinkedList;
import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.app.config.InputConfig;
import com.neonsoup.engine.app.config.InputConfigBuilder;

public abstract class ApplicationInput {

    protected final Application app;

    protected InputConfig config;

    protected Collection<KeyboardListener> keyboardListeners = new NoModCheckLinkedList<>();
    protected Collection<MouseListener> mouseListeners = new NoModCheckLinkedList<>();
    protected Collection<WindowFocusListener> windowFocusListeners = new NoModCheckLinkedList<>();

    protected MouseInputFocusHolder mouseInputFocus = null;


    public ApplicationInput(Application app, InputConfig config) {
        this.app = app;
        this.config = new InputConfigBuilder(config);
    }


    public void addKeyboardListener(KeyboardListener listener) {
        if (listener != null && !keyboardListeners.contains(listener)) {
            keyboardListeners.add(listener);
        }
    }


    public void removeKeyboardListener(KeyboardListener listener) {
        keyboardListeners.remove(listener);
    }


    public void addMouseListener(MouseListener listener) {
        if (listener != null && !mouseListeners.contains(listener)) {
            mouseListeners.add(listener);
        }
    }


    public void removeMouseListener(MouseListener listener) {
        mouseListeners.remove(listener);
    }


    public void addWindowFocusEventListener(WindowFocusListener listener) {
        if (listener != null && !windowFocusListeners.contains(listener)) {
            windowFocusListeners.add(listener);
        }
    }


    public void removeWindowFocusEventListener(WindowFocusListener listener) {
        windowFocusListeners.remove(listener);
    }


    public abstract void pollEvents();


    public abstract boolean hasWindowFocus();


    public abstract MouseCursorMode getMouseCursorMode();


    public abstract void setMouseCursorMode(MouseCursorMode mode);


    public abstract float getMouseCursorX();


    public abstract float getMouseCursorY();


    public abstract float getPrevMouseCursorX();


    public abstract float getPrevMouseCursorY();


    public abstract void initInMainThread();


    public abstract void disposeInMainThread();


    public MouseInputFocusHolder getCurrentMouseInputFocusHolder() {
        return this.mouseInputFocus;
    }


    public void acquireMouseInputFocus(MouseInputFocusHolder focusHolder) {
        if (this.mouseInputFocus != null) {
            this.mouseInputFocus.onMouseInputFocusLost();
        }

        this.mouseInputFocus = focusHolder;

        if (focusHolder != null) {
            focusHolder.onMouseInputFocusAcquired();
        }
    }


    public void requestMouseInputFocusRelease() {
        if (mouseInputFocus != null) {
            mouseInputFocus.onMouseInputFocusLost();
        }

        mouseInputFocus = null;
    }

}
