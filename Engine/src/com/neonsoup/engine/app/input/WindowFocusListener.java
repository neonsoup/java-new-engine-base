package com.neonsoup.engine.app.input;

public interface WindowFocusListener {

    void onWindowFocusAcquired(WindowFocusEvent event);


    void onWindowFocusLost(WindowFocusEvent event);

}
