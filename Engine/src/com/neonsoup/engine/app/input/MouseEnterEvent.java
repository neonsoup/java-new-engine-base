package com.neonsoup.engine.app.input;

public class MouseEnterEvent {

    public boolean entered;


    public MouseEnterEvent() {
    }


    @Override
    public String toString() {
        return "MouseEnterEvent [Entered: " + entered + "]";
    }

}
