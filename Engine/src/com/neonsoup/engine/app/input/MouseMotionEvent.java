package com.neonsoup.engine.app.input;

public class MouseMotionEvent {

    public float cursorX;
    public float cursorY;

    public float prevCursorX;
    public float prevCursorY;

    public float deltaX;
    public float deltaY;


    public MouseMotionEvent() {
    }


    @Override
    public String toString() {
        return "MouseMotionEvent [Cursor position: ("
               + cursorX
               + ", "
               + cursorY
               + "); Delta: ("
               + deltaX
               + ", "
               + deltaY
               + ")]";
    }

}
