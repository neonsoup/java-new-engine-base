package com.neonsoup.engine.app.input;

public class MouseKeyEvent {

    public MouseKey key;
    public MouseKeyAction keyAction;


    public MouseKeyEvent() {
    }


    @Override
    public String toString() {
        return "MouseKeyEvent [Key: " + key + "; Key action: " + keyAction + "]";
    }

}
