package com.neonsoup.engine.app.input;

public class MouseScrollEvent {

    public float verticalScroll;


    public MouseScrollEvent() {
    }


    @Override
    public String toString() {
        return "MouseScrollEvent [Vertical scroll: " + verticalScroll + "]";
    }

}
