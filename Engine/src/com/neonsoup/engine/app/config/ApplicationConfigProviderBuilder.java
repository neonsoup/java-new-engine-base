package com.neonsoup.engine.app.config;

public class ApplicationConfigProviderBuilder implements ApplicationConfigProvider {

    public final ApplicationConfigBuilder applicationConfig = new ApplicationConfigBuilder();
    public final RealTimeLoopConfigBuilder realTimeLoopConfig = new RealTimeLoopConfigBuilder();
    public final GraphicsConfigBuilder graphicsConfig = new GraphicsConfigBuilder();
    public final WindowConfigBuilder windowConfig = new WindowConfigBuilder();
    public final CanvasConfigBuilder canvasConfig = new CanvasConfigBuilder();
    public final InputConfigBuilder inputConfig = new InputConfigBuilder();


    public ApplicationConfigProviderBuilder() {
    }


    @Override
    public ApplicationConfig getApplicationConfig() {
        return applicationConfig;
    }


    @Override
    public RealTimeLoopConfig getRealTimeLoopConfig() {
        return realTimeLoopConfig;
    }


    @Override
    public GraphicsConfig getGraphicsConfig() {
        return graphicsConfig;
    }


    @Override
    public WindowConfig getWindowConfig() {
        return windowConfig;
    }


    @Override
    public CanvasConfig getCanvasConfig() {
        return canvasConfig;
    }


    @Override
    public InputConfig getInputConfig() {
        return inputConfig;
    }

}
