package com.neonsoup.engine.app.config;

public class CanvasConfigBuilder implements CanvasConfig {

    private int redBits = 8, greenBits = 8, blueBits = 8;
    private int alphaBits = 8;

    private int depthBits = 24;

    private int stencilBits = 8;

    private int width = 640;
    private int height = 480;


    public CanvasConfigBuilder() {
    }


    public CanvasConfigBuilder(CanvasConfig src) {
        this.redBits = src.getRedBits();
        this.greenBits = src.getGreenBits();
        this.blueBits = src.getBlueBits();

        this.alphaBits = src.getAlphaBits();

        this.depthBits = src.getDepthBits();

        this.stencilBits = src.getStencilBits();

        this.width = src.getWidth();
        this.height = src.getHeight();
    }


    @Override
    public int getRedBits() {
        return redBits;
    }


    @Override
    public int getGreenBits() {
        return greenBits;
    }


    @Override
    public int getBlueBits() {
        return blueBits;
    }


    @Override
    public int getAlphaBits() {
        return alphaBits;
    }


    @Override
    public int getDepthBits() {
        return depthBits;
    }


    @Override
    public int getStencilBits() {
        return stencilBits;
    }


    @Override
    public int getWidth() {
        return width;
    }


    @Override
    public int getHeight() {
        return height;
    }


    public void setRedBits(int redBits) {
        if (redBits < 0) {
            throw new IllegalArgumentException("Red bits must not be a negative number");
        }

        this.redBits = redBits;
    }


    public void setGreenBits(int greenBits) {
        if (greenBits < 0) {
            throw new IllegalArgumentException("Green bits must not be a negative number");
        }

        this.greenBits = greenBits;
    }


    public void setBlueBits(int blueBits) {
        if (blueBits < 0) {
            throw new IllegalArgumentException("Blue bits must not be a negative number");
        }

        this.blueBits = blueBits;
    }


    public void setAlphaBits(int alphaBits) {
        if (alphaBits < 0) {
            throw new IllegalArgumentException("Alpha bits must not be a negative number");
        }

        this.alphaBits = alphaBits;
    }


    public void setDepthBits(int depthBits) {
        if (depthBits < 0) {
            throw new IllegalArgumentException("Depth bits must not be a negative number");
        }

        this.depthBits = depthBits;
    }


    public void setStencilBits(int stencilBits) {
        if (stencilBits < 0) {
            throw new IllegalArgumentException("Stencil bits must not be a negative number");
        }

        this.stencilBits = stencilBits;
    }


    public void setWidth(int width) {
        if (width < 0) {
            throw new IllegalArgumentException("Width must not be a negative number");
        }

        this.width = width;
    }


    public void setHeight(int height) {
        if (height < 0) {
            throw new IllegalArgumentException("Height must not be a negative number");
        }

        this.height = height;
    }

}
