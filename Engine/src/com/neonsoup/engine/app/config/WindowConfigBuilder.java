package com.neonsoup.engine.app.config;

public class WindowConfigBuilder implements WindowConfig {

    private int monitorIndex = PRIMARY_MONITOR_INDEX;

    private String name = "Neonsoup engine application";

    private boolean fullscreen = false;

    private int refreshRate = DEFAULT_REFRESH_RATE;

    private boolean vsync = true;


    public WindowConfigBuilder() {
    }


    public WindowConfigBuilder(WindowConfig src) {
        this.monitorIndex = src.getMonitorIndex();
        this.name = src.getName();
        this.fullscreen = src.getFullscreen();
        this.refreshRate = src.getRefreshRate();
        this.vsync = src.getVSync();
    }


    @Override
    public int getMonitorIndex() {
        return monitorIndex;
    }


    @Override
    public String getName() {
        return name;
    }


    @Override
    public boolean getFullscreen() {
        return fullscreen;
    }


    @Override
    public int getRefreshRate() {
        return refreshRate;
    }


    @Override
    public boolean getVSync() {
        return vsync;
    }


    public void setMonitorIndex(int monitorIndex) {
        if (monitorIndex < -1) {
            throw new IllegalArgumentException("Monitor index must be a positive number, zero or PRIMARY_MONITOR_INDEX ("
                                               + PRIMARY_MONITOR_INDEX
                                               + ")");
        }

        this.monitorIndex = monitorIndex;
    }


    public void setName(String name) {
        this.name = name;
    }


    public void setFullscreen(boolean fullscreen) {
        this.fullscreen = fullscreen;
    }


    public void setRefreshRate(int refreshRate) {
        if (refreshRate < -1) {
            throw new IllegalArgumentException("Refresh rate must be a positive number, zero or DEFAULT_REFRESH_RATE ("
                                               + DEFAULT_REFRESH_RATE
                                               + ")");
        }

        this.refreshRate = refreshRate;
    }


    public void setVsync(boolean vsync) {
        this.vsync = vsync;
    }

}
