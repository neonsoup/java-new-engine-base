package com.neonsoup.engine.app.config;

public class RealTimeLoopConfigBuilder implements RealTimeLoopConfig {

    private float targetUpdateFPS = 60.0f;
    private float targetRenderFPS = 60.0f;
    private float targetPauseFPS = 5.0f;


    public RealTimeLoopConfigBuilder() {
    }


    @Override
    public float getTargetUpdateFPS() {
        return targetUpdateFPS;
    }


    @Override
    public float getTargetRenderFPS() {
        return targetRenderFPS;
    }


    @Override
    public float getTargetPauseFPS() {
        return targetPauseFPS;
    }


    public void setTargetUpdateFPS(float targetUpdateFPS) {
        if (targetUpdateFPS < 0.0f) {
            throw new IllegalArgumentException("Target update FPS must not be a negative number");
        }

        this.targetUpdateFPS = targetUpdateFPS;
    }


    public void setTargetRenderFPS(float targetRenderFPS) {
        if (targetRenderFPS < 0.0f) {
            throw new IllegalArgumentException("Target render FPS must not be a negative number");
        }

        this.targetRenderFPS = targetRenderFPS;
    }


    public void setTargetPauseFPS(float targetPauseFPS) {
        if (targetPauseFPS < 0.0f) {
            throw new IllegalArgumentException("Target pause FPS must not be a negative number");
        }

        this.targetPauseFPS = targetPauseFPS;
    }

}
