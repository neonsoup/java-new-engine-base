package com.neonsoup.engine.app.config;

public interface RealTimeLoopConfig {

    float getTargetUpdateFPS();


    float getTargetRenderFPS();


    float getTargetPauseFPS();

}
