package com.neonsoup.engine.app.config;

import java.io.File;

import com.neonsoup.engine.app.state.ApplicationStateFactory;

public class ApplicationConfigBuilder implements ApplicationConfig {

    private ApplicationStateFactory stateFactory;
    private Object initialStateKey = null;
    private File baseDirectory;


    public ApplicationConfigBuilder() {
    }


    public ApplicationConfigBuilder(ApplicationConfig src) {
        this.stateFactory = src.getStateFactory();
        this.initialStateKey = src.getInitialStateKey();
        this.baseDirectory = src.getBaseDirectory();
    }


    @Override
    public ApplicationStateFactory getStateFactory() {
        return stateFactory;
    }


    @Override
    public Object getInitialStateKey() {
        return initialStateKey;
    }


    @Override
    public File getBaseDirectory() {
        return baseDirectory;
    }


    public void setStateFactory(ApplicationStateFactory stateFactory) {
        if (stateFactory == null) {
            throw new IllegalArgumentException("stateFactory is null");
        }

        this.stateFactory = stateFactory;
    }


    public void setInitialStateKey(Object initialStateKey) {
        this.initialStateKey = initialStateKey;
    }


    public void setBaseDirectory(File baseDirectory) {
        if (baseDirectory == null) {
            throw new IllegalArgumentException("baseDirectory is null");
        }

        this.baseDirectory = baseDirectory;
    }

}
