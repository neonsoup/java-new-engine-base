package com.neonsoup.engine.app.config;

public interface ApplicationConfigProvider {

    ApplicationConfig getApplicationConfig();


    RealTimeLoopConfig getRealTimeLoopConfig();


    GraphicsConfig getGraphicsConfig();


    WindowConfig getWindowConfig();


    CanvasConfig getCanvasConfig();


    InputConfig getInputConfig();

}
