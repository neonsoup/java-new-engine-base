package com.neonsoup.engine.app.config;

import java.io.File;

import com.neonsoup.engine.app.state.ApplicationStateFactory;

public interface ApplicationConfig {

    ApplicationStateFactory getStateFactory();


    Object getInitialStateKey();


    File getBaseDirectory();

}
