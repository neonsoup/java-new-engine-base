package com.neonsoup.engine.app.config;

public interface WindowConfig {

    public static final int DEFAULT_REFRESH_RATE = -1;
    public static final int PRIMARY_MONITOR_INDEX = -1;


    int getMonitorIndex();


    String getName();


    boolean getFullscreen();


    int getRefreshRate();


    boolean getVSync();

}
