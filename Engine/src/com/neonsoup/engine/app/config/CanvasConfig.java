package com.neonsoup.engine.app.config;

public interface CanvasConfig {

    int getRedBits();


    int getGreenBits();


    int getBlueBits();


    int getAlphaBits();


    int getDepthBits();


    int getStencilBits();


    int getWidth();


    int getHeight();

}
