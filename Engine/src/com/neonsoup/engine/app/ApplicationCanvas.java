package com.neonsoup.engine.app;

import com.neonsoup.engine.app.config.CanvasConfig;
import com.neonsoup.engine.app.config.CanvasConfigBuilder;

public abstract class ApplicationCanvas {

    protected final Application app;

    protected int width, height;

    protected CanvasConfigBuilder config;


    public ApplicationCanvas(Application app, CanvasConfig config) {
        this.app = app;
        this.config = new CanvasConfigBuilder(config);

        this.width = config.getWidth();
        this.height = config.getHeight();
    }


    public Application getApplication() {
        return app;
    }


    public CanvasConfig getCurrentConfig() {
        return config;
    }


    public int getWidth() {
        return width;
    }


    public int getHeight() {
        return height;
    }


    public abstract void initInMainThread();


    public abstract void disposeInMainThread();


    public abstract boolean isDoubleBuffered();


    public void swapBuffers() {
        throw new UnsupportedOperationException("This viewport is not double buffered");
    }

}
