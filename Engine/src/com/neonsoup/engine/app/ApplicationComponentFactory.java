package com.neonsoup.engine.app;

import com.neonsoup.engine.app.config.CanvasConfig;
import com.neonsoup.engine.app.config.GraphicsConfig;
import com.neonsoup.engine.app.config.InputConfig;
import com.neonsoup.engine.app.config.RealTimeLoopConfig;
import com.neonsoup.engine.app.config.WindowConfig;
import com.neonsoup.engine.app.input.ApplicationInput;

public interface ApplicationComponentFactory {

    void setApplication(Application app);


    boolean hasGraphics();


    ApplicationGraphics createGraphics(GraphicsConfig config);


    boolean hasCanvas();


    ApplicationCanvas createCanvas(CanvasConfig config);


    boolean hasWindow();


    ApplicationWindow createWindow(WindowConfig config);


    boolean hasRealTimeLoop();


    RealTimeApplicationLoop createRealTimeLoop(RealTimeLoopConfig config);


    boolean hasInput();


    ApplicationInput createInput(InputConfig config);

}
