package com.neonsoup.engine.app;

public interface ApplicationLifecycleListener {

    void onStart();


    void onPauseRequested();


    void onPause();


    void onResume();


    void onStop();

}
