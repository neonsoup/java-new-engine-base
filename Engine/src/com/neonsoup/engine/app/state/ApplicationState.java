package com.neonsoup.engine.app.state;

import java.util.List;

import com.neonsoup.common.util.NoModCheckLinkedList;
import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.app.ApplicationLifecycleListener;
import com.neonsoup.engine.app.input.KeyboardEvent;
import com.neonsoup.engine.app.input.MouseEnterEvent;
import com.neonsoup.engine.app.input.MouseInputFocusHolder;
import com.neonsoup.engine.app.input.MouseKeyEvent;
import com.neonsoup.engine.app.input.MouseMotionEvent;
import com.neonsoup.engine.app.input.MouseScrollEvent;

public abstract class ApplicationState implements MouseInputFocusHolder {

    protected Application app;

    protected final List<ApplicationLifecycleListener> appLifecycleListeners = new NoModCheckLinkedList<>();


    public ApplicationState() {
    }


    public void addApplicationLifecycleListener(ApplicationLifecycleListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException("listener must not be null");
        }

        if (!appLifecycleListeners.contains(listener)) {
            appLifecycleListeners.add(0, listener);
        }
    }


    public void removeApplicationLifecycleListener(ApplicationLifecycleListener listener) {
        if (listener == null) {
            return;
        }

        appLifecycleListeners.remove(listener);
    }


    public void setApplication(Application app) {
        if (app == null) {
            throw new IllegalArgumentException("Application must not be null");
        }

        this.app = app;
    }


    public Application getApplication() {
        return app;
    }


    public void onStateEnter() {
    }


    public void onStateLeave() {
    }


    public void onStateUpdate(float dt, long dtNanos) {
    }


    public void onStateRender(float dtSinceLastUpdate, long dtSinceLastUpdateNanos) {
    }


    /**
     * Returns true if application should close.
     * 
     * @return true if application should close.
     */
    public boolean onStateWindowCloseRequested() {
        return false;
    }


    public void onKeyboardEvent(KeyboardEvent event) {
    }


    public void acquireMouseInputFocus() {
        app.getInput().acquireMouseInputFocus(this);
    }


    public void releaseMouseInputFocus() {
        if (app.getInput().getCurrentMouseInputFocusHolder() == this) {
            app.getInput().requestMouseInputFocusRelease();
        } else {
            throw new IllegalStateException("This state currently has no mouse input focus");
        }
    }


    @Override
    public void onMouseInputFocusAcquired() {
    }


    @Override
    public void onMouseInputFocusLost() {
    }


    @Override
    public void onMouseMoved(MouseMotionEvent event) {
    }


    @Override
    public void onMouseEnteredWindow(MouseEnterEvent event) {
    }


    @Override
    public void onMouseLeftWindow(MouseEnterEvent event) {
    }


    @Override
    public void onMouseKeyEvent(MouseKeyEvent event) {
    }


    @Override
    public void onMouseScrollEvent(MouseScrollEvent event) {
    }


    public void onApplicationStart() {
        for (ApplicationLifecycleListener listener : appLifecycleListeners) {
            listener.onStart();
        }
    }


    public void onApplicationPauseRequested() {
        for (ApplicationLifecycleListener listener : appLifecycleListeners) {
            listener.onPauseRequested();
        }
    }


    public void onApplicationPause() {
        for (ApplicationLifecycleListener listener : appLifecycleListeners) {
            listener.onPause();
        }
    }


    public void onApplicationResume() {
        for (ApplicationLifecycleListener listener : appLifecycleListeners) {
            listener.onResume();
        }
    }


    public void onApplicationStop() {
        for (ApplicationLifecycleListener listener : appLifecycleListeners) {
            listener.onStop();
        }
    }

}
