package com.neonsoup.engine.app.state;

import com.neonsoup.engine.app.Application;

public interface ApplicationStateFactory {

    void setApplication(Application app);


    ApplicationState createState(Object key);

}
