package com.neonsoup.engine.app.state;

import com.neonsoup.engine.app.Application;

public abstract class AbstractApplicationStateFactory implements ApplicationStateFactory {

    protected Application app;


    public AbstractApplicationStateFactory() {
    }


    @Override
    public void setApplication(Application app) {
        this.app = app;
    }

}
