package com.neonsoup.engine.app;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;

import java.util.logging.Level;
import java.util.logging.LogRecord;

import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GLCapabilities;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.app.config.GraphicsConfig;
import com.neonsoup.engine.async.AsyncWorker;
import com.neonsoup.engine.graphics.DebugMesh2D;
import com.neonsoup.engine.graphics.Texture2D;
import com.neonsoup.engine.graphics.TextureAtlas;
import com.neonsoup.engine.graphics.gl.GLSharedContext;
import com.neonsoup.engine.graphics.gl.GLStateProperties;
import com.neonsoup.engine.graphics.material.Material;
import com.neonsoup.engine.graphics.text.BitmapFont;
import com.neonsoup.engine.graphics.text.Font;
import com.neonsoup.engine.graphics.text.TrueTypeFont;
import com.neonsoup.engine.gui.ApplicationGUI;
import com.neonsoup.engine.res.font.FontManager;
import com.neonsoup.engine.res.material.MaterialManager;
import com.neonsoup.engine.res.texture.Texture2DManager;
import com.neonsoup.engine.res.textureatlas.TextureAtlasManager;
import com.neonsoup.engine3d.graphics.model.Model;
import com.neonsoup.engine3d.res.model.ModelManager;

public abstract class ApplicationGraphics {

    public static final int GL_VERSION_MAJOR = 3;
    public static final int GL_VERSION_MINOR = 2;

    public static final int DEFAULT_TEXTURE_CACHE_SIZE = 26;
    public static final int DEFAULT_FONT_CACHE_SIZE = 8;
    public static final int DEFAULT_MATERIAL_CACHE_SIZE = 8;
    public static final int DEFAULT_MODEL_CACHE_SIZE = 8;
    public static final int DEFAULT_TEXTURE_ATLAS_CACHE_SIZE = DEFAULT_TEXTURE_CACHE_SIZE;


    protected class ResourceLoadingWorkerLifecycleHandler extends AbstractDisposable implements
                                                          AsyncWorker.LifecycleHandler {

        protected GLSharedContext sharedContext;


        protected ResourceLoadingWorkerLifecycleHandler(GLSharedContext sharedContext) {
            this.sharedContext = sharedContext;
        }


        @Override
        public void initWorkerThread() {
            if (sharedContext != null) {
                sharedContext.makeCurrent();
            }
        }


        @Override
        public void disposeWorkerThread() {
        }


        @Override
        public void dispose() {
            if (disposed) {
                return;
            }

            if (sharedContext != null) {
                sharedContext.dispose();
            }

            disposed = true;
        }

    }


    protected final Application app;

    protected final ApplicationGUI gui;

    protected AsyncWorker resourceLoadingWorker;

    protected Texture2DManager texture2DManager;
    protected FontManager fontManager;
    protected MaterialManager materialManager;
    protected ModelManager modelManager;
    protected TextureAtlasManager textureAtlasManager;

    protected GLStateProperties defaultGLStateProperties;


    public ApplicationGraphics(Application app, GraphicsConfig config) {
        this.gui = new ApplicationGUI(app);
        this.app = app;
    }


    public Application getApplication() {
        return app;
    }


    public ApplicationGUI getGUI() {
        return gui;
    }


    public AsyncWorker getResourceLoadingWorker() {
        return resourceLoadingWorker;
    }


    public Texture2DManager getTexture2DManager() {
        return texture2DManager;
    }


    public FontManager getFontManager() {
        return fontManager;
    }


    public MaterialManager getMaterialManager() {
        return materialManager;
    }


    public ModelManager getModelManager() {
        return modelManager;
    }


    public TextureAtlasManager getTextureAtlasManager() {
        return textureAtlasManager;
    }


    public GLStateProperties getDefaultGLStateProperties() {
        return defaultGLStateProperties;
    }


    public void initInMainThread() {
        if (app.hasWindow()) {
            app.getWindow().initInMainThread();
        }

        if (app.hasCanvas()) {
            app.getCanvas().initInMainThread();
        }

        {
            resourceLoadingWorker = new AsyncWorker(app,
                                                    new ResourceLoadingWorkerLifecycleHandler(createGLSharedContext()),
                                                    "ApplicationGraphics resource loading worker");

            texture2DManager = new Texture2DManager(app, DEFAULT_TEXTURE_CACHE_SIZE);
            app.registerResourceManager(Texture2D.class, texture2DManager);

            fontManager = new FontManager(app, DEFAULT_FONT_CACHE_SIZE);
            app.registerResourceManager(Font.class, fontManager);

            materialManager = new MaterialManager(app, DEFAULT_MATERIAL_CACHE_SIZE);
            app.registerResourceManager(Material.class, materialManager);

            modelManager = new ModelManager(app, DEFAULT_MODEL_CACHE_SIZE);
            app.registerResourceManager(Model.class, modelManager);

            textureAtlasManager = new TextureAtlasManager(app, DEFAULT_TEXTURE_ATLAS_CACHE_SIZE);
            app.registerResourceManager(TextureAtlas.class, textureAtlasManager);
        }

        BitmapFont.initStatic();
        TrueTypeFont.initStatic();

        DebugMesh2D.initStatic(app);

        gui.initInMainThread();
    }


    public void initMainContext() {
        GLCapabilities caps = GL.createCapabilities(true);
        com.neonsoup.engine.graphics.gl.GL.initDebugLoggerOutput();

        {
            StringBuilder extsString = new StringBuilder();

            extsString.append("List of loaded GL extensions in main thread:\n");

            if (caps.OpenGL30) {
                int numExt = glGetInteger(GL_NUM_EXTENSIONS);

                for (int i = 0; i < numExt; i++) {
                    extsString.append(glGetStringi(GL_EXTENSIONS, i));
                    extsString.append(' ');
                }
            } else {
                extsString.append(glGetString(GL_EXTENSIONS));
            }

            LogRecord record = new LogRecord(Level.INFO, extsString.toString());

            com.neonsoup.engine.graphics.gl.GL.log.log(record);
        }

        defaultGLStateProperties = new GLStateProperties();

        defaultGLStateProperties.setOverrideAll();

        defaultGLStateProperties.setFramebufferSRGB(true);
    }


    public void disposeMainContext() {
        com.neonsoup.engine.graphics.gl.GL.disposeDebugOutput();
    }


    public void disposeInMainThread() {
        {
            gui.disposeInMainThread();

            DebugMesh2D.disposeStatic();

            BitmapFont.disposeStatic();
            TrueTypeFont.disposeStatic();

            textureAtlasManager.dispose();
            modelManager.dispose();
            materialManager.dispose();
            fontManager.dispose();
            texture2DManager.dispose();

            resourceLoadingWorker.dispose();
        }

        if (app.hasCanvas()) {
            app.getCanvas().disposeInMainThread();
        }

        if (app.hasWindow()) {
            app.getWindow().disposeInMainThread();
        }

        disposeSharedContexts();
        disposeMainContext();
    }


    protected abstract void disposeSharedContexts();


    public abstract GLSharedContext createGLSharedContext();

}
