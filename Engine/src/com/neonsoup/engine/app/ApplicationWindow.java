package com.neonsoup.engine.app;

import com.neonsoup.engine.app.config.WindowConfig;
import com.neonsoup.engine.app.config.WindowConfigBuilder;

public abstract class ApplicationWindow {

    protected final Application app;

    protected WindowConfigBuilder config;


    public ApplicationWindow(Application app, WindowConfig config) {
        this.app = app;
        this.config = new WindowConfigBuilder(config);
    }


    public Application getApplication() {
        return app;
    }


    public WindowConfig getCurrentConfig() {
        return config;
    }


    public abstract void initInMainThread();


    public abstract void disposeInMainThread();

}
