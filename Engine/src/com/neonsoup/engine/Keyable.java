package com.neonsoup.engine;

public interface Keyable {

    Object getKey();

}
