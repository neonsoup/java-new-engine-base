package com.neonsoup.engine.res;

import com.neonsoup.engine.Disposable;

public interface ResourceKey<O extends Disposable> {

    @Override
    int hashCode();


    @Override
    boolean equals(Object obj);

}
