package com.neonsoup.engine.res;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.neonsoup.engine.app.Application;

public class CompositeResourceInfoLocator extends AbstractResourceInfoLocator {

    private final List<ResourceInfoLocator> locators = new ArrayList<>(6);


    public CompositeResourceInfoLocator(Application application) {
        super(application);
    }


    public void registerLocator(ResourceInfoLocator locator) {
        locators.add(locator);
    }


    public void unregisterLocator(ResourceInfoLocator locator) {
        locators.remove(locator);
    }


    @Override
    public ResourceInfo locateResourceInfo(String path) throws IOException {
        for (ResourceInfoLocator locator : locators) {
            ResourceInfo result = locator.locateResourceInfo(path);

            if (result != null) {
                return result;
            }
        }

        return null;
    }

}
