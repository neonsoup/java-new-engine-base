package com.neonsoup.engine.res.textureatlas;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.async.AsyncWorker;
import com.neonsoup.engine.graphics.TextureAtlas;
import com.neonsoup.engine.res.ReadyResource;
import com.neonsoup.engine.res.Resource;
import com.neonsoup.engine.res.ResourceKey;
import com.neonsoup.engine.res.ResourceLoadingTask;
import com.neonsoup.engine.res.ResourceManager;

public class TextureAtlasManager extends ResourceManager<TextureAtlas> {

    private static final Logger log = Logger.getLogger(TextureAtlasManager.class.getName());


    public TextureAtlasManager(Application app, int cacheSize) {
        super(app, cacheSize);
    }


    @Override
    protected AsyncWorker getLoadingWorker(Application app) {
        return app.getGraphics().getResourceLoadingWorker();
    }


    @Override
    protected Logger getLogger() {
        return log;
    }


    @Override
    protected Resource<TextureAtlas> createDependencyResource(ResourceKey<TextureAtlas> key) throws Exception {
        return createDependencyResource(key);
    }


    @Override
    protected Resource<TextureAtlas> createReadyResource(ResourceKey<TextureAtlas> key) throws Exception {
        TextureAtlasKey texAtlasKey = (TextureAtlasKey) key;

        TextureAtlas texAtlas = TextureAtlas.load(texAtlasKey.name.canonicalName, app);

        return new ReadyResource<TextureAtlas>(this, key, texAtlas);

    }


    @Override
    protected ResourceLoadingTask<TextureAtlas> createResourceLoadingTask(ResourceKey<TextureAtlas> key,
                                                                          Resource<TextureAtlas> res) {
        return new ResourceLoadingTask<TextureAtlas>(this, key, res) {

            @Override
            public void execute() {
                TextureAtlasKey texAtlasKey = (TextureAtlasKey) key;

                try {
                    TextureAtlas texAtlas = TextureAtlas.load(texAtlasKey.name.canonicalName, app);

                    this.resourceObject = texAtlas;

                    addLoadedResource(res);
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Error loading texture atlas by key \"" + key + "\"", e);

                    addLoadedResource(res);
                }
            }

        };
    }

}
