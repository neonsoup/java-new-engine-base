package com.neonsoup.engine.res.textureatlas;

import com.neonsoup.engine.graphics.TextureAtlas;
import com.neonsoup.engine.res.ResourceKey;
import com.neonsoup.engine.res.ResourceName;

public class TextureAtlasKey implements ResourceKey<TextureAtlas> {

    protected final ResourceName name;


    public TextureAtlasKey(String name) {
        this.name = new ResourceName(name);
    }


    @Override
    public int hashCode() {
        return name.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TextureAtlasKey)) {
            return false;
        }

        TextureAtlasKey other = (TextureAtlasKey) obj;

        return other.name.equals(this.name);
    }


    @Override
    public String toString() {
        return "TextureAtlasKey [" + name + "]";
    }

}
