package com.neonsoup.engine.res;

import java.io.IOException;

import com.neonsoup.engine.app.Application;

public interface ResourceInfoLocator {

    ResourceInfo locateResourceInfo(String path) throws IOException;


    Application getApplication();

}
