package com.neonsoup.engine.res;

import com.neonsoup.engine.Disposable;

public class AsyncResource<O extends Disposable> extends AbstractResource<O> {

    protected ResourceLoadingTask<O> loadingTask = null;


    public AsyncResource(ResourceManager<O> manager, ResourceKey<O> key) {
        super(manager, key);
    }


    @Override
    public RequestState getRequestState() {
        return loadingTask.getRequestState();
    }


    @Override
    public void waitForRequestExecution() {
        ensureUndisposed();

        loadingTask.waitForRequestExecution();
    }


    protected void setLoadingTask(ResourceLoadingTask<O> loadingTask) {
        this.loadingTask = loadingTask;
    }


    @Override
    public O getResourceObject() {
        ensureUndisposed();

        return loadingTask.getResourceObject();
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        loadingTask.waitForRequestExecution();

        O resourceObj = loadingTask.getResourceObject();

        if (resourceObj != null) {
            resourceObj.dispose();
        }

        manager.disposeLoadedResource(this);

        disposed = true;
    }

}
