package com.neonsoup.engine.res;

import com.neonsoup.engine.Disposable;

public class ReadyResource<O extends Disposable> extends AbstractResource<O> {

    protected final O resourceObject;


    public ReadyResource(ResourceManager<O> manager, ResourceKey<O> key, O resourceObject) {
        super(manager, key);

        this.resourceObject = resourceObject;
    }


    @Override
    public RequestState getRequestState() {
        return RequestState.EXECUTED;
    }


    @Override
    public void waitForRequestExecution() {
        ensureUndisposed();
    }


    @Override
    public O getResourceObject() {
        ensureUndisposed();

        return resourceObject;
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        resourceObject.dispose();
        manager.disposeLoadedResource(this);

        disposed = true;
    }

}
