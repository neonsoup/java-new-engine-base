package com.neonsoup.engine.res;

public class Resources {

    private Resources() {
    }


    public static String getResourceExtension(String name) {
        int lastSeparator = Math.max(name.lastIndexOf("\\"), name.lastIndexOf("/"));

        if (lastSeparator == -1) {
            lastSeparator = 0;
        }

        int firstDot = name.indexOf('.', lastSeparator);

        if (firstDot == -1) {
            return "";
        }

        return name.substring(firstDot + 1);
    }

}
