package com.neonsoup.engine.res.texture;

import com.neonsoup.engine.graphics.Texture2D;
import com.neonsoup.engine.graphics.gl.GLTexture;
import com.neonsoup.engine.res.ResourceKey;
import com.neonsoup.engine.res.ResourceName;

public final class Texture2DKey implements ResourceKey<Texture2D> {

    protected final ResourceName name;
    protected final GLTexture.Filter initialMinFilter, initialMagFilter;


    public Texture2DKey(String path) {
        this(path, null, null);
    }


    public Texture2DKey(String name, GLTexture.Filter initialMinFilter, GLTexture.Filter initialMagFilter) {
        this.name = new ResourceName(name);
        this.initialMinFilter = initialMinFilter;
        this.initialMagFilter = initialMagFilter;
    }


    @Override
    public int hashCode() {
        return name.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Texture2DKey)) {
            return false;
        }

        Texture2DKey other = (Texture2DKey) obj;

        return other.name.equals(this.name);
    }


    @Override
    public String toString() {
        return "Texture2DKey [" + name + "]";
    }

}
