package com.neonsoup.engine.res.texture;

import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.async.AsyncWorker;
import com.neonsoup.engine.graphics.Texture2D;
import com.neonsoup.engine.graphics.gl.GL;
import com.neonsoup.engine.graphics.gl.GLTextureData;
import com.neonsoup.engine.res.ReadyResource;
import com.neonsoup.engine.res.Resource;
import com.neonsoup.engine.res.ResourceInfoLocator;
import com.neonsoup.engine.res.ResourceKey;
import com.neonsoup.engine.res.ResourceLoadingTask;
import com.neonsoup.engine.res.ResourceManager;
import com.neonsoup.engine.res.Resources;

public class Texture2DManager extends ResourceManager<Texture2D> {

    private static final Logger log = Logger.getLogger(Texture2DManager.class.getName());


    public Texture2DManager(Application app, int cacheSize) {
        super(app, cacheSize);
    }


    @Override
    protected AsyncWorker getLoadingWorker(Application app) {
        return app.getGraphics().getResourceLoadingWorker();
    }


    @Override
    protected Logger getLogger() {
        return log;
    }


    @Override
    protected Resource<Texture2D> createDependencyResource(ResourceKey<Texture2D> key) throws Exception {
        Resource<Texture2D> res = createReadyResource(key);

        GL.syncGpuCommandsComplete();

        return res;
    }


    @Override
    protected Resource<Texture2D> createReadyResource(ResourceKey<Texture2D> key) throws Exception {
        Texture2DKey tex2DKey = (Texture2DKey) key;

        String extension = Resources.getResourceExtension(tex2DKey.name.canonicalName);

        ResourceInfoLocator streamLocator = app.getResourceInfoLocator();

        try (InputStream in = streamLocator.locateResourceInfo(tex2DKey.name.canonicalName).getResourceStream()) {
            GLTextureData data = GLTextureData.loadColorTextureData(in, extension);

            Texture2D tex = null;

            try {
                tex = new Texture2D(data, tex2DKey.initialMinFilter, tex2DKey.initialMagFilter);
            } finally {
                data.dispose();
            }

            return new ReadyResource<Texture2D>(this, key, tex);
        }
    }


    @Override
    protected ResourceLoadingTask<Texture2D> createResourceLoadingTask(ResourceKey<Texture2D> key,
                                                                       Resource<Texture2D> res) {

        return new ResourceLoadingTask<Texture2D>(this, key, res) {

            @Override
            public void execute() {
                Texture2DKey tex2DKey = (Texture2DKey) key;

                String extension = Resources.getResourceExtension(tex2DKey.name.canonicalName);

                ResourceInfoLocator streamLocator = manager.getApplication().getResourceInfoLocator();

                try (InputStream in = streamLocator.locateResourceInfo(tex2DKey.name.canonicalName)
                        .getResourceStream()) {

                    GLTextureData data = GLTextureData.loadColorTextureData(in, extension);

                    Texture2D tex = null;

                    try {
                        tex = new Texture2D(data, tex2DKey.initialMinFilter, tex2DKey.initialMagFilter);
                    } finally {
                        data.dispose();
                    }

                    try {
                        GL.syncGpuCommandsComplete();

                        this.resourceObject = tex;

                        addLoadedResource(res);
                    } catch (Exception e) {
                        tex.dispose();

                        throw new RuntimeException(e);
                    }
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Error loading texture by key \"" + key + "\"", e);

                    addLoadedResource(res);
                }
            }

        };

    }

}
