package com.neonsoup.engine.res;

import com.neonsoup.engine.Disposable;

public class ResourceHandle<O extends Disposable> {

    protected final Resource<O> res;


    public ResourceHandle(Resource<O> res) {
        this.res = res;

        synchronized (res.getManager()) {
            res.incrementReferenceCount();
        }
    }


    public ResourceHandle<O> copy() {
        return new ResourceHandle<>(res);
    }


    public Resource<O> getResource() {
        return res;
    }


    public O getResourceObject() {
        return res.getResourceObject();
    }


    public void release() {
        synchronized (res.getManager()) {
            if (res.decrementReferenceCount() <= 0) {
                res.disposeCached();
            }
        }
    }

}
