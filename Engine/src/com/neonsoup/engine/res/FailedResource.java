package com.neonsoup.engine.res;

import com.neonsoup.engine.Disposable;

public class FailedResource<O extends Disposable> extends AbstractResource<O> {

    public FailedResource(ResourceManager<O> manager, ResourceKey<O> key) {
        super(manager, key);
    }


    @Override
    public RequestState getRequestState() {
        return RequestState.FAILED;
    }


    @Override
    public void waitForRequestExecution() {
        ensureUndisposed();
    }


    @Override
    public O getResourceObject() {
        ensureUndisposed();

        return null;
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        manager.disposeLoadedResource(this);

        disposed = true;
    }

}
