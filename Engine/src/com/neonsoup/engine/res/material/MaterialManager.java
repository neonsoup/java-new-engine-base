package com.neonsoup.engine.res.material;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonValue;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.async.AsyncWorker;
import com.neonsoup.engine.graphics.RenderProperties.OverridePolicy;
import com.neonsoup.engine.graphics.gl.GL;
import com.neonsoup.engine.graphics.gl.GLShaderProgram;
import com.neonsoup.engine.graphics.gl.GLStateProperties;
import com.neonsoup.engine.graphics.gl.GLStateProperties.BlendFunc;
import com.neonsoup.engine.graphics.gl.GLStateProperties.CullFace;
import com.neonsoup.engine.graphics.material.Material;
import com.neonsoup.engine.graphics.material.MaterialFeatureKey;
import com.neonsoup.engine.res.ReadyResource;
import com.neonsoup.engine.res.Resource;
import com.neonsoup.engine.res.ResourceInfo;
import com.neonsoup.engine.res.ResourceInfoLocator;
import com.neonsoup.engine.res.ResourceKey;
import com.neonsoup.engine.res.ResourceLoadingTask;
import com.neonsoup.engine.res.ResourceManager;

public class MaterialManager extends ResourceManager<Material> {

    private static final Logger log = Logger.getLogger(MaterialManager.class.getName());

    private static final String DEFAULT_SHADER_DIR = "res\\material\\default\\";

    private static final Map<String, String> DEFAULT_MATERIAL_FILES = new HashMap<>();

    static {
        DEFAULT_MATERIAL_FILES.put("model_empty", "model_empty.mat");
    }


    public MaterialManager(Application app, int cacheSize) {
        super(app, cacheSize);
    }


    @Override
    protected AsyncWorker getLoadingWorker(Application app) {
        return app.getGraphics().getResourceLoadingWorker();
    }


    @Override
    protected Logger getLogger() {
        return log;
    }


    @Override
    protected Resource<Material> createDependencyResource(ResourceKey<Material> key) throws Exception {
        Resource<Material> res = createReadyResource(key);

        GL.syncGpuCommandsComplete();

        return res;
    }


    private Material loadMaterial(String path, Set<MaterialFeatureKey> requestedFeatures) throws Exception {
        ResourceInfoLocator streamLocator = app.getResourceInfoLocator();

        ResourceInfo textInfo = streamLocator.locateResourceInfo(path);

        try (JsonReader json = Json.createReader(textInfo.getResourceStream())) {
            JsonObject rootJson = json.readObject();
            JsonObject techniquesJson = rootJson.getJsonObject("techniques");

            GLStateProperties[] renderStateProperties = new GLStateProperties[32];
            GLShaderProgram[] shaders = new GLShaderProgram[32];
            try {
                int techBits = 0;

                for (Map.Entry<String, JsonValue> techEntry : techniquesJson.entrySet()) {
                    int techNum = Integer.parseInt(techEntry.getKey());

                    techBits |= 1 << techNum;

                    JsonObject techJson = (JsonObject) techEntry.getValue();

                    Map<String, String> shaderDefines = null;

                    {
                        Map<String, String> availableFeatures = new HashMap<>();

                        {
                            JsonObject featuresJson = techJson.getJsonObject("features");

                            if (featuresJson != null) {
                                for (Map.Entry<String, JsonValue> ent : featuresJson.entrySet()) {
                                    availableFeatures.put(ent.getKey().toUpperCase(),
                                                          ((JsonString) ent.getValue()).getString());
                                }
                            }
                        }

                        Set<String> requestedFeatureNames = new HashSet<>(requestedFeatures.size());

                        for (MaterialFeatureKey feature : requestedFeatures) {
                            requestedFeatureNames.add(feature.getName());
                        }

                        Set<String> validFeatureNames = new HashSet<>(availableFeatures.keySet());
                        validFeatureNames.retainAll(requestedFeatureNames);

                        shaderDefines = new HashMap<>(validFeatureNames.size());

                        for (String name : validFeatureNames) {
                            shaderDefines.put(availableFeatures.get(name).toUpperCase(), "1");
                        }
                    }

                    {
                        String vertexSource = app.loadTextUTF8(techJson.getString("vertex_shader"));
                        String fragmentSource = app.loadTextUTF8(techJson.getString("fragment_shader"));

                        shaders[techNum] = new GLShaderProgram(vertexSource,
                                                               null,
                                                               fragmentSource,
                                                               shaderDefines,
                                                               null,
                                                               null);
                    }

                    GLStateProperties techniqueStateProps = renderStateProperties[techNum] = new GLStateProperties();

                    {
                        String polygonMode = techJson.getString("polygon_mode", null);

                        if (polygonMode != null) {
                            techniqueStateProps.setPolygonModeOverride(OverridePolicy.OVERRIDE);
                            techniqueStateProps.setPolygonMode(GLStateProperties.PolygonMode.valueOf(polygonMode));
                        }
                    }

                    {
                        String polyOffsetStr = techJson.getString("polygon_offset", null);

                        if (polyOffsetStr != null) {
                            if (polyOffsetStr.equalsIgnoreCase("false")) {

                                techniqueStateProps.setPolygonOffsetOverride(OverridePolicy.OVERRIDE);

                                techniqueStateProps.setPolygonOffsetFill(false);
                                techniqueStateProps.setPolygonOffsetLine(false);
                                techniqueStateProps.setPolygonOffsetPoint(false);
                            } else {
                                throw new IOException("Unrecognised polygon offset value \"" + polyOffsetStr + "\"");
                            }
                        } else {
                            JsonObject polyOffsetJson = techJson.getJsonObject("polygon_offset");

                            if (polyOffsetJson != null) {

                                boolean fill = true;
                                boolean line = true;
                                boolean point = true;

                                {
                                    String offsetMode = polyOffsetJson.getString("fill", null);

                                    if (offsetMode != null) {
                                        fill = Boolean.valueOf(offsetMode);
                                    }

                                    offsetMode = polyOffsetJson.getString("line", null);

                                    if (offsetMode != null) {
                                        line = Boolean.valueOf(offsetMode);
                                    }

                                    offsetMode = polyOffsetJson.getString("point", null);

                                    if (offsetMode != null) {
                                        point = Boolean.valueOf(offsetMode);
                                    }
                                }

                                float factor = Float.parseFloat(polyOffsetJson.getString("factor"));
                                float units = Float.parseFloat(polyOffsetJson.getString("units"));

                                techniqueStateProps.setPolygonOffsetOverride(OverridePolicy.OVERRIDE);

                                techniqueStateProps.setPolygonOffsetFill(fill);
                                techniqueStateProps.setPolygonOffsetLine(line);
                                techniqueStateProps.setPolygonOffsetPoint(point);

                                techniqueStateProps.setPolygonOffsetFactor(factor);
                                techniqueStateProps.setPolygonOffsetUnits(units);
                            }
                        }
                    }

                    {
                        String depthTest = techJson.getString("depth_test", null);

                        if (depthTest != null) {
                            techniqueStateProps.setDepthTestOverride(OverridePolicy.OVERRIDE);
                            techniqueStateProps.setDepthTestEnabled(Boolean.valueOf(depthTest));
                        }
                    }

                    {
                        String depthWrite = techJson.getString("depth_write", null);

                        if (depthWrite != null) {
                            techniqueStateProps.setDepthWriteOverride(OverridePolicy.OVERRIDE);
                            techniqueStateProps.setDepthWriteEnabled(Boolean.valueOf(depthWrite));
                        }
                    }

                    {
                        String cullFace = techJson.getString("cull_face", null);

                        if (cullFace != null) {
                            techniqueStateProps.setCullFaceOverride(OverridePolicy.OVERRIDE);

                            if (cullFace.equalsIgnoreCase("false")) {

                                techniqueStateProps.setCullFaceEnabled(false);
                            } else {
                                techniqueStateProps.setCullFaceEnabled(true);
                                techniqueStateProps.setCullFace(CullFace.valueOf(cullFace));
                            }
                        }
                    }

                    {
                        String blendingStr = techJson.getString("blending", null);

                        if (blendingStr != null) {
                            if (blendingStr.equalsIgnoreCase("false")) {

                                techniqueStateProps.setBlendingOverride(OverridePolicy.OVERRIDE);
                                techniqueStateProps.setBlendingEnabled(false);
                            } else {
                                throw new IOException("Unrecognised blending value \"" + blendingStr + "\"");
                            }
                        } else {
                            JsonObject blendingJson = techJson.getJsonObject("blending");

                            if (blendingJson != null) {
                                techniqueStateProps.setBlendingOverride(OverridePolicy.OVERRIDE);
                                techniqueStateProps.setBlendingEnabled(true);

                                String func_src = blendingJson.getString("func_src", null);
                                String func_dst = blendingJson.getString("func_dst", null);

                                String func_src_alpha = blendingJson.getString("func_src_alpha", null);
                                String func_src_rgb = blendingJson.getString("func_src_rgb", null);

                                String func_dst_alpha = blendingJson.getString("func_src_alpha", null);
                                String func_dst_rgb = blendingJson.getString("func_src_rgb", null);

                                if (func_src != null) {
                                    BlendFunc src = BlendFunc.valueOf(func_src);

                                    techniqueStateProps.setBlendFuncSrcALPHA(src);
                                    techniqueStateProps.setBlendFuncSrcRGB(src);
                                } else {
                                    techniqueStateProps.setBlendFuncSrcALPHA(BlendFunc.valueOf(func_src_alpha));
                                    techniqueStateProps.setBlendFuncSrcRGB(BlendFunc.valueOf(func_src_rgb));
                                }

                                if (func_dst != null) {
                                    BlendFunc dst = BlendFunc.valueOf(func_dst);

                                    techniqueStateProps.setBlendFuncDstALPHA(dst);
                                    techniqueStateProps.setBlendFuncDstRGB(dst);
                                } else {
                                    techniqueStateProps.setBlendFuncDstALPHA(BlendFunc.valueOf(func_dst_alpha));
                                    techniqueStateProps.setBlendFuncDstRGB(BlendFunc.valueOf(func_dst_rgb));
                                }
                            }
                        }
                    }
                }

                return new Material(techBits, renderStateProperties, shaders, requestedFeatures);

            } catch (Exception ex) {
                for (GLShaderProgram shader : shaders) {
                    if (shader != null) {
                        shader.dispose();
                    }
                }

                throw new RuntimeException(ex);
            }
        }
    }


    @Override
    protected Resource<Material> createReadyResource(ResourceKey<Material> key) throws Exception {
        MaterialKey matKey = (MaterialKey) key;

        String path;

        if (matKey.defaultMaterialName != null) {
            String defaultName = DEFAULT_MATERIAL_FILES.get(matKey.defaultMaterialName);

            if (defaultName == null) {
                throw new IllegalArgumentException("No default material with name \""
                                                   + matKey.defaultMaterialName
                                                   + "\" was found");
            }

            path = DEFAULT_SHADER_DIR + defaultName;
        } else {
            path = matKey.name.canonicalName;
        }

        Material mat = loadMaterial(path, matKey.features);

        return new ReadyResource<Material>(this, key, mat);
    }


    @Override
    protected ResourceLoadingTask<Material> createResourceLoadingTask(ResourceKey<Material> key,
                                                                      Resource<Material> res) {

        return new ResourceLoadingTask<Material>(this, key, res) {

            @Override
            public void execute() {
                try {
                    MaterialKey matKey = (MaterialKey) key;

                    String path;

                    if (matKey.defaultMaterialName != null) {
                        String defaultName = DEFAULT_MATERIAL_FILES.get(matKey.defaultMaterialName);

                        if (defaultName == null) {
                            throw new IllegalArgumentException("No default material with name \""
                                                               + matKey.defaultMaterialName
                                                               + "\" was found");
                        }

                        path = DEFAULT_SHADER_DIR + defaultName;
                    } else {
                        path = matKey.name.canonicalName;
                    }

                    this.resourceObject = loadMaterial(path, matKey.features);

                    GL.syncGpuCommandsComplete();

                    addLoadedResource(res);
                } catch (Exception ex) {
                    log.log(Level.SEVERE, "Error loading material by key \"" + key + "\"", ex);

                    addLoadedResource(res);
                }
            }

        };

    }

}
