package com.neonsoup.engine.res.material;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.neonsoup.engine.graphics.material.Material;
import com.neonsoup.engine.graphics.material.MaterialFeatureKey;
import com.neonsoup.engine.res.ResourceKey;
import com.neonsoup.engine.res.ResourceName;

public class MaterialKey implements ResourceKey<Material> {

    protected final String defaultMaterialName;

    protected final ResourceName name;

    protected final Set<MaterialFeatureKey> features;


    protected MaterialKey(String name, String defaultMaterialName, Collection<MaterialFeatureKey> features) {
        this.name = name == null ? null : new ResourceName(name);
        this.defaultMaterialName = defaultMaterialName;
        this.features = new HashSet<>(features);
    }


    public static MaterialKey fromPath(String path, MaterialFeatureKey... features) {
        return new MaterialKey(path, null, Arrays.asList(features));
    }


    public static MaterialKey fromDefaultName(String defaultName, MaterialFeatureKey... features) {
        return new MaterialKey(null, defaultName, Arrays.asList(features));
    }


    public Set<MaterialFeatureKey> getFeatures() {
        return Collections.unmodifiableSet(features);
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        if (defaultMaterialName != null) {
            result = prime * result + defaultMaterialName.hashCode();
        } else {
            result = prime * result + name.hashCode();
        }

        result = prime * result + features.hashCode();

        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof MaterialKey)) {
            return false;
        }

        MaterialKey other = (MaterialKey) obj;

        if (this.defaultMaterialName != null) {
            return this.defaultMaterialName.equals(other.defaultMaterialName) && this.features.equals(other.features);
        } else {
            return this.name.equals(other.name) && this.features.equals(other.features);
        }
    }


    @Override
    public String toString() {
        return "MaterialKey ["
               + (defaultMaterialName != null ? defaultMaterialName : name)
               + "], Features: "
               + features.toString();
    }

}
