package com.neonsoup.engine.res;

public final class ResourceName {

    public final String canonicalName;

    private final String lowerCase;


    public ResourceName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Resource name must not be null");
        }

        this.canonicalName = name.replace('/', '\\').trim();

        this.lowerCase = this.canonicalName.toLowerCase();
    }


    @Override
    public int hashCode() {
        return lowerCase.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ResourceName)) {
            return false;
        }

        return ((ResourceName) obj).lowerCase.equals(this.lowerCase);
    }


    @Override
    public String toString() {
        return canonicalName;
    }

}
