package com.neonsoup.engine.res;

import com.neonsoup.engine.app.Application;

public abstract class AbstractResourceInfoLocator implements ResourceInfoLocator {

    protected final Application app;


    public AbstractResourceInfoLocator(Application application) {
        this.app = application;
    }


    @Override
    public Application getApplication() {
        return app;
    }

}
