package com.neonsoup.engine.res;

import com.neonsoup.engine.Disposable;
import com.neonsoup.engine.async.AsyncWorkerTask;

public abstract class ResourceLoadingTask<O extends Disposable> extends AsyncWorkerTask {

    protected final ResourceManager<O> manager;

    protected final ResourceKey<O> key;

    protected final Resource<O> res;

    protected O resourceObject = null;


    public ResourceLoadingTask(ResourceManager<O> manager, ResourceKey<O> key, Resource<O> res) {
        this.manager = manager;
        this.key = key;
        this.res = res;
    }


    public O getResourceObject() {
        synchronized (manager.asyncLoadingLock) {
            return resourceObject;
        }
    }


    protected void addLoadedResource(Resource<O> res) {
        manager.addLoadedResource(res);
    }

}
