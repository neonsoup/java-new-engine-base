package com.neonsoup.engine.res;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.neonsoup.engine.app.Application;

public class FileSystemResourceInfoLocator extends AbstractResourceInfoLocator {

    protected static final class FileSystemResourceInfo implements ResourceInfo {

        protected final String name;
        protected final File file;


        protected FileSystemResourceInfo(String name, File file) {
            this.name = name;
            this.file = file;
        }


        @Override
        public InputStream getResourceStream() throws IOException {
            return new FileInputStream(file);
        }


        @Override
        public long getResourceSize() throws IOException {
            return file.length();
        }


        @Override
        public String getResourcePath() {
            return name;
        }

    }


    private final File baseDirectory;


    public FileSystemResourceInfoLocator(Application application, File baseDirectory) {
        super(application);

        this.baseDirectory = baseDirectory;
    }


    public FileSystemResourceInfoLocator(Application application, String baseDirectory) {
        super(application);

        this.baseDirectory = new File(baseDirectory);
    }


    @Override
    public ResourceInfo locateResourceInfo(String path) throws IOException {
        File file = new File(baseDirectory, path);

        if (file.exists()) {
            return new FileSystemResourceInfo(path, file);
        }

        return null;
    }

}
