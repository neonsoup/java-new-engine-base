package com.neonsoup.engine.res;

import com.neonsoup.engine.CachedDisposable;
import com.neonsoup.engine.Disposable;
import com.neonsoup.engine.Keyable;
import com.neonsoup.engine.async.Request;

public interface Resource<O extends Disposable> extends CachedDisposable, Request, Keyable {

    ResourceKey<O> getKey();


    O getResourceObject();


    ResourceManager<O> getManager();


    int getReferenceCount();


    int incrementReferenceCount();


    int decrementReferenceCount();

}
