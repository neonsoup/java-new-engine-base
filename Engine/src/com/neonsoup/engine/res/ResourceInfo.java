package com.neonsoup.engine.res;

import java.io.IOException;
import java.io.InputStream;

public interface ResourceInfo {

    String getResourcePath();


    InputStream getResourceStream() throws IOException;


    long getResourceSize() throws IOException;

}
