package com.neonsoup.engine.res;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.Disposable;

public abstract class AbstractResource<O extends Disposable> extends AbstractDisposable implements Resource<O> {

    protected final ResourceKey<O> key;

    protected final ResourceManager<O> manager;

    protected int refCount = 0;


    public AbstractResource(ResourceManager<O> manager, ResourceKey<O> key) {
        this.key = key;
        this.manager = manager;
    }


    @Override
    public ResourceKey<O> getKey() {
        return key;
    }


    @Override
    public ResourceManager<O> getManager() {
        return manager;
    }


    @Override
    public int getReferenceCount() {
        ensureUndisposed();

        return refCount;
    }


    @Override
    public int incrementReferenceCount() {
        ensureUndisposed();

        this.manager.resourceCache.addUses(key, 1);

        return ++refCount;
    }


    @Override
    public int decrementReferenceCount() {
        ensureUndisposed();

        return --refCount;
    }


    @Override
    public void disposeCached() {
        manager.disposeResourceCached(this);
    }

}
