package com.neonsoup.engine.res;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.neonsoup.engine.app.Application;

public class ApplicationResourceInfoLocator extends CompositeResourceInfoLocator {

    private static final Logger log = Logger.getLogger(ApplicationResourceInfoLocator.class.getName());


    public ApplicationResourceInfoLocator(Application application) {
        super(application);

        File baseAppDir = application.getBaseDirectory();

        registerLocator(new FileSystemResourceInfoLocator(application, baseAppDir));
        registerLocator(new FileSystemResourceInfoLocator(application, new File(baseAppDir, "res")));

        try {
            File engineDirectory = new File(Application.class.getProtectionDomain().getCodeSource().getLocation()
                    .toURI());

            if (engineDirectory.getName().equalsIgnoreCase("bin")) {
                engineDirectory = engineDirectory.getParentFile();
            }

            if (!baseAppDir.equals(engineDirectory)) {
                registerLocator(new FileSystemResourceInfoLocator(application, engineDirectory));
                registerLocator(new FileSystemResourceInfoLocator(application, new File(engineDirectory, "res")));
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Exception trying to find alternative engine base directory", e);
        }

    }

}
