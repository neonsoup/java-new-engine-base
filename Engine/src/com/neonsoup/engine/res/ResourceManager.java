package com.neonsoup.engine.res;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.Disposable;
import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.async.AsyncWorker;
import com.neonsoup.engine.util.Cache;
import com.neonsoup.engine.util.UseCountCache;

public abstract class ResourceManager<O extends Disposable> extends AbstractDisposable {

    protected final Application app;

    protected final AsyncWorker worker;

    protected final Cache<Resource<O>> resourceCache;

    protected final Map<ResourceKey<O>, Resource<O>> loadedResources = new HashMap<>();

    protected final Map<ResourceKey<O>, Resource<O>> pendingResources = new HashMap<>();

    protected final Object asyncLoadingLock = new Object();


    public ResourceManager(Application app, int cacheSize) {
        this.app = app;

        this.worker = getLoadingWorker(app);

        this.resourceCache = new UseCountCache<>(cacheSize);
    }


    public Application getApplication() {
        return app;
    }


    protected abstract AsyncWorker getLoadingWorker(Application app);


    protected void addLoadedResource(Resource<O> res) {
        ensureUndisposed();

        ResourceKey<O> key = res.getKey();

        synchronized (asyncLoadingLock) {
            pendingResources.remove(key);
            loadedResources.put(key, res);
        }
    }


    protected void disposeLoadedResource(Resource<O> res) {
        ensureUndisposed();

        ResourceKey<O> key = res.getKey();

        synchronized (asyncLoadingLock) {
            loadedResources.remove(key);
        }
    }


    protected void disposeResourceCached(Resource<O> res) {
        ensureUndisposed();

        ResourceKey<O> key = res.getKey();

        synchronized (asyncLoadingLock) {
            loadedResources.remove(key);
            pendingResources.remove(key);

            resourceCache.cacheObject(res);
        }
    }


    public synchronized ResourceHandle<O> loadResourceImmediate(ResourceKey<O> key) {
        return loadResourceImmediate(key, false);
    }


    public synchronized ResourceHandle<O> loadResourceDependency(ResourceKey<O> key) {
        return loadResourceImmediate(key, true);
    }


    private ResourceHandle<O> loadResourceImmediate(ResourceKey<O> key, boolean asDependency) {
        ensureUndisposed();

        synchronized (asyncLoadingLock) {
            resourceCache.addUses(key, 1);

            Resource<O> res = resourceCache.popEntry(key);

            if (res != null) {
                loadedResources.put(key, res);

                return new ResourceHandle<>(res);
            }

            res = loadedResources.get(key);

            if (res != null) {
                return new ResourceHandle<>(res);
            }

            res = pendingResources.get(key);

            if (res != null) {
                res.waitForRequestExecution();

                return new ResourceHandle<>(res);
            }
        }

        try {
            Resource<O> res;

            if (asDependency) {
                res = createDependencyResource(key);
            } else {
                res = createReadyResource(key);
            }

            try {
                synchronized (asyncLoadingLock) {
                    loadedResources.put(key, res);
                }

                return new ResourceHandle<>(res);
            } catch (Exception e) {
                res.getResourceObject().dispose();

                throw new RuntimeException(e);
            }
        } catch (Exception e) {
            getLogger().log(Level.SEVERE, "Error loading resource \"" + key + "\"", e);

            Resource<O> res = new FailedResource<O>(this, key);

            synchronized (asyncLoadingLock) {
                loadedResources.put(key, res);
            }

            return new ResourceHandle<>(res);
        }
    }


    protected abstract Logger getLogger();


    protected abstract Resource<O> createReadyResource(ResourceKey<O> key) throws Exception;


    protected abstract Resource<O> createDependencyResource(ResourceKey<O> key) throws Exception;


    public synchronized ResourceHandle<O> loadResourceAsync(ResourceKey<O> key) {

        ensureUndisposed();

        synchronized (asyncLoadingLock) {
            resourceCache.addUses(key, 1);

            Resource<O> res = resourceCache.popEntry(key);

            if (res != null) {
                loadedResources.put(key, res);

                return new ResourceHandle<>(res);
            }

            res = loadedResources.get(key);

            if (res != null) {
                return new ResourceHandle<>(res);
            }

            res = pendingResources.get(key);

            if (res != null) {
                return new ResourceHandle<>(res);
            }
        }

        {
            AsyncResource<O> res = new AsyncResource<O>(this, key);
            ResourceLoadingTask<O> task = createResourceLoadingTask(key, res);

            res.setLoadingTask(task);

            synchronized (asyncLoadingLock) {
                pendingResources.put(key, res);
            }

            worker.enqueueTask(task);

            return new ResourceHandle<>(res);
        }
    }


    protected abstract ResourceLoadingTask<O> createResourceLoadingTask(ResourceKey<O> key, Resource<O> res);


    @Override
    public synchronized void dispose() {
        if (disposed) {
            return;
        }

        {
            Collection<Resource<O>> pendingResourcesCopy;

            synchronized (asyncLoadingLock) {
                pendingResourcesCopy = new ArrayList<>(pendingResources.values());
            }

            for (Resource<O> request : pendingResourcesCopy) {
                request.waitForRequestExecution();
            }

            for (Resource<O> res : pendingResourcesCopy) {
                res.dispose();
            }
        }

        for (Resource<O> res : new ArrayList<>(loadedResources.values())) {
            res.dispose();
        }

        resourceCache.dispose();

        disposed = true;
    }

}
