package com.neonsoup.engine.res.font;

import com.neonsoup.engine.graphics.text.Font;
import com.neonsoup.engine.res.ResourceKey;
import com.neonsoup.engine.res.ResourceName;

public class FontKey implements ResourceKey<Font> {

    public enum Type {
        TRUETYPE, BITMAP;
    }


    protected final ResourceName name;

    protected final Type type;

    protected final int size;

    protected final int oversample_h, oversample_v;


    public FontKey(String name, int size, int oversample_h, int oversamle_v) {
        this.name = new ResourceName(name);

        if (name.toLowerCase().endsWith(".ttf")) {
            type = Type.TRUETYPE;
        } else {
            type = Type.BITMAP;
        }

        this.size = size;
        this.oversample_h = oversample_h;
        this.oversample_v = oversamle_v;
    }


    public Type getType() {
        return type;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        if (type == Type.TRUETYPE) {
            result = prime * result + oversample_v;
            result = prime * result + oversample_h;
        }
        result = prime * result + size;
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FontKey other = (FontKey) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (type != other.type)
            return false;

        if (type == Type.TRUETYPE) {
            if (oversample_v != other.oversample_v)
                return false;
            if (oversample_h != other.oversample_h)
                return false;
        }

        if (size != other.size)
            return false;
        return true;
    }


    @Override
    public String toString() {
        return "FontKey [Name: \""
               + name
               + "\"; type: "
               + type
               + "; size: "
               + size
               + "; oversample_h: "
               + oversample_h
               + "; oversample_v: "
               + oversample_v
               + "]";
    }

}
