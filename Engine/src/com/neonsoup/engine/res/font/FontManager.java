package com.neonsoup.engine.res.font;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.async.AsyncWorker;
import com.neonsoup.engine.graphics.gl.GL;
import com.neonsoup.engine.graphics.text.BitmapFont;
import com.neonsoup.engine.graphics.text.Font;
import com.neonsoup.engine.graphics.text.TrueTypeFont;
import com.neonsoup.engine.res.ReadyResource;
import com.neonsoup.engine.res.Resource;
import com.neonsoup.engine.res.ResourceInfo;
import com.neonsoup.engine.res.ResourceInfoLocator;
import com.neonsoup.engine.res.ResourceKey;
import com.neonsoup.engine.res.ResourceLoadingTask;
import com.neonsoup.engine.res.ResourceManager;

public class FontManager extends ResourceManager<Font> {

    private static final Logger log = Logger.getLogger(FontManager.class.getName());


    public FontManager(Application app, int cacheSize) {
        super(app, cacheSize);
    }


    @Override
    protected AsyncWorker getLoadingWorker(Application app) {
        return app.getGraphics().getResourceLoadingWorker();
    }


    @Override
    protected Logger getLogger() {
        return log;
    }


    @Override
    protected Resource<Font> createDependencyResource(ResourceKey<Font> key) throws Exception {
        Resource<Font> res = createReadyResource(key);

        GL.syncGpuCommandsComplete();

        return res;
    }


    @Override
    protected Resource<Font> createReadyResource(ResourceKey<Font> key) throws Exception {
        FontKey fontKey = (FontKey) key;

        ResourceInfoLocator streamLocator = app.getResourceInfoLocator();

        ResourceInfo resInfo = streamLocator.locateResourceInfo(fontKey.name.canonicalName);

        Font font;
        if (fontKey.getType() == FontKey.Type.BITMAP) {
            font = BitmapFont.loadFont(app, resInfo, fontKey.size);
        } else {
            font = TrueTypeFont.loadFont(resInfo, fontKey.size, fontKey.oversample_h, fontKey.oversample_v);
        }

        return new ReadyResource<Font>(this, key, font);
    }


    @Override
    protected ResourceLoadingTask<Font> createResourceLoadingTask(ResourceKey<Font> key, Resource<Font> res) {
        return new ResourceLoadingTask<Font>(this, key, res) {

            @Override
            public void execute() {
                ResourceInfoLocator streamLocator = manager.getApplication().getResourceInfoLocator();

                try {
                    FontKey fontKey = (FontKey) key;

                    ResourceInfo resInfo = streamLocator.locateResourceInfo(fontKey.name.canonicalName);

                    Font font;
                    if (fontKey.getType() == FontKey.Type.BITMAP) {
                        font = BitmapFont.loadFont(app, resInfo, fontKey.size);
                    } else {
                        font = TrueTypeFont.loadFont(resInfo, fontKey.size, fontKey.oversample_h, fontKey.oversample_v);
                    }

                    try {
                        GL.syncGpuCommandsComplete();

                        this.resourceObject = font;

                        addLoadedResource(res);
                    } catch (Exception e) {
                        font.dispose();

                        throw new RuntimeException(e);
                    }
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Error loading font by key \"" + key + "\"", e);

                    addLoadedResource(res);
                }
            }

        };

    }

}
