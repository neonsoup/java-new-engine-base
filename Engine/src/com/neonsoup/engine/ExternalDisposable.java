package com.neonsoup.engine;

public interface ExternalDisposable extends Disposable {

    Disposable getExternalDisposer();

}
