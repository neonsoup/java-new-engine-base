package com.neonsoup.engine.async;

public interface Request {

    static enum RequestState {
        PENDING, EXECUTED, FAILED;
    }


    RequestState getRequestState();


    void waitForRequestExecution();

}
