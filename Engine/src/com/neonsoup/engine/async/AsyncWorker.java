package com.neonsoup.engine.async;

import java.lang.Thread.State;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.Disposable;
import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.async.Request.RequestState;

public class AsyncWorker extends AbstractDisposable {

    public static interface LifecycleHandler extends Disposable {

        void initWorkerThread();


        void disposeWorkerThread();


        void dispose();

    }


    private static final Logger log = Logger.getLogger(AsyncWorker.class.getName());

    protected final LifecycleHandler handler;

    protected final Application app;

    protected final String name;

    protected volatile boolean stopThread = false;

    protected final Thread thread;

    protected final Queue<AsyncWorkerTask> unprocessedTasks = new ConcurrentLinkedQueue<>();
    protected final Object unprocessedTasksLock = new Object();

    protected final Object idleLock = new Object();
    protected boolean idle = true;

    protected boolean stopped = false;


    public AsyncWorker(Application application, LifecycleHandler handler, String name) {
        this.app = application;

        this.handler = handler;

        this.name = name;

        thread = new Thread("Async worker thread") {

            @Override
            public void run() {
                try {
                    if (handler != null) {
                        handler.initWorkerThread();
                    }

                    while (!stopThread) {

                        while (true) {
                            AsyncWorkerTask task = unprocessedTasks.poll();

                            if (task == null) {
                                break;
                            }

                            synchronized (task.processLock) {
                                if (task.isDisposed()) {
                                    // dispose method has been called before
                                    continue;
                                }

                                try {
                                    task.execute();

                                    if (task.yield) {
                                        task.yield = false;

                                        unprocessedTasks.add(task);

                                        continue;
                                    } else {
                                        task.setState(RequestState.EXECUTED);
                                    }
                                } catch (Exception e) {
                                    log.log(Level.SEVERE,
                                            "Error processing async task \""
                                                          + task.toString()
                                                          + "\" in worker \""
                                                          + name
                                                          + "\"",
                                            e);

                                    task.setState(RequestState.FAILED);
                                }

                                task.processLock.notifyAll();
                            }
                        }

                        if (stopThread) {
                            break;
                        }

                        synchronized (idleLock) {
                            while (idle) {
                                try {
                                    idleLock.wait();
                                } catch (InterruptedException e) {
                                }
                            }

                            idle = true;
                        }
                    }

                    synchronized (unprocessedTasksLock) {
                        if (handler != null) {
                            handler.disposeWorkerThread();
                        }

                        stopped = true;
                    }

                } finally {
                    synchronized (unprocessedTasksLock) {

                        for (AsyncWorkerTask task : unprocessedTasks) {

                            synchronized (task.processLock) {
                                log.log(Level.SEVERE,
                                        "Error processing async task \""
                                                      + task.toString()
                                                      + "\" in worker \""
                                                      + name
                                                      + "\", the worker was disposed");

                                task.setState(RequestState.FAILED);

                                task.processLock.notifyAll();
                            }
                        }
                    }
                }
            }

        };

        thread.setDaemon(true);
    }


    public Application getApplication() {
        return app;
    }


    public void wakeUp() {
        ensureUndisposed();

        if (thread.getState() == State.NEW) {
            thread.start();
        }

        synchronized (idleLock) {
            idle = false;
            idleLock.notify();
        }
    }


    public void enqueueTask(AsyncWorkerTask task) {
        ensureUndisposed();

        if (task.getRequestState() != RequestState.PENDING) {
            throw new IllegalArgumentException();
        }

        synchronized (unprocessedTasksLock) {
            if (stopped) {
                task.setState(RequestState.FAILED);

                log.log(Level.SEVERE,
                        "Error processing async task \""
                                      + task.toString()
                                      + "\" in worker \""
                                      + name
                                      + "\", the worker was disposed");
            } else {
                unprocessedTasks.add(task);
                wakeUp();
            }
        }
    }


    @Override
    public boolean isDisposed() {
        if (handler != null) {
            return thread.getState() == Thread.State.TERMINATED && handler.isDisposed();
        }

        return thread.getState() == Thread.State.TERMINATED;
    }


    @Override
    public void dispose() {
        if (isDisposed()) {
            return;
        }

        if (thread.getState() != State.NEW && !stopThread) {
            stopThread = true;
            wakeUp();
        }

        try {
            thread.join();
        } catch (InterruptedException e) {
        }

        if (handler != null) {
            handler.dispose();
        }
    }


    @Override
    public String toString() {
        return "AsyncWorker \"" + name + "\"";
    }

}
