package com.neonsoup.engine.async;

import com.neonsoup.engine.AbstractDisposable;

public abstract class AsyncWorkerTask extends AbstractDisposable implements Request {

    protected final Object processLock = new Object();

    protected Object result = null;

    boolean yield = false;

    protected RequestState state = RequestState.PENDING;


    public AsyncWorkerTask() {
    }


    public abstract void execute();


    protected void yield() {
        ensureUndisposed();

        this.yield = true;
    }


    public Object getProcessLock() {
        ensureUndisposed();

        return processLock;
    }


    public Object getResult() {
        synchronized (processLock) {
            return result;
        }
    }


    @Override
    public RequestState getRequestState() {
        synchronized (processLock) {
            return state;
        }
    }


    protected void setState(RequestState state) {
        this.state = state;
    }


    @Override
    public void waitForRequestExecution() {
        synchronized (processLock) {
            while (this.state == RequestState.PENDING) {
                try {
                    processLock.wait();
                } catch (InterruptedException e) {
                }
            }
        }
    }


    @Override
    public boolean isDisposed() {
        synchronized (processLock) {
            return disposed;
        }
    }


    @Override
    public void dispose() {
        synchronized (processLock) {
            if (disposed) {
                return;
            }

            disposed = true;
            state = RequestState.FAILED;
        }
    }

}
