package com.neonsoup.engine;

public abstract class AbstractDisposable implements Disposable {

    protected boolean disposed = false;


    public AbstractDisposable() {
    }


    public boolean isDisposed() {
        return disposed;
    }


    public void dispose() {
        if (disposed) {
            return;
        }

        disposed = true;
    }


    protected void ensureUndisposed() {
        if (isDisposed()) {
            throw new IllegalStateException("This object is disposed");
        }
    }

}
