package com.neonsoup.engine;

public interface AsyncDisposable extends Disposable {

    void disposeAsync();

}
