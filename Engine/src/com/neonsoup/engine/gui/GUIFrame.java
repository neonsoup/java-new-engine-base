package com.neonsoup.engine.gui;

public class GUIFrame extends GUIContainer {

    public GUIFrame() {
    }


    @Override
    public void setView(GUIElementView view) {
        if (view != null && (view.topSceneNode == null || view.bottomSceneNode == null)) {
            throw new IllegalArgumentException("view's scene nodes are not properly initialized");
        }

        if (this.view != null && parent == null && guiLayer != null) {
            guiLayer.sceneNode.detachChild(this.view.topSceneNode);
        }

        super.setView(view);

        if (view != null && parent == null && guiLayer != null) {
            guiLayer.sceneNode.attachChild(this.view.topSceneNode);
        }
    }


    @Override
    public void addElement(GUIElement element) {
        super.addElement(element);

        element.setGUIFrame(this);
    }


    @Override
    public void removeElement(GUIElement element) {
        super.removeElement(element);

        element.setGUIFrame(null);
    }


    @Override
    public void pack() {
        super.pack();

        int w = 0;
        int h = 0;

        for (GUIElement child : children) {
            w = Math.max(w, child.localLocation.x + child.width);
            h = Math.max(h, child.localLocation.y + child.height);
        }

        w += view.getHorizontalMargin() * 2;
        h += view.getVerticalMargin() * 2;

        view.setSize(w, h);
    }

}
