package com.neonsoup.engine.gui;

import com.neonsoup.engine.app.input.MouseEnterEvent;
import com.neonsoup.engine.app.input.MouseKeyEvent;
import com.neonsoup.engine.app.input.MouseMotionEvent;
import com.neonsoup.engine.app.input.MouseScrollEvent;

public interface GUIMouseInputFocusHolder {

    void onGlobalFocusLost(FocusEventSource sourceType, Object source);


    void onMouseMoved(MouseMotionEvent event);


    void onMouseLeftWindow(MouseEnterEvent event);


    void onMouseEnteredWindow(MouseEnterEvent event);


    void onMouseKeyEvent(MouseKeyEvent event);


    void onMouseScrollEvent(MouseScrollEvent event);

}
