package com.neonsoup.engine.gui;

public abstract class GUIFrameView extends GUIElementView {

    protected GUIFrame guiFrame;


    public GUIFrameView() {
    }


    @Override
    protected void setGUIElement(GUIElement guiElement) {
        super.setGUIElement(guiElement);

        this.guiFrame = (GUIFrame) guiElement;
    }

}
