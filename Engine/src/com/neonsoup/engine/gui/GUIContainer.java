package com.neonsoup.engine.gui;

import java.util.ArrayList;
import java.util.List;

import com.neonsoup.engine.app.input.ApplicationInput;
import com.neonsoup.engine.app.input.InputType;
import com.neonsoup.engine.app.input.MouseEnterEvent;
import com.neonsoup.engine.app.input.MouseKeyEvent;
import com.neonsoup.engine.app.input.MouseMotionEvent;
import com.neonsoup.engine.app.input.MouseScrollEvent;

public abstract class GUIContainer extends GUIElement {

    protected final List<GUIElement> children = new ArrayList<>(8);

    protected GUIElement hoveredChild = null;


    public GUIContainer() {
    }


    @Override
    public void setView(GUIElementView view) {
        if (view != null && (view.topSceneNode == null || view.bottomSceneNode == null)) {
            throw new IllegalArgumentException("view's scene nodes are not properly initialized");
        }

        if (this.view != null) {
            for (GUIElement child : children) {
                if (child.view != null) {
                    this.view.bottomSceneNode.detachChild(child.view.topSceneNode);
                }
            }
        }

        super.setView(view);

        if (view != null) {
            for (GUIElement child : children) {
                if (child.view != null) {
                    this.view.bottomSceneNode.attachChild(child.view.topSceneNode);
                }
            }
        }
    }


    @Override
    public void updateView() {
        super.updateView();

        if (isVisible) {
            for (GUIElement child : children) {
                child.updateView();
            }
        }
    }


    @Override
    protected void setGUILayer(GUILayer guiLayer) {
        super.setGUILayer(guiLayer);

        for (GUIElement child : children) {
            child.setGUILayer(guiLayer);
        }
    }


    @Override
    protected void setGUIFrame(GUIFrame frame) {
        super.setGUIFrame(frame);

        for (GUIElement child : children) {
            child.setGUIFrame(frame);
        }
    }


    public void addElement(GUIElement element) {
        if (element.parent != null || element.guiLayer != null) {
            throw new IllegalArgumentException("element already has a parent");
        }

        children.add(element);

        element.setParent(this);

        if (guiLayer != null) {
            element.setGUILayer(guiLayer);
        }
        if (guiFrame != null) {
            element.setGUIFrame(guiFrame);
        }

        if (guiLayer.gui.hasInputFocus() && guiLayer.isActiveLayer()) {
            ApplicationInput in = guiLayer.gui.app.getInput();

            int x = (int) in.getMouseCursorX();
            int y = (int) in.getMouseCursorY();

            GUIElement prevHoveredChukd = this.hoveredChild;
            GUIElement newHoveredChild = getChildAtPoint(x, y);

            if (newHoveredChild != this.hoveredChild) {
                this.hoveredChild = newHoveredChild;

                if (prevHoveredChukd != null) {
                    prevHoveredChukd.onMouseHoveredOut(x, y);
                }

                if (newHoveredChild != null) {
                    newHoveredChild.onMouseHoveredIn(x, y);
                }
            }
        }

        if (this.view != null && element.view != null) {
            this.view.bottomSceneNode.attachChild(element.view.topSceneNode);
        }
    }


    public void removeElement(GUIElement element) {
        if (element.parent != this) {
            return;
        }

        children.remove(element);

        if (element == hoveredChild) {
            hoveredChild = null;
        }

        element.setParent(null);

        if (guiLayer != null) {
            element.setGUILayer(null);
        }
        if (guiFrame != null) {
            element.setGUIFrame(null);
        }

        if (this.view != null && element.view != null) {
            this.view.bottomSceneNode.detachChild(element.view.topSceneNode);
        }
    }


    @Override
    public void pack() {
        super.pack();

        for (GUIElement child : children) {
            child.pack();
        }
    }


    protected GUIElement getChildAtPoint(int x, int y) {
        for (GUIElement child : children) {
            int cx = child.getGlobalLocationX();
            int cy = child.getGlobalLocationY();

            if (cx <= x && cy <= y && cx + child.getWidth() > x && cy + child.getHeight() > y) {
                return child;
            }
        }

        return null;
    }


    @Override
    public void onMouseHoveredIn(int cursorX, int cursorY) {
        super.onMouseHoveredIn(cursorX, cursorY);

        GUIElement newHoveredChild = getChildAtPoint(cursorX, cursorY);

        if (newHoveredChild != null) {
            this.hoveredChild = newHoveredChild;

            newHoveredChild.onMouseHoveredIn(cursorX, cursorY);
        }
    }


    @Override
    public void onMouseMoved(MouseMotionEvent event) {
        super.onMouseMoved(event);

        int x = (int) event.cursorX;
        int y = (int) event.cursorY;

        GUIElement newHoveredChild = getChildAtPoint(x, y);

        if (newHoveredChild != this.hoveredChild) {
            GUIElement oldHoveredChild = this.hoveredChild;

            this.hoveredChild = newHoveredChild;

            if (oldHoveredChild != null) {
                int px = (int) event.prevCursorX;
                int py = (int) event.prevCursorY;

                oldHoveredChild.onMouseMoved(event);
                oldHoveredChild.onMouseHoveredOut(px, py);
            }

            if (newHoveredChild != null) {
                newHoveredChild.onMouseHoveredIn(x, y);
                newHoveredChild.onMouseMoved(event);
            }
        }

        if (this.hoveredChild != null) {
            this.hoveredChild.onMouseMoved(event);
        }
    }


    public void onMouseHoveredOut(int lastCursorX, int lastCursorY) {
        super.onMouseHoveredOut(lastCursorX, lastCursorY);

        if (this.hoveredChild != null) {
            GUIElement oldHoveredChild = this.hoveredChild;

            this.hoveredChild = null;

            oldHoveredChild.onMouseHoveredOut(lastCursorX, lastCursorY);
        }
    }


    @Override
    public void onMouseLeftWindow(MouseEnterEvent event) {
        super.onMouseLeftWindow(event);

        if (this.hoveredChild != null) {
            GUIElement oldHoveredChild = this.hoveredChild;

            this.hoveredChild = null;

            oldHoveredChild.onMouseLeftWindow(event);
        }
    }


    @Override
    public void onMouseEnteredWindow(MouseEnterEvent event) {
        super.onMouseEnteredWindow(event);

        for (GUIElement child : children) {
            child.onMouseEnteredWindow(event);
        }
    }


    @Override
    public void onGlobalFocusLost(int inputTypes, FocusEventSource sourceType, Object source) {
        super.onGlobalFocusLost(inputTypes, sourceType, source);

        if ((inputTypes & InputType.INPUT_TYPE_BIT_MOUSE) != 0) {
            hoveredChild = null;
        }

        for (GUIElement child : children) {
            child.onGlobalFocusLost(inputTypes, sourceType, source);
        }
    }


    @Override
    public void onGlobalFocusAcquired(int inputTypes, FocusEventSource sourceType, Object source) {
        super.onGlobalFocusAcquired(inputTypes, sourceType, source);

        for (GUIElement child : children) {
            child.onGlobalFocusAcquired(inputTypes, sourceType, source);
        }
    }


    @Override
    public void onMouseKeyEvent(MouseKeyEvent event) {
        super.onMouseKeyEvent(event);

        if (hoveredChild != null) {
            hoveredChild.onMouseKeyEvent(event);
        }
    }


    @Override
    public void onMouseScrollEvent(MouseScrollEvent event) {
        super.onMouseScrollEvent(event);

        if (hoveredChild != null) {
            hoveredChild.onMouseScrollEvent(event);
        }
    }

}
