package com.neonsoup.engine.gui;

import com.neonsoup.engine.scene.SceneNode;

public class GUIEmptyElementView extends GUIElementView {

    public GUIEmptyElementView() {
        SceneNode sceneNode = new SceneNode(ApplicationGUI.getSceneConfig());

        setTopSceneNode(sceneNode);
        setBottomSceneNode(sceneNode);
    }


    @Override
    protected void update() {
    }

}
