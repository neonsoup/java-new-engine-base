package com.neonsoup.engine.gui;

public class GUILinearContainer extends GUIContainer {

    public static enum Orientation {
        VERTICAL, HORIZONTAL;
    }


    protected final Orientation orientation;
    protected int gap = 4;


    public GUILinearContainer(Orientation orientation) {
        this.orientation = orientation;
    }


    public Orientation getOrientation() {
        return orientation;
    }


    public int getGap() {
        return gap;
    }


    public void setGap(int gap) {
        this.gap = gap;
    }


    @Override
    public void pack() {
        super.pack();

        int minPerpendicularSize = 0;
        int totalLength = 0;

        for (int i = 0; i < children.size(); i++) {
            GUIElement child = children.get(i);

            if (orientation == Orientation.VERTICAL) {
                minPerpendicularSize = Math.max(minPerpendicularSize, child.width);
                totalLength += child.height;
            } else {
                minPerpendicularSize = Math.max(minPerpendicularSize, child.height);
                totalLength += child.width;
            }

            if (i < children.size() - 1) {
                totalLength += gap;
            }
        }

        if (orientation == Orientation.VERTICAL) {
            view.setSize(minPerpendicularSize + view.topMargin * 2, totalLength + view.leftMargin * 2);
        } else {
            view.setSize(totalLength + view.topMargin * 2, minPerpendicularSize + view.leftMargin * 2);
        }

        int takenSize = 0;

        for (int i = 0; i < children.size(); i++) {
            GUIElement child = children.get(i);

            if (orientation == Orientation.VERTICAL) {
                child.view.setSize(minPerpendicularSize, child.height);
                child.view.setLocalLocation(0, takenSize);

                takenSize += child.height + gap;
            } else {
                child.view.setSize(child.width, minPerpendicularSize);
                child.view.setLocalLocation(takenSize, 0);

                takenSize += child.width + gap;
            }
        }
    }

}
