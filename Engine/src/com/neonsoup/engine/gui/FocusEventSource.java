package com.neonsoup.engine.gui;

public enum FocusEventSource {
    WINDOW, GUI, GUI_LAYER, FOCUS_HOLDER;
}
