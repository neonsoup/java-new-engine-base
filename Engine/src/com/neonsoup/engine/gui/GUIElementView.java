package com.neonsoup.engine.gui;

import org.joml.Vector2i;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.scene.SceneNode;

public abstract class GUIElementView extends AbstractDisposable {

    protected SceneNode topSceneNode;
    protected SceneNode bottomSceneNode;

    protected GUIElement guiElement;

    protected final Vector2i localLocation = new Vector2i();
    protected int width = 5, height = 5;

    protected int topMargin, leftMargin;


    public GUIElementView() {
    }


    public void setLocalLocation(int x, int y) {
        this.localLocation.set(x, y);

        topSceneNode.setTranslation(x, y);

        if (guiElement != null) {
            guiElement.setLocalLocation(x, y);
        }
    }


    public int getLocalLocationX() {
        return localLocation.x;
    }


    public int getLocalLocationY() {
        return localLocation.y;
    }


    public int getGlobalLocationX() {
        if (guiElement != null) {
            return guiElement.getGlobalLocationX();
        }

        return localLocation.x;
    }


    public int getGlobalLocationY() {
        if (guiElement != null) {
            return guiElement.getGlobalLocationY();
        }

        return localLocation.y;
    }


    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;

        if (guiElement != null) {
            guiElement.setSize(width, height);
        }
    }


    public int getWidth() {
        return width;
    }


    public int getHeight() {
        return height;
    }


    public int getHorizontalMargin() {
        return topMargin;
    }


    public void setHorizontalMargin(int hMargin) {
        this.topMargin = hMargin;
    }


    public int getVerticalMargin() {
        return leftMargin;
    }


    public void setVerticalMargin(int vMargin) {
        this.leftMargin = vMargin;
    }


    protected void setTopSceneNode(SceneNode topSceneNode) {
        this.topSceneNode = topSceneNode;
    }


    protected void setBottomSceneNode(SceneNode bottomSceneNode) {
        this.bottomSceneNode = bottomSceneNode;
    }


    protected void setGUIElement(GUIElement guiElement) {
        this.guiElement = guiElement;

        if (guiElement != null) {
            guiElement.setSize(width, height);
            guiElement.setTopLeftMargin(topMargin, leftMargin);
            guiElement.setLocalLocation(localLocation.x, localLocation.y);
        }
    }


    protected void setVisible(boolean visible) {

    }


    protected abstract void update();

}
