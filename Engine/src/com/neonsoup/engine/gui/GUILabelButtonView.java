package com.neonsoup.engine.gui;

public abstract class GUILabelButtonView extends GUIButtonView {

    protected GUILabelButton guiLabelButton;


    public GUILabelButtonView() {
    }


    protected abstract void notifyButtonStateChanged(GUILabelButton.State newState);


    protected abstract void notifyButtonLabelTextChanged(String labelText);


    @Override
    protected void setGUIElement(GUIElement guiElement) {
        super.setGUIElement(guiElement);

        this.guiLabelButton = (GUILabelButton) guiElement;

        this.notifyButtonStateChanged(guiLabelButton.getButtonState());
        this.notifyButtonLabelTextChanged(guiLabelButton.getLabelText());
    }

}
