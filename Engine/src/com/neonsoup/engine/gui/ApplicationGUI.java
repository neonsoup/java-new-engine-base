package com.neonsoup.engine.gui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.app.ApplicationCanvas;
import com.neonsoup.engine.app.input.MouseCursorMode;
import com.neonsoup.engine.app.input.MouseEnterEvent;
import com.neonsoup.engine.app.input.MouseInputFocusHolder;
import com.neonsoup.engine.app.input.MouseKeyEvent;
import com.neonsoup.engine.app.input.MouseMotionEvent;
import com.neonsoup.engine.app.input.MouseScrollEvent;
import com.neonsoup.engine.app.input.WindowFocusEvent;
import com.neonsoup.engine.app.input.WindowFocusListener;
import com.neonsoup.engine.gui.defaultgui.DefaultGUIViewFactory;
import com.neonsoup.engine.scene.Camera;
import com.neonsoup.engine.scene.CameraNode;
import com.neonsoup.engine.scene.Scene;
import com.neonsoup.engine.scene.SceneConfiguration;
import com.neonsoup.engine.scene.SceneNode;

public class ApplicationGUI implements MouseInputFocusHolder, WindowFocusListener {

    private static final SceneConfiguration SCENE_CONFIG = SceneConfiguration.createLayered2DConfiguration();


    public static SceneConfiguration getSceneConfig() {
        return SCENE_CONFIG;
    }


    protected final Application app;

    protected Scene scene;
    protected Camera camera;
    protected CameraNode cameraNode;

    protected GUISceneRenderer sceneRenderer;

    protected final Collection<GUILayer> allLayers = new HashSet<>();

    protected boolean hasInputFocus = false;

    protected final List<GUILayer> visibleLayersStack = new ArrayList<>(6);

    protected GUILayer activeLayer = null;


    public ApplicationGUI(Application app) {
        this.app = app;
    }


    protected void addLayer(GUILayer layer) {
        this.allLayers.add(layer);
    }


    public void pushLayer(GUILayer layer) {
        if (layer == null) {
            throw new IllegalArgumentException();
        }
        if (layer.gui != this) {
            throw new IllegalArgumentException();
        }

        visibleLayersStack.add(layer);
        scene.getRootNode().attachChild(layer.sceneNode);
        activeLayer = layer;

        layer.onGUILayerSetActive();
    }


    public void popLayer() {
        if (visibleLayersStack.size() == 0) {
            throw new IllegalStateException();
        }

        GUILayer oldLayer = visibleLayersStack.remove(visibleLayersStack.size() - 1);
        scene.getRootNode().detachChild(oldLayer.sceneNode);

        oldLayer.onGUILayerSetUnactive();

        if (visibleLayersStack.isEmpty()) {
            activeLayer = null;
        } else {
            activeLayer = visibleLayersStack.get(visibleLayersStack.size() - 1);
            activeLayer.onGUILayerSetActive();
        }
    }


    public GUILayer getActiveLayer() {
        if (visibleLayersStack.isEmpty()) {
            return null;
        }

        return visibleLayersStack.get(visibleLayersStack.size() - 1);
    }


    public void draw() {
        for (GUILayer layer : visibleLayersStack) {
            for (GUIFrame frame : layer.frames) {
                frame.updateView();
            }
        }

        scene.updateTransform();

        sceneRenderer.render();
    }


    public boolean hasInputFocus() {
        return hasInputFocus;
    }


    public void acquireInputFocus() {
        if (app.getInput().getCurrentMouseInputFocusHolder() != this) {
            app.getInput().acquireMouseInputFocus(this);
            app.getInput().setMouseCursorMode(MouseCursorMode.NORMAL);
        }
    }


    public void releaseInputFocus() {
        if (app.getInput().getCurrentMouseInputFocusHolder() == this) {
            app.getInput().requestMouseInputFocusRelease();
        } else {
            throw new IllegalStateException("This GUI currently has no mouse input focus");
        }
    }


    @Override
    public void onWindowFocusAcquired(WindowFocusEvent event) {
        if (activeLayer != null) {
            activeLayer.onWindowFocusAcquired(event);
        }
    }


    @Override
    public void onWindowFocusLost(WindowFocusEvent event) {
        if (activeLayer != null) {
            activeLayer.onWindowFocusLost(event);
        }
    }


    @Override
    public void onMouseInputFocusAcquired() {
        if (activeLayer != null) {
            activeLayer.onGUIInputFocusAcquired();

            hasInputFocus = true;
        }
    }


    @Override
    public void onMouseInputFocusLost() {
        if (activeLayer != null) {
            hasInputFocus = false;

            activeLayer.onGUIInputFocusLost();
        }
    }


    @Override
    public void onMouseMoved(MouseMotionEvent event) {
        if (activeLayer != null) {
            activeLayer.onMouseMoved(event);
        }
    }


    @Override
    public void onMouseEnteredWindow(MouseEnterEvent event) {
        if (activeLayer != null) {
            activeLayer.onMouseEnteredWindow(event);
        }
    }


    @Override
    public void onMouseLeftWindow(MouseEnterEvent event) {
        if (activeLayer != null) {
            activeLayer.onMouseLeftWindow(event);
        }
    }


    @Override
    public void onMouseKeyEvent(MouseKeyEvent event) {
        if (activeLayer != null) {
            activeLayer.onMouseKeyEvent(event);
        }
    }


    @Override
    public void onMouseScrollEvent(MouseScrollEvent event) {
        if (activeLayer != null) {
            activeLayer.onMouseScrollEvent(event);
        }
    }


    public void initInMainThread() {
        {
            scene = new Scene(SCENE_CONFIG);
            scene.setRootNode(new SceneNode(SCENE_CONFIG));
            sceneRenderer = new GUISceneRenderer(app, this);

            camera = new Camera();
            ApplicationCanvas canvas = app.getCanvas();
            camera.setOrthoProjection2D(0.0f, canvas.getWidth(), canvas.getHeight(), 0.0f);

            cameraNode = new CameraNode(SCENE_CONFIG);
            cameraNode.setCamera(camera);

            scene.getRootNode().attachChild(cameraNode);
            scene.setRenderCameraNode(cameraNode);
        }

        DefaultGUIViewFactory.initStatic(app);
    }


    public void disposeInMainThread() {
        DefaultGUIViewFactory.disposeStatic(app);
    }

}
