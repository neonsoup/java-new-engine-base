package com.neonsoup.engine.gui.defaultgui;

import java.nio.FloatBuffer;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.graphics.MaterialMesh;
import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;
import com.neonsoup.engine.graphics.gl.GLBuffer;
import com.neonsoup.engine.graphics.gl.GLMesh;
import com.neonsoup.engine.graphics.gl.GLVertexAttributeData;
import com.neonsoup.engine.graphics.gl.GLBuffer.MapAccess;
import com.neonsoup.engine.graphics.material.MaterialProperties;
import com.neonsoup.engine.gui.ApplicationGUI;
import com.neonsoup.engine.scene.MeshNode;

public class ColorRectMesh extends AbstractDisposable {

    protected final GLBuffer bufPos;
    protected final MaterialMesh matMesh;
    protected final MeshNode meshNode;
    protected float r, g, b, a;


    public ColorRectMesh() {
        bufPos = new GLBuffer(2 * 4 * 4, GLBuffer.Target.ARRAY_BUFFER, GLBuffer.Usage.DYNAMIC_DRAW);

        GLVertexAttributeData colorAttrib = new GLVertexAttributeData(bufPos,
                                                                      DefaultGUIViewFactory.COLOR_RECT_ATTRIB_SIGNATURE_POSITION);

        GLMesh mesh = new GLMesh(GLMesh.Mode.TRIANGLE_STRIP, 4, new GLVertexAttributeData[] { colorAttrib }, null);

        matMesh = new MaterialMesh(mesh,
                                   DefaultGUIViewFactory.COLOR_RECT_MATERIAL,
                                   new MaterialProperties[] { new MaterialProperties(DefaultGUIViewFactory.COLOR_RECT_MATERIAL,
                                                                                     1) {

                                       @Override
                                       public void setProperties(SceneRenderer renderer, RenderTechnique technique) {
                                           material.getShader(technique.getId())
                                                   .uniform4f(DefaultGUIViewFactory.COLOR_RECT_COLOR_UNIFORM,
                                                              r,
                                                              g,
                                                              b,
                                                              a);
                                       }


                                       @Override
                                       public void unsetProperties(SceneRenderer renderer, RenderTechnique technique) {
                                       }

                                   } });

        meshNode = new MeshNode(matMesh, ApplicationGUI.getSceneConfig());
    }


    public void setColor(float r, float g, float b, float a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }


    public void setSize(int width, int height) {
        bufPos.bind();
        FloatBuffer buf = bufPos.mapBuffer(MapAccess.WRITE_ONLY).asFloatBuffer();

        buf.put(0.0f);
        buf.put(height);

        buf.put(0.0f);
        buf.put(0.0f);

        buf.put(width);
        buf.put(height);

        buf.put(width);
        buf.put(0.0f);

        buf.rewind();

        bufPos.unmapBuffer();
        bufPos.unbind();
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        matMesh.dispose();

        disposed = true;
    }

}
