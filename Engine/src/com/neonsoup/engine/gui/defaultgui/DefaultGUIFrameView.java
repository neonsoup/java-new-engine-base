package com.neonsoup.engine.gui.defaultgui;

import com.neonsoup.engine.gui.ApplicationGUI;
import com.neonsoup.engine.gui.GUIFrameView;
import com.neonsoup.engine.scene.SceneNode;

public class DefaultGUIFrameView extends GUIFrameView {

    public static final float FRAME_COLOR = 0.6f;

    protected boolean sizeDirty = false;

    protected final ColorRectMesh mesh;

    protected SceneNode bottomNode = new SceneNode(ApplicationGUI.getSceneConfig());


    public DefaultGUIFrameView() {
        leftMargin = topMargin = 4;

        mesh = new ColorRectMesh();

        mesh.setColor(FRAME_COLOR, FRAME_COLOR, FRAME_COLOR, 1.0f);

        mesh.meshNode.attachChild(bottomNode);

        bottomNode.setTranslation(topMargin, leftMargin);

        setTopSceneNode(mesh.meshNode);
        setBottomSceneNode(bottomNode);
    }


    @Override
    public void setSize(int width, int height) {
        super.setSize(width, height);

        sizeDirty = true;
    }


    @Override
    protected void update() {
        if (sizeDirty) {
            mesh.setSize(width, height);

            sizeDirty = false;
        }
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        mesh.dispose();

        disposed = true;
    }

}
