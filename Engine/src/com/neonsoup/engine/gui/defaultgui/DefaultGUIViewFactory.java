package com.neonsoup.engine.gui.defaultgui;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.graphics.gl.GLVertexAttributeSignature;
import com.neonsoup.engine.graphics.material.Material;
import com.neonsoup.engine.graphics.text.Font;
import com.neonsoup.engine.gui.GUIFrame;
import com.neonsoup.engine.gui.GUILabelButton;
import com.neonsoup.engine.res.ResourceHandle;
import com.neonsoup.engine.res.font.FontKey;
import com.neonsoup.engine.res.material.MaterialKey;

public final class DefaultGUIViewFactory {

    private DefaultGUIViewFactory() {
    }


    protected static ResourceHandle<Material> COLOR_RECT_MATERIAL_HANDLE;
    protected static Material COLOR_RECT_MATERIAL;
    protected static int COLOR_RECT_COLOR_UNIFORM;

    protected static final GLVertexAttributeSignature COLOR_RECT_ATTRIB_SIGNATURE_POSITION;
    static {
        COLOR_RECT_ATTRIB_SIGNATURE_POSITION = new GLVertexAttributeSignature("in_Position",
                                                                              GLVertexAttributeSignature.Type.FLOAT,
                                                                              2);
    }

    protected static ResourceHandle<Font> FONT_HANDLE;
    protected static Font FONT;


    public static void initStatic(Application app) {
        COLOR_RECT_MATERIAL_HANDLE = app.getGraphics().getMaterialManager()
                .loadResourceImmediate(MaterialKey.fromPath("material\\default\\gui\\gui_colored_rectangle.mat"));

        COLOR_RECT_MATERIAL = COLOR_RECT_MATERIAL_HANDLE.getResourceObject();

        COLOR_RECT_COLOR_UNIFORM = COLOR_RECT_MATERIAL.getShader(0).getUniformLocation("u_Color");

        FONT_HANDLE = app.getGraphics().getFontManager()
                .loadResourceImmediate(new FontKey("font\\bitmap\\pixel12x8\\pixel12x8.json", 24, 1, 1));

        FONT = FONT_HANDLE.getResourceObject();
    }


    public static void disposeStatic(Application app) {
        COLOR_RECT_MATERIAL_HANDLE.release();
    }


    public static void createFrameView(GUIFrame frame) {
        frame.setView(new DefaultGUIFrameView());
    }


    public static void createLabelButtonView(GUILabelButton button) {
        button.setView(new DefaultGUILabelButtonView());
    }

}
