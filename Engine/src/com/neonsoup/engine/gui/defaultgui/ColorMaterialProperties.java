package com.neonsoup.engine.gui.defaultgui;

import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;
import com.neonsoup.engine.graphics.material.MaterialProperties;

class ColorMaterialProperties extends MaterialProperties {

    private final float r, g, b, a;


    public ColorMaterialProperties(float r, float g, float b, float a) {
        super(DefaultGUIViewFactory.COLOR_RECT_MATERIAL, 1);

        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }


    @Override
    public void setProperties(SceneRenderer renderer, RenderTechnique technique) {
        material.getShader(technique.getId()).uniform4f(DefaultGUIViewFactory.COLOR_RECT_COLOR_UNIFORM, r, g, b, a);
    }


    @Override
    public void unsetProperties(SceneRenderer renderer, RenderTechnique technique) {
    }

}