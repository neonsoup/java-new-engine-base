package com.neonsoup.engine.gui.defaultgui;

import com.neonsoup.engine.graphics.text.Text;
import com.neonsoup.engine.graphics.text.TextBatchAttributes;
import com.neonsoup.engine.gui.ApplicationGUI;
import com.neonsoup.engine.gui.GUILabelButton.State;
import com.neonsoup.engine.gui.GUILabelButtonView;
import com.neonsoup.engine.scene.TextNode;

public class DefaultGUILabelButtonView extends GUILabelButtonView {

    public static final float BUTTON_COLOR = 0.8f;
    public static final int MARGIN = 2;

    protected static final TextBatchAttributes batchAttribs = new TextBatchAttributes();
    static {
        batchAttribs.flipVertical = true;
        batchAttribs.textColor.set(0.0f, 0.0f, 0.0f, 1.0f);
    }

    protected boolean sizeDirty = false;

    protected final ColorRectMesh mesh;

    protected int labelTextWidth = 0;

    protected final Text text;

    protected final TextNode textNode;


    public DefaultGUILabelButtonView() {
        mesh = new ColorRectMesh();

        mesh.setColor(BUTTON_COLOR, BUTTON_COLOR, BUTTON_COLOR, 1.0f);

        setTopSceneNode(mesh.meshNode);
        setBottomSceneNode(mesh.meshNode);

        text = new Text(DefaultGUIViewFactory.FONT_HANDLE, batchAttribs, "");

        textNode = new TextNode(ApplicationGUI.getSceneConfig(), text);

        mesh.meshNode.attachChild(textNode);
    }


    @Override
    public void setSize(int width, int height) {
        super.setSize(width, height);

        sizeDirty = true;
    }


    @Override
    protected void notifyButtonStateChanged(State newState) {
        switch (newState) {
            case ACTIVE_HOVERED:
                mesh.setColor(1.0f, 0.0f, 0.0f, 1.0f);
                break;

            case PRESSED:
                mesh.setColor(0.0f, 1.0f, 0.0f, 1.0f);
                break;

            default:
                mesh.setColor(BUTTON_COLOR, BUTTON_COLOR, BUTTON_COLOR, 1.0f);
                break;
        }
    }


    @Override
    protected void notifyButtonLabelTextChanged(String labelText) {
        labelTextWidth = (int) Math.ceil(DefaultGUIViewFactory.FONT.measureTextWidth(labelText));

        setSize(MARGIN * 2 + labelTextWidth, MARGIN * 2 + DefaultGUIViewFactory.FONT.getFontSize());

        int x = MARGIN;
        int y = MARGIN + DefaultGUIViewFactory.FONT.getAscent();

        text.setText(labelText);

        textNode.setTranslation(x, y);
    }


    @Override
    protected void update() {
        if (sizeDirty) {
            mesh.setSize(width, height);

            sizeDirty = false;
        }
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        mesh.dispose();
        text.dispose();

        disposed = true;
    }

}
