package com.neonsoup.engine.gui;

import com.neonsoup.engine.app.input.InputType;
import com.neonsoup.engine.app.input.MouseEnterEvent;
import com.neonsoup.engine.app.input.MouseKey;
import com.neonsoup.engine.app.input.MouseKeyAction;
import com.neonsoup.engine.app.input.MouseKeyEvent;
import com.neonsoup.engine.app.input.MouseMotionEvent;
import com.neonsoup.engine.app.input.MouseScrollEvent;

public class GUILabelButton extends GUIButton {

    public static enum State {
        UNACTIVE, ACTIVE_UNHOVERED, ACTIVE_HOVERED, PRESSED;
    }


    protected State buttonState = State.ACTIVE_UNHOVERED;

    protected String labelText = "";


    public GUILabelButton() {
    }


    public String getLabelText() {
        return labelText;
    }


    public void setLabelText(String labelText) {
        if (labelText == null) {
            labelText = "";
        }

        this.labelText = labelText;

        if (view != null) {
            ((GUILabelButtonView) view).notifyButtonLabelTextChanged(labelText);
        }
    }


    public State getButtonState() {
        return buttonState;
    }


    @Override
    public void onGlobalFocusLost(int inputTypes, FocusEventSource sourceType, Object source) {
        super.onGlobalFocusLost(inputTypes, sourceType, source);

        if ((inputTypes & InputType.INPUT_TYPE_BIT_MOUSE) != 0) {
            this.buttonState = State.ACTIVE_UNHOVERED;

            if (view != null) {
                ((GUILabelButtonView) view).notifyButtonStateChanged(this.buttonState);
            }
        }
    }


    @Override
    public void onMouseHoveredIn(int cursorX, int cursorY) {
        super.onMouseHoveredIn(cursorX, cursorY);

        this.buttonState = State.ACTIVE_HOVERED;

        if (view != null) {
            ((GUILabelButtonView) view).notifyButtonStateChanged(this.buttonState);
        }
    }


    @Override
    public void onMouseHoveredOut(int lastCursorX, int lastCursorY) {
        super.onMouseHoveredOut(lastCursorX, lastCursorY);

        this.buttonState = State.ACTIVE_UNHOVERED;

        if (view != null) {
            ((GUILabelButtonView) view).notifyButtonStateChanged(this.buttonState);
        }
    }


    @Override
    public void onMouseLeftWindow(MouseEnterEvent event) {
        super.onMouseLeftWindow(event);

        this.buttonState = State.ACTIVE_UNHOVERED;

        if (view != null) {
            ((GUILabelButtonView) view).notifyButtonStateChanged(this.buttonState);
        }
    }


    @Override
    public void onMouseKeyEvent(MouseKeyEvent event) {
        super.onMouseKeyEvent(event);

        if (event.keyAction == MouseKeyAction.PRESS && event.key == MouseKey.LEFT) {
            guiLayer.setGUIMouseInputFocusHolder(new GUIMouseInputFocusHolder() {

                @Override
                public void onGlobalFocusLost(FocusEventSource sourceType, Object source) {
                    buttonState = State.ACTIVE_UNHOVERED;

                    if (view != null) {
                        ((GUILabelButtonView) view).notifyButtonStateChanged(buttonState);
                    }
                }


                @Override
                public void onMouseKeyEvent(MouseKeyEvent event) {
                    if (event.keyAction == MouseKeyAction.RELEASE && event.key == MouseKey.LEFT) {
                        guiLayer.setGUIMouseInputFocusHolder(null);
                    }
                }


                @Override
                public void onMouseMoved(MouseMotionEvent event) {
                }


                @Override
                public void onMouseLeftWindow(MouseEnterEvent event) {
                }


                @Override
                public void onMouseEnteredWindow(MouseEnterEvent event) {
                }


                @Override
                public void onMouseScrollEvent(MouseScrollEvent event) {
                }

            });

            this.buttonState = State.PRESSED;

            if (view != null) {
                ((GUILabelButtonView) view).notifyButtonStateChanged(this.buttonState);
            }
        }
    }

}
