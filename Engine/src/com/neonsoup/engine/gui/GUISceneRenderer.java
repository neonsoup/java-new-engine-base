package com.neonsoup.engine.gui;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.graphics.RenderPass;
import com.neonsoup.engine.graphics.RenderProperties.OverridePolicy;
import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;
import com.neonsoup.engine.graphics.gl.GLStateProperties;
import com.neonsoup.engine.graphics.gl.GLStateProperties.BlendFunc;
import com.neonsoup.engine.scene.SceneNodeFilter;

public class GUISceneRenderer extends SceneRenderer {

    public static final RenderTechnique DIFFUSE_RENDER_TECHNIQUE = new RenderTechnique("diffuse", 0);

    public static final RenderPass FIRST_RENDER_PASS = new RenderPass();

    static {
        GLStateProperties props = DIFFUSE_RENDER_TECHNIQUE.getRenderStateProperties();

        props.setDepthTestOverride(OverridePolicy.OVERRIDE);
        props.setDepthTestEnabled(false);

        props.setDepthWriteOverride(OverridePolicy.OVERRIDE);
        props.setDepthWriteEnabled(false);

        props.setCullFaceOverride(OverridePolicy.OVERRIDE);
        props.setCullFaceEnabled(false);

        props.setBlendingOverride(OverridePolicy.OVERRIDE);
        props.setBlendingEnabled(true);

        props.setBlendFuncSrcALPHA(BlendFunc.SRC_ALPHA);
        props.setBlendFuncSrcRGB(BlendFunc.SRC_ALPHA);

        props.setBlendFuncDstALPHA(BlendFunc.ONE_MINUS_SRC_ALPHA);
        props.setBlendFuncDstRGB(BlendFunc.ONE_MINUS_SRC_ALPHA);
    }

    protected final ApplicationGUI gui;


    public GUISceneRenderer(Application app, ApplicationGUI gui) {
        super(app);

        this.gui = gui;
    }


    @Override
    protected void renderImpl() {
        DIFFUSE_RENDER_TECHNIQUE.beginDraw(this, scene);

        for (GUILayer layer : gui.visibleLayersStack) {
            for (GUIFrame frame : layer.frames) {
                if (frame.isVisible
                    && frame.view != null
                    && !(FIRST_RENDER_PASS.hasFilter()
                         && FIRST_RENDER_PASS.getNodeFilter()
                                 .filter(frame.view.topSceneNode) != SceneNodeFilter.Response.PASS)) {

                    frame.view.topSceneNode.drawRecursive(this, DIFFUSE_RENDER_TECHNIQUE, FIRST_RENDER_PASS);
                }
            }
        }

        DIFFUSE_RENDER_TECHNIQUE.endDraw();
    }

}
