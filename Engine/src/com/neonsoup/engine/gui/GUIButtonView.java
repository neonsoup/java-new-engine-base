package com.neonsoup.engine.gui;

public abstract class GUIButtonView extends GUIElementView {

    protected GUIButton guiButton;


    public GUIButtonView() {
    }


    @Override
    protected void setGUIElement(GUIElement guiElement) {
        super.setGUIElement(guiElement);

        this.guiButton = (GUIButton) guiElement;
    }

}
