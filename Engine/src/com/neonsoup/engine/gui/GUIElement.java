package com.neonsoup.engine.gui;

import org.joml.Vector2i;

import com.neonsoup.engine.app.input.MouseEnterEvent;
import com.neonsoup.engine.app.input.MouseKeyEvent;
import com.neonsoup.engine.app.input.MouseMotionEvent;
import com.neonsoup.engine.app.input.MouseScrollEvent;

public abstract class GUIElement {

    protected GUILayer guiLayer;

    protected GUIElementView view;

    protected final Vector2i globalLocation = new Vector2i();
    protected final Vector2i topLeftMargin = new Vector2i();
    protected final Vector2i localLocation = new Vector2i();
    protected int width, height;

    protected boolean isVisible = true;

    protected GUIContainer parent;
    protected GUIFrame guiFrame;


    public GUIElement() {
    }


    protected void setParent(GUIContainer parent) {
        this.parent = parent;

        updateGlobalLocation();
    }


    public GUIContainer getParent() {
        return parent;
    }


    public void setView(GUIElementView view) {
        if (view != null && (view.topSceneNode == null || view.bottomSceneNode == null)) {
            throw new IllegalArgumentException("view's scene nodes are not properly initialized");
        }

        if (this.view != null) {
            if (this.parent != null && this.parent.view != null) {
                this.parent.view.bottomSceneNode.detachChild(this.view.topSceneNode);
            }

            this.view.setGUIElement(null);
        }

        if (view != null) {
            if (this.parent != null && this.parent.view != null) {
                this.parent.view.bottomSceneNode.attachChild(this.view.topSceneNode);
            }

            view.setGUIElement(this);
            view.setVisible(this.isVisible);
        }

        this.view = view;
    }


    public GUIElementView getView() {
        return view;
    }


    public void updateView() {
        if (isVisible && view != null) {
            view.update();
        }
    }


    protected void setGUILayer(GUILayer guiLayer) {
        this.guiLayer = guiLayer;
    }


    public GUILayer getGUILayer() {
        return guiLayer;
    }


    protected void setGUIFrame(GUIFrame frame) {
        this.guiFrame = frame;
    }


    public void setVisible(boolean visible) {
        this.isVisible = visible;

        if (view != null) {
            view.setVisible(visible);
        }
    }


    public boolean isVisible() {
        return isVisible;
    }


    public void setTopLeftMargin(int topMargin, int leftMargin) {
        this.topLeftMargin.set(topMargin, leftMargin);

        updateGlobalLocation();
    }


    public int getLeftMargin() {
        return topLeftMargin.x;
    }


    public int getTopMargin() {
        return topLeftMargin.y;
    }


    public void setLocalLocation(int x, int y) {
        this.localLocation.set(x, y);

        updateGlobalLocation();
    }


    protected int getLocalLocationX() {
        return localLocation.x;
    }


    protected int getLocalLocationY() {
        return localLocation.y;
    }


    protected int getGlobalLocationX() {
        return globalLocation.x;
    }


    protected int getGlobalLocationY() {
        return globalLocation.y;
    }


    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }


    protected int getWidth() {
        return width;
    }


    protected int getHeight() {
        return height;
    }


    protected void updateGlobalLocation() {
        if (parent != null) {
            this.globalLocation.set(parent.globalLocation);
            this.globalLocation.add(topLeftMargin);
            this.globalLocation.add(localLocation);
        } else {
            this.globalLocation.set(localLocation);
            this.globalLocation.add(topLeftMargin);
        }
    }


    public void pack() {
        if (view == null) {
            throw new IllegalStateException();
        }
    }


    public void onGlobalFocusAcquired(int inputTypes, FocusEventSource sourceType, Object source) {
    }


    public void onGlobalFocusLost(int inputTypes, FocusEventSource sourceType, Object source) {
    }


    public void onMouseKeyEvent(MouseKeyEvent event) {
    }


    public void onMouseScrollEvent(MouseScrollEvent event) {
    }


    public void onMouseHoveredIn(int cursorX, int cursorY) {
    }


    public void onMouseHoveredOut(int lastCursorX, int lastCursorY) {
    }


    public void onMouseMoved(MouseMotionEvent event) {
    }


    public void onMouseEnteredWindow(MouseEnterEvent event) {
    }


    public void onMouseLeftWindow(MouseEnterEvent event) {
    }

}
