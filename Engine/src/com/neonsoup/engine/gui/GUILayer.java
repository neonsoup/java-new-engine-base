package com.neonsoup.engine.gui;

import java.util.ArrayList;
import java.util.List;

import com.neonsoup.engine.app.input.ApplicationInput;
import com.neonsoup.engine.app.input.InputType;
import com.neonsoup.engine.app.input.MouseEnterEvent;
import com.neonsoup.engine.app.input.MouseKeyEvent;
import com.neonsoup.engine.app.input.MouseMotionEvent;
import com.neonsoup.engine.app.input.MouseScrollEvent;
import com.neonsoup.engine.app.input.WindowFocusEvent;
import com.neonsoup.engine.scene.SceneNode;

public class GUILayer {

    protected final ApplicationGUI gui;
    protected final List<GUIFrame> frames = new ArrayList<GUIFrame>(8);

    protected final SceneNode sceneNode = new SceneNode(ApplicationGUI.getSceneConfig());

    protected GUIFrame hoveredFrame = null;

    protected GUIMouseInputFocusHolder mouseInputFocusHolder;


    public GUILayer(ApplicationGUI gui) {
        this.gui = gui;

        gui.addLayer(this);
    }


    public ApplicationGUI getGUI() {
        return gui;
    }


    public void addFrame(GUIFrame frame) {
        if (frame.getGUILayer() != null) {
            throw new IllegalArgumentException();
        }

        this.frames.add(frame);
        frame.setGUILayer(this);

        if (frame.view != null) {
            sceneNode.attachChild(frame.view.topSceneNode);
        }

        if (mouseInputFocusHolder != null) {
            ApplicationInput input = gui.app.getInput();

            int x = (int) input.getMouseCursorX();
            int y = (int) input.getMouseCursorY();

            GUIFrame prevHoveredFrame = this.hoveredFrame;
            GUIFrame newHoveredFrame = getFrameAtPoint(x, y);

            if (newHoveredFrame != prevHoveredFrame) {
                this.hoveredFrame = newHoveredFrame;

                if (prevHoveredFrame != null) {
                    prevHoveredFrame.onMouseHoveredOut(x, y);
                }

                newHoveredFrame.onMouseHoveredIn(x, y);
            }
        }
    }


    public void removeFrame(GUIFrame frame) {
        if (frame.getGUILayer() != this) {
            return;
        }

        this.frames.remove(frame);
        frame.setGUILayer(null);
        if (frame.view != null) {
            sceneNode.detachChild(frame.view.topSceneNode);
        }
    }


    protected GUIFrame getFrameAtPoint(int x, int y) {
        for (int i = frames.size() - 1; i >= 0; i--) {
            GUIFrame frame = frames.get(i);

            if (frame.localLocation.x <= x
                && frame.localLocation.y <= y
                && frame.localLocation.x + frame.width > x
                && frame.localLocation.y + frame.height > y) {

                return frame;
            }
        }

        return null;
    }


    public void onGUILayerSetActive() {
        for (GUIFrame frame : frames) {
            frame.onGlobalFocusAcquired(InputType.INPUT_TYPE_BITS_ALL, FocusEventSource.GUI_LAYER, this);
        }

        ApplicationInput input = gui.app.getInput();

        int x = (int) input.getMouseCursorX();
        int y = (int) input.getMouseCursorY();

        GUIFrame newHoveredFrame = getFrameAtPoint(x, y);

        if (newHoveredFrame != null) {
            this.hoveredFrame = newHoveredFrame;

            newHoveredFrame.onMouseHoveredIn(x, y);
        }
    }


    public void setGUIMouseInputFocusHolder(GUIMouseInputFocusHolder mouseInputFocusHolder) {
        GUIMouseInputFocusHolder oldHolder = this.mouseInputFocusHolder;

        if (oldHolder == null && mouseInputFocusHolder == null) {
            return;
        }

        this.mouseInputFocusHolder = mouseInputFocusHolder;

        if (oldHolder != null) {
            oldHolder.onGlobalFocusLost(FocusEventSource.FOCUS_HOLDER, mouseInputFocusHolder);
        }

        if (mouseInputFocusHolder == null) {
            for (GUIFrame frame : frames) {
                frame.onGlobalFocusAcquired(InputType.INPUT_TYPE_BIT_MOUSE,
                                            FocusEventSource.FOCUS_HOLDER,
                                            mouseInputFocusHolder);
            }

            ApplicationInput input = gui.app.getInput();

            int x = (int) input.getMouseCursorX();
            int y = (int) input.getMouseCursorY();

            GUIFrame newHoveredFrame = getFrameAtPoint(x, y);

            if (newHoveredFrame != null) {
                this.hoveredFrame = newHoveredFrame;

                newHoveredFrame.onMouseHoveredIn(x, y);
            }
        } else if (oldHolder == null) {
            hoveredFrame = null;

            for (GUIFrame frame : frames) {
                frame.onGlobalFocusLost(InputType.INPUT_TYPE_BIT_MOUSE,
                                        FocusEventSource.FOCUS_HOLDER,
                                        mouseInputFocusHolder);
            }
        }
    }


    public void onGUILayerSetUnactive() {
        hoveredFrame = null;

        int bits = InputType.INPUT_TYPE_BITS_ALL;

        if (mouseInputFocusHolder != null) {
            bits &= ~InputType.INPUT_TYPE_BIT_MOUSE;
        }

        if (mouseInputFocusHolder != null) {
            GUIMouseInputFocusHolder oldFocusHolder = mouseInputFocusHolder;

            mouseInputFocusHolder = null;

            oldFocusHolder.onGlobalFocusLost(FocusEventSource.GUI_LAYER, this);
        }

        if (bits != 0) {
            for (GUIFrame frame : frames) {
                frame.onGlobalFocusLost(bits, FocusEventSource.GUI_LAYER, this);
            }
        }
    }


    public boolean isActiveLayer() {
        return gui.getActiveLayer() == this;
    }


    public void onWindowFocusAcquired(WindowFocusEvent event) {
        for (GUIFrame frame : frames) {
            frame.onGlobalFocusAcquired(InputType.INPUT_TYPE_BITS_ALL, FocusEventSource.WINDOW, gui.app.getWindow());
        }

        ApplicationInput input = gui.app.getInput();

        int x = (int) input.getMouseCursorX();
        int y = (int) input.getMouseCursorY();

        GUIFrame newHoveredFrame = getFrameAtPoint(x, y);

        if (newHoveredFrame != null) {
            this.hoveredFrame = newHoveredFrame;

            newHoveredFrame.onMouseHoveredIn(x, y);
        }
    }


    public void onWindowFocusLost(WindowFocusEvent event) {
        hoveredFrame = null;

        int bits = InputType.INPUT_TYPE_BITS_ALL;

        if (mouseInputFocusHolder != null) {
            bits &= ~InputType.INPUT_TYPE_BIT_MOUSE;
        }

        if (mouseInputFocusHolder != null) {
            GUIMouseInputFocusHolder oldFocusHolder = mouseInputFocusHolder;

            mouseInputFocusHolder = null;

            oldFocusHolder.onGlobalFocusLost(FocusEventSource.WINDOW, gui.app.getWindow());
        }

        for (GUIFrame frame : frames) {
            frame.onGlobalFocusLost(bits, FocusEventSource.WINDOW, gui.app.getWindow());
        }
    }


    public void onGUIInputFocusAcquired() {
        for (GUIFrame frame : frames) {
            frame.onGlobalFocusAcquired(InputType.INPUT_TYPE_BITS_ALL, FocusEventSource.GUI, gui);
        }

        ApplicationInput input = gui.app.getInput();

        int x = (int) input.getMouseCursorX();
        int y = (int) input.getMouseCursorY();

        GUIFrame newHoveredFrame = getFrameAtPoint(x, y);

        if (newHoveredFrame != null) {
            this.hoveredFrame = newHoveredFrame;

            newHoveredFrame.onMouseHoveredIn(x, y);
        }
    }


    public void onGUIInputFocusLost() {
        hoveredFrame = null;

        int bits = InputType.INPUT_TYPE_BITS_ALL;

        if (mouseInputFocusHolder != null) {
            bits &= ~InputType.INPUT_TYPE_BIT_MOUSE;
        }

        if (mouseInputFocusHolder != null) {
            GUIMouseInputFocusHolder oldFocusHolder = mouseInputFocusHolder;

            mouseInputFocusHolder = null;

            oldFocusHolder.onGlobalFocusLost(FocusEventSource.GUI, gui);
        }

        for (GUIFrame frame : frames) {
            frame.onGlobalFocusLost(bits, FocusEventSource.GUI, gui);
        }
    }


    public void onMouseMoved(MouseMotionEvent event) {
        if (mouseInputFocusHolder != null) {
            mouseInputFocusHolder.onMouseMoved(event);
        } else {
            int px = (int) event.prevCursorX;
            int py = (int) event.prevCursorY;

            int x = (int) event.cursorX;
            int y = (int) event.cursorY;

            GUIFrame prevHoveredFrame = this.hoveredFrame;
            GUIFrame currentHoveredFrame = getFrameAtPoint(x, y);

            if (prevHoveredFrame != currentHoveredFrame) {
                this.hoveredFrame = currentHoveredFrame;

                if (prevHoveredFrame != null) {
                    prevHoveredFrame.onMouseMoved(event);
                    prevHoveredFrame.onMouseHoveredOut(px, py);
                }

                if (currentHoveredFrame != null) {
                    currentHoveredFrame.onMouseHoveredIn(x, y);
                    currentHoveredFrame.onMouseMoved(event);
                }
            } else if (this.hoveredFrame != null) {
                this.hoveredFrame.onMouseMoved(event);
            }
        }
    }


    public void onMouseEnteredWindow(MouseEnterEvent event) {
        if (mouseInputFocusHolder != null) {
            mouseInputFocusHolder.onMouseEnteredWindow(event);
        } else {
            for (GUIFrame frame : frames) {
                frame.onMouseEnteredWindow(event);
            }
        }
    }


    public void onMouseLeftWindow(MouseEnterEvent event) {
        if (mouseInputFocusHolder != null) {
            mouseInputFocusHolder.onMouseLeftWindow(event);
        } else {
            this.hoveredFrame = null;

            for (GUIFrame frame : frames) {
                frame.onMouseLeftWindow(event);
            }
        }
    }


    public void onMouseKeyEvent(MouseKeyEvent event) {
        if (mouseInputFocusHolder != null) {
            mouseInputFocusHolder.onMouseKeyEvent(event);
        } else {
            if (hoveredFrame != null) {
                hoveredFrame.onMouseKeyEvent(event);
            }
        }
    }


    public void onMouseScrollEvent(MouseScrollEvent event) {
        if (mouseInputFocusHolder != null) {
            mouseInputFocusHolder.onMouseScrollEvent(event);
        } else {
            if (hoveredFrame != null) {
                hoveredFrame.onMouseScrollEvent(event);
            }
        }
    }


    public void disposeAllAttachedData() {
        for (GUIFrame frame : frames) {
            GUIElementView view = frame.view;

            if (view != null) {
                frame.setView(null);
                view.dispose();
            }
        }
    }

}
