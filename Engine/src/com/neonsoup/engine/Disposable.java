package com.neonsoup.engine;

public interface Disposable {

    boolean isDisposed();


    void dispose();

}
