package com.neonsoup.engine;

public interface CachedDisposable extends Disposable {

    void disposeCached();

}
