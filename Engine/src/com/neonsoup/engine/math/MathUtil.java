package com.neonsoup.engine.math;

public final class MathUtil {

    public static final float PI = (float) Math.PI;

    public static final float TWO_PI = (float) (Math.PI * 2.0);
    public static final float HALF_PI = (float) (Math.PI * 0.5);
    public static final float THIRD_PI = (float) (Math.PI / 3.0);
    public static final float QUARTER_PI = (float) (Math.PI * 0.25);

    public static final float SQRT_2 = (float) Math.sqrt(2.0);


    private MathUtil() {
    }


    public static int ceilDiv(int x, int y) {
        return 1 + ((x - 1) / y);
    }

}
