package com.neonsoup.engine.scene;

import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;
import com.neonsoup.engine.graphics.Sprite;

public class SpriteNode extends SceneNode {

    protected Sprite sprite;


    public SpriteNode(SceneConfiguration config, Sprite sprite) {
        super(config);

        this.sprite = sprite;
    }


    public Sprite getSprite() {
        return sprite;
    }


    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }


    @Override
    public void draw(SceneRenderer renderer, RenderTechnique technique) {
        if (sprite != null) {
            sprite.draw(renderer, technique, scene, this);
        }
    }

}
