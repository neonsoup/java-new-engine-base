package com.neonsoup.engine.scene;

import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;
import com.neonsoup.engine.graphics.text.Text;

public class TextNode extends SceneNode {

    protected Text text = null;


    public TextNode(SceneConfiguration config) {
        super(config);
    }


    public TextNode(SceneConfiguration config, Text text) {
        super(config);

        this.text = text;
    }


    public Text getText() {
        return text;
    }


    public void setText(Text text) {
        this.text = text;
    }


    @Override
    public void draw(SceneRenderer renderer, RenderTechnique technique) {
        if (text != null) {
            text.draw(renderer, technique, scene, this);
        }
    }

}
