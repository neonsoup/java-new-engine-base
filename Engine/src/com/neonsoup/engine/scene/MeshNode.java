package com.neonsoup.engine.scene;

import com.neonsoup.engine.graphics.MaterialMesh;
import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;

public class MeshNode extends SceneNode {

    private MaterialMesh mesh;


    public MeshNode(SceneConfiguration config) {
        super(config);
    }


    public MeshNode(MaterialMesh mesh, SceneConfiguration config) {
        super(config);

        setMesh(mesh);
    }


    public MaterialMesh getMesh() {
        return mesh;
    }


    public void setMesh(MaterialMesh mesh) {
        this.mesh = mesh;
    }


    @Override
    public void draw(SceneRenderer renderer, RenderTechnique technique) {
        if (mesh != null) {
            mesh.draw(renderer, technique, scene, this);
        }
    }

}
