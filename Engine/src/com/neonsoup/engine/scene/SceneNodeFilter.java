package com.neonsoup.engine.scene;

public interface SceneNodeFilter {

    enum Response {
        PASS, DENY;
    }


    Response filter(SceneNode node);

}
