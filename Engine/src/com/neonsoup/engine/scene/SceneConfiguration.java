package com.neonsoup.engine.scene;

import java.util.Arrays;

public final class SceneConfiguration {

    public static enum SceneType {

        TYPE_3D {

            @Override
            public boolean is2D() {
                return false;
            }
        },

        TYPE_LAYERED_2D {

            @Override
            public boolean is2D() {
                return true;
            }

        },

        TYPE_HALF_ISOMETRIC_SPLIT_LAYER_2D {

            @Override
            public boolean is2D() {
                return true;
            }

        };

        public abstract boolean is2D();
    }


    private final SceneType type;

    private final float[] splitLayers;


    protected SceneConfiguration(SceneType type) {
        this.type = type;

        this.splitLayers = null;
    }


    protected SceneConfiguration(SceneType type, float[] splitLayers) {
        this.type = type;

        this.splitLayers = Arrays.copyOf(splitLayers, splitLayers.length);
    }


    public static SceneConfiguration create3DConfiguration() {
        return new SceneConfiguration(SceneType.TYPE_3D);
    }


    public static SceneConfiguration createLayered2DConfiguration() {
        return new SceneConfiguration(SceneType.TYPE_LAYERED_2D);
    }


    public static SceneConfiguration createHalfIsometricSplitLayers2DConfiguration(float[] splitLayers) {
        return new SceneConfiguration(SceneType.TYPE_HALF_ISOMETRIC_SPLIT_LAYER_2D, splitLayers);
    }


    public SceneType getSceneType() {
        return type;
    }


    public int getSplitLayerCount() {
        if (type != SceneType.TYPE_HALF_ISOMETRIC_SPLIT_LAYER_2D) {
            throw new IllegalStateException();
        }

        return splitLayers.length;
    }


    public float getSplitLayer(int idx) {
        if (type != SceneType.TYPE_HALF_ISOMETRIC_SPLIT_LAYER_2D) {
            throw new IllegalStateException();
        }

        return splitLayers[idx];
    }

}
