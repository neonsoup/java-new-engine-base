package com.neonsoup.engine.scene;

import com.neonsoup.engine.graphics.RenderPass;
import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;

public class Scene {

    protected SceneNode rootNode;
    protected CameraNode cameraNode;

    protected final SceneConfiguration config;


    public Scene(SceneConfiguration config) {
        this.config = config;
    }


    public SceneNode getRootNode() {
        return rootNode;
    }


    public void setRootNode(SceneNode rootNode) {
        if (rootNode != null && rootNode.scene != null) {
            throw new IllegalArgumentException("rootNode is already attached to a scene");
        }

        if (rootNode != null) {
            rootNode.setScene(this);
        } else if (this.rootNode != null) {
            this.rootNode.setScene(null);
        }

        this.rootNode = rootNode;
    }


    public void updateTransform() {
        if (rootNode != null) {
            rootNode.updateTransform();
        }
    }


    public CameraNode getRenderCameraNode() {
        return cameraNode;
    }


    public void setRenderCameraNode(CameraNode cameraNode) {
        this.cameraNode = cameraNode;
    }


    public void drawRecursive(SceneRenderer renderer, RenderTechnique technique, RenderPass renderPass) {
        if (rootNode != null) {
            rootNode.drawRecursive(renderer, technique, renderPass);
        }
    }

}
