package com.neonsoup.engine.scene;

import org.joml.Matrix4f;
import org.joml.Matrix4fc;
import org.joml.Quaternionf;
import org.joml.Vector3f;
import org.joml.Vector4f;

public class Camera {

    protected float fovy, aspect;

    protected float width, height;

    protected float zNear, zFar;

    protected float left, right, bottom, top;

    protected final Matrix4f projectionMatrix = new Matrix4f();

    protected final Matrix4f viewMatrix = new Matrix4f();

    protected final Matrix4f invProjectionView = new Matrix4f();


    public Camera() {
    }


    public void setPerspectiveProjection(float fovy, float aspect, float zNear, float zFar) {
        this.fovy = fovy;
        this.aspect = aspect;
        this.zNear = zNear;
        this.zFar = zFar;

        projectionMatrix.setPerspective(fovy, aspect, zNear, zFar);
    }


    public void setOrthoProjection2D(float width, float height) {
        this.width = width;
        this.height = height;

        projectionMatrix.setOrthoSymmetric(width, height, -1.0f, 1.0f);
    }


    public void setOrthoProjection2D(float left, float right, float bottom, float top) {
        projectionMatrix.setOrtho2D(left, right, bottom, top);

        this.left = left;
        this.right = right;
        this.bottom = bottom;
        this.top = top;

        this.width = Math.abs(right - left);
        this.height = Math.abs(top - bottom);
    }


    public Matrix4fc getProjectionMatrix() {
        return projectionMatrix;
    }


    public Matrix4fc getViewMatrix() {
        return viewMatrix;
    }


    public Matrix4fc getInvProjectionViewMatrix() {
        return invProjectionView;
    }


    private final Quaternionf rotationTemp = new Quaternionf();
    private final Vector3f locationTemp = new Vector3f();


    public void updateViewMatrix(SceneNode cameraNode) {
        Matrix4fc matrix = cameraNode.getGlobalTransformMatrix();

        rotationTemp.setFromNormalized(matrix);

        rotationTemp.invert();

        matrix.getTranslation(locationTemp);

        viewMatrix.rotation(rotationTemp);
        viewMatrix.translate(-locationTemp.x, -locationTemp.y, -locationTemp.z);

        invProjectionView.set(projectionMatrix);
        invProjectionView.mul(viewMatrix);
        invProjectionView.invert();
    }


    public void screenToView(Vector4f in, Vector4f out) {
        in.mul(invProjectionView, out);

        out.x /= out.w;
        out.y /= out.w;
        out.z /= out.w;
    }


    public Vector3f screenToRay(float screenX, float screenY) {
        Vector4f screenNear = new Vector4f(screenX, screenY, -1, 1);
        Vector4f screenFar = new Vector4f(screenX, screenY, 1, 1);

        screenNear.mul(invProjectionView);
        screenFar.mul(invProjectionView);

        screenNear.x /= screenNear.w;
        screenNear.y /= screenNear.w;
        screenNear.z /= screenNear.w;

        screenFar.x /= screenFar.w;
        screenFar.y /= screenFar.w;
        screenFar.z /= screenFar.w;

        Vector3f result = new Vector3f(screenFar.x, screenFar.y, screenFar.z);
        result.sub(screenNear.x, screenNear.y, screenNear.z);

        return result;
    }

}
