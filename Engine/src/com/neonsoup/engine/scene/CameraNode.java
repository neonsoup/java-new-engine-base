package com.neonsoup.engine.scene;

public class CameraNode extends SceneNode {

    protected Camera camera = null;


    public CameraNode(SceneConfiguration config) {
        super(config);

        this.camera = new Camera();
    }


    public Camera getCamera() {
        return camera;
    }


    public void setCamera(Camera camera) {
        this.camera = camera;
        markTransformDirty();
    }


    @Override
    protected void onUpdateTransform() {
        if (camera != null) {
            camera.updateViewMatrix(this);
        }
    }

}
