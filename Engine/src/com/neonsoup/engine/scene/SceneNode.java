package com.neonsoup.engine.scene;

import java.util.ArrayList;
import java.util.List;

import org.joml.Matrix4f;
import org.joml.Matrix4fc;
import org.joml.Quaternionf;
import org.joml.Quaternionfc;
import org.joml.Vector2fc;
import org.joml.Vector3f;
import org.joml.Vector3fc;

import com.neonsoup.engine.graphics.RenderPass;
import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;
import com.neonsoup.engine.scene.SceneNodeFilter.Response;

public class SceneNode {

    protected final Vector3f translation = new Vector3f();
    protected final Quaternionf rotation = new Quaternionf();
    protected final Vector3f scale = new Vector3f(1.0f, 1.0f, 1.0f);
    protected float rotation2D = 0.0f;

    protected final Matrix4f localTransformMatrix = new Matrix4f();
    protected final Matrix4f globalTransformMatrix = new Matrix4f();

    protected boolean transformDirty = false;

    protected Scene scene;

    protected SceneNode parent = null;
    protected final List<SceneNode> children = new ArrayList<>(4);

    protected final SceneConfiguration.SceneType sceneType;

    protected int layer = -1;


    public SceneNode(SceneConfiguration config) {
        this.sceneType = config.getSceneType();
    }


    public Scene getScene() {
        return scene;
    }


    protected void setScene(Scene scene) {
        this.scene = scene;

        for (SceneNode child : children) {
            child.setScene(scene);
        }
    }


    public int getLayer() {
        return layer;
    }


    public void setLayer(int layer) {
        this.layer = layer;
    }


    public Vector3fc getTranslation() {
        return translation;
    }


    public float getTranslationX() {
        return translation.x;
    }


    public float getTranslationY() {
        return translation.y;
    }


    public float getTranslationZ() {
        return translation.z;
    }


    public Quaternionfc getRotation() {
        return rotation;
    }


    /**
     * @return rotation along Z axis if no 3D rotation methods were used,
     *         otherwise NaN
     */
    public float getRotation2D() {
        return rotation2D;
    }


    public Vector3fc getScale() {
        return scale;
    }


    public void setTranslation(Vector3fc loc) {
        this.translation.set(loc);
        markTransformDirty();
    }


    public void setTranslation(Vector2fc loc2D) {
        this.translation.set(loc2D.x(), loc2D.y(), 0.0f);
        markTransformDirty();
    }


    public void setTranslation(float x, float y, float z) {
        this.translation.set(x, y, z);
        markTransformDirty();
    }


    public void setTranslation(float x, float y) {
        this.translation.set(x, y, 0.0f);
        markTransformDirty();
    }


    public void setRotation(Quaternionfc rot) {
        this.rotation.set(rot);
        this.rotation2D = Float.NaN;
        markTransformDirty();
    }


    public void setRotationQuat(float x, float y, float z, float w) {
        this.rotation.set(x, y, z, w);
        this.rotation2D = Float.NaN;
        markTransformDirty();
    }


    public void setRotation(float rot2D) {
        this.rotation.setAngleAxis(rot2D, 0.0f, 0.0f, 1.0f);
        this.rotation2D = rot2D;
        markTransformDirty();
    }


    public void setScale(Vector3fc scale) {
        this.scale.set(scale);
        markTransformDirty();
    }


    public void setScale(Vector2fc scale2D) {
        this.scale.set(scale2D.x(), scale2D.y(), 1.0f);
        markTransformDirty();
    }


    public void translate(float tx, float ty, float tz) {
        this.translation.add(tx, ty, tz);
        markTransformDirty();
    }


    public void translate(float tx, float ty) {
        this.translate(tx, ty, 0.0f);
    }


    public void translate(Vector3fc t) {
        this.translation.add(t);
        markTransformDirty();
    }


    public void translate(Vector2fc t) {
        this.translate(t.x(), t.y(), 0.0f);
    }


    public void rotate(float rotx, float roty, float rotz) {
        this.rotation.rotate(rotx, roty, rotz);
        this.rotation2D = Float.NaN;
        markTransformDirty();
    }


    public void rotateX(float rot) {
        this.rotation.rotateX(rot);
        this.rotation2D = Float.NaN;
        markTransformDirty();
    }


    public void rotateY(float rot) {
        this.rotation.rotateY(rot);
        this.rotation2D = Float.NaN;
        markTransformDirty();
    }


    public void rotateZ(float rot) {
        this.rotation.rotateZ(rot);
        this.rotation2D += rot;
        markTransformDirty();
    }


    public void rotate(float rot2D) {
        rotateZ(rot2D);
    }


    public void scale(float sx, float sy, float sz) {
        this.scale.mul(sx, sy, sz);
        markTransformDirty();
    }


    public void scale(float sx, float sy) {
        this.scale(sx, sy, 1.0f);
    }


    public void scale(Vector3fc s) {
        this.scale.mul(s);
        markTransformDirty();
    }


    public void scale(Vector2fc scale2D) {
        this.scale.mul(scale2D.x(), scale2D.y(), 1.0f);
        markTransformDirty();
    }


    public Matrix4fc getGlobalTransformMatrix() {
        return globalTransformMatrix;
    }


    public void attachChild(SceneNode child) {
        if (child.parent != null) {
            throw new IllegalArgumentException("child is already attached to a node");
        }
        if (child.scene != null) {
            throw new IllegalArgumentException("child is already attached to a scene");
        }

        children.add(child);

        child.parent = this;

        child.setScene(scene);

        child.markTransformDirty();
    }


    public void detachChild(SceneNode child) {
        if (child.parent != this) {
            throw new IllegalArgumentException("child is not attached to this node");
        }

        child.parent = null;

        children.remove(child);

        child.setScene(null);
    }


    public void markTransformDirty() {
        transformDirty = true;

        for (int i = 0; i < children.size(); i++) {
            children.get(i).markTransformDirty();
        }
    }


    public void updateTransform() {
        if (transformDirty) {
            localTransformMatrix.identity();

            localTransformMatrix.translate(translation);
            localTransformMatrix.rotate(rotation);
            localTransformMatrix.scale(scale);

            if (parent != null) {
                globalTransformMatrix.set(parent.globalTransformMatrix);
                globalTransformMatrix.mul(localTransformMatrix);
            } else {
                globalTransformMatrix.set(localTransformMatrix);
            }

            onUpdateTransform();
        }

        for (int i = 0; i < children.size(); i++) {
            children.get(i).updateTransform();
        }

        transformDirty = false;
    }


    protected void onUpdateTransform() {
    }


    public void drawRecursive(SceneRenderer renderer, RenderTechnique technique, RenderPass renderPass) {
        if (!(renderPass.hasFilter() && renderPass.getNodeFilter().filter(this) != Response.PASS)) {
            draw(renderer, technique);
        }

        for (SceneNode child : children) {
            child.drawRecursive(renderer, technique, renderPass);
        }
    }


    public void draw(SceneRenderer renderer, RenderTechnique technique) {
    }

}
