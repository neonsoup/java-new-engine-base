package com.neonsoup.engine.graphics;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.graphics.gl.GLBuffer;
import com.neonsoup.engine.graphics.gl.GLBuffer.MapAccess;
import com.neonsoup.engine.graphics.gl.GLMesh;
import com.neonsoup.engine.graphics.gl.GLVertexAttributeData;
import com.neonsoup.engine.graphics.gl.GLVertexAttributeSignature;
import com.neonsoup.engine.graphics.material.Material;
import com.neonsoup.engine.graphics.material.MaterialProperties;
import com.neonsoup.engine.res.ResourceHandle;
import com.neonsoup.engine.res.material.MaterialKey;

public class DebugMesh2D extends AbstractDisposable {

    public static final GLVertexAttributeSignature ATTRIB_SIG_POS = new GLVertexAttributeSignature("in_Position",
                                                                                                   GLVertexAttributeSignature.Type.FLOAT,
                                                                                                   2);

    public static final GLVertexAttributeSignature ATTRIB_SIG_COLOR = new GLVertexAttributeSignature("in_Color",
                                                                                                     GLVertexAttributeSignature.Type.FLOAT,
                                                                                                     4);

    protected static ResourceHandle<Material> MATERIAL_HANDLE;
    protected static Material MATERIAL;


    public static void initStatic(Application app) {
        MATERIAL_HANDLE = app.getGraphics().getMaterialManager()
                .loadResourceImmediate(MaterialKey.fromPath("res\\material\\default\\debug_2d.mat"));

        MATERIAL = MATERIAL_HANDLE.getResourceObject();
    }


    public static void disposeStatic() {
        MATERIAL_HANDLE.release();
    }


    protected final int maxElements;

    protected final GLBuffer bufPosLine;
    protected final GLBuffer bufColorLine;

    protected final MaterialMesh lineMatMesh;

    protected final GLMesh lineMesh;

    protected ByteBuffer bufMapPosLine;
    protected ByteBuffer bufMapColorLine;

    protected FloatBuffer bufMapPosLineFloat;
    protected FloatBuffer bufMapColorLineFloat;

    protected int lines = 0;

    protected final GLBuffer bufPosTri;
    protected final GLBuffer bufColorTri;

    protected final MaterialMesh triMatMesh;

    protected final GLMesh triMesh;

    protected ByteBuffer bufMapPosTri;
    protected ByteBuffer bufMapColorTri;

    protected FloatBuffer bufMapPosTriFloat;
    protected FloatBuffer bufMapColorTriFloat;

    protected int tris = 0;


    public DebugMesh2D(int maxElements) {

        this.maxElements = maxElements;

        bufPosLine = new GLBuffer(maxElements * 2 * 2 * 4, GLBuffer.Target.ARRAY_BUFFER, GLBuffer.Usage.DYNAMIC_DRAW);
        bufColorLine = new GLBuffer(maxElements * 2 * 4 * 4, GLBuffer.Target.ARRAY_BUFFER, GLBuffer.Usage.DYNAMIC_DRAW);

        lineMesh = new GLMesh(GLMesh.Mode.LINES,
                              GLMesh.ProvokingVertex.FIRST,
                              maxElements,
                              new GLVertexAttributeData[] { new GLVertexAttributeData(bufPosLine, ATTRIB_SIG_POS),
                                                            new GLVertexAttributeData(bufColorLine, ATTRIB_SIG_COLOR) },
                              null);

        this.lineMatMesh = new MaterialMesh(lineMesh, MATERIAL, new MaterialProperties[] {});

        bufPosTri = new GLBuffer(maxElements * 3 * 2 * 4, GLBuffer.Target.ARRAY_BUFFER, GLBuffer.Usage.DYNAMIC_DRAW);
        bufColorTri = new GLBuffer(maxElements * 3 * 4 * 4, GLBuffer.Target.ARRAY_BUFFER, GLBuffer.Usage.DYNAMIC_DRAW);

        triMesh = new GLMesh(GLMesh.Mode.TRIANGLES,
                             GLMesh.ProvokingVertex.FIRST,
                             maxElements,
                             new GLVertexAttributeData[] { new GLVertexAttributeData(bufPosTri, ATTRIB_SIG_POS),
                                                           new GLVertexAttributeData(bufColorTri, ATTRIB_SIG_COLOR) },
                             null);

        triMatMesh = new MaterialMesh(triMesh, MATERIAL, new MaterialProperties[] {});
    }


    public MaterialMesh getLineMaterialMesh() {
        return lineMatMesh;
    }


    public MaterialMesh getPolygonMaterialMesh() {
        return triMatMesh;
    }


    public void addLine(float x0, float y0, float x1, float y1, float r, float g, float b, float a) {
        if (lines == maxElements) {
            throw new IllegalStateException("Line buffer overflow");
        }

        bufMapPosLineFloat.put(lines * 4, x0);
        bufMapPosLineFloat.put(lines * 4 + 1, y0);

        bufMapPosLineFloat.put(lines * 4 + 2, x1);
        bufMapPosLineFloat.put(lines * 4 + 3, y1);

        bufMapColorLineFloat.put(lines * 8, r);
        bufMapColorLineFloat.put(lines * 8 + 1, g);
        bufMapColorLineFloat.put(lines * 8 + 2, b);
        bufMapColorLineFloat.put(lines * 8 + 3, a);

        bufMapColorLineFloat.put(lines * 8 + 4, r);
        bufMapColorLineFloat.put(lines * 8 + 5, g);
        bufMapColorLineFloat.put(lines * 8 + 6, b);
        bufMapColorLineFloat.put(lines * 8 + 7, a);

        lines++;
    }


    public void addTriangle(float x0,
                            float y0,
                            float x1,
                            float y1,
                            float x2,
                            float y2,
                            float r,
                            float g,
                            float b,
                            float a) {

        if (tris == maxElements) {
            throw new IllegalStateException("Triangle buffer overflow");
        }

        bufMapPosTriFloat.put(tris * 6, x0);
        bufMapPosTriFloat.put(tris * 6 + 1, y0);

        bufMapPosTriFloat.put(tris * 6 + 2, x1);
        bufMapPosTriFloat.put(tris * 6 + 3, y1);

        bufMapPosTriFloat.put(tris * 6 + 4, x2);
        bufMapPosTriFloat.put(tris * 6 + 5, y2);

        bufMapColorTriFloat.put(tris * 12, r);
        bufMapColorTriFloat.put(tris * 12 + 1, g);
        bufMapColorTriFloat.put(tris * 12 + 2, b);
        bufMapColorTriFloat.put(tris * 12 + 3, a);

        bufMapColorTriFloat.put(tris * 12 + 4, r);
        bufMapColorTriFloat.put(tris * 12 + 5, g);
        bufMapColorTriFloat.put(tris * 12 + 6, b);
        bufMapColorTriFloat.put(tris * 12 + 7, a);

        bufMapColorTriFloat.put(tris * 12 + 8, r);
        bufMapColorTriFloat.put(tris * 12 + 9, g);
        bufMapColorTriFloat.put(tris * 12 + 10, b);
        bufMapColorTriFloat.put(tris * 12 + 11, a);

        tris++;
    }


    /**
     * Draws two triangles with points 0, 1, 2 and 0, 3, 1
     */
    public void addQuad(float x0,
                        float y0,
                        float x1,
                        float y1,
                        float x2,
                        float y2,
                        float x3,
                        float y3,
                        float r,
                        float g,
                        float b,
                        float a) {

        addTriangle(x0, y0, x1, y1, x2, y2, r, g, b, a);
        addTriangle(x0, y0, x3, y3, x1, y1, r, g, b, a);
    }


    public void mapBuffers() {
        bufPosLine.bind();

        bufMapPosLine = bufPosLine.mapBuffer(MapAccess.WRITE_ONLY);
        bufMapPosLine.rewind();
        bufMapPosLineFloat = bufMapPosLine.asFloatBuffer();

        bufColorLine.bind();

        bufMapColorLine = bufColorLine.mapBuffer(MapAccess.WRITE_ONLY);
        bufMapColorLine.rewind();
        bufMapColorLineFloat = bufMapColorLine.asFloatBuffer();

        bufPosTri.bind();

        bufMapPosTri = bufPosTri.mapBuffer(MapAccess.WRITE_ONLY);
        bufMapPosTri.rewind();
        bufMapPosTriFloat = bufMapPosTri.asFloatBuffer();

        bufColorTri.bind();

        bufMapColorTri = bufColorTri.mapBuffer(MapAccess.WRITE_ONLY);
        bufMapColorTri.rewind();
        bufMapColorTriFloat = bufMapColorTri.asFloatBuffer();

        bufColorTri.unbind();
    }


    public void clear() {
        lines = 0;
        tris = 0;
    }


    public void unmapBuffers() {
        bufPosLine.bind();
        bufPosLine.unmapBuffer();

        bufColorLine.bind();
        bufColorLine.unmapBuffer();

        bufPosTri.bind();
        bufPosTri.unmapBuffer();

        bufColorTri.bind();
        bufColorTri.unmapBuffer();

        bufColorTri.unbind();

        bufMapPosLineFloat = null;
        bufMapColorLineFloat = null;
        bufMapPosTriFloat = null;
        bufMapColorTriFloat = null;

        lineMatMesh.setDrawRange(0, lines * 2);
        triMatMesh.setDrawRange(0, tris * 3);
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        lineMatMesh.dispose();
        triMatMesh.dispose();

        disposed = true;
    }

}
