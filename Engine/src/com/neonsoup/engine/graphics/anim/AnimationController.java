package com.neonsoup.engine.graphics.anim;

public abstract class AnimationController {

    protected final AnimationChannel channel;


    public AnimationController(AnimationChannel channel) {
        this.channel = channel;
    }


    public AnimationChannel getAnimationChannel() {
        return channel;
    }


    public abstract void update(AnimationChannel.Track track, float time);

}
