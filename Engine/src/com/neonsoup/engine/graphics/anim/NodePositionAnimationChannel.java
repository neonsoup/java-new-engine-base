package com.neonsoup.engine.graphics.anim;

import org.joml.Vector2f;
import org.joml.Vector3f;

public class NodePositionAnimationChannel extends AnimationChannel {

    protected class NodePositionAnimationTrack extends Track {

        protected final float fromX, fromY, fromZ;
        protected final float toX, toY, toZ;


        protected NodePositionAnimationTrack(float start,
                                             float end,
                                             float fromPosX,
                                             float fromPosY,
                                             float fromPosZ,
                                             float toPosX,
                                             float toPosY,
                                             float toPosZ) {

            super(start, end);

            this.fromX = fromPosX;
            this.fromY = fromPosY;
            this.fromZ = fromPosZ;

            this.toX = toPosX;
            this.toY = toPosY;
            this.toZ = toPosZ;
        }

    }


    public NodePositionAnimationChannel() {
    }


    public void addTrack(float start, float end, Vector2f fromPos, Vector2f toPos) {
        addTrack(start, end, fromPos.x, fromPos.y, 0.0f, toPos.x, toPos.y, 0.0f);
    }


    public void addTrack(float start, float end, float fromPosX, float fromPosY, float toPosX, float toPosY) {
        addTrack(start, end, fromPosX, fromPosY, 0.0f, toPosX, toPosY, 0.0f);
    }


    public void addTrack(float start, float end, Vector3f fromPos, Vector3f toPos) {
        addTrack(start, end, fromPos.x, fromPos.y, fromPos.z, toPos.x, toPos.y, toPos.z);
    }


    public void addTrack(float start,
                         float end,
                         float fromPosX,
                         float fromPosY,
                         float fromPosZ,
                         float toPosX,
                         float toPosY,
                         float toPosZ) {

        addTrack(new NodePositionAnimationTrack(start, end, fromPosX, fromPosY, fromPosZ, toPosX, toPosY, toPosZ));
    }

}
