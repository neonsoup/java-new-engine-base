package com.neonsoup.engine.graphics.anim;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public abstract class AnimationChannel {

    public abstract class Track {

        protected final float start;
        protected final float end;


        protected Track(float start, float end) {
            if (start > end) {
                throw new IllegalArgumentException("Track start must not be greater than track end");
            }
            if (start == end) {
                throw new IllegalArgumentException("Tracks of zero duration are not allowed");
            }

            this.start = start;
            this.end = end;
        }


        public float getStart() {
            return start;
        }


        public float getEnd() {
            return end;
        }

    }


    protected final List<Track> tracks = new LinkedList<>();
    protected float duration = 0.0f;


    public AnimationChannel() {
    }


    public float getDuration() {
        return duration;
    }


    protected void addTrack(Track track) {
        ListIterator<Track> it = tracks.listIterator();

        while (it.hasNext()) {
            Track next = it.next();

            if (next.start > track.start) {
                it.previous();
                break;
            }
        }

        it.add(track);

        duration = Math.max(duration, track.start + track.end);
    }

}
