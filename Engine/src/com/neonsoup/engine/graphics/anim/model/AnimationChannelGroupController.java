package com.neonsoup.engine.graphics.anim.model;

public interface AnimationChannelGroupController {

    void update(float[] values, int indexFrom, int len);

}
