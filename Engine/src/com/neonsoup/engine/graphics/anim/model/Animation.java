package com.neonsoup.engine.graphics.anim.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Animation {

    protected final String name;

    protected final float duration;

    protected final Map<String, AnimationChannelGroup> groups;

    protected final List<AnimationChannel> channels;


    public Animation(String name, AnimationChannelGroup[] groups, AnimationChannel[] channels, float duration) {
        this.groups = new HashMap<>(groups.length);
        this.channels = new ArrayList<>(channels.length);

        this.name = name;

        this.duration = duration;

        for (AnimationChannelGroup group : groups) {
            this.groups.put(group.name, group);
        }

        for (AnimationChannel track : channels) {
            this.channels.add(track);
        }
    }

}
