package com.neonsoup.engine.graphics.anim.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AnimationChannel {

    protected final String name;

    protected final List<AnimationKeyframe> keyframes;

    protected final int keyframeCount;

    public AnimationChannel(String name, AnimationKeyframe[] keyframes) {
        this.keyframeCount = keyframes.length;
        
        this.keyframes = new ArrayList<>(keyframes.length);

        for (AnimationKeyframe kf : keyframes) {
            this.keyframes.add(kf);
        }

        Collections.sort(this.keyframes,
                         (AnimationKeyframe k1, AnimationKeyframe k2) -> Float.compare(k1.time, k2.time));

        this.name = name;
    }

}
