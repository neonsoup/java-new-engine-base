package com.neonsoup.engine.graphics.anim.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import gnu.trove.impl.Constants;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;

public class AnimationSet {

    protected final static class AnimationRecord {

        protected final Animation animation;

        protected final int[] channels;
        protected final int[] groups;


        protected AnimationRecord(Animation animation, int[] channels, int[] groups) {
            this.animation = animation;
            this.channels = channels;
            this.groups = groups;
        }

    }


    protected final Map<String, Animation> animations;
    protected final Map<String, AnimationRecord> animationRecords;

    protected final TObjectIntMap<String> groupIndices = new TObjectIntHashMap<>(Constants.DEFAULT_CAPACITY,
                                                                                 Constants.DEFAULT_LOAD_FACTOR,
                                                                                 -1);

    protected final AnimationChannel[] channels;
    protected final AnimationChannelGroup[] groups;

    protected final int[] groupChannelBeginIndices;


    public AnimationSet(Animation[] animations) {
        this.animations = new HashMap<>(animations.length);
        this.animationRecords = new HashMap<>(animations.length);

        int channelCount = 0;
        int groupCount = 0;

        List<AnimationChannel> totalChannels = new ArrayList<>();
        Set<AnimationChannelGroup> totalGroups = new HashSet<>();

        TIntList totalGroupChannelBeginIndices = new TIntArrayList();

        for (Animation anim : animations) {
            this.animations.put(anim.name, anim);

            TIntList animationChannelIndices = new TIntArrayList();
            TIntList animationGroupIndices = new TIntArrayList();

            for (AnimationChannelGroup group : anim.groups.values()) {
                int size = group.channels.size();

                if (totalGroups.add(group)) {
                    animationGroupIndices.add(groupCount);

                    groupIndices.put(group.name, groupCount++);

                    totalGroupChannelBeginIndices.add(channelCount);

                    for (int i = 0; i < size; i++, channelCount++) {
                        animationChannelIndices.add(channelCount);
                    }

                    for (AnimationChannel chan : group.channels) {
                        totalChannels.add(chan);
                    }
                } else {
                    animationGroupIndices.add(groupIndices.get(group.name));
                }
            }

            animationRecords
                    .put(anim.name,
                         new AnimationRecord(anim, animationChannelIndices.toArray(), animationGroupIndices.toArray()));
        }

        this.channels = totalChannels.toArray(new AnimationChannel[totalChannels.size()]);
        this.groups = totalGroups.toArray(new AnimationChannelGroup[totalGroups.size()]);
        this.groupChannelBeginIndices = totalGroupChannelBeginIndices.toArray();
    }

}
