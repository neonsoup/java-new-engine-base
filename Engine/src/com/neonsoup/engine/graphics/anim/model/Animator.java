
package com.neonsoup.engine.graphics.anim.model;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import com.neonsoup.common.util.NoModCheckLinkedList;
import com.neonsoup.engine.graphics.anim.model.AnimationSet.AnimationRecord;

public class Animator {

    protected final AnimationSet animSet;

    protected final float[] values;

    protected final int[] currentChannelKeyframes;

    protected AnimationRecord currentLoopAnimation = null;

    protected float currentAnimTime = 0.0f;

    protected final List<AnimationChannelGroupController>[] groupControllers;


    @SuppressWarnings("unchecked")
    public Animator(AnimationSet animSet) {
        this.animSet = animSet;

        this.values = new float[animSet.channels.length];

        this.currentChannelKeyframes = new int[animSet.channels.length];
        Arrays.fill(currentChannelKeyframes, -1);

        this.groupControllers = (List<AnimationChannelGroupController>[]) new List[animSet.groups.length];
        for (int i = 0; i < animSet.groups.length; i++) {
            this.groupControllers[i] = new NoModCheckLinkedList<>();
        }
    }


    public void addChannelGroupController(String groupName, AnimationChannelGroupController controller) {
        if (controller == null) {
            throw new IllegalArgumentException("controller must not be null");
        }

        int idx = animSet.groupIndices.get(groupName);

        if (idx == -1) {
            throw new NoSuchElementException("No animation channel group named \"" + groupName + "\" was found");
        }

        if (!groupControllers[idx].contains(controller)) {
            groupControllers[idx].add(controller);
        }
    }


    public void removeChannelGroupController(String groupName, AnimationChannelGroupController controller) {
        if (controller == null) {
            return;
        }

        int idx = animSet.groupIndices.get(groupName);

        if (idx == -1) {
            throw new NoSuchElementException("No animation channel group named \"" + groupName + "\" was found");
        }

        groupControllers[idx].remove(controller);
    }


    public void loopAnimation(String animName) {
        currentLoopAnimation = animSet.animationRecords.get(animName);

        if (currentLoopAnimation == null) {
            throw new NoSuchElementException("No animation named \"" + animName + "\" was found");
        }

        currentAnimTime = 0.0f;

        Arrays.fill(currentChannelKeyframes, -1);

        for (int i = 0; i < currentLoopAnimation.channels.length; i++) {
            int chanIdx = currentLoopAnimation.channels[i];

            AnimationChannel chan = animSet.channels[chanIdx];

            if (chan.keyframeCount > 1) {
                AnimationKeyframe kf = chan.keyframes.get(0);

                if (kf.time <= 0.0f) {
                    int idx = 0;

                    while (idx < chan.keyframeCount) {
                        AnimationKeyframe nextKf = chan.keyframes.get(idx + 1);

                        if (nextKf.time > 0.0f) {
                            break;
                        }

                        kf = nextKf;
                        idx++;
                    }

                    if (idx < chan.keyframeCount) {
                        values[chanIdx] = kf.value;
                    }

                    currentChannelKeyframes[chanIdx] = idx;
                }
            } else if (chan.keyframeCount == 1) {
                AnimationKeyframe kf = chan.keyframes.get(0);

                if (kf.time <= 0.0f) {
                    values[chanIdx] = kf.value;

                    currentChannelKeyframes[chanIdx] = 0;
                }
            }
        }
    }


    public void update(float dt) {
        if (currentLoopAnimation == null) {
            return;
        }

        float duration = currentLoopAnimation.animation.duration;

        if (dt + currentAnimTime >= duration) {
            float timeLeft = dt - (duration - currentAnimTime);

            for (int i = 0; i < currentLoopAnimation.channels.length; i++) {
                int chanIdx = currentLoopAnimation.channels[i];

                AnimationChannel chan = animSet.channels[chanIdx];

                if (chan.keyframeCount != 0) {
                    AnimationKeyframe kf = chan.keyframes.get(chan.keyframeCount - 1);

                    values[chanIdx] = kf.value;
                    currentChannelKeyframes[chanIdx] = -1;
                }
            }

            currentAnimTime = timeLeft % duration;
        }

        currentAnimTime += dt;

        for (int i = 0; i < currentLoopAnimation.channels.length; i++) {
            int chanIdx = currentLoopAnimation.channels[i];

            AnimationChannel chan = animSet.channels[chanIdx];

            if (chan.keyframeCount == 1) {
                AnimationKeyframe kf = chan.keyframes.get(0);

                if (currentAnimTime >= kf.time) {
                    values[chanIdx] = kf.value;
                    currentChannelKeyframes[chanIdx] = 0;
                }
            } else if (chan.keyframeCount > 1) {
                int currentKfIdx = currentChannelKeyframes[chanIdx];

                if (currentKfIdx == chan.keyframeCount) {
                    values[chanIdx] = chan.keyframes.get(chan.keyframeCount - 1).value;

                    continue;
                }

                int nextKfIdx = currentKfIdx + 1;

                AnimationKeyframe nextKf = null;

                while (nextKfIdx < chan.keyframeCount) {
                    nextKf = chan.keyframes.get(nextKfIdx);

                    if (nextKf.time > currentAnimTime) {
                        break;
                    }

                    currentKfIdx++;
                    nextKfIdx++;
                }

                if (currentKfIdx == -1) {
                    continue;
                } else if (nextKfIdx < chan.keyframeCount) {
                    AnimationKeyframe currentKf = chan.keyframes.get(currentKfIdx);

                    values[chanIdx] = currentKf.interpolate(currentAnimTime, nextKf);
                    currentChannelKeyframes[chanIdx] = currentKfIdx;
                } else {
                    values[chanIdx] = chan.keyframes.get(chan.keyframeCount - 1).value;
                    currentChannelKeyframes[chanIdx] = chan.keyframeCount;
                }
            }
        }

        for (int i = 0; i < currentLoopAnimation.groups.length; i++) {
            int groupIdx = currentLoopAnimation.groups[i];

            AnimationChannelGroup group = animSet.groups[groupIdx];

            for (AnimationChannelGroupController controller : groupControllers[groupIdx]) {
                controller.update(values, animSet.groupChannelBeginIndices[groupIdx], group.channels.size());
            }
        }
    }

}
