package com.neonsoup.engine.graphics.anim.model;

import java.util.ArrayList;
import java.util.List;

public class AnimationChannelGroup {

    protected final String name;

    protected final List<AnimationChannel> channels;


    public AnimationChannelGroup(String name, AnimationChannel[] channels) {
        this.name = name;

        this.channels = new ArrayList<>(channels.length);
        for (AnimationChannel chan : channels) {
            this.channels.add(chan);
        }
    }


    public String getName() {
        return name;
    }

}
