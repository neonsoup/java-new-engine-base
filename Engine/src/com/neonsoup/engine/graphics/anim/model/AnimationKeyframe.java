package com.neonsoup.engine.graphics.anim.model;

public class AnimationKeyframe {

    public final float time;
    public final float value;


    public AnimationKeyframe(float time, float value) {
        this.time = time;
        this.value = value;
    }


    public float interpolate(float currentTime, AnimationKeyframe next) {
        float dv = next.value - value;
        float frac = (currentTime - time) / (next.time - time);

        return value + dv * frac;
    }

}
