package com.neonsoup.engine.graphics.anim;

public class TriggerAnimationChannel<T> extends AnimationChannel {

    protected class TriggerAnimationTrack extends Track {

        protected final T data;


        protected TriggerAnimationTrack(float start, float duration, T data) {
            super(start, duration);

            this.data = data;
        }
    }


    public TriggerAnimationChannel() {
    }


    public void addTrack(float start, float end, T data) {
        addTrack(new TriggerAnimationTrack(start, end, data));
    }

}
