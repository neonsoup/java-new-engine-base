package com.neonsoup.engine.graphics.anim;

import com.neonsoup.engine.graphics.anim.AnimationChannel.Track;
import com.neonsoup.engine.scene.SceneNode;

public class NodePositionAnimationController extends AnimationController {

    protected final SceneNode node;


    public NodePositionAnimationController(AnimationChannel channel, SceneNode node) {
        super(channel);

        this.node = node;
    }


    @Override
    public void update(Track track, float time) {
        float start = track.getStart();
        float end = track.getEnd();

        NodePositionAnimationChannel.NodePositionAnimationTrack animTrack;
        animTrack = (NodePositionAnimationChannel.NodePositionAnimationTrack) track;

        float frac = (time - start) / (end - start);

        if (Float.isFinite(frac)) {
            node.setTranslation((animTrack.fromX + (animTrack.toX - animTrack.fromX) * frac),
                                (animTrack.fromY + (animTrack.toY - animTrack.fromY) * frac),
                                (animTrack.fromZ + (animTrack.toZ - animTrack.fromZ) * frac));
        } else {
            node.setTranslation(animTrack.toX, animTrack.toY, animTrack.toZ);
        }
    }

}
