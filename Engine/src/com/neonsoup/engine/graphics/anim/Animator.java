package com.neonsoup.engine.graphics.anim;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;

public class Animator {

    public static enum Mode {
        STOP, PLAY_ONCE, LOOP, LOOP_REGION, PLAY_FROM_START_TO_REGION_END_THEN_LOOP_REGION;
    }


    private static final int STATUS_NOT_STARTED = 0;
    private static final int STATUS_PLAYING = 1;
    private static final int STATUS_FINISHED = 2;

    private List<AnimationController> channelControllers = new ArrayList<>();

    private float maxDuration = 0.0f;

    private float currentTime = 0.0f;

    private List<ListIterator<AnimationChannel.Track>> iterators = new ArrayList<>();

    private List<AnimationChannel.Track> currentTracks = new ArrayList<>();
    private TIntList currentTrackStatuses = new TIntArrayList();

    private float regionStart = 0.0f;
    private float regionEnd = -1.0f;

    private boolean firstLoop = false;

    private Mode mode = Mode.STOP;


    public Animator() {
    }


    public void addChannelController(AnimationController channelController) {
        if (mode != Mode.STOP) {
            throw new IllegalStateException("Can not modify animator in not stopped state");
        }

        if (!channelControllers.contains(channelController)) {
            channelControllers.add(channelController);

            iterators.add(null);

            currentTracks.add(null);
            currentTrackStatuses.add(STATUS_NOT_STARTED);

            maxDuration = Math.max(maxDuration, channelController.channel.duration);
        } else {
            throw new IllegalArgumentException();
        }
    }


    public void removeChannelController(AnimationController channelController) {
        if (mode != Mode.STOP) {
            throw new IllegalStateException("Can not modify animator in not stopped state");
        }

        int idx = channelControllers.indexOf(channelController);

        channelControllers.remove(idx);
        iterators.remove(idx);
        currentTracks.remove(idx);
        currentTrackStatuses.removeAt(idx);

        if (maxDuration == channelController.channel.duration) {
            maxDuration = 0.0f;

            for (AnimationController controllerIt : channelControllers) {
                maxDuration = Math.max(maxDuration, controllerIt.channel.duration);
            }
        }
    }


    public void setMode(Mode mode) {
        reset(mode, 0.0f, -1.0f);
    }


    public void setMode(Mode mode, float regionStart, float regionEnd) {
        reset(mode, regionStart, regionEnd);
    }


    public Mode getMode() {
        return mode;
    }


    @SuppressWarnings("incomplete-switch")
    protected void reset(Mode mode, float regionStart, float regionEnd) {
        switch (mode) {
            case LOOP_REGION:
            case PLAY_FROM_START_TO_REGION_END_THEN_LOOP_REGION: {
                if (regionEnd < regionStart) {
                    throw new IllegalArgumentException("Region start end must be greater than or equal to region start");
                }
            }
        }

        this.regionStart = regionStart;
        this.regionEnd = regionEnd;

        this.firstLoop = true;

        switch (mode) {
            case LOOP:
            case PLAY_ONCE:
            case PLAY_FROM_START_TO_REGION_END_THEN_LOOP_REGION: {
                currentTime = 0.0f;

                for (int i = 0; i < channelControllers.size(); i++) {
                    iterators.set(i, channelControllers.get(i).channel.tracks.listIterator());
                    currentTracks.set(i, null);
                    currentTrackStatuses.set(i, STATUS_NOT_STARTED);
                }

                break;
            }

            case LOOP_REGION: {
                initLoopRegion();

                break;
            }

            case STOP: {
                currentTime = 0.0f;

                for (int i = 0; i < channelControllers.size(); i++) {
                    iterators.set(i, null);
                    currentTracks.set(i, null);
                    currentTrackStatuses.set(i, STATUS_NOT_STARTED);
                }

                break;
            }

            default:
                throw new IllegalArgumentException();
        }

        this.mode = mode;
    }


    private void initLoopRegion() {
        currentTime = regionStart;

        for (int i = 0; i < channelControllers.size(); i++) {
            ListIterator<AnimationChannel.Track> it = channelControllers.get(i).channel.tracks.listIterator();

            iterators.set(i, it);

            while (it.hasNext()) {
                AnimationChannel.Track track = it.next();

                if (track.end > regionStart) {
                    it.previous();
                    break;
                }
            }

            currentTracks.set(i, null);
            currentTrackStatuses.set(i, STATUS_NOT_STARTED);
        }

    }


    public float update(float dt) {
        switch (mode) {
            case LOOP:
            case LOOP_REGION:
            case PLAY_FROM_START_TO_REGION_END_THEN_LOOP_REGION: {
                float leftDt = dt;

                while (leftDt > 0.0f) {
                    float cycleDt = updateCycle(leftDt);

                    if (leftDt - cycleDt >= leftDt) {
                        return 0.0f;
                    }

                    leftDt -= cycleDt;
                }

                return dt;
            }

            case PLAY_ONCE:
                return updateCycle(dt);

            case STOP:
                return 0.0f;

            default:
                throw new RuntimeException();
        }
    }


    @SuppressWarnings("incomplete-switch")
    protected float updateCycle(float dt) {
        if (mode == Mode.STOP) {
            return dt;
        }

        float newTime = Math.min(maxDuration, currentTime + dt);

        switch (mode) {
            case LOOP_REGION:
            case PLAY_FROM_START_TO_REGION_END_THEN_LOOP_REGION: {
                newTime = Math.min(newTime, regionEnd);

                break;
            }
        }

        loop: for (int i = 0; i < channelControllers.size(); i++) {
            AnimationChannel.Track currentTrack = currentTracks.get(i);

            float currentTime = this.currentTime;
            float leftDt = newTime - currentTime;
            boolean switchToNextTrack = false;

            while (leftDt > 0.0f || switchToNextTrack) {

                if (switchToNextTrack || currentTrack == null) {
                    ListIterator<AnimationChannel.Track> it = iterators.get(i);

                    if (it.hasNext()) {
                        currentTrack = it.next();

                        currentTrackStatuses.set(i, STATUS_NOT_STARTED);

                    } else {
                        currentTrack = null;

                        currentTrackStatuses.set(i, STATUS_FINISHED);

                        break;
                    }
                }

                switchToNextTrack = false;

                switch (mode) {
                    case LOOP_REGION:
                    case PLAY_FROM_START_TO_REGION_END_THEN_LOOP_REGION: {
                        if (currentTrack.start >= regionEnd) {
                            break loop;
                        }

                        break;
                    }
                }

                if (currentTrack.start > newTime) {
                    break;
                }

                if (currentTrack.end >= currentTime) {
                    int status = currentTrackStatuses.get(i);

                    if (status == STATUS_NOT_STARTED) {
                        currentTrackStatuses.set(i, STATUS_PLAYING);
                    }
                }

                if (currentTrackStatuses.get(i) != STATUS_PLAYING) {
                    break;
                }

                {
                    ListIterator<AnimationChannel.Track> it = iterators.get(i);

                    float trackDt;
                    AnimationChannel.Track nextTrack;

                    if (it.hasNext()) {
                        nextTrack = it.next();

                        if (nextTrack.start <= newTime) {
                            switchToNextTrack = true;
                            trackDt = Math.min(Math.max(nextTrack.start - currentTime, 0.0f), leftDt);
                        } else {
                            trackDt = leftDt;
                        }

                        it.previous();
                    } else {
                        trackDt = leftDt;
                    }

                    {
                        float time = currentTime + trackDt;
                        time = Math.max(currentTrack.start, Math.min(currentTrack.end, time));

                        channelControllers.get(i).update(currentTrack, time);
                    }

                    if (newTime >= currentTrack.end) {
                        switchToNextTrack = true;
                    }

                    leftDt -= trackDt;
                    currentTime += trackDt;
                }

                switch (mode) {
                    case PLAY_ONCE:
                    case LOOP: {
                        if (newTime >= maxDuration) {
                            switchToNextTrack = true;
                        }

                        break;
                    }

                    case LOOP_REGION:
                    case PLAY_FROM_START_TO_REGION_END_THEN_LOOP_REGION: {
                        if (newTime >= regionEnd) {
                            switchToNextTrack = true;
                        }

                        break;
                    }
                }

            }

            currentTracks.set(i, currentTrack);
        }

        switch (mode) {
            case PLAY_ONCE:
            case LOOP: {
                if (maxDuration <= 0.0f) {
                    currentTime = 0.0f;

                    if (mode == Mode.PLAY_ONCE) {
                        reset(Mode.STOP, regionStart, regionEnd);
                    } else {
                        reset(mode, regionStart, regionEnd);
                    }

                    return 0.0f;
                }

                if (currentTime + dt >= maxDuration) {
                    currentTime = 0.0f;

                    if (mode == Mode.PLAY_ONCE) {
                        reset(Mode.STOP, regionStart, regionEnd);
                    } else {
                        reset(mode, regionStart, regionEnd);
                    }

                    return currentTime + dt - maxDuration;
                }

                currentTime += dt;

                return dt;
            }

            case LOOP_REGION: {
                if (regionEnd - regionStart <= 0.0f) {
                    currentTime = 0.0f;

                    reset(mode, regionStart, regionEnd);

                    return 0.0f;
                }

                if (currentTime + dt >= regionEnd) {
                    currentTime = 0.0f;

                    reset(mode, regionStart, regionEnd);

                    return currentTime + dt - regionEnd;
                }

                currentTime += dt;

                return dt;
            }

            case PLAY_FROM_START_TO_REGION_END_THEN_LOOP_REGION: {
                if (firstLoop ? (regionEnd <= 0.0f) : (regionEnd - regionStart <= 0.0f)) {
                    currentTime = 0.0f;

                    firstLoop = false;

                    initLoopRegion();

                    return 0.0f;
                }

                if (currentTime + dt >= regionEnd) {
                    currentTime = 0.0f;

                    firstLoop = false;

                    initLoopRegion();

                    return currentTime + dt - regionEnd;
                }

                currentTime += dt;

                return dt;
            }

            default:
                throw new RuntimeException();
        }
    }

}
