package com.neonsoup.engine.graphics.anim.sprite;

import com.neonsoup.engine.graphics.Sprite;
import com.neonsoup.engine.graphics.anim.AnimationChannel;
import com.neonsoup.engine.graphics.anim.AnimationChannel.Track;
import com.neonsoup.engine.graphics.anim.AnimationController;

public class SpriteAnimationController extends AnimationController {

    protected final Sprite sprite;


    public SpriteAnimationController(AnimationChannel channel, Sprite sprite) {
        super(channel);

        this.sprite = sprite;
    }


    @Override
    public void update(Track track, float time) {
        float start = track.getStart();
        float end = track.getEnd();

        SpriteAnimationChannel.SpriteAnimationTrack animTrack = (SpriteAnimationChannel.SpriteAnimationTrack) track;

        float timeWithinTrack = time - start;

        int frame = animTrack.fromFrame
                    + (int) ((animTrack.toFrame - animTrack.fromFrame + 1) * (timeWithinTrack / (end - start)));

        frame = Math.min(animTrack.toFrame, Math.max(frame, animTrack.fromFrame));

        animTrack.element.setDiffuseTextureCoords(sprite, frame, animTrack.mirrorX, animTrack.mirrorY);
    }

}
