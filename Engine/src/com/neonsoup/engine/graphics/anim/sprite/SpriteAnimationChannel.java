package com.neonsoup.engine.graphics.anim.sprite;

import com.neonsoup.engine.graphics.TextureAtlas;
import com.neonsoup.engine.graphics.anim.AnimationChannel;

public class SpriteAnimationChannel extends AnimationChannel {

    protected class SpriteAnimationTrack extends Track {

        protected final String animationName;

        protected final TextureAtlas.Element element;

        protected final int fromFrame, toFrame;

        protected final boolean mirrorX, mirrorY;


        protected SpriteAnimationTrack(float start, float end, String animationName, boolean mirrorX, boolean mirrorY) {

            super(start, end);

            this.animationName = animationName;
            this.element = atlas.getElement(animationName);

            this.fromFrame = 0;
            this.toFrame = element.getFrameCount() - 1;

            this.mirrorX = mirrorX;
            this.mirrorY = mirrorY;
        }


        protected SpriteAnimationTrack(float start,
                                       float end,
                                       String animationName,
                                       int fromFrame,
                                       int toFrame,
                                       boolean mirrorX,
                                       boolean mirrorY) {

            super(start, end);

            this.animationName = animationName;
            this.element = atlas.getElement(animationName);

            this.fromFrame = fromFrame;
            this.toFrame = toFrame;

            this.mirrorX = mirrorX;
            this.mirrorY = mirrorY;
        }

    }


    protected final TextureAtlas atlas;


    public SpriteAnimationChannel(TextureAtlas atlas) {
        this.atlas = atlas;
    }


    public void addTrack(float start, float end, String animationName) {
        this.addTrack(new SpriteAnimationTrack(start, end, animationName, false, false));
    }


    public void addTrack(float start, float end, String animationName, int fromFrame, int toFrame) {
        this.addTrack(new SpriteAnimationTrack(start, end, animationName, fromFrame, toFrame, false, false));
    }


    public void addTrack(float start, float end, String animationName, boolean mirrorX, boolean mirrorY) {
        this.addTrack(new SpriteAnimationTrack(start, end, animationName, mirrorX, mirrorY));
    }


    public void addTrack(float start,
                         float end,
                         String animationName,
                         int fromFrame,
                         int toFrame,
                         boolean mirrorX,
                         boolean mirrorY) {

        this.addTrack(new SpriteAnimationTrack(start, end, animationName, fromFrame, toFrame, mirrorX, mirrorY));
    }

}
