package com.neonsoup.engine.graphics;

import com.neonsoup.engine.graphics.gl.GLStateProperties;
import com.neonsoup.engine.scene.Scene;

public class RenderTechnique {

    private final String name;
    private final int id;
    private GLStateProperties renderStateProps = new GLStateProperties();


    public RenderTechnique(String name, int id) {
        this.name = name;
        this.id = id;
    }


    public int getId() {
        return id;
    }


    public GLStateProperties getRenderStateProperties() {
        return renderStateProps;
    }


    public void drawRecursive(SceneRenderer renderer, Scene scene, RenderPass renderPass) {
        renderStateProps.beginDraw(renderer.getRenderStateProperties());

        scene.drawRecursive(renderer, this, renderPass);

        renderStateProps.endDraw();
    }


    public void beginDraw(SceneRenderer renderer, Scene scene) {
        renderStateProps.beginDraw(renderer.getRenderStateProperties());
    }


    public void endDraw() {
        renderStateProps.endDraw();
    }


    @Override
    public String toString() {
        return "RenderTechnique [Name: \"" + name + "\"; id: " + id + "]";
    }

}
