package com.neonsoup.engine.graphics.material;

public interface MaterialFeatureKey {

    String getName();


    @Override
    int hashCode();


    @Override
    boolean equals(Object obj);

}
