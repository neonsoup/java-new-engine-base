package com.neonsoup.engine.graphics.material;

public abstract class AbstractMaterialFeatureKey implements MaterialFeatureKey {

    protected final String name;
    protected final String upperCaseName;


    public AbstractMaterialFeatureKey(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name must not be null");
        }

        this.name = name;
        this.upperCaseName = name.toUpperCase();
    }


    @Override
    public String getName() {
        return name;
    }

}
