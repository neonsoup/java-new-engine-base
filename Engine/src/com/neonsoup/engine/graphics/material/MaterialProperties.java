package com.neonsoup.engine.graphics.material;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;

public abstract class MaterialProperties extends AbstractDisposable {

    protected final int techniques;

    protected Material material;


    public MaterialProperties(Material material, int techniqueBits) {
        this.techniques = techniqueBits;
        this.material = material;
    }


    public Material getMaterial() {
        return material;
    }


    public int getTechniqueBits() {
        return techniques;
    }


    public abstract void setProperties(SceneRenderer renderer, RenderTechnique technique);


    public abstract void unsetProperties(SceneRenderer renderer, RenderTechnique technique);

}
