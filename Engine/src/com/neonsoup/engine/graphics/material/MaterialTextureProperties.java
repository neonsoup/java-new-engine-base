package com.neonsoup.engine.graphics.material;

import java.util.Arrays;

import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;
import com.neonsoup.engine.graphics.Texture;
import com.neonsoup.engine.graphics.gl.GLTexture;

public class MaterialTextureProperties extends MaterialProperties {

    private final String[] uniformNames;
    private final Texture[] textures;
    private final int[] textureUnits;

    private final int[][] uniformLocations;


    public MaterialTextureProperties(Material material,
                                     int techniques,
                                     String uniformName,
                                     Texture texture,
                                     int textureUnit) {

        super(material, techniques);

        this.uniformNames = new String[] { uniformName };
        this.textures = new Texture[] { texture };
        this.textureUnits = new int[] { textureUnit };

        int minTechCount = Math.min(material.getTechniqueCount(), 32 - Integer.numberOfLeadingZeros(techniques));

        this.uniformLocations = new int[minTechCount][1];

        linkUniformLocations();
    }


    public MaterialTextureProperties(Material material,
                                     int techniques,
                                     String[] uniformNames,
                                     Texture[] textures,
                                     int[] textureUnits) {

        super(material, techniques);

        this.uniformNames = Arrays.copyOf(uniformNames, uniformNames.length);
        this.textures = Arrays.copyOf(textures, textures.length);
        this.textureUnits = Arrays.copyOf(textureUnits, textureUnits.length);

        int minTechCount = Math.min(material.getTechniqueCount(), 32 - Integer.numberOfLeadingZeros(techniques));

        this.uniformLocations = new int[minTechCount][uniformNames.length];

        linkUniformLocations();
    }


    private void linkUniformLocations() {
        int t = techniques;
        int ti = 0;

        while (t != 0) {
            if ((t & 1) != 0) {
                int[] uniformLocations = this.uniformLocations[ti];

                for (int i = 0; i < uniformNames.length; i++) {
                    uniformLocations[i] = material.getShader(ti).getUniformLocation(uniformNames[i]);
                }
            }

            t >>>= 1;
            ti++;
        }

    }


    @Override
    public void setProperties(SceneRenderer renderer, RenderTechnique technique) {
        int techniqueId = technique.getId();

        int[] uniformLocations = this.uniformLocations[techniqueId];

        for (int i = 0; i < uniformNames.length; i++) {
            GLTexture.activeTexture(textureUnits[i]);

            textures[i].getGLTexture().bind();

            material.getShader(techniqueId).uniform1i(uniformLocations[i], textureUnits[i]);
        }
    }


    @Override
    public void unsetProperties(SceneRenderer renderer, RenderTechnique technique) {
        for (int i = 0; i < uniformNames.length; i++) {
            GLTexture.activeTexture(textureUnits[i]);

            textures[i].getGLTexture().unbind();
        }
    }

}
