package com.neonsoup.engine.graphics.material;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;
import com.neonsoup.engine.graphics.gl.GLShaderProgram;
import com.neonsoup.engine.graphics.gl.GLStateProperties;
import com.neonsoup.engine.scene.Scene;
import com.neonsoup.engine.scene.SceneNode;

public class Material extends AbstractDisposable {

    public static final String NODE_MODEL_MATRIX_UNIFORM_NAME = "u_ModelMatrix";
    public static final String NODE_VIEW_MATRIX_UNIFORM_NAME = "u_ViewMatrix";
    public static final String NODE_PROJECTION_MATRIX_UNIFORM_NAME = "u_ProjectionMatrix";

    protected final int techniques;

    protected final GLShaderProgram[] techniqueShaders;

    protected final GLStateProperties[] renderStateProperties;

    protected final int[] modelMatrixUniform;
    protected final int[] viewMatrixUniform;
    protected final int[] projectionMatrixUniform;

    protected final Set<MaterialFeatureKey> features;


    public Material(int techniques,
                    GLStateProperties[] techniqueRenderStateProperties,
                    GLShaderProgram[] techniqueShaders,
                    Collection<MaterialFeatureKey> features) {

        this.techniques = techniques;

        int techniqueCount = 32 - Integer.numberOfLeadingZeros(techniques);

        this.techniqueShaders = new GLShaderProgram[techniqueCount];

        modelMatrixUniform = new int[techniqueCount];
        viewMatrixUniform = new int[techniqueCount];
        projectionMatrixUniform = new int[techniqueCount];

        renderStateProperties = new GLStateProperties[techniqueCount];

        for (int i = 0; i < techniqueCount; i++) {
            if (((1 << i) & techniques) == 0) {
                continue;
            }

            this.techniqueShaders[i] = techniqueShaders[i];

            GLShaderProgram shader = techniqueShaders[i];

            if (shader != null) {
                modelMatrixUniform[i] = shader.getUniformLocation(NODE_MODEL_MATRIX_UNIFORM_NAME);
                viewMatrixUniform[i] = shader.getUniformLocation(NODE_VIEW_MATRIX_UNIFORM_NAME);
                projectionMatrixUniform[i] = shader.getUniformLocation(NODE_PROJECTION_MATRIX_UNIFORM_NAME);
            }

            renderStateProperties[i] = techniqueRenderStateProperties[i];
        }

        this.features = new HashSet<>(features);
    }


    public int getTechniqueBits() {
        return techniques;
    }


    public int getTechniqueCount() {
        return techniqueShaders.length;
    }


    public GLShaderProgram getShader(int technique) {
        return techniqueShaders[technique];
    }


    public GLStateProperties getRenderStateProperties(int technique) {
        return renderStateProperties[technique];
    }


    public void beginDraw(SceneRenderer renderer, RenderTechnique technique, Scene scene, SceneNode sceneNode) {
        this.getRenderStateProperties(technique.getId()).beginDraw(technique.getRenderStateProperties());

        setNodeUniforms(renderer, technique, scene, sceneNode);
    }


    public void endDraw(SceneRenderer renderer, RenderTechnique technique, Scene scene, SceneNode sceneNode) {
        this.getRenderStateProperties(technique.getId()).endDraw();
    }


    public void setNodeUniforms(SceneRenderer renderer, RenderTechnique technique, Scene scene, SceneNode sceneNode) {
        ensureUndisposed();

        int techniqueId = technique.getId();

        GLShaderProgram shader = techniqueShaders[techniqueId];

        int location;

        if ((location = modelMatrixUniform[techniqueId]) != -1) {
            shader.uniformMatrix4f(location, sceneNode.getGlobalTransformMatrix());
        }

        if ((location = viewMatrixUniform[techniqueId]) != -1) {
            shader.uniformMatrix4f(location, scene.getRenderCameraNode().getCamera().getViewMatrix());
        }

        if ((location = projectionMatrixUniform[techniqueId]) != -1) {
            shader.uniformMatrix4f(location, scene.getRenderCameraNode().getCamera().getProjectionMatrix());
        }
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        for (int i = 0; i < techniqueShaders.length; i++) {
            GLShaderProgram shader = techniqueShaders[i];

            if (shader != null) {
                shader.dispose();
            }
        }

        disposed = true;
    }

}
