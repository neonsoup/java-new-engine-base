package com.neonsoup.engine.graphics.material;

public class StringMaterialFeatureKey extends AbstractMaterialFeatureKey {

    public StringMaterialFeatureKey(String name) {
        super(name);
    }


    @Override
    public int hashCode() {
        return upperCaseName.hashCode();
    }


    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof StringMaterialFeatureKey)) {
            return false;
        }

        StringMaterialFeatureKey key = (StringMaterialFeatureKey) obj;

        return key.upperCaseName.equals(this.upperCaseName);
    }


    @Override
    public String toString() {
        return name;
    }

}
