package com.neonsoup.engine.graphics;

import static org.lwjgl.opengl.GL11.*;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.Disposable;
import com.neonsoup.engine.ExternalDisposable;
import com.neonsoup.engine.graphics.gl.GLTexture;
import com.neonsoup.engine.graphics.gl.GLTexture.Filter;
import com.neonsoup.engine.graphics.gl.GLTexture.InternalFormat;

public abstract class Texture extends AbstractDisposable implements ExternalDisposable {

    protected GLTexture glTexture;
    protected Filter minFilter = Filter.DEFAULT_MIN, magFilter = Filter.DEFAULT_MAG;


    public Texture() {
    }


    public GLTexture getGLTexture() {
        return glTexture;
    }


    public boolean hasAlpha() {
        InternalFormat format = glTexture.getInternalFormat();
        return format.isColor() && format.colorHasAlpha();
    }


    public Filter getMinFilter() {
        return minFilter;
    }


    public void setMinFilter(Filter minFilter) {
        glTexture.preSetMinFilter(minFilter);
        this.minFilter = minFilter;
    }


    public Filter getMagFilter() {
        return magFilter;
    }


    public void setMagFilter(Filter magFilter) {
        glTexture.preSetMagFilter(magFilter);
        this.magFilter = magFilter;
    }


    public void generateMipmap() {
        if (minFilter.isMipmap()) {

            glTexture.bind();

            try {
                glTexture.generateMipmap();
            } finally {
                glTexture.unbind();
            }
        } else {
            throw new IllegalStateException();
        }
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        glTexture.dispose();

        disposed = true;
    }


    protected static class Disposer extends AbstractDisposable {

        private final int id;


        protected Disposer(int id) {
            this.id = id;
        }


        @Override
        public void dispose() {
            if (disposed) {
                return;
            }

            glDeleteTextures(id);

            disposed = true;
        }

    }


    @Override
    public Disposable getExternalDisposer() {
        return new Disposer(glTexture.getGlTextureHandle());
    }

}
