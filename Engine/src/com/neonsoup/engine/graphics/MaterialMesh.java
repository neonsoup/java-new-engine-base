package com.neonsoup.engine.graphics;

import java.util.ArrayList;
import java.util.List;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.graphics.gl.GLMesh;
import com.neonsoup.engine.graphics.gl.GLMeshBinding;
import com.neonsoup.engine.graphics.material.Material;
import com.neonsoup.engine.graphics.material.MaterialProperties;
import com.neonsoup.engine.res.ResourceHandle;
import com.neonsoup.engine.scene.Scene;
import com.neonsoup.engine.scene.SceneNode;

public class MaterialMesh extends AbstractDisposable {

    protected final GLMesh mesh;

    protected int techniqueBits;

    protected ResourceHandle<Material> materialHandle = null;
    protected final Material material;
    protected final List<MaterialProperties> materialProperties;

    protected final GLMeshBinding[] meshBindings;

    protected int from, count;


    public MaterialMesh(GLMesh mesh, ResourceHandle<Material> materialHandle, MaterialProperties[] materialProperties) {
        this(mesh, materialHandle.getResourceObject(), materialProperties);

        this.materialHandle = materialHandle.copy();
    }


    public MaterialMesh(GLMesh mesh, Material material, MaterialProperties[] materialProperties) {
        this.mesh = mesh;

        this.techniqueBits = material.getTechniqueBits();

        this.material = material;
        this.materialProperties = new ArrayList<>(materialProperties.length);

        for (int i = 0; i < materialProperties.length; i++) {
            this.materialProperties.add(materialProperties[i]);
        }

        int techniqueCount = material.getTechniqueCount();

        this.meshBindings = new GLMeshBinding[techniqueCount];

        for (int i = 0; i < techniqueCount; i++) {
            if ((techniqueBits & (1 << i)) != 0) {
                this.meshBindings[i] = new GLMeshBinding(material.getShader(i), mesh);
            }
        }

        this.from = 0;
        this.count = mesh.getCount();
    }


    public GLMesh getMesh() {
        return mesh;
    }


    public Material getMaterial() {
        return material;
    }


    public void addPropeties(MaterialProperties props) {
        if (props == null) {
            throw new IllegalArgumentException("props must not be null");
        }

        if (!materialProperties.contains(props)) {
            if (props.getMaterial() != this.material) {
                throw new IllegalArgumentException("props are not assigned to this mesh material");
            }

            materialProperties.add(props);
        }
    }


    public void removeProperties(MaterialProperties props) {
        materialProperties.remove(props);
    }


    public void setDrawRange(int from, int count) {
        this.from = from;
        this.count = count;
    }


    public void draw(SceneRenderer renderer, RenderTechnique technique, Scene scene, SceneNode sceneNode) {
        ensureUndisposed();

        int techniqueId = technique.getId();

        if (((1 << techniqueId) & techniqueBits) == 0) {
            return;
        }

        GLMeshBinding meshBinding = meshBindings[techniqueId];

        meshBinding.bind();
        try {
            material.beginDraw(renderer, technique, scene, sceneNode);

            for (MaterialProperties props : materialProperties) {
                if (props != null && ((1 << techniqueId) & props.getTechniqueBits()) != 0) {
                    props.setProperties(renderer, technique);
                }
            }

            mesh.draw(from, count);

            for (MaterialProperties props : materialProperties) {
                if (props != null && ((1 << techniqueId) & props.getTechniqueBits()) != 0) {
                    props.unsetProperties(renderer, technique);
                }
            }

            material.endDraw(renderer, technique, scene, sceneNode);
        } finally {
            meshBinding.unbind();
        }
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        if (materialHandle != null) {
            materialHandle.release();
        }

        for (int i = 0; i < meshBindings.length; i++) {
            GLMeshBinding meshBinding = meshBindings[i];

            if (meshBinding != null) {
                meshBinding.dispose();
            }
        }

        for (MaterialProperties props : materialProperties) {
            if (props != null) {
                props.dispose();
            }
        }

        mesh.disposeAllAttachedData();

        disposed = true;
    }

}
