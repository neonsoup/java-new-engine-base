package com.neonsoup.engine.graphics;

import java.io.BufferedInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.res.ResourceInfo;
import com.neonsoup.engine.res.ResourceInfoLocator;

public class TextureAtlas extends AbstractDisposable {

    public static enum ElementType {
        SINGLE, ANIMATION_ROW;
    }


    public class Element {

        protected final String name;

        protected final ElementType type;

        protected int minX, minY;
        protected int width, height;

        protected int frameCount = 0;


        protected Element(String name, ElementType type) {
            this.name = name;
            this.type = type;
        }


        public int getFrameCount() {
            return frameCount;
        }


        public void setDiffuseTextureCoords(Sprite sprite) {
            setDiffuseTextureCoords(sprite, false, false);
        }


        public void setDiffuseTextureCoords(Sprite sprite, boolean mirrorX, boolean mirrorY) {
            float w = (float) TextureAtlas.this.width;
            float h = (float) TextureAtlas.this.height;

            int minX = this.minX + elementBorder;
            int maxX = this.minX + width - elementBorder;

            int minY = this.minY + elementBorder;
            int maxY = this.minY + height - elementBorder;

            if (mirrorX) {
                int t = minX;
                minX = maxX;
                maxX = t;
            }
            if (mirrorY) {
                int t = minY;
                minY = maxY;
                maxY = t;
            }

            sprite.setDiffuseTextureCoords(minX / w, 1.0f - (maxY / h), maxX / w, 1.0f - (minY / h));
        }


        public void setDiffuseTextureCoords(Sprite sprite, int frame) {
            setDiffuseTextureCoords(sprite, frame, false, false);
        }


        public void setDiffuseTextureCoords(Sprite sprite, int frame, boolean mirrorX, boolean mirrorY) {
            float w = (float) TextureAtlas.this.width;
            float h = (float) TextureAtlas.this.height;

            int minX = (this.minX + frame * width) + elementBorder;
            int maxX = (this.minX + frame * width) + width - elementBorder;

            int minY = this.minY + elementBorder;
            int maxY = this.minY + height - elementBorder;

            if (mirrorX) {
                int t = minX;
                minX = maxX;
                maxX = t;
            }
            if (mirrorY) {
                int t = minY;
                minY = maxY;
                maxY = t;
            }

            sprite.setDiffuseTextureCoords(minX / w, 1.0f - (maxY / h), maxX / w, 1.0f - (minY / h));
        }

    }


    protected int elementBorder;
    protected int width, height;

    protected List<Element> elements;
    protected Map<String, Element> elementsByName;


    protected TextureAtlas() {
    }


    public Element getElement(String name) {
        return elementsByName.get(name);
    }


    public static TextureAtlas load(String path, Application app) throws Exception {
        ResourceInfoLocator streamLocator = app.getResourceInfoLocator();

        ResourceInfo textInfo = streamLocator.locateResourceInfo(path);

        TextureAtlas result;

        try (BufferedInputStream bufIn = new BufferedInputStream(textInfo.getResourceStream());
             JsonReader json = Json.createReader(bufIn)) {

            JsonObject rootJson = json.readObject();

            result = new TextureAtlas();

            result.elementBorder = 0;

            if (rootJson.containsKey("element_border")) {
                result.elementBorder = rootJson.getInt("element_border");
            }

            JsonArray atlasSize = rootJson.getJsonArray("size");

            result.width = atlasSize.getInt(0);
            result.height = atlasSize.getInt(1);

            if (rootJson.containsKey("elements")) {
                JsonArray elementsJson = rootJson.getJsonArray("elements");

                List<Element> elements = new ArrayList<>(elementsJson.size());
                Map<String, Element> elementsByName = new HashMap<>(elementsJson.size());

                result.elements = elements;
                result.elementsByName = elementsByName;

                for (JsonValue elementJson : elementsJson) {
                    JsonObject elementObject = (JsonObject) elementJson;

                    String name = elementObject.getString("name");
                    ElementType type = ElementType.valueOf(elementObject.getString("type").toUpperCase());

                    Element element = result.new Element(name, type);

                    JsonArray position = elementObject.getJsonArray("position");

                    element.minX = position.getInt(0);
                    element.minY = position.getInt(1);

                    JsonArray size = elementObject.getJsonArray("size");

                    element.width = size.getInt(0);
                    element.height = size.getInt(1);

                    switch (type) {
                        case ANIMATION_ROW: {
                            element.frameCount = elementObject.getInt("frame_count");

                            break;
                        }

                        case SINGLE: {
                            break;
                        }

                        default:
                            throw new RuntimeException("Unsupported element type \"" + type + "\"");
                    }

                    elements.add(element);
                    elementsByName.put(name, element);
                }
            } else {
                result.elements = Collections.emptyList();
                result.elementsByName = Collections.emptyMap();
            }

        }

        return result;
    }

}
