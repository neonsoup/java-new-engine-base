package com.neonsoup.engine.graphics.gl;

import static org.lwjgl.opengl.GL20.*;

public class GLMeshBinding extends GLUnmanagedObject {

    private final GLShaderProgram shader;

    private final GLVertexArray vao = new GLVertexArray();

    private final GLMesh mesh;


    public GLMeshBinding(GLShaderProgram shader, GLMesh mesh) {
        this.shader = shader;

        this.mesh = mesh;

        vao.bind();

        try {
            for (int attrib = 0; attrib < mesh.getAttribDataCount(); attrib++) {
                GLVertexAttributeData attribData = mesh.getAttribData(attrib);
                if (attribData != null) {

                    int location = shader.getAttributeLocation(attribData.attributeSignature.name);

                    if (location < 0) {
                        continue;
                    }

                    attribData.ensureBufferTarget(GLBuffer.Target.ARRAY_BUFFER);

                    attribData.buffer.bind();

                    glEnableVertexAttribArray(location);
                    attribData.setVertexAttribPointer(location);

                    if (GLSettings.isGLErrorCheckingEnabled()) {
                        int[] err = GLException.getErrors();
                        if (err.length > 0) {
                            throw new GLException(err);
                        }
                    }
                }
            }

            GLBuffer.Target.ARRAY_BUFFER.unbindBuffer();

            if (mesh.getIndexData() != null) {
                mesh.getIndexData().bind();
            }

            vao.setInitialized(true);
        } finally {
            vao.unbind();
        }
    }


    public GLShaderProgram getShader() {
        return shader;
    }


    public GLMesh getMesh() {
        return mesh;
    }


    public void bind() {
        shader.beginUse();

        vao.bind();
    }


    public void unbind() {
        shader.endUse();

        vao.unbind();
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        vao.dispose();

        disposed = true;
    }

}
