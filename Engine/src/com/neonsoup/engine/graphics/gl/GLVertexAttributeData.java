package com.neonsoup.engine.graphics.gl;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

public class GLVertexAttributeData extends GLBufferData {

    protected final GLVertexAttributeSignature attributeSignature;
    protected final int stride;

    protected final boolean normalized;


    public GLVertexAttributeData(GLBuffer buffer, GLVertexAttributeSignature attribute) {
        this(buffer, 0, attribute, 0, false);
    }


    public GLVertexAttributeData(GLBuffer buffer,
                                 GLVertexAttributeSignature attribute,
                                 int stride,
                                 boolean normalized) {

        this(buffer, 0, attribute, stride, normalized);
    }


    public GLVertexAttributeData(GLBuffer buffer,
                                 int offset,
                                 GLVertexAttributeSignature attribute,
                                 int stride,
                                 boolean normalized) {

        super(buffer, offset, false);

        if (attribute == null) {
            throw new IllegalArgumentException("Attribute must not be null");
        }
        if (stride < 0) {
            throw new IllegalArgumentException("Stride must be a positive number or zero");
        }

        this.attributeSignature = attribute;
        this.stride = stride;

        this.normalized = normalized;
    }


    public GLVertexAttributeSignature getAttributeSignature() {
        return attributeSignature;
    }


    public int getStride() {
        return stride;
    }


    public void setVertexAttribPointer(int attribLocation) {
        if (attributeSignature.type.isInteger()) {
            glVertexAttribIPointer(attribLocation,
                                   attributeSignature.numComponents,
                                   attributeSignature.type.glType,
                                   stride,
                                   offset);
        } else {
            glVertexAttribPointer(attribLocation,
                                  attributeSignature.numComponents,
                                  attributeSignature.type.glType,
                                  normalized,
                                  stride,
                                  offset);
        }
    }

}
