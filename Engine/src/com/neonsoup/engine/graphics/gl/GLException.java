package com.neonsoup.engine.graphics.gl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;

import java.util.Arrays;

public class GLException extends RuntimeException {

    private static final long serialVersionUID = -6267355171434747110L;

    private final int[] glErrorCodes;
    private final boolean errorCodesPresent;


    public GLException() {
        super();

        glErrorCodes = NO_ERRORS_ARRAY;
        errorCodesPresent = false;
    }


    public GLException(int[] glErrorCodes) {
        super();

        checkGLErrorCodes(glErrorCodes);

        this.glErrorCodes = glErrorCodes.length == 0 ? NO_ERRORS_ARRAY
                                                     : Arrays.copyOf(glErrorCodes, glErrorCodes.length);
        this.errorCodesPresent = glErrorCodes.length > 0;
    }


    public GLException(String msg, int[] glErrorCodes) {
        super(msg);

        checkGLErrorCodes(glErrorCodes);

        this.glErrorCodes = glErrorCodes.length == 0 ? NO_ERRORS_ARRAY
                                                     : Arrays.copyOf(glErrorCodes, glErrorCodes.length);
        this.errorCodesPresent = glErrorCodes.length > 0;
    }


    public GLException(String message) {
        super(message);

        glErrorCodes = NO_ERRORS_ARRAY;
        errorCodesPresent = false;
    }


    public GLException(Throwable cause) {
        super(cause);

        glErrorCodes = NO_ERRORS_ARRAY;
        errorCodesPresent = false;
    }


    public GLException(String message, Throwable cause) {
        super(message, cause);

        glErrorCodes = NO_ERRORS_ARRAY;
        errorCodesPresent = false;
    }


    public GLException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);

        glErrorCodes = NO_ERRORS_ARRAY;
        errorCodesPresent = false;
    }


    private static void checkGLErrorCodes(int[] errorCodes) {
        for (int i = 0; i < errorCodes.length; i++) {
            switch (errorCodes[i]) {
                default:
                    throw new IllegalArgumentException("Invalid GL error code at index " + i + ": " + errorCodes[i]);
                case GL_NO_ERROR:
                case GL_INVALID_ENUM:
                case GL_INVALID_VALUE:
                case GL_INVALID_OPERATION:
                case GL_STACK_OVERFLOW:
                case GL_STACK_UNDERFLOW:
                case GL_OUT_OF_MEMORY:
                case GL_INVALID_FRAMEBUFFER_OPERATION:
            }
        }
    }


    public static String translateGLErrorString(int errorCode) {
        switch (errorCode) {
            case GL_NO_ERROR:
                return "No error";
            case GL_INVALID_ENUM:
                return "Invalid enum";
            case GL_INVALID_VALUE:
                return "Invalid value";
            case GL_INVALID_OPERATION:
                return "Invalid operation";
            case GL_STACK_OVERFLOW:
                return "Stack overflow";
            case GL_STACK_UNDERFLOW:
                return "Stack underflow";
            case GL_OUT_OF_MEMORY:
                return "Out of memory";
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                return "Invalid framebuffer operation";
            default:
                throw new IllegalArgumentException("Invalid GL error code");
        }
    }


    public boolean isGLErrorCodesPresent() {
        return errorCodesPresent;
    }


    public int[] getGLErrorCodes() {
        if (errorCodesPresent) {
            return glErrorCodes.length == 0 ? NO_ERRORS_ARRAY : Arrays.copyOf(glErrorCodes, glErrorCodes.length);
        }

        throw new IllegalStateException("GL error code is not present for this exception");
    }


    public int getGLErrorCodeCount() {
        return glErrorCodes.length;
    }


    public int getGLErrorCode(int index) {
        return glErrorCodes[index];
    }


    private static final int[] NO_ERRORS_ARRAY = new int[0];
    private static final int TOTAL_ERROR_COUNT = 7;


    public static int[] getErrors() {
        int err = glGetError();

        if (err == GL_NO_ERROR) {
            return NO_ERRORS_ARRAY;
        }

        int[] result = new int[TOTAL_ERROR_COUNT];
        result[0] = err;

        int index = 1;

        while ((err = glGetError()) != GL_NO_ERROR) {
            result[index++] = err;
        }

        return Arrays.copyOf(result, index);
    }


    @Override
    public String toString() {
        StringBuilder str = new StringBuilder(getClass().getName());

        String msg = getLocalizedMessage();

        if (msg != null && msg.length() > 0) {
            str.append(": ");
            str.append(msg);
        }

        if (errorCodesPresent) {
            if (msg != null && msg.length() > 0) {
                str.append("; Errors: ");
            } else {
                str.append(": Errors: ");
            }

            for (int i = 0; i < glErrorCodes.length; i++) {
                str.append(translateGLErrorString(glErrorCodes[i]));

                if (i < glErrorCodes.length - 1) {
                    str.append(", ");
                }
            }
        }

        return str.toString();
    }

}
