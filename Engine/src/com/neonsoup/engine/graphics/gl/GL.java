package com.neonsoup.engine.graphics.gl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL32.*;
import static org.lwjgl.opengl.KHRDebug.*;

import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.lwjgl.opengl.GLCapabilities;
import org.lwjgl.opengl.GLDebugMessageCallback;
import org.lwjgl.system.MemoryUtil;

public class GL {

    public static final Logger log = Logger.getLogger(GL.class.getName());

    private static GLDebugMessageCallback debugMessageCallback;


    private GL() {
    }


    public static void initDebugLoggerOutput() {
        GLCapabilities caps = org.lwjgl.opengl.GL.getCapabilities();

        if (caps.GL_KHR_debug) {
            glEnable(GL_DEBUG_OUTPUT);
            glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);

            glDebugMessageCallback(debugMessageCallback = new GLDebugMessageCallback() {

                @Override
                public void invoke(int source,
                                   int type,
                                   int id,
                                   int severity,
                                   int length,
                                   long message,
                                   long userParam) {

                    Level level;

                    switch (severity) {
                        case GL_DEBUG_SEVERITY_HIGH:
                            level = Level.SEVERE;
                            break;

                        case GL_DEBUG_SEVERITY_MEDIUM:
                        case GL_DEBUG_SEVERITY_LOW:
                        default:
                            level = Level.WARNING;
                            break;

                        case GL_DEBUG_SEVERITY_NOTIFICATION:
                            if (!GLSettings.isInfoLoggingEnabled()) {
                                return;
                            }

                            level = Level.INFO;
                            break;
                    }

                    StringBuilder msg = new StringBuilder(91 + length);

                    msg.append("[Source: ");
                    msg.append(debugMessageSourceToString(source));

                    msg.append(", Type: ");
                    msg.append(debugMessageTypeToString(type));

                    msg.append(", Id: ");
                    msg.append(id);

                    msg.append(", Severity: ");
                    msg.append(debugMessageSeverityToString(severity));

                    msg.append("] ");

                    msg.append(MemoryUtil.memASCII(message));

                    LogRecord record = new LogRecord(level, msg.toString());

                    log.log(record);
                }

            }, MemoryUtil.NULL);
        } else {
            log.warning("GL_KHR_Debug extension is not supported");
        }
    }


    public static void disposeDebugOutput() {
        if (debugMessageCallback != null) {
            debugMessageCallback.free();
        }
    }


    public static String debugMessageSourceToString(int glValue) {
        switch (glValue) {
            case GL_DEBUG_SOURCE_API:
                return "API";

            case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
                return "System";

            case GL_DEBUG_SOURCE_SHADER_COMPILER:
                return "Shader compiler";

            case GL_DEBUG_SOURCE_THIRD_PARTY:
                return "Third party";

            case GL_DEBUG_SOURCE_APPLICATION:
                return "Application";

            case GL_DEBUG_SOURCE_OTHER:
                return "Other";

            default:
                return "Unknown: 0x" + Integer.toHexString(glValue).toUpperCase();
        }
    }


    public static String debugMessageTypeToString(int glValue) {
        switch (glValue) {
            case GL_DEBUG_TYPE_ERROR:
                return "Error";

            case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
                return "Deprecated behavior";

            case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
                return "Undefined behavior";

            case GL_DEBUG_TYPE_PORTABILITY:
                return "Portability";

            case GL_DEBUG_TYPE_PERFORMANCE:
                return "Perfomance";

            case GL_DEBUG_TYPE_OTHER:
                return "Other";

            case GL_DEBUG_TYPE_MARKER:
                return "Marker";

            case GL_DEBUG_TYPE_PUSH_GROUP:
                return "Push group";

            case GL_DEBUG_TYPE_POP_GROUP:
                return "Pop group";

            default:
                return "Unknown: 0x" + Integer.toHexString(glValue).toUpperCase();
        }
    }


    public static String debugMessageSeverityToString(int glValue) {
        switch (glValue) {
            case GL_DEBUG_SEVERITY_HIGH:
                return "High";

            case GL_DEBUG_SEVERITY_MEDIUM:
                return "Medium";

            case GL_DEBUG_SEVERITY_LOW:
                return "Low";

            case GL_DEBUG_SEVERITY_NOTIFICATION:
                return "Notification";

            default:
                return "Unknown: 0x" + Integer.toHexString(glValue).toUpperCase();
        }
    }


    protected static final long WAIT_SYNC_NANOS = 250_000L;


    public static void syncGpuCommandsComplete() {
        long waitSync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
        {
            while (glClientWaitSync(waitSync, 0, WAIT_SYNC_NANOS) == GL_TIMEOUT_EXPIRED) {
            }
        }
        glDeleteSync(waitSync);

        int[] err = GLException.getErrors();
        if (err.length > 0) {
            throw new GLException("Error synchronizing sync object", err);
        }
    }

}
