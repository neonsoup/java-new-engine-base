package com.neonsoup.engine.graphics.gl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL31.*;

import java.nio.ByteBuffer;

public class GLBuffer extends GLUnmanagedObject {

    public static enum Target {

        ARRAY_BUFFER(GL_ARRAY_BUFFER, GL_ARRAY_BUFFER_BINDING),

        ELEMENT_ARRAY_BUFFER(GL_ELEMENT_ARRAY_BUFFER, GL_ELEMENT_ARRAY_BUFFER_BINDING),

        UNIFORM_BUFFER(GL_UNIFORM_BUFFER, GL_UNIFORM_BUFFER_BINDING);

        public final int glTarget;
        public final int glTargetBinding;


        private Target(int glTarget, int glTargetBinding) {
            this.glTarget = glTarget;
            this.glTargetBinding = glTargetBinding;
        }


        public int getBoundBufferHandle() {
            return glGetInteger(glTargetBinding);
        }


        public void unbindBuffer() {
            glBindBuffer(glTarget, 0);
        }


        public void bindBuffer(int bufferHandle) {
            glBindBuffer(glTarget, bufferHandle);
        }


        public void bindBufferRange(int bufferHandle, int index, int offset, int size) {
            glBindBufferRange(glTarget, index, bufferHandle, offset, size);
        }


        public void bindBufferBase(int bufferHandle, int index) {
            glBindBufferBase(glTarget, index, bufferHandle);
        }

    }


    public static enum Usage {

        STREAM_DRAW(GL_STREAM_DRAW),

        STREAM_READ(GL_STREAM_READ),

        STREAM_COPY(GL_STREAM_COPY),

        STATIC_DRAW(GL_STATIC_DRAW),

        STATIC_READ(GL_STATIC_READ),

        STATIC_COPY(GL_STATIC_COPY),

        DYNAMIC_DRAW(GL_DYNAMIC_DRAW),

        DYNAMIC_READ(GL_DYNAMIC_READ),

        DYNAMIC_COPY(GL_DYNAMIC_COPY);

        public final int glUsage;


        private Usage(int glUsage) {
            this.glUsage = glUsage;
        }

    }


    public static enum MapAccess {

        READ_ONLY(GL_READ_ONLY),

        WRITE_ONLY(GL_WRITE_ONLY),

        READ_WRITE(GL_READ_WRITE);

        public final int glAccess;


        private MapAccess(int glAccess) {
            this.glAccess = glAccess;
        }

    }


    private final int glBufferHandle;

    private long size;

    private Target target;
    private Usage usage;

    private ByteBuffer mapBuffer = null;


    public GLBuffer(Target target, Usage usage, ByteBuffer data) {
        this(data.remaining(), target, usage, data);
    }


    public GLBuffer(long size, Target target, Usage usage) {
        this(size, target, usage, null);
    }


    private GLBuffer(long size, Target target, Usage usage, ByteBuffer data) {
        if (size < 0L && data == null) {
            throw new IllegalArgumentException("Size must not be a negative number");
        }
        if (target == null) {
            throw new IllegalArgumentException("Target must not be null");
        }

        if (usage == null) {
            usage = Usage.STATIC_DRAW;
        }

        glBufferHandle = glGenBuffers();

        if (glBufferHandle == 0) {
            throw new GLException("Could not create new buffer object", GLException.getErrors());
        }

        // int prevBound = target.getBoundBufferHandle();
        try {
            target.bindBuffer(glBufferHandle);

            if (data != null) {
                glBufferData(target.glTarget, data, usage.glUsage);
            } else {
                glBufferData(target.glTarget, size, usage.glUsage);
            }

            if (GLSettings.isGLErrorCheckingEnabled()) {
                int[] err = GLException.getErrors();
                if (err.length > 0) {
                    throw new GLException("Could not initialize new buffer object", err);
                }
            }
        } catch (Exception e) {
            glDeleteBuffers(glBufferHandle);

            throw e;
        } finally {
            target.bindBuffer(0);
        }
        // finally {
        // target.bindBuffer(prevBound);
        // }

        if (data == null) {
            this.size = size;
        } else {
            this.size = data.remaining();
        }
        this.usage = usage;

        this.target = target;
    }


    public int getGLBufferHandle() {
        ensureUndisposed();

        return glBufferHandle;
    }


    public long getSize() {
        return size;
    }


    public Target getTarget() {
        return target;
    }


    public void setTarget(Target target) {
        ensureUndisposed();
        // ensureUnbound();
        if (target == null) {
            throw new IllegalArgumentException("Target must not be null");
        }

        this.target = target;
    }


    public Usage getUsage() {
        return usage;
    }


    public void bind() {
        ensureUndisposed();

        target.bindBuffer(glBufferHandle);
    }


    public void bindRange(int index, int offset, int size) {
        ensureUndisposed();
        ensureBufferTargetCompatibleWithBindRange();

        target.bindBufferRange(glBufferHandle, index, offset, size);
    }


    public void bindBase(int index) {
        ensureUndisposed();
        ensureBufferTargetCompatibleWithBindRange();

        target.bindBufferBase(glBufferHandle, index);
    }


    private void ensureBufferTargetCompatibleWithBindRange() {
        if (target != Target.UNIFORM_BUFFER) {
            throw new IllegalStateException("The buffer target \""
                                            + target
                                            + "\" is not compatible with bindRange or bindBase");
        }
    }


    public void unbind() {
        ensureUndisposed();

        // int bound = target.getBoundBufferHandle();
        //
        // if (bound == glBufferHandle) {

        if (GLSettings.isGLErrorCheckingEnabled() && target.getBoundBufferHandle() != glBufferHandle) {
            throw new IllegalStateException("This buffer was not bound");
        }

        target.bindBuffer(0);
        // } else {
        // throw new IllegalStateException("This buffer was not bound");
        // }
    }


    public void bufferData(ByteBuffer data) {
        this.bufferData(this.usage, data);
    }


    public void bufferData(Usage usage, ByteBuffer data) {
        ensureUndisposed();
        if (usage == null) {
            throw new IllegalArgumentException("Usage must not be null");
        }
        if (data == null) {
            throw new IllegalArgumentException("Data buffer must not be null");
        }

        // int prevBound = target.getBoundBufferHandle();
        // try {
        // if (prevBound != glBufferHandle) {
        // target.bindBuffer(glBufferHandle);
        // }

        glBufferData(target.glTarget, data, usage.glUsage);

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not set new buffer data", err);
            }
        }
        // }
        // finally {
        // if (prevBound != glBufferHandle) {
        // target.bindBuffer(prevBound);
        // }
        // }

        this.size = data.remaining();
        this.usage = usage;
    }


    public void bufferSubData(ByteBuffer data) {
        bufferSubData(0, data);
    }


    public void bufferSubData(long offset, ByteBuffer data) {
        ensureUndisposed();
        if (data == null) {
            throw new IllegalArgumentException("Data buffer must not be null");
        }

        glBufferSubData(target.glTarget, offset, data);

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not set buffer sub data", err);
            }
        }
    }


    public ByteBuffer mapBuffer(MapAccess access) {
        ensureUndisposed();
        if (access == null) {
            throw new IllegalArgumentException("Access must not be null");
        }

        // int prevBound = target.getBoundBufferHandle();
        // try {
        // if (prevBound != glBufferHandle) {
        // target.bindBuffer(glBufferHandle);
        // }

        mapBuffer = glMapBuffer(target.glTarget, access.glAccess, size, mapBuffer);

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not map buffer object", err);
            }
        }

        return mapBuffer;
        // } finally {
        // if (prevBound != glBufferHandle) {
        // target.bindBuffer(prevBound);
        // }
        // }
    }


    public void unmapBuffer() {
        ensureUndisposed();

        // int prevBound = target.getBoundBufferHandle();
        // try {
        // if (prevBound != glBufferHandle) {
        // target.bindBuffer(glBufferHandle);
        // }
        //
        // int mapped = glGetBufferParameteri(target.glTarget,
        // GL_BUFFER_MAPPED);
        //
        // if (mapped == GL_TRUE) {

        glUnmapBuffer(target.glTarget);

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not unmap buffer object", err);
            }
        }

        // } else {
        // throw new IllegalStateException("This buffer was not mapped");
        // }
        // } finally {
        // if (prevBound != glBufferHandle) {
        // target.bindBuffer(prevBound);
        // }
        // }
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        glDeleteBuffers(glBufferHandle);

        mapBuffer = null;

        disposed = true;
    }


    // protected void ensureUnbound() {
    // if (target.getBoundBufferHandle() == this.glBufferHandle) {
    // throw new IllegalStateException("Can't perform this operation while
    // buffer is bound");
    // }
    // }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        if (disposed) {
            result.append("Disposed ");
        }

        result.append(super.toString());

        result.append(" [Size: ");
        result.append(size);

        result.append(", Target: ");
        result.append(target);

        result.append(", Usage: ");
        result.append(usage);

        result.append(", GL buffer handle: ");
        result.append(glBufferHandle);

        result.append(']');

        return result.toString();
    }

}
