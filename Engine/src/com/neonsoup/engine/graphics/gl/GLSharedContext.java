package com.neonsoup.engine.graphics.gl;

import com.neonsoup.engine.Disposable;

public interface GLSharedContext extends Disposable {

    void makeCurrent();

}
