package com.neonsoup.engine.graphics.gl;

import com.neonsoup.engine.AbstractDisposable;

public abstract class GLUnmanagedObject extends AbstractDisposable {

    public GLUnmanagedObject() {
    }

}
