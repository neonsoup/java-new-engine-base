package com.neonsoup.engine.graphics.gl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL21.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL31.*;

import java.nio.ByteBuffer;

import org.lwjgl.system.MemoryUtil;

public class GLTexture extends GLUnmanagedObject {

    public static enum Target {

        TEXTURE_1D(GL_TEXTURE_1D, GL_TEXTURE_BINDING_1D),

        TEXTURE_2D(GL_TEXTURE_2D, GL_TEXTURE_BINDING_2D),

        TEXTURE_3D(GL_TEXTURE_3D, GL_TEXTURE_BINDING_3D),

        TEXTURE_2D_ARRAY(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_BINDING_2D_ARRAY);

        public final int glTarget;
        public final int glTargetBinding;


        private Target(int glTarget, int glTargetBinding) {
            this.glTarget = glTarget;
            this.glTargetBinding = glTargetBinding;
        }


        public int getBoundTextureHandle() {
            return glGetInteger(glTargetBinding);
        }


        public void bindTexture(int textureHandle) {
            glBindTexture(glTarget, textureHandle);
        }


        public boolean hasHeight() {
            return this != TEXTURE_1D;
        }


        public boolean hasDepth() {
            switch (this) {
                case TEXTURE_1D:
                case TEXTURE_2D:
                    return false;

                case TEXTURE_2D_ARRAY:
                case TEXTURE_3D:
                    return true;

                default:
                    throw new AssertionError();
            }
        }

    }


    public static enum Filter {

        NEAREST(GL_NEAREST),

        NEAREST_MIPMAP_NEAREST(GL_NEAREST_MIPMAP_NEAREST),

        NEAREST_MIPMAP_LINEAR(GL_NEAREST_MIPMAP_LINEAR),

        LINEAR(GL_LINEAR),

        LINEAR_MIPMAP_NEAREST(GL_LINEAR_MIPMAP_NEAREST),

        LINEAR_MIPMAP_LINEAR(GL_LINEAR_MIPMAP_LINEAR);

        public final int glFilter;


        private Filter(int glFilter) {
            this.glFilter = glFilter;
        }


        public static final Filter DEFAULT_MIN = LINEAR_MIPMAP_LINEAR;
        public static final Filter DEFAULT_MAG = LINEAR;


        public boolean isMipmap() {
            switch (this) {
                case NEAREST:
                case LINEAR:
                    return false;

                default:
                    return true;
            }
        }

    }


    public static enum InternalFormat {

        DEPTH(GL_DEPTH_COMPONENT),

        DEPTH16(GL_DEPTH_COMPONENT16),

        DEPTH24(GL_DEPTH_COMPONENT24),

        DEPTH32F(GL_DEPTH_COMPONENT32F),

        DEPTH_STENCIL(GL_DEPTH_STENCIL),

        DEPTH24_STENCIL8(GL_DEPTH24_STENCIL8),

        DEPTH32F_STENCIL8(GL_DEPTH32F_STENCIL8),

        RED(GL_RED),

        RG(GL_RG),

        RGB(GL_RGB),

        RGBA(GL_RGBA),

        R16F(GL_R16F),

        RG16F(GL_RG16F),

        RGB16F(GL_RGB16F),

        RGBA16F(GL_RGBA16F),

        R32F(GL_R32F),

        RG32F(GL_RG32F),

        RGB32F(GL_RGB32F),

        RGBA32F(GL_RGBA32F),

        R8_SNORM(GL_R8_SNORM),

        RG8_SNORM(GL_RG8_SNORM),

        RGB8_SNORM(GL_RGB8_SNORM),

        RGBA8_SNORM(GL_RGBA8_SNORM),

        R16_SNORM(GL_R16_SNORM),

        RG16_SNORM(GL_RG16_SNORM),

        RGB16_SNORM(GL_RGB16_SNORM),

        RGBA16_SNORM(GL_RGBA16_SNORM),

        R8(GL_R8),

        RG8(GL_RG8),

        RGB8(GL_RGB8),

        RGBA8(GL_RGBA8),

        R8I(GL_R8I),

        RG8I(GL_RG8I),

        RGB8I(GL_RGB8I),

        RGBA8I(GL_RGBA8I),

        R8UI(GL_R8UI),

        RG8UI(GL_RG8UI),

        RGB8UI(GL_RGB8UI),

        RGBA8UI(GL_RGBA8UI),

        R16(GL_R16),

        RG16(GL_RG16),

        RGB16(GL_RGB16),

        RGBA16(GL_RGBA16),

        R16I(GL_R16I),

        RG16I(GL_RG16I),

        RGB16I(GL_RGB16I),

        RGBA16I(GL_RGBA16I),

        R16UI(GL_R16UI),

        RG16UI(GL_RG16UI),

        RGB16UI(GL_RGB16UI),

        RGBA16UI(GL_RGBA16UI),

        R32I(GL_R32I),

        RG32I(GL_RG32I),

        RGB32I(GL_RGB32I),

        RGBA32I(GL_RGBA32I),

        R32UI(GL_R32UI),

        RG32UI(GL_RG32UI),

        RGB32UI(GL_RGB32UI),

        RGBA32UI(GL_RGBA32UI),

        SRGB8(GL_SRGB8),

        SRGB8_ALPHA8(GL_SRGB8_ALPHA8);

        public final int glInternalFormat;


        private InternalFormat(int glInternalFormat) {
            this.glInternalFormat = glInternalFormat;
        }


        public static final InternalFormat DEFAULT = RGBA8;


        public boolean isCompatibleWithGLGenerateMipmap() {
            return this.isColor() && !(this.isColorSignedInteger() || this.isColorUnsignedInteger());
        }


        public boolean isColor() {
            switch (this) {
                case DEPTH:
                case DEPTH16:
                case DEPTH24:
                case DEPTH32F:
                case DEPTH_STENCIL:
                case DEPTH24_STENCIL8:
                case DEPTH32F_STENCIL8:
                    return false;

                default:
                    return true;
            }
        }


        public boolean isOnlyDepth() {
            switch (this) {
                case DEPTH:
                case DEPTH16:
                case DEPTH24:
                case DEPTH32F:
                    return true;
                default:
                    return false;
            }
        }


        public boolean isDepthStencil() {
            switch (this) {
                case DEPTH_STENCIL:
                case DEPTH24_STENCIL8:
                case DEPTH32F_STENCIL8:
                    return true;
                default:
                    return false;
            }
        }


        public boolean isExactColorFormatDefined() {
            switch (this) {
                case RED:
                case RG:
                case RGB:
                case RGBA:
                    return false;

                default:
                    return true;
            }
        }


        public boolean colorHasAlpha() {
            switch (this) {
                case RGBA:
                case RGBA16F:
                case RGBA32F:
                case RGBA8_SNORM:
                case RGBA16_SNORM:
                case RGBA8:
                case RGBA16:
                case RGBA8I:
                case RGBA8UI:
                case RGBA16I:
                case RGBA16UI:
                case RGBA32I:
                case RGBA32UI:
                case SRGB8_ALPHA8:
                    return true;

                default:
                    return false;
            }
        }


        public boolean isColorSRGB() {
            switch (this) {
                case SRGB8:
                case SRGB8_ALPHA8:
                    return true;

                default:
                    return false;
            }
        }


        public boolean isColorUnsignedNormalized() {
            switch (this) {
                case R8:
                case RG8:
                case RGB8:
                case RGBA8:
                case R16:
                case RG16:
                case RGB16:
                case RGBA16:
                    return true;

                default:
                    return false;
            }
        }


        public boolean isColorSignedNormalized() {
            switch (this) {
                case R8_SNORM:
                case RG8_SNORM:
                case RGB8_SNORM:
                case RGBA8_SNORM:
                case R16_SNORM:
                case RG16_SNORM:
                case RGB16_SNORM:
                case RGBA16_SNORM:
                    return true;

                default:
                    return false;
            }
        }


        public boolean isColorUnsignedInteger() {
            switch (this) {
                case R8UI:
                case RG8UI:
                case RGB8UI:
                case RGBA8UI:
                case R16UI:
                case RG16UI:
                case RGB16UI:
                case RGBA16UI:
                case R32UI:
                case RG32UI:
                case RGB32UI:
                case RGBA32UI:
                    return true;

                default:
                    return false;
            }
        }


        public boolean isColorSignedInteger() {
            switch (this) {
                case R8I:
                case RG8I:
                case RGB8I:
                case RGBA8I:
                case R16I:
                case RG16I:
                case RGB16I:
                case RGBA16I:
                case R32I:
                case RG32I:
                case RGB32I:
                case RGBA32I:
                    return true;

                default:
                    return false;
            }
        }


        public boolean isColorFloat() {
            switch (this) {
                case R16F:
                case RG16F:
                case RGB16F:
                case RGBA16F:
                case R32F:
                case RG32F:
                case RGB32F:
                case RGBA32F:
                    return true;

                default:
                    return false;
            }
        }


        public int getColorElementSize() {
            switch (this) {
                case R8:
                case RG8:
                case RGB8:
                case RGBA8:

                case R8I:
                case RG8I:
                case RGB8I:
                case RGBA8I:

                case R8UI:
                case RG8UI:
                case RGB8UI:
                case RGBA8UI:

                case R8_SNORM:
                case RG8_SNORM:
                case RGB8_SNORM:
                case RGBA8_SNORM:

                case SRGB8:
                case SRGB8_ALPHA8:

                    return 8;

                case R16F:
                case RG16F:
                case RGB16F:
                case RGBA16F:

                case R16:
                case RG16:
                case RGB16:
                case RGBA16:

                case R16I:
                case RG16I:
                case RGB16I:
                case RGBA16I:

                case R16UI:
                case RG16UI:
                case RGB16UI:
                case RGBA16UI:

                case R16_SNORM:
                case RG16_SNORM:
                case RGB16_SNORM:
                case RGBA16_SNORM:

                    return 16;

                case R32F:
                case RG32F:
                case RGB32F:
                case RGBA32F:

                case R32I:
                case RG32I:
                case RGB32I:
                case RGBA32I:

                case R32UI:
                case RG32UI:
                case RGB32UI:
                case RGBA32UI:

                    return 32;

                default:
                    throw new UnsupportedOperationException("This constant is not color");
            }
        }

    }


    public static enum DataType {

        UNSIGNED_BYTE(GL_UNSIGNED_BYTE),

        BYTE(GL_BYTE),

        UNSIGNED_SHORT(GL_UNSIGNED_SHORT),

        SHORT(GL_SHORT),

        UNSIGNED_INT(GL_UNSIGNED_INT),

        INT(GL_INT),

        FLOAT(GL_FLOAT),

        UNSIGNED_INT_24_8(GL_UNSIGNED_INT_24_8),

        FLOAT_32_UNSIGNED_INT_24_8(GL_FLOAT_32_UNSIGNED_INT_24_8_REV);

        public final int glType;


        private DataType(int glType) {
            this.glType = glType;
        }


        public static final DataType DEFAULT = UNSIGNED_BYTE;

    }


    public static enum DataFormat {

        RED(GL_RED),

        RG(GL_RG),

        RGB(GL_RGB),

        BGR(GL_BGR),

        RGBA(GL_RGBA),

        BGRA(GL_BGRA),

        RED_INTEGER(GL_RED_INTEGER),

        RG_INTEGER(GL_RG_INTEGER),

        RGB_INTEGER(GL_RGB_INTEGER),

        BGR_INTEGER(GL_BGR_INTEGER),

        RGBA_INTEGER(GL_RGBA_INTEGER),

        BGRA_INTEGER(GL_BGRA_INTEGER),

        DEPTH(GL_DEPTH_COMPONENT),

        DEPTH_STENCIL(GL_DEPTH_STENCIL);

        public final int glFormat;


        private DataFormat(int glFormat) {
            this.glFormat = glFormat;
        }


        public boolean isInteger() {
            switch (this) {
                case RED_INTEGER:
                case RG_INTEGER:
                case RGB_INTEGER:
                case BGR_INTEGER:
                case RGBA_INTEGER:
                case BGRA_INTEGER:
                    return true;

                default:
                    return false;
            }
        }


        public static final DataFormat DEFAULT = RGBA;

    }


    public static enum PackAlignment {

        ALIGNMENT_1(1), ALIGNMENT_2(2), ALIGNMENT_4(4), ALIGNMENT_8(8);

        public final int glAlignment;


        private PackAlignment(int glAlignment) {
            this.glAlignment = glAlignment;
        }


        public static PackAlignment valueOf(int align) {
            switch (align) {
                case 1:
                    return ALIGNMENT_1;

                case 2:
                    return ALIGNMENT_2;

                case 4:
                    return ALIGNMENT_4;

                case 8:
                    return ALIGNMENT_8;

                default:
                    throw new IllegalArgumentException("Alignment value must be 1, 2, 4 or 8. Passed value: " + align);
            }
        }

    }


    public static void unpackAlignment(PackAlignment align) {
        glPixelStorei(GL_UNPACK_ALIGNMENT, align.glAlignment);
    }


    public static void activeTexture(int textureUnitIndex) {
        if (textureUnitIndex < 0 || textureUnitIndex >= 32) {
            throw new IllegalArgumentException("Texture unit index must be not a negative number less than 32, "
                                               + "passed value: "
                                               + textureUnitIndex);
        }

        glActiveTexture(GL_TEXTURE0 + textureUnitIndex);
    }


    public static final int DEFAULT_BASE_LEVEL = 0;
    public static final int DEFAULT_MAX_LEVEL = 1000;

    private final int glTextureHandle;

    private Target target;

    private InternalFormat internalFormat;

    private Filter minFilter = Filter.DEFAULT_MIN, magFilter = Filter.DEFAULT_MAG;

    private int baseLevel = DEFAULT_BASE_LEVEL, maxLevel = DEFAULT_MAX_LEVEL;

    private boolean propertiesDirty = false;

    private boolean mipmapDirty = true;
    private int mipmapBaseLevel;


    public GLTexture(Target target, InternalFormat internalFormat) {
        if (target == null) {
            throw new IllegalArgumentException("Target must not be null");
        }

        glTextureHandle = glGenTextures();

        if (glTextureHandle == 0) {
            throw new GLException("Could not create new texture object", GLException.getErrors());
        }

        this.target = target;

        if (internalFormat == null) {
            internalFormat = InternalFormat.DEFAULT;
        }

        this.internalFormat = internalFormat;
    }


    public int getGlTextureHandle() {
        ensureUndisposed();

        return glTextureHandle;
    }


    public Target getTarget() {
        return target;
    }


    public Filter getMinFilter() {
        return minFilter;
    }


    public void preSetMinFilter(Filter filter) {
        if (filter == null) {
            throw new IllegalArgumentException("filter must not be null");
        }

        if (filter != minFilter) {
            propertiesDirty |= true;

            this.minFilter = filter;
        }
    }


    public void setMinFilter(Filter filter) {
        if (filter == null) {
            throw new IllegalArgumentException("filter must not be null");
        }

        this.minFilter = filter;

        setGLTextureMinFilterInternal(filter);
    }


    public Filter getMagFilter() {
        return magFilter;
    }


    public void preSetMagFilter(Filter filter) {
        if (filter == null) {
            throw new IllegalArgumentException("filter must not be null");
        } else if (filter.isMipmap()) {
            throw new IllegalArgumentException("Magnification filter must not use mipmap, passed filter value: "
                                               + filter);
        }

        if (filter != magFilter) {
            propertiesDirty |= true;

            this.magFilter = filter;
        }
    }


    public void setMagFilter(Filter filter) {
        if (filter == null) {
            throw new IllegalArgumentException("filter must not be null");
        } else if (filter.isMipmap()) {
            throw new IllegalArgumentException("Magnification filter must not use mipmap, passed filter value: "
                                               + filter);
        }

        this.magFilter = filter;

        setGLTextureMagFilterInternal(filter);
    }


    public int getBaseLevel() {
        return baseLevel;
    }


    public void preSetBaseLevel(int baseLevel) {
        if (baseLevel != this.baseLevel) {
            propertiesDirty |= true;

            this.baseLevel = baseLevel;
        }
    }


    public void setBaseLevel(int baseLevel) {
        this.baseLevel = baseLevel;

        setGLTextureBaseLevelInternal(baseLevel);
    }


    public int getMaxLevel() {
        return maxLevel;
    }


    public void preSetMaxLevel(int maxLevel) {
        if (maxLevel != this.maxLevel) {
            propertiesDirty |= true;

            this.maxLevel = maxLevel;
        }
    }


    public void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;

        setGLTextureMaxLevelInternal(maxLevel);
    }


    public InternalFormat getInternalFormat() {
        return internalFormat;
    }


    public void bind() {
        ensureUndisposed();

        target.bindTexture(glTextureHandle);

        updateProperties();
    }


    public void updateProperties() {
        if (propertiesDirty) {
            setGLTextureMinFilterInternal(this.minFilter);
            setGLTextureMagFilterInternal(this.magFilter);
            setGLTextureMaxLevelInternal(this.maxLevel);
            setGLTextureBaseLevelInternal(this.baseLevel);

            propertiesDirty = false;
        }
    }


    public void unbind() {
        ensureUndisposed();

        if (GLSettings.isGLErrorCheckingEnabled() && target.getBoundTextureHandle() != glTextureHandle) {
            throw new IllegalStateException("This texture was not bound");
        }

        target.bindTexture(0);
    }


    public void texImage2D(GLTextureData data) {
        if (data.getDimension() != GLTextureData.Dimension.TEXTURE_2D) {
            throw new IllegalArgumentException("data dimension must be TEXTURE_2D");
        }
        if (data.getInternalFormat() != this.internalFormat) {
            throw new IllegalArgumentException("data internal format ("
                                               + data.getInternalFormat()
                                               + ") does not match texture internal format ("
                                               + this.internalFormat
                                               + ")");
        }

        unpackAlignment(data.getPackAlignment());

        glTexImage2D(target.glTarget,
                     0,
                     internalFormat.glInternalFormat,
                     data.getWidth(),
                     data.getHeight(),
                     0,
                     data.getDataFormat().glFormat,
                     data.getDataType().glType,
                     data.getDataBuffer());

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not set texture 2D data:\n" + data.toString(), err);
            }
        }

        mipmapDirty = true;
    }


    public void texImage2D(int level,
                           int width,
                           int height,
                           DataFormat dataFormat,
                           DataType dataType,
                           ByteBuffer data) {

        if (data == null) {
            glTexImage2D(this.target.glTarget,
                         level,
                         this.internalFormat.glInternalFormat,
                         width,
                         height,
                         0,
                         dataFormat.glFormat,
                         dataType.glType,
                         MemoryUtil.NULL);
        } else {
            glTexImage2D(this.target.glTarget,
                         level,
                         this.internalFormat.glInternalFormat,
                         width,
                         height,
                         0,
                         dataFormat.glFormat,
                         dataType.glType,
                         data);
        }

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not set texture 2D data "
                                      + data
                                      + " with parameters [Level: "
                                      + level
                                      + ", width: "
                                      + width
                                      + ", height: "
                                      + height
                                      + ", format: "
                                      + dataFormat
                                      + ", type: "
                                      + dataType
                                      + "]",
                                      err);
            }
        }

        mipmapDirty = true;
    }


    public void texSubImage2D(int level,
                              int width,
                              int height,
                              DataFormat dataFormat,
                              DataType dataType,
                              ByteBuffer data) {

        texSubImage2D(level, 0, 0, width, height, dataFormat, dataType, data);
    }


    public void texSubImage2D(int level,
                              int xoffset,
                              int yoffset,
                              int width,
                              int height,
                              DataFormat dataFormat,
                              DataType dataType,
                              ByteBuffer data) {

        if (data == null) {
            glTexSubImage2D(this.target.glTarget,
                            level,
                            xoffset,
                            yoffset,
                            width,
                            height,
                            dataFormat.glFormat,
                            dataType.glType,
                            MemoryUtil.NULL);
        } else {
            glTexSubImage2D(this.target.glTarget,
                            level,
                            xoffset,
                            yoffset,
                            width,
                            height,
                            dataFormat.glFormat,
                            dataType.glType,
                            data);
        }

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not set texture 2D sub data "
                                      + data
                                      + " with parameters [Level: "
                                      + level
                                      + ", x offset: "
                                      + xoffset
                                      + ", y offset: "
                                      + yoffset
                                      + ", width: "
                                      + width
                                      + ", height: "
                                      + height
                                      + ", format: "
                                      + dataFormat
                                      + ", type: "
                                      + dataType
                                      + "]",
                                      err);
            }
        }

        mipmapDirty = true;
    }


    public void texImage3D(int level,
                           int width,
                           int height,
                           int depth,
                           DataFormat dataFormat,
                           DataType dataType,
                           ByteBuffer data) {

        if (data == null) {
            glTexImage3D(this.target.glTarget,
                         level,
                         this.internalFormat.glInternalFormat,
                         width,
                         height,
                         depth,
                         0,
                         dataFormat.glFormat,
                         dataType.glType,
                         MemoryUtil.NULL);
        } else {
            glTexImage3D(this.target.glTarget,
                         level,
                         this.internalFormat.glInternalFormat,
                         width,
                         height,
                         depth,
                         0,
                         dataFormat.glFormat,
                         dataType.glType,
                         data);
        }

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not set texture 3D data "
                                      + data
                                      + " with parameters [Level: "
                                      + level
                                      + ", width: "
                                      + width
                                      + ", height: "
                                      + height
                                      + ", depth: "
                                      + depth
                                      + ", format: "
                                      + dataFormat
                                      + ", type: "
                                      + dataType
                                      + "]",
                                      err);
            }
        }

        mipmapDirty = true;
    }


    public void texSubImage3DArrayLayer(GLTextureData data, int zoffset) {
        if (data.getDimension() != GLTextureData.Dimension.TEXTURE_2D) {
            throw new IllegalArgumentException("data dimension must be TEXTURE_2D");
        }
        if (data.getInternalFormat() != this.internalFormat) {
            throw new IllegalArgumentException("data internal format ("
                                               + data.getInternalFormat()
                                               + ") does not match texture internal format ("
                                               + this.internalFormat
                                               + ")");
        }

        unpackAlignment(data.getPackAlignment());

        glTexSubImage3D(target.glTarget,
                        0,
                        0,
                        0,
                        zoffset,
                        data.getWidth(),
                        data.getHeight(),
                        1,
                        data.getDataFormat().glFormat,
                        data.getDataType().glType,
                        data.getDataBuffer());

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not set texture 2D data:\n" + data.toString(), err);
            }
        }

        mipmapDirty = true;
    }


    public void texSubImage3D(int level,
                              int width,
                              int height,
                              int depth,
                              DataFormat dataFormat,
                              DataType dataType,
                              ByteBuffer data) {

        texSubImage3D(level, 0, 0, 0, width, height, depth, dataFormat, dataType, data);
    }


    public void texSubImage3D(int level,
                              int xoffset,
                              int yoffset,
                              int zoffset,
                              int width,
                              int height,
                              int depth,
                              DataFormat dataFormat,
                              DataType dataType,
                              ByteBuffer data) {

        if (data == null) {
            glTexSubImage3D(this.target.glTarget,
                            level,
                            xoffset,
                            yoffset,
                            zoffset,
                            width,
                            height,
                            depth,
                            dataFormat.glFormat,
                            dataType.glType,
                            MemoryUtil.NULL);
        } else {
            glTexSubImage3D(this.target.glTarget,
                            level,
                            xoffset,
                            yoffset,
                            zoffset,
                            width,
                            height,
                            depth,
                            dataFormat.glFormat,
                            dataType.glType,
                            data);
        }

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not set texture 2D sub data "
                                      + data
                                      + " with parameters [Level: "
                                      + level
                                      + ", x offset: "
                                      + xoffset
                                      + ", y offset: "
                                      + yoffset
                                      + ", width: "
                                      + width
                                      + ", height: "
                                      + height
                                      + ", depth: "
                                      + depth
                                      + ", format: "
                                      + dataFormat
                                      + ", type: "
                                      + dataType
                                      + "]",
                                      err);
            }
        }

        mipmapDirty = true;
    }


    public void generateMipmap() {
        checkForIllegalMipmapGenerationInternalFormat();

        glGenerateMipmap(target.glTarget);

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();

            if (err.length > 0) {
                throw new GLException("Could not generate texture mipmap", err);
            }
        }

        mipmapBaseLevel = baseLevel;
        mipmapDirty = false;
    }


    public void updateMipmap() {
        if (minFilter.isMipmap() && (mipmapDirty || mipmapBaseLevel != baseLevel)) {
            generateMipmap();
        }
    }


    private void setGLTextureMinFilterInternal(Filter filter) {
        glTexParameteri(this.target.glTarget, GL_TEXTURE_MIN_FILTER, filter.glFilter);

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not set texture min filter to " + filter, err);
            }
        }
    }


    private void setGLTextureMagFilterInternal(Filter filter) {
        glTexParameteri(this.target.glTarget, GL_TEXTURE_MAG_FILTER, filter.glFilter);

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not set texture mag filter to " + filter, err);
            }
        }
    }


    private void setGLTextureBaseLevelInternal(int baseLevel) {
        glTexParameteri(this.target.glTarget, GL_TEXTURE_BASE_LEVEL, baseLevel);

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not set texture base level to " + baseLevel, err);
            }
        }
    }


    private void setGLTextureMaxLevelInternal(int maxLevel) {
        glTexParameteri(this.target.glTarget, GL_TEXTURE_MAX_LEVEL, maxLevel);

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException("Could not set texture max level to " + maxLevel, err);
            }
        }
    }


    private void checkForIllegalMipmapGenerationInternalFormat() {
        if ((this.internalFormat.isColor()
             && (this.internalFormat.isColorSignedInteger() || this.internalFormat.isColorUnsignedInteger()))
            || this.internalFormat.isDepthStencil()
            || this.internalFormat.isOnlyDepth()) {

            throw new IllegalStateException("Illegal internal format for automatic mipmap generation: "
                                            + this.internalFormat);
        }
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        glDeleteTextures(glTextureHandle);

        disposed = true;
    }


    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        if (disposed) {
            result.append("Disposed ");
        }

        result.append(super.toString());

        result.append(" [Target: ");
        result.append(target);

        result.append(", Internal format: ");
        result.append(internalFormat);

        result.append(", MinFilter: ");
        result.append(minFilter);

        result.append(", MagFilter: ");
        result.append(magFilter);

        result.append(", Base level: ");
        result.append(baseLevel);

        result.append(", Max level: ");
        result.append(maxLevel);

        result.append(", GL texture handle: ");
        result.append(glTextureHandle);

        result.append(']');

        return result.toString();
    }

}
