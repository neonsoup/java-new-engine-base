package com.neonsoup.engine.graphics.gl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;

public class GLVertexArray extends GLUnmanagedObject {

    private final int glVertexArrayHandle;

    private boolean initialized = false;


    public GLVertexArray() {
        glVertexArrayHandle = glGenVertexArrays();

        if (glVertexArrayHandle == 0) {
            throw new GLException("Could not create new vertex array object", GLException.getErrors());
        }
    }


    public boolean isInitialized() {
        return initialized;
    }


    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }


    public int getGLVertexArrayHandle() {
        ensureUndisposed();

        return glVertexArrayHandle;
    }


    public void bind() {
        ensureUndisposed();

        glBindVertexArray(glVertexArrayHandle);
    }


    public void unbind() {
        ensureUndisposed();

        // int bound = glGetInteger(GL_VERTEX_ARRAY_BINDING);

        // if (bound == glVertexArrayHandle) {

        if (GLSettings.isGLErrorCheckingEnabled() && glGetInteger(GL_VERTEX_ARRAY_BINDING) != glVertexArrayHandle) {
            throw new IllegalStateException("This vertex array was not bound");
        }

        glBindVertexArray(0);
        // } else {
        // throw new IllegalStateException("This vertex array was not bound");
        // }
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        glDeleteVertexArrays(glVertexArrayHandle);

        disposed = true;
    }


    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        if (disposed) {
            result.append("Disposed ");
        }

        result.append(super.toString());

        result.append(" [GL vertex array handle: ");
        result.append(glVertexArrayHandle);

        result.append(']');

        return result.toString();
    }

}
