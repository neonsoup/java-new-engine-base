package com.neonsoup.engine.graphics.gl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import org.joml.Vector4f;

import com.neonsoup.engine.graphics.RenderProperties.OverridePolicy;

public class GLStateProperties {

    public static enum PolygonMode {

        POINTL(GL_POINT), LINE(GL_LINE), FILL(GL_FILL);

        public final int glPolygonMode;


        private PolygonMode(int glPolygonMode) {
            this.glPolygonMode = glPolygonMode;
        }

    }


    public static enum BlendEquation {

        ADD(GL_FUNC_ADD),

        SUBTRACT(GL_FUNC_SUBTRACT),

        REVERSE_SUBTRACT(GL_FUNC_REVERSE_SUBTRACT),

        MIN(GL_MIN),

        MAX(GL_MAX);

        public final int glBlendEquation;


        private BlendEquation(int glBlendEquation) {
            this.glBlendEquation = glBlendEquation;
        }

    }


    public static enum BlendFunc {

        ZERO(GL_ZERO),

        ONE(GL_ONE),

        SRC_COLOR(GL_SRC_COLOR),

        ONE_MINUS_SRC_COLOR(GL_ONE_MINUS_SRC_COLOR),

        DST_COLOR(GL_DST_COLOR),

        ONE_MINUS_DST_COLOR(GL_ONE_MINUS_DST_COLOR),

        SRC_ALPHA(GL_SRC_ALPHA),

        ONE_MINUS_SRC_ALPHA(GL_ONE_MINUS_SRC_ALPHA),

        DST_ALPHA(GL_DST_ALPHA),

        ONE_MINUS_DST_ALPHA(GL_ONE_MINUS_DST_ALPHA),

        CONSTANT_COLOR(GL_CONSTANT_COLOR),

        ONE_MINUS_CONSTANT_COLOR(GL_ONE_MINUS_CONSTANT_COLOR),

        CONSTANT_ALPHA(GL_CONSTANT_ALPHA),

        ONE_MINUS_CONSTANT_ALPHA(GL_ONE_MINUS_CONSTANT_ALPHA),

        SRC_ALPHA_SATURATE(GL_SRC_ALPHA_SATURATE);

        public final int glBlendFunc;


        private BlendFunc(int glBlendFunc) {
            this.glBlendFunc = glBlendFunc;
        }

    }


    public static enum CullFace {

        FRONT(GL_FRONT), BACK(GL_BACK), FRONT_AND_BACK(GL_FRONT_AND_BACK);

        public final int glCullFace;


        private CullFace(int glCullFace) {
            this.glCullFace = glCullFace;
        }

    }


    private GLStateProperties parent = null;

    private OverridePolicy blendingOverride = OverridePolicy.REMAIN_UNCHANGED;
    private boolean blendEnabled = false;

    private BlendEquation blendEquationRGB = BlendEquation.ADD;
    private BlendEquation blendEquationALPHA = BlendEquation.ADD;

    private BlendFunc blendFuncSrcRGB = BlendFunc.ONE;
    private BlendFunc blendFuncDstRGB = BlendFunc.ZERO;
    private BlendFunc blendFuncSrcALPHA = BlendFunc.ONE;
    private BlendFunc blendFuncDstALPHA = BlendFunc.ZERO;

    private final Vector4f blendColor = new Vector4f(0.0f, 0.0f, 0.0f, 0.0f);

    private OverridePolicy polygonModeOverride = OverridePolicy.REMAIN_UNCHANGED;
    private PolygonMode polygonMode = PolygonMode.FILL;

    private OverridePolicy polygonOffsetOverride = OverridePolicy.REMAIN_UNCHANGED;
    private boolean polygonOffsetFill = false;
    private boolean polygonOffsetLine = false;
    private boolean polygonOffsetPoint = false;

    private float polygonOffsetFactor = 0.0f;
    private float polygonOffsetUnits = 0.0f;

    private OverridePolicy framebufferSRGBOverride = OverridePolicy.REMAIN_UNCHANGED;
    private boolean framebufferSRGB = false;

    private OverridePolicy cullFaceOverride = OverridePolicy.REMAIN_UNCHANGED;
    private boolean cullFaceEnabled = true;
    private CullFace cullFace = CullFace.BACK;

    private OverridePolicy depthTestOverride = OverridePolicy.REMAIN_UNCHANGED;
    private boolean depthTestEnabled = true;

    private OverridePolicy depthWriteOverride = OverridePolicy.REMAIN_UNCHANGED;
    private boolean depthWriteEnabled = true;

    private boolean inDrawBlock = false;


    public GLStateProperties() {
    }


    public GLStateProperties(GLStateProperties src) {
        this.set(src);
    }


    public void set(GLStateProperties src) {
        this.parent = src.parent;

        this.blendingOverride = src.blendingOverride;
        this.blendEnabled = src.blendEnabled;

        this.blendEquationRGB = src.blendEquationRGB;
        this.blendEquationALPHA = src.blendEquationALPHA;

        this.blendFuncSrcRGB = src.blendFuncSrcRGB;
        this.blendFuncDstRGB = src.blendFuncDstRGB;
        this.blendFuncSrcALPHA = src.blendFuncSrcALPHA;
        this.blendFuncDstALPHA = src.blendFuncDstALPHA;

        this.blendColor.set(src.blendColor);

        this.polygonModeOverride = src.polygonModeOverride;
        this.polygonMode = src.polygonMode;

        this.polygonOffsetOverride = src.polygonOffsetOverride;
        this.polygonOffsetFill = src.polygonOffsetFill;
        this.polygonOffsetLine = src.polygonOffsetLine;
        this.polygonOffsetPoint = src.polygonOffsetPoint;

        this.polygonOffsetFactor = src.polygonOffsetFactor;
        this.polygonOffsetUnits = src.polygonOffsetUnits;

        this.framebufferSRGBOverride = src.framebufferSRGBOverride;
        this.framebufferSRGB = src.framebufferSRGB;

        this.cullFaceOverride = src.cullFaceOverride;
        this.cullFaceEnabled = src.cullFaceEnabled;
        this.cullFace = src.cullFace;

        this.depthTestOverride = src.depthTestOverride;
        this.depthTestEnabled = src.depthTestEnabled;

        this.depthWriteOverride = src.depthWriteOverride;
        this.depthWriteEnabled = src.depthWriteEnabled;
    }


    public OverridePolicy getBlendingOverride() {
        return blendingOverride;
    }


    public void setBlendingOverride(OverridePolicy blendingOverride) {
        this.blendingOverride = blendingOverride;
    }


    public boolean isBlendingEnabled() {
        return blendEnabled;
    }


    public void setBlendingEnabled(boolean blendEnabled) {
        this.blendEnabled = blendEnabled;
    }


    public BlendEquation getBlendEquationRGB() {
        return blendEquationRGB;
    }


    public void setBlendEquationRGB(BlendEquation blendEquationRGB) {
        this.blendEquationRGB = blendEquationRGB;
    }


    public BlendEquation getBlendEquationALPHA() {
        return blendEquationALPHA;
    }


    public void setBlendEquationALPHA(BlendEquation blendEquationALPHA) {
        this.blendEquationALPHA = blendEquationALPHA;
    }


    public BlendFunc getBlendFuncSrcRGB() {
        return blendFuncSrcRGB;
    }


    public void setBlendFuncSrcRGB(BlendFunc blendFuncSrcRGB) {
        this.blendFuncSrcRGB = blendFuncSrcRGB;
    }


    public BlendFunc getBlendFuncDstRGB() {
        return blendFuncDstRGB;
    }


    public void setBlendFuncDstRGB(BlendFunc blendFuncDstRGB) {
        this.blendFuncDstRGB = blendFuncDstRGB;
    }


    public BlendFunc getBlendFuncSrcALPHA() {
        return blendFuncSrcALPHA;
    }


    public void setBlendFuncSrcALPHA(BlendFunc blendFuncSrcALPHA) {
        this.blendFuncSrcALPHA = blendFuncSrcALPHA;
    }


    public BlendFunc getBlendFuncDstALPHA() {
        return blendFuncDstALPHA;
    }


    public void setBlendFuncDstALPHA(BlendFunc blendFuncDstALPHA) {
        this.blendFuncDstALPHA = blendFuncDstALPHA;
    }


    public OverridePolicy getPolygonModeOverride() {
        return polygonModeOverride;
    }


    public void setPolygonModeOverride(OverridePolicy polygonModeOverride) {
        this.polygonModeOverride = polygonModeOverride;
    }


    public PolygonMode getPolygonMode() {
        return polygonMode;
    }


    public void setPolygonMode(PolygonMode glPolygonMode) {
        this.polygonMode = glPolygonMode;
    }


    public OverridePolicy getPolygonOffsetOverride() {
        return polygonOffsetOverride;
    }


    public void setPolygonOffsetOverride(OverridePolicy polygonOffsetOverride) {
        this.polygonOffsetOverride = polygonOffsetOverride;
    }


    public boolean isPolygonOffsetFillEnabled() {
        return polygonOffsetFill;
    }


    public void setPolygonOffsetFill(boolean polygonOffsetFill) {
        this.polygonOffsetFill = polygonOffsetFill;
    }


    public boolean isPolygonOffsetLineEnabled() {
        return polygonOffsetLine;
    }


    public void setPolygonOffsetLine(boolean polygonOffsetLine) {
        this.polygonOffsetLine = polygonOffsetLine;
    }


    public boolean isPolygonOffsetPointEnabled() {
        return polygonOffsetPoint;
    }


    public void setPolygonOffsetPoint(boolean polygonOffsetPoint) {
        this.polygonOffsetPoint = polygonOffsetPoint;
    }


    public float getPolygonOffsetFactor() {
        return polygonOffsetFactor;
    }


    public void setPolygonOffsetFactor(float polygonOffsetFactor) {
        this.polygonOffsetFactor = polygonOffsetFactor;
    }


    public float getPolygonOffsetUnits() {
        return polygonOffsetUnits;
    }


    public void setPolygonOffsetUnits(float polygonOffsetUnits) {
        this.polygonOffsetUnits = polygonOffsetUnits;
    }


    public OverridePolicy getFramebufferSRGBOverride() {
        return framebufferSRGBOverride;
    }


    public void setFramebufferSRGBOverride(OverridePolicy framebufferSRGBOverride) {
        this.framebufferSRGBOverride = framebufferSRGBOverride;
    }


    public boolean getFramebufferSRGB() {
        return framebufferSRGB;
    }


    public void setFramebufferSRGB(boolean framebufferSRGB) {
        this.framebufferSRGB = framebufferSRGB;
    }


    public void setBlendColor(float r, float g, float b, float a) {
        blendColor.set(r, g, b, a);
    }


    public float getBlendColorRed() {
        return blendColor.x;
    }


    public float getBlendColorGreen() {
        return blendColor.y;
    }


    public float getBlendColorBlue() {
        return blendColor.z;
    }


    public float getBlendColorAlpha() {
        return blendColor.w;
    }


    public OverridePolicy getCullFaceOverride() {
        return cullFaceOverride;
    }


    public void setCullFaceOverride(OverridePolicy cullFaceOverride) {
        this.cullFaceOverride = cullFaceOverride;
    }


    public boolean isCullFaceEnabled() {
        return cullFaceEnabled;
    }


    public void setCullFaceEnabled(boolean cullFaceEnabled) {
        this.cullFaceEnabled = cullFaceEnabled;
    }


    public CullFace getCullFace() {
        return cullFace;
    }


    public void setCullFace(CullFace cullFace) {
        this.cullFace = cullFace;
    }


    public OverridePolicy getDepthTestOverride() {
        return depthTestOverride;
    }


    public void setDepthTestOverride(OverridePolicy depthTestOverride) {
        this.depthTestOverride = depthTestOverride;
    }


    public boolean isDepthTestEnabled() {
        return depthTestEnabled;
    }


    public void setDepthTestEnabled(boolean depthTestEnabled) {
        this.depthTestEnabled = depthTestEnabled;
    }


    public OverridePolicy getDepthWriteOverride() {
        return depthWriteOverride;
    }


    public void setDepthWriteOverride(OverridePolicy depthWriteOverride) {
        this.depthWriteOverride = depthWriteOverride;
    }


    public boolean isDepthWriteEnabled() {
        return depthWriteEnabled;
    }


    public void setDepthWriteEnabled(boolean depthWriteEnabled) {
        this.depthWriteEnabled = depthWriteEnabled;
    }


    public void setOverrideAll() {
        blendingOverride = OverridePolicy.OVERRIDE;
        polygonModeOverride = OverridePolicy.OVERRIDE;
        polygonOffsetOverride = OverridePolicy.OVERRIDE;
        framebufferSRGBOverride = OverridePolicy.OVERRIDE;
        cullFaceOverride = OverridePolicy.OVERRIDE;
        depthTestOverride = OverridePolicy.OVERRIDE;
        depthWriteOverride = OverridePolicy.OVERRIDE;
    }


    public void beginDraw(GLStateProperties parent) {
        if (inDrawBlock) {
            throw new IllegalStateException();
        }

        inDrawBlock = true;

        this.parent = parent;

        if (blendingOverride == OverridePolicy.OVERRIDE) {
            setBlendingInternal();
        }

        if (polygonModeOverride == OverridePolicy.OVERRIDE) {
            setPolygonModeInternal();
        }

        if (polygonOffsetOverride == OverridePolicy.OVERRIDE) {
            setPolygonOffsetInternal();
        }

        if (framebufferSRGBOverride == OverridePolicy.OVERRIDE) {
            setFramebufferSRGBInternal();
        }

        if (cullFaceOverride == OverridePolicy.OVERRIDE) {
            setCullFaceInternal();
        }

        if (depthTestOverride == OverridePolicy.OVERRIDE) {
            setDepthTestInternal();
        }

        if (depthWriteOverride == OverridePolicy.OVERRIDE) {
            setDepthWriteInternal();
        }
    }


    public void endDraw() {
        if (!inDrawBlock) {
            throw new IllegalStateException();
        }

        if (parent != null) {
            if (blendingOverride == OverridePolicy.OVERRIDE) {
                parent.restoreBlending();
            }

            if (polygonModeOverride == OverridePolicy.OVERRIDE) {
                parent.restorePolygonMode();
            }

            if (polygonOffsetOverride == OverridePolicy.OVERRIDE) {
                parent.restorePolygonOffset();
            }

            if (framebufferSRGBOverride == OverridePolicy.OVERRIDE) {
                parent.restoreFramebufferSRGB();
            }

            if (cullFaceOverride == OverridePolicy.OVERRIDE) {
                parent.restoreCullFace();
            }

            if (depthTestOverride == OverridePolicy.OVERRIDE) {
                parent.restoreDepthTest();
            }

            if (depthWriteOverride == OverridePolicy.OVERRIDE) {
                parent.restoreDepthWrite();
            }
        }

        this.parent = null;

        inDrawBlock = false;
    }


    protected void restoreBlending() {
        if (blendingOverride == OverridePolicy.OVERRIDE) {
            setBlendingInternal();
        } else if (parent != null) {
            parent.restoreBlending();
        }
    }


    private void setBlendingInternal() {
        if (blendEnabled) {
            glEnable(GL_BLEND);
        } else {
            glDisable(GL_BLEND);
        }

        glBlendEquationSeparate(blendEquationRGB.glBlendEquation, blendEquationALPHA.glBlendEquation);

        glBlendFuncSeparate(blendFuncSrcRGB.glBlendFunc,
                            blendFuncDstRGB.glBlendFunc,
                            blendFuncSrcALPHA.glBlendFunc,
                            blendFuncDstALPHA.glBlendFunc);

        glBlendColor(blendColor.x, blendColor.y, blendColor.z, blendColor.w);
    }


    protected void restorePolygonOffset() {
        if (polygonOffsetOverride == OverridePolicy.OVERRIDE) {
            setPolygonOffsetInternal();
        } else if (parent != null) {
            parent.restorePolygonOffset();
        }
    }


    private void setPolygonModeInternal() {
        glPolygonMode(GL_FRONT_AND_BACK, polygonMode.glPolygonMode);
    }


    protected void restorePolygonMode() {
        if (polygonModeOverride == OverridePolicy.OVERRIDE) {
            setPolygonModeInternal();
        } else if (parent != null) {
            parent.restorePolygonMode();
        }
    }


    private void setPolygonOffsetInternal() {
        if (polygonOffsetFill) {
            glEnable(GL_POLYGON_OFFSET_FILL);
        } else {
            glDisable(GL_POLYGON_OFFSET_FILL);
        }

        if (polygonOffsetLine) {
            glEnable(GL_POLYGON_OFFSET_LINE);
        } else {
            glDisable(GL_POLYGON_OFFSET_LINE);
        }

        if (polygonOffsetPoint) {
            glEnable(GL_POLYGON_OFFSET_POINT);
        } else {
            glDisable(GL_POLYGON_OFFSET_POINT);
        }

        glPolygonOffset(polygonOffsetFactor, polygonOffsetUnits);
    }


    protected void restoreFramebufferSRGB() {
        if (framebufferSRGBOverride == OverridePolicy.OVERRIDE) {
            setFramebufferSRGBInternal();
        } else if (parent != null) {
            parent.restoreFramebufferSRGB();
        }
    }


    private void setFramebufferSRGBInternal() {
        if (framebufferSRGB) {
            glEnable(GL_FRAMEBUFFER_SRGB);
        } else {
            glDisable(GL_FRAMEBUFFER_SRGB);
        }
    }


    protected void restoreCullFace() {
        if (cullFaceOverride == OverridePolicy.OVERRIDE) {
            setCullFaceInternal();
        } else if (parent != null) {
            parent.restoreCullFace();
        }
    }


    private void setCullFaceInternal() {
        if (cullFaceEnabled) {
            glEnable(GL_CULL_FACE);
        } else {
            glDisable(GL_CULL_FACE);
        }

        glCullFace(cullFace.glCullFace);
    }


    protected void restoreDepthTest() {
        if (depthTestOverride == OverridePolicy.OVERRIDE) {
            setDepthTestInternal();
        } else if (parent != null) {
            parent.restoreDepthTest();
        }
    }


    private void setDepthTestInternal() {
        if (depthTestEnabled) {
            glEnable(GL_DEPTH_TEST);
        } else {
            glDisable(GL_DEPTH_TEST);
        }
    }


    protected void restoreDepthWrite() {
        if (depthWriteOverride == OverridePolicy.OVERRIDE) {
            setDepthWriteInternal();
        } else if (parent != null) {
            parent.restoreDepthWrite();
        }
    }


    private void setDepthWriteInternal() {
        glDepthMask(depthWriteEnabled);
    }

}
