package com.neonsoup.engine.graphics.gl;

import java.awt.Transparency;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.awt.image.SampleModel;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.system.MemoryUtil;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.graphics.gl.GLTexture.DataFormat;
import com.neonsoup.engine.graphics.gl.GLTexture.DataType;
import com.neonsoup.engine.graphics.gl.GLTexture.InternalFormat;
import com.neonsoup.engine.graphics.gl.GLTexture.PackAlignment;

public class GLTextureData extends AbstractDisposable {

    public static enum Dimension {
        TEXTURE_1D, TEXTURE_2D, TEXTURE_3D;
    }


    private final Dimension dimension;

    private final int w, h, d;

    private final GLTexture.InternalFormat internalFormat;

    private final GLTexture.DataFormat dataFormat;
    private final GLTexture.DataType dataType;

    private final PackAlignment packAlign;

    private final ByteBuffer data;


    public GLTextureData(Dimension dimension,
                         int w,
                         int h,
                         int d,
                         InternalFormat internalFormat,
                         DataFormat dataFormat,
                         DataType dataType,
                         PackAlignment packAlign,
                         int dataSize) {

        this(dimension, w, h, d, internalFormat, dataFormat, dataType, packAlign, MemoryUtil.memAlloc(dataSize));
    }


    private GLTextureData(Dimension dimension,
                          int w,
                          int h,
                          int d,
                          InternalFormat internalFormat,
                          DataFormat dataFormat,
                          DataType dataType,
                          PackAlignment packAlign,
                          ByteBuffer data) {

        this.dimension = dimension;

        this.w = w;
        this.h = h;
        this.d = d;

        this.internalFormat = internalFormat;

        this.dataFormat = dataFormat;
        this.dataType = dataType;

        this.packAlign = packAlign;

        this.data = data;
    }


    public Dimension getDimension() {
        return dimension;
    }


    public int getWidth() {
        return w;
    }


    public int getHeight() {
        return h;
    }


    public int getDepth() {
        return d;
    }


    public GLTexture.InternalFormat getInternalFormat() {
        return internalFormat;
    }


    public GLTexture.DataFormat getDataFormat() {
        return dataFormat;
    }


    public GLTexture.DataType getDataType() {
        return dataType;
    }


    public GLTexture.PackAlignment getPackAlignment() {
        return packAlign;
    }


    public ByteBuffer getDataBuffer() {
        return data;
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        MemoryUtil.memFree(data);

        disposed = true;
    }


    private static final int CS_TAG_SRGB = 1;
    private static final int CS_TAG_LINEAR_RGB = 2;
    private static final int CS_TAG_GRAY = 3;

    private static final ColorSpace CS_SRGB = ColorSpace.getInstance(ColorSpace.CS_sRGB);
    private static final ColorSpace CS_LINEAR_RGB = ColorSpace.getInstance(ColorSpace.CS_LINEAR_RGB);
    private static final ColorSpace CS_GRAY = ColorSpace.getInstance(ColorSpace.CS_GRAY);


    public static GLTextureData loadColorTextureData(InputStream in, String format) throws IOException {
        BufferedImage srcImg = ImageIO.read(in);

        return createColorTextureData(srcImg);
    }


    public static GLTextureData createColorTextureData(BufferedImage srcImg) throws IOException {
        ColorModel srcColorModel = srcImg.getColorModel();

        boolean alpha = srcColorModel.hasAlpha();

        int w = srcImg.getWidth();
        int h = srcImg.getHeight();

        int destDataType = srcColorModel.getTransferType();

        ColorSpace srcColorSpace = srcColorModel.getColorSpace();

        int srcColorSpaceTag = 0;

        if (srcColorSpace.equals(CS_SRGB)) {
            srcColorSpaceTag = CS_TAG_SRGB;
        } else if (srcColorSpace.equals(CS_LINEAR_RGB)) {
            srcColorSpaceTag = CS_TAG_LINEAR_RGB;
        } else if (srcColorSpace.equals(CS_GRAY)) {
            srcColorSpaceTag = CS_TAG_GRAY;
        } else {
            throw new IOException("Unsupported source image color space: " + srcColorSpace);
        }

        if (srcColorSpaceTag == CS_TAG_SRGB) {
            destDataType = DataBuffer.TYPE_BYTE;
        }

        ColorModel destColorModel = new ComponentColorModel(srcColorSpaceTag == CS_TAG_GRAY ? CS_LINEAR_RGB
                                                                                            : srcColorSpace,
                                                            alpha,
                                                            false,
                                                            alpha ? Transparency.TRANSLUCENT : Transparency.OPAQUE,
                                                            destDataType);

        int dataBytesPerPixel = srcColorModel.getNumComponents();

        switch (destDataType) {
            case DataBuffer.TYPE_SHORT:
            case DataBuffer.TYPE_USHORT:
                dataBytesPerPixel *= 2;
                break;
        }

        ByteBuffer data = MemoryUtil.memAlloc(w * h * dataBytesPerPixel);

        {
            WritableRaster destRaster = Raster.createInterleavedRaster(destDataType,
                                                                       w,
                                                                       h,
                                                                       destColorModel.getNumComponents(),
                                                                       null);

            BufferedImage destImg = new BufferedImage(destColorModel, destRaster, false, null);

            ColorConvertOp colorConvert = new ColorConvertOp(srcColorSpace, destColorModel.getColorSpace(), null);

            destImg = colorConvert.filter(srcImg, destImg);

            int rowSize = destColorModel.getNumComponents() * w;

            switch (destDataType) {
                case DataBuffer.TYPE_BYTE: {
                    byte[] originalData = ((DataBufferByte) destRaster.getDataBuffer()).getData();
                    byte[] rowSwapData = new byte[originalData.length];

                    for (int row = 0; row < h; row++) {
                        int newRow = h - row - 1;

                        System.arraycopy(originalData, row * rowSize, rowSwapData, newRow * rowSize, rowSize);
                    }

                    data.put(rowSwapData);
                    data.rewind();
                    break;
                }

                case DataBuffer.TYPE_USHORT:
                case DataBuffer.TYPE_SHORT: {
                    short[] originalData;
                    if (destDataType == DataBuffer.TYPE_SHORT) {
                        originalData = ((DataBufferShort) destRaster.getDataBuffer()).getData();
                    } else {
                        originalData = ((DataBufferUShort) destRaster.getDataBuffer()).getData();
                    }

                    short[] rowSwapData = new short[originalData.length];

                    for (int row = 0; row < h; row++) {
                        int newRow = h - row - 1;

                        System.arraycopy(originalData, row * rowSize, rowSwapData, newRow * rowSize, rowSize);
                    }

                    data.asShortBuffer().put(rowSwapData);
                    break;
                }
            }
        }

        InternalFormat internalFormat = null;
        switch (destDataType) {
            case DataBuffer.TYPE_BYTE:
                if (srcColorSpaceTag == CS_TAG_SRGB) {
                    internalFormat = alpha ? InternalFormat.SRGB8_ALPHA8 : InternalFormat.SRGB8;
                } else {
                    internalFormat = alpha ? InternalFormat.RGBA8 : InternalFormat.RGB8;
                }
                break;

            case DataBuffer.TYPE_SHORT:
            case DataBuffer.TYPE_USHORT:
                internalFormat = alpha ? InternalFormat.RGBA16 : InternalFormat.RGB16;
                break;
        }

        DataFormat dataFormat = alpha ? DataFormat.RGBA : DataFormat.RGB;

        DataType dataType = null;
        switch (destDataType) {
            case DataBuffer.TYPE_BYTE:
                dataType = DataType.UNSIGNED_BYTE;
                break;

            case DataBuffer.TYPE_SHORT:
            case DataBuffer.TYPE_USHORT:
                dataType = DataType.UNSIGNED_SHORT;
                break;
        }

        return new GLTextureData(Dimension.TEXTURE_2D,
                                 w,
                                 h,
                                 1,
                                 internalFormat,
                                 dataFormat,
                                 dataType,
                                 PackAlignment.ALIGNMENT_1,
                                 data);
    }


    public static GLTextureData loadAlphaTextureData(InputStream in, String format) throws IOException {
        BufferedImage srcImg = ImageIO.read(in);

        ColorModel srcColorModel = srcImg.getColorModel();

        int w = srcImg.getWidth();
        int h = srcImg.getHeight();

        if (!srcColorModel.hasAlpha()) {
            throw new IOException("Source image has no alpha channel");
        }

        int destDataType = srcColorModel.getTransferType();

        int dataBytesPerPixel = 0;

        switch (destDataType) {
            case DataBuffer.TYPE_BYTE:
                dataBytesPerPixel = 1;
                break;

            case DataBuffer.TYPE_SHORT:
            case DataBuffer.TYPE_USHORT:
                dataBytesPerPixel = 2;
                break;
        }

        ByteBuffer data = MemoryUtil.memAlloc(w * h * dataBytesPerPixel);

        Object alphaData;

        {
            int[] bands = new int[] { srcColorModel.getNumColorComponents() };
            SampleModel alphaSampleModel = srcImg.getSampleModel().createSubsetSampleModel(bands);

            alphaData = alphaSampleModel.getDataElements(0, 0, w, h, null, srcImg.getData().getDataBuffer());
        }

        switch (destDataType) {
            case DataBuffer.TYPE_BYTE: {
                byte[] originalData = (byte[]) alphaData;
                byte[] rowSwapData = new byte[originalData.length];

                for (int row = 0; row < h; row++) {
                    int newRow = h - row - 1;

                    System.arraycopy(originalData, row * w, rowSwapData, newRow * w, w);
                }

                data.put(rowSwapData);
                data.rewind();
                break;
            }

            case DataBuffer.TYPE_SHORT:
            case DataBuffer.TYPE_USHORT: {
                short[] originalData = (short[]) alphaData;
                short[] rowSwapData = new short[originalData.length];

                for (int row = 0; row < h; row++) {
                    int newRow = h - row - 1;

                    System.arraycopy(originalData, row * w, rowSwapData, newRow * w, w);
                }

                data.asShortBuffer().put(rowSwapData);
                break;
            }
        }

        data.rewind();

        InternalFormat internalFormat = null;
        switch (destDataType) {
            case DataBuffer.TYPE_BYTE:
                internalFormat = InternalFormat.R8;
                break;

            case DataBuffer.TYPE_SHORT:
            case DataBuffer.TYPE_USHORT:
                internalFormat = InternalFormat.R16;
                break;
        }

        DataFormat dataFormat = DataFormat.RED;

        DataType dataType = null;
        switch (destDataType) {
            case DataBuffer.TYPE_BYTE:
                dataType = DataType.UNSIGNED_BYTE;
                break;

            case DataBuffer.TYPE_SHORT:
            case DataBuffer.TYPE_USHORT:
                dataType = DataType.UNSIGNED_SHORT;
                break;
        }

        return new GLTextureData(Dimension.TEXTURE_2D,
                                 w,
                                 h,
                                 1,
                                 internalFormat,
                                 dataFormat,
                                 dataType,
                                 PackAlignment.ALIGNMENT_1,
                                 data);
    }

}
