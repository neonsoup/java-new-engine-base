package com.neonsoup.engine.graphics.gl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL21.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL31.*;
import static org.lwjgl.opengl.GL32.*;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Matrix3fc;
import org.joml.Matrix3x2fc;
import org.joml.Matrix4fc;
import org.joml.Vector2fc;
import org.joml.Vector3fc;
import org.joml.Vector4fc;
import org.lwjgl.system.MemoryStack;

import gnu.trove.impl.Constants;
import gnu.trove.map.TIntIntMap;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.procedure.TObjectIntProcedure;

public class GLShaderProgram extends GLUnmanagedObject {

    public class UniformBlock {

        private final String name;
        private final int index;

        private final int binding;

        private final int size;

        private final int[] uniformIndices;

        private final TIntIntMap uniformOffsets;
        private final TIntIntMap uniformArrayStrides;
        private final TIntIntMap uniformMatrixStrides;


        protected UniformBlock(String name,
                               int index,
                               int binding,
                               int size,
                               int[] uniformIndices,
                               TIntIntMap uniformOffsets,
                               TIntIntMap uniformArrayStrides,
                               TIntIntMap uniformMatrixStrides) {

            this.name = name;
            this.index = index;

            this.binding = binding;

            this.size = size;

            this.uniformIndices = uniformIndices;

            this.uniformOffsets = uniformOffsets;
            this.uniformArrayStrides = uniformArrayStrides;
            this.uniformMatrixStrides = uniformMatrixStrides;
        }


        public GLShaderProgram getShader() {
            return GLShaderProgram.this;
        }


        public String getName() {
            return name;
        }


        public int getIndex() {
            return index;
        }


        public int getBinding() {
            return binding;
        }


        public int getSize() {
            return size;
        }


        public int getUniformCount() {
            return uniformIndices.length;
        }


        public int getUniformIndexForWithinBlockIndex(int uniformIndexWithinBlock) {
            return uniformIndices[uniformIndexWithinBlock];
        }


        public int getUniformOffset(int uniformIndex) {
            return uniformOffsets.get(uniformIndex);
        }


        public int getUniformArrayStride(int uniformIndex) {
            return uniformArrayStrides.get(uniformIndex);
        }


        public int getUniformMatrixStride(int uniformIndex) {
            return uniformMatrixStrides.get(uniformIndex);
        }


        @Override
        public String toString() {
            StringBuilder result = new StringBuilder(getClass().getSimpleName());

            result.append(" [Name: ");
            result.append(name);

            result.append("; Index: ");
            result.append(index);

            result.append("; Binding: ");
            result.append(binding);

            result.append("; Size: ");
            result.append(size);

            if (uniformIndices.length > 0) {
                result.append("; Uniforms: ");

                for (int uniform = 0; uniform < uniformIndices.length; uniform++) {
                    result.append(activeUniformNamesByIndices[uniformIndices[uniform]]);

                    result.append(" [Offset: ");
                    result.append(uniformOffsets.get(uniformIndices[uniform]));

                    int stride;
                    if ((stride = uniformArrayStrides.get(uniformIndices[uniform])) > 0) {
                        result.append("; Array stride: ");
                        result.append(stride);
                    }

                    if ((stride = uniformMatrixStrides.get(uniformIndices[uniform])) > 0) {
                        result.append("; Matrix stride: ");
                        result.append(stride);
                    }

                    result.append(']');

                    if (uniform < uniformIndices.length - 1) {
                        result.append("; ");
                    }
                }

            }

            result.append(']');

            return result.toString();
        }

    }


    private final int glProgramHandle;

    private final int glVertexShaderHandle;
    private final int glGeometryShaderHandle;
    private final int glFragmentShaderHandle;

    private final TObjectIntMap<String> activeUniformLocations;
    private final TObjectIntMap<String> activeUniformIndices;
    private final String[] activeUniformNamesByIndices;

    private final Map<String, UniformBlock> activeUniformBlocks;

    protected final TObjectIntMap<String> activeAttributeLocations;
    protected final String[] activeAttributeNamesByIndices;


    public GLShaderProgram(String vertexSource, String fragmentSource) {
        this(vertexSource, null, fragmentSource, null, null, null);
    }


    public GLShaderProgram(String vertexSource, String geometrySource, String fragmentSource) {
        this(vertexSource, geometrySource, fragmentSource, null, null, null);
    }


    public GLShaderProgram(String vertexSource,
                           String fragmentSource,
                           Map<String, String> defines,
                           TObjectIntMap<String> attributeLocations,
                           TObjectIntMap<String> fragmentOutputLocations) {

        this(vertexSource, null, fragmentSource, defines, attributeLocations, fragmentOutputLocations);
    }


    public GLShaderProgram(String vertexSource,
                           String geometrySource,
                           String fragmentSource,
                           Map<String, String> defines,
                           TObjectIntMap<String> attributeLocations,
                           TObjectIntMap<String> fragmentOutputLocations) {

        if (vertexSource == null) {
            throw new IllegalArgumentException("vertexSource is null");
        }
        if (fragmentSource == null) {
            throw new IllegalArgumentException("fragmentSource is null");
        }

        glVertexShaderHandle = compileShader(GL_VERTEX_SHADER, vertexSource, defines);

        if (geometrySource != null) {
            try {
                glGeometryShaderHandle = compileShader(GL_GEOMETRY_SHADER, geometrySource, defines);
            } catch (Exception e) {
                glDeleteShader(glVertexShaderHandle);

                throw e;
            }
        } else {
            glGeometryShaderHandle = 0;
        }

        try {
            glFragmentShaderHandle = compileShader(GL_FRAGMENT_SHADER, fragmentSource, defines);
        } catch (Exception e) {
            glDeleteShader(glVertexShaderHandle);

            if (geometrySource != null) {
                glDeleteShader(glGeometryShaderHandle);
            }

            throw e;
        }

        glProgramHandle = glCreateProgram();

        if (glProgramHandle == 0) {
            int[] err = GLException.getErrors();

            glDeleteShader(glVertexShaderHandle);
            if (glGeometryShaderHandle != 0) {
                glDeleteShader(glGeometryShaderHandle);
            }
            glDeleteShader(glFragmentShaderHandle);

            throw new GLException("Could not create new program", err);
        }

        try {
            glAttachShader(glProgramHandle, glVertexShaderHandle);
            if (glGeometryShaderHandle != 0) {
                glAttachShader(glProgramHandle, glGeometryShaderHandle);
            }
            glAttachShader(glProgramHandle, glFragmentShaderHandle);

            if (attributeLocations != null) {
                attributeLocations.forEachEntry(new TObjectIntProcedure<String>() {

                    @Override
                    public boolean execute(String attribName, int attribIndex) {

                        glBindAttribLocation(glProgramHandle, attribIndex, attribName);

                        if (GLSettings.isGLErrorCheckingEnabled()) {
                            int[] err = GLException.getErrors();

                            if (err.length > 0) {
                                disposeInternal();

                                throw new GLException("Could not bind attribute location "
                                                      + attribIndex
                                                      + " for attribute \""
                                                      + attribName
                                                      + "\"",
                                                      err);
                            }
                        }

                        return true;
                    }

                });
            }

            if (fragmentOutputLocations != null) {
                fragmentOutputLocations.forEachEntry(new TObjectIntProcedure<String>() {

                    @Override
                    public boolean execute(String outputName, int outputIndex) {

                        glBindFragDataLocation(glProgramHandle, outputIndex, outputName);

                        if (GLSettings.isGLErrorCheckingEnabled()) {
                            int[] err = GLException.getErrors();

                            if (err.length > 0) {
                                disposeInternal();

                                throw new GLException("Could not bind fragment output location "
                                                      + outputIndex
                                                      + " for fragment output \""
                                                      + outputName
                                                      + "\"",
                                                      err);
                            }
                        }

                        return true;
                    }

                });
            }

            glLinkProgram(glProgramHandle);

            if (glGetProgrami(glProgramHandle, GL_LINK_STATUS) == GL_FALSE) {

                String infoLog = glGetProgramInfoLog(glProgramHandle);

                disposeInternal();

                throw new GLException("Could not link program. Program info log:\n" + infoLog, GLException.getErrors());
            }

            {

                int activeUniformCount = glGetProgrami(glProgramHandle, GL_ACTIVE_UNIFORMS);

                activeUniformLocations = new TObjectIntHashMap<>(activeUniformCount, Constants.DEFAULT_LOAD_FACTOR, -1);
                activeUniformIndices = new TObjectIntHashMap<>(activeUniformCount, Constants.DEFAULT_LOAD_FACTOR, -1);
                activeUniformNamesByIndices = new String[activeUniformCount];

                for (int i = 0; i < activeUniformCount; i++) {
                    String name = glGetActiveUniformName(glProgramHandle, i);

                    activeUniformLocations.put(name, glGetUniformLocation(glProgramHandle, name));
                    activeUniformIndices.put(name, i);
                    activeUniformNamesByIndices[i] = name;
                }
            }

            {
                int activeUnformBlockCount = glGetProgrami(glProgramHandle, GL_ACTIVE_UNIFORM_BLOCKS);

                activeUniformBlocks = new HashMap<>(activeUnformBlockCount);

                for (int i = 0; i < activeUnformBlockCount; i++) {
                    String name = glGetActiveUniformBlockName(glProgramHandle, i);

                    int dataSize;

                    int blockBinding;

                    int[] blockUniformIndices;

                    TIntIntMap uniformOffsets;
                    TIntIntMap uniformArrayStrides;
                    TIntIntMap uniformMatrixStrides;

                    {
                        dataSize = glGetActiveUniformBlocki(glProgramHandle, i, GL_UNIFORM_BLOCK_DATA_SIZE);

                        int blockUniformCount = glGetActiveUniformBlocki(glProgramHandle,
                                                                         i,
                                                                         GL_UNIFORM_BLOCK_ACTIVE_UNIFORMS);

                        blockBinding = glGetActiveUniformBlocki(glProgramHandle, i, GL_UNIFORM_BLOCK_BINDING);

                        blockUniformIndices = new int[blockUniformCount];

                        glGetActiveUniformBlockiv(glProgramHandle,
                                                  i,
                                                  GL_UNIFORM_BLOCK_ACTIVE_UNIFORM_INDICES,
                                                  blockUniformIndices);

                        Arrays.sort(blockUniformIndices);

                        int[] uniformParams = new int[blockUniformCount];

                        uniformOffsets = new TIntIntHashMap(blockUniformCount);
                        {
                            glGetActiveUniformsiv(glProgramHandle,
                                                  blockUniformIndices,
                                                  GL_UNIFORM_OFFSET,
                                                  uniformParams);

                            for (int u = 0; u < blockUniformCount; u++) {
                                uniformOffsets.put(blockUniformIndices[u], uniformParams[u]);
                            }
                        }

                        uniformArrayStrides = new TIntIntHashMap(blockUniformCount);
                        {
                            glGetActiveUniformsiv(glProgramHandle,
                                                  blockUniformIndices,
                                                  GL_UNIFORM_ARRAY_STRIDE,
                                                  uniformParams);

                            for (int u = 0; u < blockUniformCount; u++) {
                                uniformArrayStrides.put(blockUniformIndices[u], uniformParams[u]);
                            }
                        }

                        uniformMatrixStrides = new TIntIntHashMap(blockUniformCount);
                        {
                            glGetActiveUniformsiv(glProgramHandle,
                                                  blockUniformIndices,
                                                  GL_UNIFORM_MATRIX_STRIDE,
                                                  uniformParams);

                            for (int u = 0; u < blockUniformCount; u++) {
                                uniformMatrixStrides.put(blockUniformIndices[u], uniformParams[u]);
                            }
                        }
                    }

                    UniformBlock block = new UniformBlock(name,
                                                          i,
                                                          blockBinding,
                                                          dataSize,
                                                          blockUniformIndices,
                                                          uniformOffsets,
                                                          uniformArrayStrides,
                                                          uniformMatrixStrides);

                    activeUniformBlocks.put(name, block);
                }
            }

            {
                int activeAttributeCount = glGetProgrami(glProgramHandle, GL_ACTIVE_ATTRIBUTES);

                activeAttributeLocations = new TObjectIntHashMap<>(activeAttributeCount,
                                                                   Constants.DEFAULT_LOAD_FACTOR,
                                                                   -1);

                activeAttributeNamesByIndices = new String[activeAttributeCount];

                try (MemoryStack stack = MemoryStack.stackPush()) {
                    IntBuffer size = stack.mallocInt(1);
                    IntBuffer type = stack.mallocInt(1);

                    for (int i = 0; i < activeAttributeCount; i++) {
                        String name = glGetActiveAttrib(glProgramHandle, i, size, type);

                        int location = glGetAttribLocation(glProgramHandle, name);

                        activeAttributeLocations.put(name, location);

                        activeAttributeNamesByIndices[i] = name;
                    }
                }
            }

        } catch (Exception e) {
            disposeInternal();

            throw e;
        }
    }


    private int compileShader(int type, String source, Map<String, String> defines) {
        switch (type) {
            default:
                throw new IllegalArgumentException("Illegal shader type");
            case GL_VERTEX_SHADER:
            case GL_GEOMETRY_SHADER:
            case GL_FRAGMENT_SHADER:
        }

        if (defines != null && !defines.isEmpty()) {
            StringBuilder definesStr = new StringBuilder(100 * defines.size());

            for (Map.Entry<String, String> ent : defines.entrySet()) {
                definesStr.append("#define ");
                definesStr.append(ent.getKey());
                definesStr.append(' ');
                definesStr.append(ent.getValue());
                definesStr.append("\n");
            }

            StringBuilder sourceStr = new StringBuilder(definesStr.length() + source.length());

            String[] lines = source.split("\\R");

            int versionLine = -1;

            for (int i = 0; i < lines.length; i++) {
                if (lines[i].indexOf("#version") >= 0) {
                    versionLine = i;
                    break;
                }
            }

            if (versionLine >= 0) {
                int idx = 0;

                for (; idx <= versionLine; idx++) {
                    sourceStr.append(lines[idx]);
                    sourceStr.append('\n');
                }

                sourceStr.append(definesStr);

                for (; idx < lines.length; idx++) {
                    sourceStr.append(lines[idx]);
                    sourceStr.append('\n');
                }
            } else {
                sourceStr.append(definesStr);
                sourceStr.append(source);
            }

            source = sourceStr.toString();
        }

        int handle = glCreateShader(type);

        if (handle == 0) {
            throw new GLException("Could not create new "
                                  + shaderTypeString(type)
                                  + " shader",
                                  GLException.getErrors());
        }

        try {
            glShaderSource(handle, source);
            glCompileShader(handle);

            int status = glGetShaderi(handle, GL_COMPILE_STATUS);

            if (status == GL_FALSE) {
                int[] err = GLException.getErrors();

                String msg = glGetShaderInfoLog(handle);

                throw new GLException("Could not compile "
                                      + shaderTypeString(type)
                                      + " shader. Shader info log:\n"
                                      + msg,
                                      err);
            }
        } catch (Exception e) {
            glDeleteShader(handle);

            throw e;
        }

        return handle;
    }


    private static String shaderTypeString(int type) {
        switch (type) {
            case GL_VERTEX_SHADER:
                return "vertex";

            case GL_GEOMETRY_SHADER:
                return "geometry";

            case GL_FRAGMENT_SHADER:
                return "fragment";

            default:
                throw new IllegalArgumentException("Illegal shader type");
        }
    }


    private void disposeInternal() {
        glDetachShader(glProgramHandle, glVertexShaderHandle);
        glDeleteShader(glVertexShaderHandle);

        if (glGeometryShaderHandle != 0) {
            glDetachShader(glProgramHandle, glGeometryShaderHandle);
            glDeleteShader(glGeometryShaderHandle);
        }

        glDetachShader(glProgramHandle, glFragmentShaderHandle);
        glDeleteShader(glFragmentShaderHandle);

        glDeleteProgram(glProgramHandle);
    }


    public int getGLProgramHandle() {
        ensureUndisposed();

        return glProgramHandle;
    }


    public boolean validate() {
        ensureUndisposed();

        glValidateProgram(glProgramHandle);

        return glGetProgrami(glProgramHandle, GL_VALIDATE_STATUS) == GL_TRUE;
    }


    public String getInfoLog() {
        ensureUndisposed();

        return glGetProgramInfoLog(glProgramHandle);
    }


    public int getAttributeLocation(String name) {
        ensureUndisposed();
        if (name == null) {
            throw new IllegalArgumentException("Attribute name must not be null");
        }

        return activeAttributeLocations.get(name);
    }


    public int getUniformLocation(String name) {
        ensureUndisposed();
        if (name == null) {
            throw new IllegalArgumentException("Uniform name must not be null");
        }

        return activeUniformLocations.get(name);
    }


    public int getUniformIndex(String name) {
        ensureUndisposed();
        if (name == null) {
            throw new IllegalArgumentException("Uniform name must not be null");
        }

        return activeUniformIndices.get(name);
    }


    public UniformBlock getUniformBlock(String blockName) {
        ensureUndisposed();
        if (blockName == null) {
            throw new IllegalArgumentException("Block name must not be null");
        }

        return activeUniformBlocks.get(blockName);
    }


    public void beginUse() {
        ensureUndisposed();

        glUseProgram(glProgramHandle);
    }


    public void endUse() {
        ensureUndisposed();

        // int used = glGetInteger(GL_CURRENT_PROGRAM);

        // if (used == glProgramHandle) {

        if (GLSettings.isGLErrorCheckingEnabled() && glGetInteger(GL_CURRENT_PROGRAM) != glProgramHandle) {
            throw new IllegalStateException("This program was not in use");
        }

        glUseProgram(0);
        // } else {
        // throw new IllegalStateException("This program was not in use");
        // }
    }


    public void uniform1i(int location, int v0) {
        glUniform1i(location, v0);
    }


    public void uniform1f(int location, float v0) {
        glUniform1f(location, v0);
    }


    public void uniform2f(int location, float v0, float v1) {
        glUniform2f(location, v0, v1);
    }


    public void uniform2f(int location, Vector2fc v) {
        glUniform2f(location, v.x(), v.y());
    }


    public void uniform3f(int location, float v0, float v1, float v2) {
        glUniform3f(location, v0, v1, v2);
    }


    public void uniform3f(int location, Vector3fc v) {
        glUniform3f(location, v.x(), v.y(), v.z());
    }


    public void uniform4f(int location, float v0, float v1, float v2, float v3) {
        glUniform4f(location, v0, v1, v2, v3);
    }


    public void uniform4f(int location, Vector4fc v) {
        glUniform4f(location, v.x(), v.y(), v.z(), v.w());
    }


    public void uniformMatrix3f(int location, Matrix3fc mat) {
        try (MemoryStack stack = MemoryStack.stackPush()) {

            FloatBuffer buf = stack.mallocFloat(9);

            mat.get(buf);

            buf.rewind();

            glUniformMatrix3fv(location, false, buf);
        }
    }


    public void uniformMatrix3x2f(int location, Matrix3x2fc mat) {
        try (MemoryStack stack = MemoryStack.stackPush()) {

            FloatBuffer buf = stack.mallocFloat(6);

            mat.get(buf);

            buf.rewind();

            glUniformMatrix3x2fv(location, false, buf);
        }
    }


    public void uniformMatrix4f(int location, Matrix4fc mat) {
        try (MemoryStack stack = MemoryStack.stackPush()) {

            FloatBuffer buf = stack.mallocFloat(16);

            mat.get(buf);

            buf.rewind();

            glUniformMatrix4fv(location, false, buf);
        }
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        disposeInternal();

        disposed = true;
    }


    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();

        if (disposed) {
            result.append("Disposed ");
        }

        result.append(this.getClass().getSimpleName());

        result.append(" [GL program handle: ");
        result.append(glProgramHandle);

        result.append(']');

        if (!activeAttributeLocations.isEmpty()) {
            result.append("\n\nActive attributes:\n");

            for (int attrib = 0; attrib < activeAttributeLocations.size(); attrib++) {
                String name = activeAttributeNamesByIndices[attrib];
                result.append(name);

                result.append(" [Index: ");
                result.append(attrib);

                result.append("; Location: ");
                result.append(activeAttributeLocations.get(name));

                if (attrib < activeAttributeLocations.size() - 1) {
                    result.append("];\n");
                } else {
                    result.append(']');
                }
            }
        }

        if (!activeUniformLocations.isEmpty()) {
            result.append("\n\nActive uniforms:\n");

            for (int uniform = 0; uniform < activeUniformLocations.size(); uniform++) {
                String name = activeUniformNamesByIndices[uniform];
                result.append(name);

                result.append(" [Index: ");
                result.append(uniform);

                result.append("; Location: ");
                result.append(activeUniformLocations.get(name));

                if (uniform < activeUniformLocations.size() - 1) {
                    result.append("];\n");
                } else {
                    result.append(']');
                }
            }
        }

        if (!activeUniformBlocks.isEmpty()) {
            result.append("\n\nActive uniform blocks:\n");

            List<UniformBlock> blocks = new ArrayList<>(activeUniformBlocks.values());

            Collections.sort(blocks, new Comparator<UniformBlock>() {

                @Override
                public int compare(UniformBlock o1, UniformBlock o2) {
                    return Integer.compare(o1.index, o2.index);
                }

            });

            for (int block = 0; block < blocks.size(); block++) {
                result.append(blocks.get(block).toString());

                if (block < blocks.size() - 1) {
                    result.append(";\n");
                }
            }
        }

        return result.toString();
    }

}
