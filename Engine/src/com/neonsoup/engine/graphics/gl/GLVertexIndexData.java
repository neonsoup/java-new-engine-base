package com.neonsoup.engine.graphics.gl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;

public class GLVertexIndexData extends GLBufferData {

    public static enum Type {

        UNSIGNED_BYTE(GL_UNSIGNED_BYTE, 1),

        UNSIGNED_SHORT(GL_UNSIGNED_SHORT, 2),

        UNSIGNED_INT(GL_UNSIGNED_INT, 4);

        public final int glType;
        public final int elementSize;


        private Type(int glType, int elementSize) {
            this.glType = glType;
            this.elementSize = elementSize;
        }

    }


    protected final Type type;


    public GLVertexIndexData(GLBuffer buffer, Type type) {
        this(buffer, 0, type);
    }


    public GLVertexIndexData(GLBuffer buffer, int offset, Type type) {
        super(buffer, offset, false);

        if (type == null) {
            throw new IllegalArgumentException("Type must not be null");
        }
        checkOffset(offset);

        this.type = type;
    }


    public Type getType() {
        return type;
    }


    public void bind() {
        ensureBufferTarget(GLBuffer.Target.ELEMENT_ARRAY_BUFFER);
        buffer.bind();
    }


    public void unbind() {
        if (GLSettings.isGLErrorCheckingEnabled()
            && glGetInteger(GL_ELEMENT_ARRAY_BUFFER_BINDING) != buffer.getGLBufferHandle()) {

            throw new IllegalStateException("This buffer was not bound");
        } else {
            buffer.unbind();
        }
    }

}
