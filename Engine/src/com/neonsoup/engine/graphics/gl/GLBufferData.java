package com.neonsoup.engine.graphics.gl;

public abstract class GLBufferData {

    protected final GLBuffer buffer;

    protected final boolean mutable;
    protected int offset;


    public GLBufferData(GLBuffer buffer) {
        this(buffer, 0, true);
    }


    public GLBufferData(GLBuffer buffer, int offset) {
        this(buffer, offset, true);
    }


    public GLBufferData(GLBuffer buffer, int offset, boolean mutable) {
        if (buffer == null) {
            throw new IllegalArgumentException("Buffer must not be null");
        }
        checkOffset(offset);

        this.mutable = mutable;

        this.buffer = buffer;
        this.offset = offset;
    }


    public boolean isMutable() {
        return mutable;
    }


    public GLBuffer getBuffer() {
        return buffer;
    }


    public int getOffset() {
        return offset;
    }


    public void setOffset(int offset) {
        if (mutable) {
            checkOffset(offset);

            this.offset = offset;
        } else {
            throw new UnsupportedOperationException("This buffer data is immutable");
        }
    }


    protected void checkOffset(int offset) {
        if (offset < 0) {
            throw new IndexOutOfBoundsException("Offset must be a positive number or zero");
        }
    }


    protected void ensureBufferTarget(GLBuffer.Target target) {
        if (buffer.getTarget() != target) {
            throw new IllegalStateException("Buffer target must be " + target);
        }
    }

}
