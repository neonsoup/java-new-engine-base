package com.neonsoup.engine.graphics.gl;

import static org.lwjgl.opengl.GL11.*;

public enum GLFace {

    FRONT(GL_FRONT), BACK(GL_BACK), FRONT_AND_BACK(GL_FRONT_AND_BACK);

    public final int glFace;


    private GLFace(int glFace) {
        this.glFace = glFace;
    }

}
