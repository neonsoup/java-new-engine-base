package com.neonsoup.engine.graphics.gl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;

public final class GLVertexAttributeSignature {

    public static enum Type {

        BYTE(GL_BYTE, 1),

        UNSIGNED_BYTE(GL_UNSIGNED_BYTE, 1),

        SHORT(GL_SHORT, 2),

        UNSIGNED_SHORT(GL_UNSIGNED_SHORT, 2),

        INT(GL_INT, 4),

        UNSIGNED_INT(GL_UNSIGNED_INT, 4),

        FLOAT(GL_FLOAT, 4),

        DOUBLE(GL_DOUBLE, 8);

        public final int glType;
        public final int elementSize;


        private Type(int glType, int elementSize) {
            this.glType = glType;
            this.elementSize = elementSize;
        }


        private static final Type[] values = values();


        public boolean isInteger() {
            return this != FLOAT && this != DOUBLE;
        }


        public static Type forGLType(int glType) {
            for (Type type : values) {
                if (type.glType == glType) {
                    return type;
                }
            }

            throw new IllegalArgumentException("No attribute type with code " + glType + " found");
        }

    }


    public final String name;

    public final Type type;
    public final int numComponents;


    public GLVertexAttributeSignature(String name, Type type, int numComponents) {
        checkType(type);
        checkNumComponents(numComponents, type);

        this.name = name;

        this.type = type;
        this.numComponents = numComponents;
    }


    private void checkType(Type type) {
        if (type == null) {
            throw new IllegalArgumentException("Type must not be null");
        }
    }


    private void checkNumComponents(int numComponents, Type type) {
        if (type.isInteger() && numComponents == GL_BGRA) {
            throw new IllegalArgumentException("GL_BRGA numComponents is not applicable for integer attributes");
        }
        if ((numComponents < 1 || numComponents > 4) && numComponents != GL_BGRA) {
            throw new IllegalArgumentException("numComponents must be a positive number less than or equal to 4, or GL_BGRA");
        }
    }

}
