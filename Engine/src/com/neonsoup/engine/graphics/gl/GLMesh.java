package com.neonsoup.engine.graphics.gl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL32.*;

import java.util.Arrays;

public class GLMesh {

    public enum ProvokingVertex {

        FIRST(GL_FIRST_VERTEX_CONVENTION), LAST(GL_LAST_VERTEX_CONVENTION);

        public final int glProvokingVertex;


        private ProvokingVertex(int glProvokingVertex) {
            this.glProvokingVertex = glProvokingVertex;
        }

    }


    public enum Mode {

        POINTS(GL_POINTS),

        LINE_STRIP(GL_LINE_STRIP),

        LINE_LOOP(GL_LINE_LOOP),

        LINES(GL_LINES),

        TRIANGLE_STRIP(GL_TRIANGLE_STRIP),

        TRIANGLE_FAN(GL_TRIANGLE_FAN),

        TRIANGLES(GL_TRIANGLES),

        LINE_STRIP_ADJACENCY(GL_LINE_STRIP_ADJACENCY),

        LINES_ADJACENCY(GL_LINES_ADJACENCY),

        TRIANGLE_STRIP_ADJACENCY(GL_TRIANGLE_STRIP_ADJACENCY),

        TRIANGLES_ADJACENCY(GL_TRIANGLES_ADJACENCY);

        public final int glMode;


        private Mode(int glMode) {
            this.glMode = glMode;
        }

    }


    private final Mode mode;
    private final ProvokingVertex provokingVertex;
    private final int count;

    protected final GLVertexAttributeData[] attribData;
    protected final GLVertexIndexData indexData;


    public GLMesh(Mode mode, int count, GLVertexAttributeData[] attribData, GLVertexIndexData indexData) {
        this(mode, ProvokingVertex.FIRST, count, attribData, indexData);
    }


    public GLMesh(Mode mode,
                  ProvokingVertex provokingVertex,
                  int count,
                  GLVertexAttributeData[] attribData,
                  GLVertexIndexData indexData) {

        if (mode == null) {
            throw new IllegalArgumentException("Mode must not be null");
        }
        if (count < 0) {
            throw new IllegalArgumentException("Count must be a positive number or zero");
        }
        if (attribData == null) {
            throw new IllegalArgumentException("Attribute data array must not be null");
        }
        for (int i = 0; i < attribData.length; i++) {
            if (attribData[i] == null) {
                throw new IllegalArgumentException("Attribute data [" + i + "] is null");
            }
        }

        this.mode = mode;
        this.provokingVertex = provokingVertex;
        this.count = count;

        this.attribData = Arrays.copyOf(attribData, attribData.length);
        this.indexData = indexData;
    }


    public Mode getMode() {
        return mode;
    }


    public ProvokingVertex getProvokingVertex() {
        return provokingVertex;
    }


    public int getCount() {
        return count;
    }


    public int getAttribDataCount() {
        return attribData.length;
    }


    public GLVertexAttributeData getAttribData(int index) {
        return attribData[index];
    }


    public GLVertexAttributeData getAttribData(String name) {
        for (GLVertexAttributeData attrib : attribData) {
            if (attrib.getAttributeSignature().name.equals(name)) {
                return attrib;
            }
        }

        throw new IllegalArgumentException("No attribute data \"" + name + "\" was found");
    }


    public GLVertexAttributeData[] getAttribData() {
        return Arrays.copyOf(attribData, attribData.length);
    }


    public GLVertexIndexData getIndexData() {
        return indexData;
    }


    public void draw() {
        draw(0, count);
    }


    public void draw(int from, int count) {
        glProvokingVertex(provokingVertex.glProvokingVertex);

        if (indexData != null) {
            glDrawElements(mode.glMode,
                           count,
                           indexData.getType().glType,
                           indexData.getOffset() + from * indexData.getType().elementSize);
        } else {
            glDrawArrays(mode.glMode, from, count);
        }

        if (GLSettings.isGLErrorCheckingEnabled()) {
            int[] err = GLException.getErrors();
            if (err.length > 0) {
                throw new GLException(err);
            }
        }
    }


    public void disposeBuffers() {
        for (GLBufferData data : attribData) {
            if (!data.getBuffer().isDisposed()) {
                data.getBuffer().dispose();
            }
        }

        if (indexData != null && !indexData.getBuffer().isDisposed()) {
            indexData.getBuffer().dispose();
        }
    }


    public void disposeAllAttachedData() {
        disposeBuffers();
    }

}
