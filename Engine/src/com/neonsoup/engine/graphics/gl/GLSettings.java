package com.neonsoup.engine.graphics.gl;

public final class GLSettings {

    private static boolean checkGLErrors = true;

    private static boolean logInfo = false;


    private GLSettings() {
    }


    public static void setGLErrorChecking(boolean enabled) {
        checkGLErrors = enabled;
    }


    public static boolean isGLErrorCheckingEnabled() {
        return checkGLErrors;
    }


    public static void setInfoLogging(boolean enabled) {
        logInfo = enabled;
    }


    public static boolean isInfoLoggingEnabled() {
        return logInfo;
    }

}
