package com.neonsoup.engine.graphics.text;

public class TextDrawingContext {

    public float cursorX;
    public float cursorY;

    public int prevCodePoint = -1;


    public TextDrawingContext() {
    }


    public void reset() {
        cursorX = 0.0f;
        cursorY = 0.0f;

        prevCodePoint = -1;
    }

}
