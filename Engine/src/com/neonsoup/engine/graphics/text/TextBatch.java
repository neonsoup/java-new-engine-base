package com.neonsoup.engine.graphics.text;

import org.joml.Matrix4fc;

import com.neonsoup.engine.Disposable;

public interface TextBatch extends Disposable {

    void beginDraw(Matrix4fc projectionMatrix, TextBatchAttributes attributes);


    void drawString(TextDrawingContext context, CharSequence text);


    void flush();


    void endDraw();

}
