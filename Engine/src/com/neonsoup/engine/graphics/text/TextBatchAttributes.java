package com.neonsoup.engine.graphics.text;

import org.joml.Vector4f;

public class TextBatchAttributes {

    public final Vector4f textColor = new Vector4f(0.0f, 0.0f, 0.0f, 1.0f);

    public boolean flipVertical = false;


    public TextBatchAttributes() {
    }


    public TextBatchAttributes(TextBatchAttributes src) {
        set(src);
    }


    public void set(TextBatchAttributes src) {
        this.textColor.set(src.textColor);
        this.flipVertical = src.flipVertical;
    }

}
