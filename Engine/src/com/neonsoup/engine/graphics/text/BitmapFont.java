package com.neonsoup.engine.graphics.text;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL32.*;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonValue;

import org.joml.Matrix4fc;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.graphics.gl.GLBuffer;
import com.neonsoup.engine.graphics.gl.GLShaderProgram;
import com.neonsoup.engine.graphics.gl.GLTexture;
import com.neonsoup.engine.graphics.gl.GLTextureData;
import com.neonsoup.engine.graphics.gl.GLVertexArray;
import com.neonsoup.engine.graphics.gl.GLVertexAttributeData;
import com.neonsoup.engine.graphics.gl.GLVertexAttributeSignature;
import com.neonsoup.engine.graphics.gl.GLVertexIndexData;
import com.neonsoup.engine.res.ResourceInfo;

import gnu.trove.iterator.TIntIterator;
import gnu.trove.list.TIntList;
import gnu.trove.list.array.TIntArrayList;

public class BitmapFont extends AbstractDisposable implements Font {

    protected static final int CHARS_PER_PAGE = 256;

    protected static final int CHARS_PER_ROW = 16;
    protected static final int CHARS_PER_COL = 16;

    protected static final int MAX_UNICODE_CODEPOINT = 0x10FFFF;

    protected static final int MAX_PAGE_COUNT = ((MAX_UNICODE_CODEPOINT + 1) / CHARS_PER_PAGE) + 1;

    protected static final String VERTEX_SHADER_SOURCE = "#version 150\n"
                                                         //
                                                         + "in vec2 in_Position;\n"

                                                         + "in vec2 in_TexCoord;\n"

                                                         + "in int in_TexPage;\n"
                                                         //
                                                         + "uniform mat4 u_ProjectionMatrix;\n"
                                                         //
                                                         + "out vec2 var_TexCoord;\n"

                                                         + "flat out int var_TexPage;\n"
                                                         //
                                                         + "void main() {\n"

                                                         + " var_TexCoord = in_TexCoord;\n"

                                                         + " var_TexPage = in_TexPage;\n"

                                                         + " vec4 pos = vec4(in_Position, 0.0, 1.0);\n"

                                                         + " gl_Position = u_ProjectionMatrix * pos;\n"

                                                         + "}";

    protected static final String FRAGMENT_SHADER_SOURCE = "#version 150\n"
                                                           //
                                                           + "in vec2 var_TexCoord;\n"

                                                           + "flat in int var_TexPage;\n"
                                                           //
                                                           + "uniform vec4 u_Color;\n"

                                                           + "uniform sampler2DArray u_Texture;\n"
                                                           //
                                                           + "out vec4 result;\n"
                                                           //
                                                           + "void main() {\n"

                                                           + " vec4 c = texture(u_Texture, vec3(var_TexCoord, float(var_TexPage)));\n"

                                                           + " result.r = u_Color.r;\n"

                                                           + " result.g = u_Color.g;\n;"

                                                           + " result.b = u_Color.b;\n;"

                                                           + " result.a = c.x;\n"

                                                           + "}";

    protected static final GLVertexAttributeSignature SHADER_ATTRIB_SIGNATURE_POSITION;
    protected static final GLVertexAttributeSignature SHADER_ATTRIB_SIGNATURE_TEX_COORD;
    protected static final GLVertexAttributeSignature SHADER_ATTRIB_SIGNATURE_TEX_PAGE;

    static {
        SHADER_ATTRIB_SIGNATURE_POSITION = new GLVertexAttributeSignature("in_Position",
                                                                          GLVertexAttributeSignature.Type.FLOAT,
                                                                          2);

        SHADER_ATTRIB_SIGNATURE_TEX_COORD = new GLVertexAttributeSignature("in_TexCoord",
                                                                           GLVertexAttributeSignature.Type.FLOAT,
                                                                           2);

        SHADER_ATTRIB_SIGNATURE_TEX_PAGE = new GLVertexAttributeSignature("in_TexPage",
                                                                          GLVertexAttributeSignature.Type.UNSIGNED_SHORT,
                                                                          1);
    }

    protected static GLShaderProgram shader;

    protected static int shaderUniformLocationProjectionMatrix;
    protected static int shaderUniformLocationColor;
    protected static int shaderUniformLocationTexture;

    protected static int shaderAttribPosition;
    protected static int shaderAttribTexCoord;
    protected static int shaderAttribTexPage;


    public static void initStatic() {
        shader = new GLShaderProgram(VERTEX_SHADER_SOURCE, FRAGMENT_SHADER_SOURCE);

        shaderUniformLocationProjectionMatrix = shader.getUniformLocation("u_ProjectionMatrix");
        shaderUniformLocationColor = shader.getUniformLocation("u_Color");
        shaderUniformLocationTexture = shader.getUniformLocation("u_Texture");

        shaderAttribPosition = shader.getAttributeLocation("in_Position");
        shaderAttribTexCoord = shader.getAttributeLocation("in_TexCoord");
        shaderAttribTexPage = shader.getAttributeLocation("in_TexPage");
    }


    public static void disposeStatic() {
        shader.dispose();
    }


    public static class Page {

        protected int[] charWidth = new int[CHARS_PER_PAGE];


        protected Page() {
        }

    }


    protected class TrueTypeFontTextBatch extends AbstractDisposable implements TextBatch {

        protected static final int BUFFER_SIZE = 256;

        protected final GLVertexArray vao;

        protected final GLBuffer bufPosition;
        protected final GLBuffer bufTexCoord;
        protected final GLBuffer bufTexPage;

        protected final GLBuffer bufIndex;

        protected final GLVertexAttributeData attribDataPosition;
        protected final GLVertexAttributeData attribDataTexCoord;
        protected final GLVertexAttributeData attribDataTexPage;

        protected final GLVertexIndexData indexData;

        protected FloatBuffer bufPositionMap;
        protected FloatBuffer bufTexCoordMap;
        protected ShortBuffer bufTexPageMap;
        protected ShortBuffer bufIndexMap;

        protected int bufferPos = 0;

        protected int vertexPos = 0;
        protected int indexPos = 0;

        protected boolean inBeginDrawBlock = false;

        protected final TextBatchAttributes attribs = new TextBatchAttributes();


        protected TrueTypeFontTextBatch() {
            vao = new GLVertexArray();

            bufPosition = new GLBuffer(BUFFER_SIZE
                                       * 4 // bytes per float
                                       * 2 // floats per attribute
                                       * 4, // vertices per glyph
                                       GLBuffer.Target.ARRAY_BUFFER,
                                       GLBuffer.Usage.DYNAMIC_DRAW);

            bufTexCoord = new GLBuffer(BUFFER_SIZE
                                       * 4 // bytes per float
                                       * 2 // floats per attribute
                                       * 4, // vertices per glyph
                                       GLBuffer.Target.ARRAY_BUFFER,
                                       GLBuffer.Usage.DYNAMIC_DRAW);

            bufTexPage = new GLBuffer(BUFFER_SIZE
                                      * 2 // bytes per short
                                      * 4, // vertices per glyph
                                      GLBuffer.Target.ARRAY_BUFFER,
                                      GLBuffer.Usage.DYNAMIC_DRAW);

            bufIndex = new GLBuffer(BUFFER_SIZE
                                    * 2 // bytes per short
                                    * 6, // indices per glyph
                                    GLBuffer.Target.ELEMENT_ARRAY_BUFFER,
                                    GLBuffer.Usage.DYNAMIC_DRAW);

            attribDataPosition = new GLVertexAttributeData(bufPosition, SHADER_ATTRIB_SIGNATURE_POSITION);
            attribDataTexCoord = new GLVertexAttributeData(bufTexCoord, SHADER_ATTRIB_SIGNATURE_TEX_COORD);
            attribDataTexPage = new GLVertexAttributeData(bufTexPage, SHADER_ATTRIB_SIGNATURE_TEX_PAGE);

            indexData = new GLVertexIndexData(bufIndex, GLVertexIndexData.Type.UNSIGNED_SHORT);
        }


        @Override
        public void beginDraw(Matrix4fc projectionMatrix, TextBatchAttributes attributes) {
            if (inBeginDrawBlock) {
                throw new IllegalStateException();
            }

            attribs.set(attributes);

            ensureUndisposed();

            glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);

            shader.beginUse();

            GLTexture.activeTexture(0);

            texture.bind();

            glUniform1i(shaderUniformLocationTexture, 0);

            shader.uniformMatrix4f(shaderUniformLocationProjectionMatrix, projectionMatrix);

            shader.uniform4f(shaderUniformLocationColor, attribs.textColor);

            if (!vao.isInitialized()) {
                vao.bind();

                glEnableVertexAttribArray(shaderAttribPosition);
                glEnableVertexAttribArray(shaderAttribTexCoord);
                glEnableVertexAttribArray(shaderAttribTexPage);

                bufPosition.bind();
                attribDataPosition.setVertexAttribPointer(shaderAttribPosition);

                bufTexCoord.bind();
                attribDataTexCoord.setVertexAttribPointer(shaderAttribTexCoord);

                bufTexPage.bind();
                attribDataTexPage.setVertexAttribPointer(shaderAttribTexPage);

                GLBuffer.Target.ARRAY_BUFFER.unbindBuffer();

                indexData.bind();

                vao.setInitialized(true);

                vao.unbind();
            }

            mapBuffers();

            inBeginDrawBlock = true;
        }


        @Override
        public void drawString(TextDrawingContext context, CharSequence text) {
            if (!inBeginDrawBlock) {
                throw new IllegalStateException();
            }

            int i = 0;

            while (i < text.length()) {
                char c = text.charAt(i++);

                int cp = c;

                if (Character.isHighSurrogate(c)) {
                    cp = Character.codePointAt(text, cp);

                    if (Character.isValidCodePoint(cp)) {
                        i++;
                    } else {
                        cp = 0xFFFD;
                    }
                } else if (Character.isLowSurrogate(c)) {
                    cp = 0xFFFD;
                }

                short pageIdx = pageNumMap[(cp / CHARS_PER_PAGE)];

                if (pageIdx == -1) {
                    cp = 0xFFFD;
                    pageIdx = pageNumMap[(0xFFFD / CHARS_PER_PAGE)];

                    if (pageIdx == -1) {
                        context.prevCodePoint = cp;
                        continue;
                    }
                }

                float posX = context.cursorX;
                float posY = context.cursorY;

                float kern = 0.0f;

                int width = pages[pageIdx].charWidth[cp & 0xFF];

                int pageX = cp & 0x0F;
                int pageY = (cp & 0xF0) >>> 4;

                if (context.prevCodePoint != -1) {
                    posX += scaleFactor * unscaledDefaultAdvance;
                }

                float scaledWidth = width * scaleFactor;

                float x0 = posX + kern;
                float y0;
                float x1 = posX + scaledWidth + kern;
                float y1;

                if (attribs.flipVertical) {
                    y0 = posY + scaledDescent;
                    y1 = posY - scaledAscent;
                } else {
                    y0 = posY - scaledDescent;
                    y1 = posY + scaledAscent;
                }

                posX += scaledWidth + kern;

                {
                    bufPositionMap.put(x0);
                    bufPositionMap.put(y0);

                    bufPositionMap.put(x1);
                    bufPositionMap.put(y1);

                    bufPositionMap.put(x0);
                    bufPositionMap.put(y1);

                    bufPositionMap.put(x1);
                    bufPositionMap.put(y0);
                }

                float s0 = (pageX * (unscaledCharMaxWidth + unscaledPageHorizontalSpacing)) / (float) bitmapWidth;
                float t0 = 1.0f
                           - (pageY * (unscaledCharHeight + unscaledPageVerticalSpacing) + unscaledCharHeight)
                             / (float) bitmapHeight;

                float s1 = (pageX * (unscaledCharMaxWidth + unscaledPageHorizontalSpacing) + width)
                           / (float) bitmapWidth;
                float t1 = 1.0f - (pageY * (unscaledCharHeight + unscaledPageVerticalSpacing)) / (float) bitmapHeight;

                bufTexCoordMap.put(s0);
                bufTexCoordMap.put(t0);

                bufTexCoordMap.put(s1);
                bufTexCoordMap.put(t1);

                bufTexCoordMap.put(s0);
                bufTexCoordMap.put(t1);

                bufTexCoordMap.put(s1);
                bufTexCoordMap.put(t0);

                {
                    bufTexPageMap.put(pageIdx);
                    bufTexPageMap.put(pageIdx);
                    bufTexPageMap.put(pageIdx);
                    bufTexPageMap.put(pageIdx);
                }

                bufIndexMap.put((short) vertexPos);
                bufIndexMap.put((short) (vertexPos + 1));
                bufIndexMap.put((short) (vertexPos + 2));

                bufIndexMap.put((short) vertexPos);
                bufIndexMap.put((short) (vertexPos + 3));
                bufIndexMap.put((short) (vertexPos + 1));

                {
                    vertexPos += 4;
                    indexPos += 6;

                    bufferPos++;
                }

                {
                    context.cursorX = posX;
                    context.cursorY = posY;

                    context.prevCodePoint = cp;
                }

                if (bufferPos >= BUFFER_SIZE) {
                    flush();
                }
            }
        }


        @Override
        public void flush() {
            if (!inBeginDrawBlock) {
                throw new IllegalStateException();
            }

            if (bufferPos == 0) {
                return;
            }

            unmapBuffers();

            drawBuffer();

            mapBuffers();
        }


        @Override
        public void endDraw() {
            if (!inBeginDrawBlock) {
                throw new IllegalStateException();
            }

            if (bufferPos == 0) {
                unmapBuffers();
            } else {
                unmapBuffers();

                drawBuffer();
            }

            texture.unbind();

            shader.endUse();

            // glDisable(GL_BLEND);

            inBeginDrawBlock = false;
        }


        protected void mapBuffers() {
            bufPosition.bind();
            bufPositionMap = bufPosition.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY).asFloatBuffer();
            bufPosition.unbind();

            bufTexCoord.bind();
            bufTexCoordMap = bufTexCoord.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY).asFloatBuffer();
            bufTexCoord.unbind();

            bufTexPage.bind();
            bufTexPageMap = bufTexPage.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY).asShortBuffer();
            bufTexPage.unbind();

            bufIndex.bind();
            bufIndexMap = bufIndex.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY).asShortBuffer();
            bufIndex.unbind();
        }


        protected void drawBuffer() {
            vao.bind();

            glDrawElements(GL_TRIANGLES, indexPos, GL_UNSIGNED_SHORT, 0);

            vertexPos = 0;
            indexPos = 0;

            bufferPos = 0;

            vao.unbind();
        }


        protected void unmapBuffers() {
            bufPositionMap.rewind();
            bufTexCoordMap.rewind();
            bufTexPageMap.rewind();
            bufIndexMap.rewind();

            bufPosition.bind();
            bufPosition.unmapBuffer();
            bufPosition.unbind();

            bufTexCoord.bind();
            bufTexCoord.unmapBuffer();
            bufTexCoord.unbind();

            bufTexPage.bind();
            bufTexPage.unmapBuffer();
            bufTexPage.unbind();

            bufIndex.bind();
            bufIndex.unmapBuffer();
            bufIndex.unbind();
        }


        @Override
        public void dispose() {
            if (inBeginDrawBlock) {
                throw new IllegalStateException();
            }

            if (this.disposed) {
                return;
            }

            vao.dispose();

            bufPosition.dispose();
            bufTexCoord.dispose();
            bufTexPage.dispose();
            bufIndex.dispose();

            this.disposed = true;
        }

    }


    protected String name;

    protected short[] pageNumMap = new short[MAX_PAGE_COUNT];

    protected Page[] pages;

    protected GLTexture texture;

    protected int fontSize;

    protected float scaleFactor;

    protected int bitmapWidth = 0;
    protected int bitmapHeight = 0;

    protected int unscaledPageHorizontalSpacing;
    protected int unscaledPageVerticalSpacing;

    protected int unscaledCharMaxWidth;
    protected int unscaledCharHeight;

    protected int unscaledAscent;
    protected int unscaledDescent;

    protected float unscaledDefaultAdvance;

    protected int scaledAscent;
    protected int scaledDescent;
    protected int scaledLeading;


    protected BitmapFont() {
    }


    @Override
    public int getFontSize() {
        return fontSize;
    }


    @Override
    public int getAscent() {
        return scaledAscent;
    }


    @Override
    public int getDescent() {
        return scaledDescent;
    }


    @Override
    public int getLineGap() {
        return scaledLeading;
    }


    public static BitmapFont loadFont(Application app, ResourceInfo resourceInfo, int fontSize) throws IOException {

        final int FILTER_LINEAR = 1, FILTER_NEAREST = 2;

        String name;
        int filter;
        String pagePathTemplate;
        int pageVerticalSpacing;
        int pageHorizontalSpacing;
        int charHeight;
        int charMaxWidth;
        int charDefaultWidth;
        int charBaseLine;
        int charLeading;
        float charDefaultAdvance;

        TIntList pagesPresent = new TIntArrayList(4);

        try (InputStream in = resourceInfo.getResourceStream();
             BufferedInputStream bufIn = new BufferedInputStream(in);
             JsonReader jsonIn = Json.createReader(bufIn)) {

            JsonObject jsonFont = jsonIn.readObject();

            name = jsonFont.getString("name");
            {
                String filterStr = jsonFont.getString("filter");

                if (filterStr.equalsIgnoreCase("nearest")) {
                    filter = FILTER_NEAREST;
                } else {
                    filter = FILTER_LINEAR;
                }
            }
            pagePathTemplate = jsonFont.getString("page_path_template");
            pageVerticalSpacing = jsonFont.getInt("page_vertical_spacing");
            pageHorizontalSpacing = jsonFont.getInt("page_horizontal_spacing");
            charHeight = jsonFont.getInt("char_height");
            charMaxWidth = jsonFont.getInt("char_max_width");
            charDefaultWidth = jsonFont.getInt("char_default_width");
            charBaseLine = jsonFont.getInt("char_base_line");
            charLeading = jsonFont.getInt("char_leading");
            charDefaultAdvance = Float.parseFloat(jsonFont.getString("char_default_advance"));

            JsonArray jsonPagesPresent = jsonFont.getJsonArray("pages_present");

            for (JsonValue val : jsonPagesPresent) {
                String strVal = ((JsonString) val).getString();

                int minus = strVal.indexOf('-');

                if (minus >= 0) {
                    String[] split = strVal.split("-");

                    int min = Integer.parseInt(split[0].trim(), 16);
                    int max = Integer.parseInt(split[1].trim(), 16);

                    for (int i = min; i <= max; i++) {
                        pagesPresent.add(i);
                    }
                } else {
                    pagesPresent.add(Integer.parseInt(strVal, 16));
                }
            }
        }

        BitmapFont font = new BitmapFont();

        font.name = name;

        font.fontSize = fontSize;

        font.scaleFactor = fontSize / (float) charHeight;

        font.bitmapWidth = charMaxWidth * CHARS_PER_ROW + pageHorizontalSpacing * (CHARS_PER_ROW - 1);
        font.bitmapHeight = charHeight * CHARS_PER_COL + pageVerticalSpacing * (CHARS_PER_COL - 1);

        font.unscaledCharMaxWidth = charMaxWidth;
        font.unscaledCharHeight = charHeight;

        font.unscaledPageHorizontalSpacing = pageHorizontalSpacing;
        font.unscaledPageVerticalSpacing = pageVerticalSpacing;

        font.unscaledAscent = charBaseLine + 1;
        font.unscaledDescent = charHeight - (charBaseLine + 1);

        font.unscaledDefaultAdvance = charDefaultAdvance;

        font.scaledAscent = (int) Math.ceil(font.unscaledAscent * font.scaleFactor);
        font.scaledDescent = (int) Math.ceil(font.unscaledDescent * font.scaleFactor);

        font.scaledLeading = (int) Math.ceil(charLeading * font.scaleFactor);

        Arrays.fill(font.pageNumMap, (short) -1);

        int totalPages = 0;

        int prevPage = -1;

        pagesPresent.sort();

        TIntIterator pagesIt = pagesPresent.iterator();

        TIntList fixedPagesPresent = new TIntArrayList(pagesPresent.size());

        while (pagesIt.hasNext()) {
            int page = pagesIt.next();

            if (page < 0 || page >= MAX_PAGE_COUNT) {
                throw new IOException("Illegal page index: " + page);
            }

            if (page != prevPage) {
                fixedPagesPresent.add(page);

                font.pageNumMap[page] = (short) totalPages;

                prevPage = page;
                totalPages++;
            }
        }

        font.pages = new Page[totalPages];

        GLTexture texture = new GLTexture(GLTexture.Target.TEXTURE_2D_ARRAY, GLTexture.InternalFormat.R8);
        font.texture = texture;

        try {

            texture.bind();

            if (filter == FILTER_LINEAR) {
                texture.setMinFilter(GLTexture.Filter.LINEAR);
                texture.setMagFilter(GLTexture.Filter.LINEAR);
            } else {
                texture.setMinFilter(GLTexture.Filter.NEAREST);
                texture.setMagFilter(GLTexture.Filter.NEAREST);
            }

            GLTexture.unpackAlignment(GLTexture.PackAlignment.ALIGNMENT_1);

            texture.texImage3D(0,
                               font.bitmapWidth,
                               font.bitmapHeight,
                               font.pages.length,
                               GLTexture.DataFormat.RED,
                               GLTexture.DataType.UNSIGNED_BYTE,
                               null);

            pagesIt = fixedPagesPresent.iterator();

            while (pagesIt.hasNext()) {
                int pageIdx = pagesIt.next();

                Page page = new Page();
                font.pages[font.pageNumMap[pageIdx]] = page;

                StringBuilder hexStr = new StringBuilder(Integer.toHexString(pageIdx));

                while (hexStr.length() < 4) {
                    hexStr.insert(0, '0');
                }

                String basePagePath = pagePathTemplate.replace("#", hexStr);
                String pngPagePath = basePagePath + ".png";
                String jsonPagePath = basePagePath + ".json";

                ResourceInfo jsonInfo = app.getResourceInfoLocator().locateResourceInfo(jsonPagePath);

                if (jsonInfo == null) {
                    Arrays.fill(page.charWidth, charDefaultWidth);
                } else {
                    try (InputStream in = jsonInfo.getResourceStream();
                         BufferedInputStream bufIn = new BufferedInputStream(in);
                         JsonReader jsonIn = Json.createReader(bufIn)) {

                        int pageDefaultCharWidth = charDefaultWidth;

                        JsonObject pageJson = jsonIn.readObject();

                        if (pageJson.containsKey("default_width")) {
                            pageDefaultCharWidth = pageJson.getInt("default_width");
                        }

                        Arrays.fill(page.charWidth, pageDefaultCharWidth);

                        if (pageJson.containsKey("char_width")) {
                            JsonArray charSets = pageJson.getJsonArray("char_width");

                            for (int i = 0; i < charSets.size(); i++) {
                                JsonArray charSet = charSets.getJsonArray(i);

                                String chars = charSet.getString(0);
                                int width = charSet.getInt(1);

                                for (int cp : chars.codePoints().toArray()) {
                                    page.charWidth[cp & 0xFF] = width;
                                }
                            }
                        }
                    }
                }

                ResourceInfo pngInfo = app.getResourceInfoLocator().locateResourceInfo(pngPagePath);

                GLTextureData texData = null;

                try (InputStream fileIn = pngInfo.getResourceStream();
                     BufferedInputStream bufIn = new BufferedInputStream(fileIn)) {

                    texData = GLTextureData.loadAlphaTextureData(bufIn, "png");

                    texture.texSubImage3DArrayLayer(texData, font.pageNumMap[pageIdx]);

                } finally {
                    if (texData != null) {
                        texData.dispose();
                    }
                }

            }

            texture.unbind();

        } catch (Exception ex) {
            texture.dispose();

            throw new IOException(ex);
        }

        return font;
    }


    @Override
    public TextBatch createTextBatch() {
        ensureUndisposed();

        return new TrueTypeFontTextBatch();
    }


    @Override
    public float measureTextWidth(String text) {
        ensureUndisposed();

        float width = 0.0f;

        int i = 0;

        int prevCp = -1;

        while (i < text.length()) {
            char c = text.charAt(i++);

            int cp = c;

            if (Character.isHighSurrogate(c)) {
                cp = Character.codePointAt(text, cp);

                if (Character.isValidCodePoint(cp)) {
                    i++;
                } else {
                    cp = 0xFFFD;
                }
            } else if (Character.isLowSurrogate(c)) {
                cp = 0xFFFD;
            }

            short pageIdx = pageNumMap[(cp / CHARS_PER_PAGE)];

            if (pageIdx == -1) {
                cp = 0xFFFD;
                pageIdx = pageNumMap[(0xFFFD / CHARS_PER_PAGE)];

                if (pageIdx == -1) {
                    prevCp = cp;
                    continue;
                }
            }

            int cpWidth = pages[pageIdx].charWidth[cp & 0xFF];

            if (prevCp != -1) {
                width += scaleFactor * unscaledDefaultAdvance;
            }

            width += cpWidth * scaleFactor;

            prevCp = cp;
        }

        return width;
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        texture.dispose();

        disposed = true;
    }

}
