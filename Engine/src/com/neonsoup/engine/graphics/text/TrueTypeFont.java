package com.neonsoup.engine.graphics.text;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL32.*;
import static org.lwjgl.stb.STBTruetype.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import org.joml.Matrix4fc;
import org.lwjgl.stb.STBTTAlignedQuad;
import org.lwjgl.stb.STBTTFontinfo;
import org.lwjgl.stb.STBTTPackContext;
import org.lwjgl.stb.STBTTPackedchar;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.MemoryUtil;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.graphics.gl.GLBuffer;
import com.neonsoup.engine.graphics.gl.GLShaderProgram;
import com.neonsoup.engine.graphics.gl.GLTexture;
import com.neonsoup.engine.graphics.gl.GLTextureData;
import com.neonsoup.engine.graphics.gl.GLVertexArray;
import com.neonsoup.engine.graphics.gl.GLVertexAttributeData;
import com.neonsoup.engine.graphics.gl.GLVertexAttributeSignature;
import com.neonsoup.engine.graphics.gl.GLVertexIndexData;
import com.neonsoup.engine.res.ResourceInfo;

public class TrueTypeFont extends AbstractDisposable implements Font {

    protected static final int CHARS_PER_PAGE = 256;

    protected static final int MAX_UNICODE_CODEPOINT = 0xFFFF;

    protected static final String VERTEX_SHADER_SOURCE = "#version 150\n"
                                                         //
                                                         + "in vec2 in_Position;\n"

                                                         + "in vec2 in_TexCoord;\n"

                                                         + "in int in_TexPage;\n"
                                                         //
                                                         + "uniform mat4 u_ProjectionMatrix;\n"
                                                         //
                                                         + "out vec2 var_TexCoord;\n"

                                                         + "flat out int var_TexPage;\n"
                                                         //
                                                         + "void main() {\n"

                                                         + " var_TexCoord = in_TexCoord;\n"

                                                         + " var_TexPage = in_TexPage;\n"

                                                         + " vec4 pos = vec4(in_Position, 0.0, 1.0);\n"

                                                         + " gl_Position = u_ProjectionMatrix * pos;\n"

                                                         + "}";

    protected static final String FRAGMENT_SHADER_SOURCE = "#version 150\n"
                                                           //
                                                           + "in vec2 var_TexCoord;\n"

                                                           + "flat in int var_TexPage;\n"
                                                           //
                                                           + "uniform vec4 u_Color;\n"

                                                           + "uniform sampler2DArray u_Texture;\n"
                                                           //
                                                           + "out vec4 result;\n"
                                                           //
                                                           + "void main() {\n"

                                                           + " vec4 c = texture(u_Texture, vec3(var_TexCoord, float(var_TexPage)));\n"

                                                           + " result.r = u_Color.r;\n"

                                                           + " result.g = u_Color.g;\n;"

                                                           + " result.b = u_Color.b;\n;"

                                                           + " result.a = c.x;\n"

                                                           + "}";

    protected static final GLVertexAttributeSignature SHADER_ATTRIB_SIGNATURE_POSITION;
    protected static final GLVertexAttributeSignature SHADER_ATTRIB_SIGNATURE_TEX_COORD;
    protected static final GLVertexAttributeSignature SHADER_ATTRIB_SIGNATURE_TEX_PAGE;

    static {
        SHADER_ATTRIB_SIGNATURE_POSITION = new GLVertexAttributeSignature("in_Position",
                                                                          GLVertexAttributeSignature.Type.FLOAT,
                                                                          2);

        SHADER_ATTRIB_SIGNATURE_TEX_COORD = new GLVertexAttributeSignature("in_TexCoord",
                                                                           GLVertexAttributeSignature.Type.FLOAT,
                                                                           2);

        SHADER_ATTRIB_SIGNATURE_TEX_PAGE = new GLVertexAttributeSignature("in_TexPage",
                                                                          GLVertexAttributeSignature.Type.UNSIGNED_SHORT,
                                                                          1);
    }
    protected static GLShaderProgram shader;

    protected static int shaderUniformLocationProjectionMatrix;
    protected static int shaderUniformLocationColor;
    protected static int shaderUniformLocationTexture;

    protected static int shaderAttribPosition;
    protected static int shaderAttribTexCoord;
    protected static int shaderAttribTexPage;


    public static void initStatic() {
        shader = new GLShaderProgram(VERTEX_SHADER_SOURCE, FRAGMENT_SHADER_SOURCE);

        shaderUniformLocationProjectionMatrix = shader.getUniformLocation("u_ProjectionMatrix");
        shaderUniformLocationColor = shader.getUniformLocation("u_Color");
        shaderUniformLocationTexture = shader.getUniformLocation("u_Texture");

        shaderAttribPosition = shader.getAttributeLocation("in_Position");
        shaderAttribTexCoord = shader.getAttributeLocation("in_TexCoord");
        shaderAttribTexPage = shader.getAttributeLocation("in_TexPage");
    }


    public static void disposeStatic() {
        shader.dispose();
    }


    public static class Page {

        protected final STBTTPackedchar.Buffer stbPackedCharBuffer;


        protected Page() {
            stbPackedCharBuffer = STBTTPackedchar.calloc(CHARS_PER_PAGE);
        }

    }


    protected class TrueTypeFontTextBatch extends AbstractDisposable implements TextBatch {

        protected static final int BUFFER_SIZE = 256;

        protected final GLVertexArray vao;

        protected final GLBuffer bufPosition;
        protected final GLBuffer bufTexCoord;
        protected final GLBuffer bufTexPage;

        protected final GLBuffer bufIndex;

        protected final GLVertexAttributeData attribDataPosition;
        protected final GLVertexAttributeData attribDataTexCoord;
        protected final GLVertexAttributeData attribDataTexPage;

        protected final GLVertexIndexData indexData;

        protected FloatBuffer bufPositionMap;
        protected FloatBuffer bufTexCoordMap;
        protected ShortBuffer bufTexPageMap;
        protected ShortBuffer bufIndexMap;

        protected int bufferPos = 0;

        protected int vertexPos = 0;
        protected int indexPos = 0;

        protected final STBTTAlignedQuad quad;

        protected final FloatBuffer bufPosX;
        protected final FloatBuffer bufPosY;

        protected boolean inBeginDrawBlock = false;

        protected final TextBatchAttributes attribs = new TextBatchAttributes();


        protected TrueTypeFontTextBatch() {
            vao = new GLVertexArray();

            bufPosition = new GLBuffer(BUFFER_SIZE
                                       * 4 // bytes per float
                                       * 2 // floats per attribute
                                       * 4, // vertices per glyph
                                       GLBuffer.Target.ARRAY_BUFFER,
                                       GLBuffer.Usage.DYNAMIC_DRAW);

            bufTexCoord = new GLBuffer(BUFFER_SIZE
                                       * 4 // bytes per float
                                       * 2 // floats per attribute
                                       * 4, // vertices per glyph
                                       GLBuffer.Target.ARRAY_BUFFER,
                                       GLBuffer.Usage.DYNAMIC_DRAW);

            bufTexPage = new GLBuffer(BUFFER_SIZE
                                      * 2 // bytes per short
                                      * 4, // vertices per glyph
                                      GLBuffer.Target.ARRAY_BUFFER,
                                      GLBuffer.Usage.DYNAMIC_DRAW);

            bufIndex = new GLBuffer(BUFFER_SIZE
                                    * 2 // bytes per short
                                    * 6, // indices per glyph
                                    GLBuffer.Target.ELEMENT_ARRAY_BUFFER,
                                    GLBuffer.Usage.DYNAMIC_DRAW);

            attribDataPosition = new GLVertexAttributeData(bufPosition, SHADER_ATTRIB_SIGNATURE_POSITION);
            attribDataTexCoord = new GLVertexAttributeData(bufTexCoord, SHADER_ATTRIB_SIGNATURE_TEX_COORD);
            attribDataTexPage = new GLVertexAttributeData(bufTexPage, SHADER_ATTRIB_SIGNATURE_TEX_PAGE);

            indexData = new GLVertexIndexData(bufIndex, GLVertexIndexData.Type.UNSIGNED_SHORT);

            quad = STBTTAlignedQuad.malloc();

            bufPosX = MemoryUtil.memAllocFloat(1);
            bufPosY = MemoryUtil.memAllocFloat(1);
        }


        @Override
        public void beginDraw(Matrix4fc projectionMatrix, TextBatchAttributes attributes) {
            if (inBeginDrawBlock) {
                throw new IllegalStateException();
            }

            attribs.set(attributes);

            ensureUndisposed();

            glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);

            shader.beginUse();

            GLTexture.activeTexture(0);

            texture.bind();

            glUniform1i(shaderUniformLocationTexture, 0);

            shader.uniformMatrix4f(shaderUniformLocationProjectionMatrix, projectionMatrix);

            shader.uniform4f(shaderUniformLocationColor, attribs.textColor);

            if (!vao.isInitialized()) {
                vao.bind();

                glEnableVertexAttribArray(shaderAttribPosition);
                glEnableVertexAttribArray(shaderAttribTexCoord);
                glEnableVertexAttribArray(shaderAttribTexPage);

                bufPosition.bind();
                attribDataPosition.setVertexAttribPointer(shaderAttribPosition);

                bufTexCoord.bind();
                attribDataTexCoord.setVertexAttribPointer(shaderAttribTexCoord);

                bufTexPage.bind();
                attribDataTexPage.setVertexAttribPointer(shaderAttribTexPage);

                GLBuffer.Target.ARRAY_BUFFER.unbindBuffer();

                indexData.bind();

                vao.setInitialized(true);

                vao.unbind();
            }

            mapBuffers();

            inBeginDrawBlock = true;
        }


        @Override
        public void drawString(TextDrawingContext context, CharSequence text) {
            if (!inBeginDrawBlock) {
                throw new IllegalStateException();
            }

            bufPosX.put(0, context.cursorX);
            bufPosY.put(0, context.cursorY);

            int i = 0;

            while (i < text.length()) {
                char c = text.charAt(i++);

                int cp = c;

                if (Character.isHighSurrogate(c)) {
                    cp = Character.codePointAt(text, cp);

                    if (Character.isValidCodePoint(cp)) {
                        i++;
                    } else {
                        cp = 0xFFFD;
                    }
                } else if (Character.isLowSurrogate(c)) {
                    cp = 0xFFFD;
                }

                short pageIdx = (short) (cp / CHARS_PER_PAGE);

                Page page = pages[pageIdx];

                stbtt_GetPackedQuad(page.stbPackedCharBuffer,
                                    bitmapWidth,
                                    bitmapHeight,
                                    cp & 0xFF,
                                    bufPosX,
                                    bufPosY,
                                    quad,
                                    false);

                float posY = bufPosY.get(0);
                float posX = bufPosX.get(0);

                float kern = 0.0f;

                if (context.prevCodePoint != -1) {
                    kern = scaleFactor * stbtt_GetCodepointKernAdvance(stbFontInfo, context.prevCodePoint, cp);

                    posX += kern;
                    bufPosX.put(0, posX);
                }

                float x0 = quad.x0() + kern;
                float y0;
                float x1 = quad.x1() + kern;
                float y1;

                if (attribs.flipVertical) {
                    y0 = posY - (posY - quad.y1());
                    y1 = posY - (posY - quad.y0());
                } else {
                    y0 = posY + (posY - quad.y0());
                    y1 = posY + (posY - quad.y1());
                }

                {
                    bufPositionMap.put(x0);
                    bufPositionMap.put(y1);

                    bufPositionMap.put(x1);
                    bufPositionMap.put(y0);

                    bufPositionMap.put(x0);
                    bufPositionMap.put(y0);

                    bufPositionMap.put(x1);
                    bufPositionMap.put(y1);
                }

                float s0 = quad.s0();
                float t0 = quad.t0();
                float s1 = quad.s1();
                float t1 = quad.t1();

                if (attribs.flipVertical) {
                    bufTexCoordMap.put(s0);
                    bufTexCoordMap.put(t0);

                    bufTexCoordMap.put(s1);
                    bufTexCoordMap.put(t1);

                    bufTexCoordMap.put(s0);
                    bufTexCoordMap.put(t1);

                    bufTexCoordMap.put(s1);
                    bufTexCoordMap.put(t0);
                } else {
                    bufTexCoordMap.put(s0);
                    bufTexCoordMap.put(t1);

                    bufTexCoordMap.put(s1);
                    bufTexCoordMap.put(t0);

                    bufTexCoordMap.put(s0);
                    bufTexCoordMap.put(t0);

                    bufTexCoordMap.put(s1);
                    bufTexCoordMap.put(t1);
                }

                {
                    bufTexPageMap.put(pageIdx);
                    bufTexPageMap.put(pageIdx);
                    bufTexPageMap.put(pageIdx);
                    bufTexPageMap.put(pageIdx);
                }

                if (attribs.flipVertical) {
                    bufIndexMap.put((short) vertexPos);
                    bufIndexMap.put((short) (vertexPos + 2));
                    bufIndexMap.put((short) (vertexPos + 1));

                    bufIndexMap.put((short) vertexPos);
                    bufIndexMap.put((short) (vertexPos + 1));
                    bufIndexMap.put((short) (vertexPos + 3));
                } else {
                    bufIndexMap.put((short) vertexPos);
                    bufIndexMap.put((short) (vertexPos + 1));
                    bufIndexMap.put((short) (vertexPos + 2));

                    bufIndexMap.put((short) vertexPos);
                    bufIndexMap.put((short) (vertexPos + 3));
                    bufIndexMap.put((short) (vertexPos + 1));
                }

                {
                    vertexPos += 4;
                    indexPos += 6;

                    bufferPos++;
                }

                {
                    context.cursorX = posX;
                    context.cursorY = posY;

                    context.prevCodePoint = cp;
                }

                if (bufferPos >= BUFFER_SIZE) {
                    flush();
                }
            }
        }


        @Override
        public void flush() {
            if (!inBeginDrawBlock) {
                throw new IllegalStateException();
            }

            if (bufferPos == 0) {
                return;
            }

            unmapBuffers();

            drawBuffer();

            mapBuffers();
        }


        @Override
        public void endDraw() {
            if (!inBeginDrawBlock) {
                throw new IllegalStateException();
            }

            if (bufferPos == 0) {
                unmapBuffers();
            } else {
                unmapBuffers();

                drawBuffer();
            }

            texture.unbind();

            shader.endUse();

            inBeginDrawBlock = false;
        }


        protected void mapBuffers() {
            bufPosition.bind();
            bufPositionMap = bufPosition.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY).asFloatBuffer();
            bufPosition.unbind();

            bufTexCoord.bind();
            bufTexCoordMap = bufTexCoord.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY).asFloatBuffer();
            bufTexCoord.unbind();

            bufTexPage.bind();
            bufTexPageMap = bufTexPage.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY).asShortBuffer();
            bufTexPage.unbind();

            bufIndex.bind();
            bufIndexMap = bufIndex.mapBuffer(GLBuffer.MapAccess.WRITE_ONLY).asShortBuffer();
            bufIndex.unbind();
        }


        protected void drawBuffer() {
            vao.bind();

            glDrawElements(GL_TRIANGLES, indexPos, GL_UNSIGNED_SHORT, 0);

            vertexPos = 0;
            indexPos = 0;

            bufferPos = 0;

            vao.unbind();
        }


        protected void unmapBuffers() {
            bufPositionMap.rewind();
            bufTexCoordMap.rewind();
            bufTexPageMap.rewind();
            bufIndexMap.rewind();

            bufPosition.bind();
            bufPosition.unmapBuffer();
            bufPosition.unbind();

            bufTexCoord.bind();
            bufTexCoord.unmapBuffer();
            bufTexCoord.unbind();

            bufTexPage.bind();
            bufTexPage.unmapBuffer();
            bufTexPage.unbind();

            bufIndex.bind();
            bufIndex.unmapBuffer();
            bufIndex.unbind();
        }


        @Override
        public void dispose() {
            if (inBeginDrawBlock) {
                throw new IllegalStateException();
            }

            if (this.disposed) {
                return;
            }

            vao.dispose();

            bufPosition.dispose();
            bufTexCoord.dispose();
            bufTexPage.dispose();
            bufIndex.dispose();

            quad.free();

            MemoryUtil.memFree(bufPosX);
            MemoryUtil.memFree(bufPosY);

            this.disposed = true;
        }

    }


    protected ByteBuffer ttf;
    protected STBTTFontinfo stbFontInfo;

    public Page[] pages = new Page[(MAX_UNICODE_CODEPOINT + 1) / CHARS_PER_PAGE];

    protected GLTexture texture;

    protected int maxCharHeight = 0;

    protected int fontSize;

    protected float scaleFactor;

    protected int bitmapWidth = 0;
    protected int bitmapHeight = 0;

    protected float ascent;
    protected float descent;
    protected float lineGap;


    public TrueTypeFont() {
    }


    @Override
    public int getFontSize() {
        return fontSize;
    }


    @Override
    public int getAscent() {
        return (int) Math.ceil(ascent);
    }


    @Override
    public int getDescent() {
        return (int) Math.ceil(descent);
    }


    @Override
    public int getLineGap() {
        return (int) Math.ceil(lineGap);
    }


    public static TrueTypeFont loadFont(ResourceInfo resourceInfo, int fontSize, int oversample_h, int oversample_v)
            throws IOException {

        long ttfSize = resourceInfo.getResourceSize();

        if (ttfSize > Integer.MAX_VALUE) {
            throw new IOException("TTF file \"" + resourceInfo.getResourcePath() + "\" is too large (" + ttfSize + ")");
        }

        ByteBuffer ttf = MemoryUtil.memAlloc((int) ttfSize);

        TrueTypeFont font = null;

        try (InputStream in = resourceInfo.getResourceStream(); ReadableByteChannel channel = Channels.newChannel(in)) {
            channel.read(ttf);
        }

        ttf.rewind();

        font = new TrueTypeFont();
        font.fontSize = fontSize;
        font.ttf = ttf;

        STBTTFontinfo fontInfo = STBTTFontinfo.malloc();

        if (!stbtt_InitFont(fontInfo, ttf, stbtt_GetFontOffsetForIndex(ttf, 0))) {
            throw new RuntimeException("Unable to initialize font \"" + resourceInfo.getResourcePath() + "\"");
        }

        font.stbFontInfo = fontInfo;

        int unscaledAscent;
        int unscaledDescent;
        int unscaledLineGap;

        try (MemoryStack stack2 = MemoryStack.stackPush()) {
            IntBuffer ascentBuf = stack2.mallocInt(1);
            IntBuffer descentBuf = stack2.mallocInt(1);
            IntBuffer lineGapBuf = stack2.mallocInt(1);

            stbtt_GetFontVMetrics(fontInfo, ascentBuf, descentBuf, lineGapBuf);

            unscaledAscent = ascentBuf.get(0);
            unscaledDescent = descentBuf.get(0);
            unscaledLineGap = lineGapBuf.get(0);
        }

        float scaleFactor = font.scaleFactor = stbtt_ScaleForPixelHeight(fontInfo, fontSize);

        font.ascent = unscaledAscent * scaleFactor;
        font.descent = -unscaledDescent * scaleFactor;
        font.lineGap = unscaledLineGap * scaleFactor;

        int left, bottom, right, top;

        try (MemoryStack stack3 = MemoryStack.stackPush()) {
            IntBuffer leftBuf = stack3.mallocInt(1);
            IntBuffer bottomBuf = stack3.mallocInt(1);
            IntBuffer rightBuf = stack3.mallocInt(1);
            IntBuffer topBuf = stack3.mallocInt(1);

            stbtt_GetFontBoundingBox(fontInfo, leftBuf, bottomBuf, rightBuf, topBuf);

            left = leftBuf.get(0);
            bottom = bottomBuf.get(0);
            right = rightBuf.get(0);
            top = topBuf.get(0);
        }

        int fontBBWidth = (int) Math.ceil(Math.abs(right - left) * scaleFactor);
        int fontBBHeight = (int) Math.ceil(Math.abs(top - bottom) * scaleFactor);

        int bitmapWidth = fontBBWidth * 16;
        int bitmapHeight = fontBBHeight * 16;

        bitmapWidth *= oversample_h;
        bitmapHeight *= oversample_v;

        font.bitmapWidth = bitmapWidth;
        font.bitmapHeight = bitmapHeight;

        GLTexture texture = new GLTexture(GLTexture.Target.TEXTURE_2D_ARRAY, GLTexture.InternalFormat.R8);

        font.texture = texture;

        try {

            texture.bind();

            texture.setMinFilter(GLTexture.Filter.LINEAR);
            texture.setMagFilter(GLTexture.Filter.LINEAR);

            GLTexture.unpackAlignment(GLTexture.PackAlignment.ALIGNMENT_1);

            texture.texImage3D(0,
                               bitmapWidth,
                               bitmapHeight,
                               font.pages.length,
                               GLTexture.DataFormat.RED,
                               GLTexture.DataType.UNSIGNED_BYTE,
                               null);

            try (STBTTPackContext context = STBTTPackContext.malloc()) {

                for (int pageIdx = 0, firstPageCodepoint = 0;
                        firstPageCodepoint <= MAX_UNICODE_CODEPOINT;
                        pageIdx++, firstPageCodepoint += CHARS_PER_PAGE) {

                    Page page = font.pages[pageIdx] = new Page();

                    GLTextureData bitmap = null;

                    try {
                        bitmap = new GLTextureData(GLTextureData.Dimension.TEXTURE_2D,
                                                   bitmapWidth,
                                                   bitmapHeight,
                                                   1,
                                                   GLTexture.InternalFormat.R8,
                                                   GLTexture.DataFormat.RED,
                                                   GLTexture.DataType.UNSIGNED_BYTE,
                                                   GLTexture.PackAlignment.ALIGNMENT_1,
                                                   bitmapWidth * bitmapHeight);

                        if (!stbtt_PackBegin(context, bitmap.getDataBuffer(), bitmapWidth, bitmapHeight, 0, 1)) {
                            throw new RuntimeException("Unable to begin pack for page " + pageIdx);
                        }
                        try {
                            stbtt_PackSetOversampling(context, oversample_h, oversample_v);

                            if (!stbtt_PackFontRange(context,
                                                     ttf,
                                                     0,
                                                     fontSize,
                                                     firstPageCodepoint,
                                                     page.stbPackedCharBuffer)) {

                                throw new RuntimeException("Unable to pack font for page " + pageIdx);
                            }

                        } finally {
                            stbtt_PackEnd(context);
                        }

                        texture.texSubImage3DArrayLayer(bitmap, pageIdx);

                    } finally {
                        if (bitmap != null) {
                            bitmap.dispose();
                        }
                    }
                }

            }

            texture.generateMipmap();

            texture.unbind();

        } catch (Exception e) {
            texture.dispose();

            throw e;
        }

        return font;
    }


    @Override
    public TextBatch createTextBatch() {
        ensureUndisposed();

        return new TrueTypeFontTextBatch();
    }


    @Override
    public float measureTextWidth(String text) {
        throw new UnsupportedOperationException();
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        for (Page page : pages) {
            page.stbPackedCharBuffer.free();
        }

        stbFontInfo.free();

        MemoryUtil.memFree(ttf);

        texture.dispose();

        disposed = true;
    }

}
