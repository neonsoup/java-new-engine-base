package com.neonsoup.engine.graphics.text;

import com.neonsoup.engine.Disposable;

public interface Font extends Disposable {

    int getFontSize();


    int getAscent();


    int getDescent();


    int getLineGap();


    TextBatch createTextBatch();


    float measureTextWidth(String text);

}
