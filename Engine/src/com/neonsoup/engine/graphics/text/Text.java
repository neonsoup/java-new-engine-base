package com.neonsoup.engine.graphics.text;

import org.joml.Matrix4f;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.graphics.RenderTechnique;
import com.neonsoup.engine.graphics.SceneRenderer;
import com.neonsoup.engine.res.ResourceHandle;
import com.neonsoup.engine.scene.Camera;
import com.neonsoup.engine.scene.Scene;
import com.neonsoup.engine.scene.SceneNode;

public class Text extends AbstractDisposable {

    protected final ResourceHandle<Font> fontHandle;
    protected final Font font;

    protected final TextBatchAttributes batchAttributes;

    protected final TextBatch batch;

    protected final TextDrawingContext context = new TextDrawingContext();

    protected String text;

    protected final Matrix4f projectionMatrix = new Matrix4f();


    public Text(ResourceHandle<Font> fontHandle, TextBatchAttributes batchAttributes, String text) {
        this.fontHandle = fontHandle.copy();
        this.font = fontHandle.getResourceObject();

        this.batchAttributes = batchAttributes;

        this.batch = font.createTextBatch();

        setText(text);
    }


    public void setText(String text) {
        this.text = text;
    }


    public void draw(SceneRenderer renderer, RenderTechnique technique, Scene scene, SceneNode sceneNode) {
        context.reset();

        Camera camera = scene.getRenderCameraNode().getCamera();

        projectionMatrix.set(camera.getProjectionMatrix());
        projectionMatrix.mul(camera.getViewMatrix());
        projectionMatrix.mul(sceneNode.getGlobalTransformMatrix());

        batch.beginDraw(projectionMatrix, batchAttributes);
        batch.drawString(context, text);
        batch.endDraw();
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        fontHandle.release();
        batch.dispose();

        disposed = true;
    }

}
