package com.neonsoup.engine.graphics;

import com.neonsoup.engine.app.Application;
import com.neonsoup.engine.graphics.gl.GLStateProperties;
import com.neonsoup.engine.scene.Scene;

public abstract class SceneRenderer {

    protected final Application app;
    protected Scene scene;

    protected final GLStateProperties renderStateProps = new GLStateProperties();


    public SceneRenderer(Application app) {
        this.app = app;
    }


    public Scene getScene() {
        return scene;
    }


    public void setScene(Scene scene) {
        this.scene = scene;
    }


    public GLStateProperties getRenderStateProperties() {
        return renderStateProps;
    }


    public void render() {
        renderStateProps.beginDraw(app.getGraphics().getDefaultGLStateProperties());

        renderImpl();

        renderStateProps.endDraw();
    }


    protected abstract void renderImpl();

}
