package com.neonsoup.engine.graphics;

import java.nio.ByteBuffer;

import com.neonsoup.engine.graphics.gl.GLTexture;
import com.neonsoup.engine.graphics.gl.GLTexture.DataFormat;
import com.neonsoup.engine.graphics.gl.GLTexture.DataType;
import com.neonsoup.engine.graphics.gl.GLTexture.Filter;
import com.neonsoup.engine.graphics.gl.GLTexture.InternalFormat;
import com.neonsoup.engine.graphics.gl.GLTexture.PackAlignment;
import com.neonsoup.engine.graphics.gl.GLTexture.Target;
import com.neonsoup.engine.graphics.gl.GLTextureData;

public class Texture2D extends Texture {

    private final int width, height;


    public Texture2D(int width, int height, GLTexture glTexture) {
        this.width = width;
        this.height = height;

        this.glTexture = glTexture;

        this.minFilter = glTexture.getMinFilter();
        this.magFilter = glTexture.getMagFilter();
    }


    public Texture2D(int width,
                     int height,
                     Filter minFilter,
                     Filter magFilter,
                     InternalFormat internalFormat,
                     DataFormat dataFormat,
                     DataType dataType,
                     ByteBuffer data) {

        this(width,
             height,
             minFilter,
             magFilter,
             internalFormat,
             dataFormat,
             dataType,
             PackAlignment.ALIGNMENT_1,
             data);
    }


    public Texture2D(int width,
                     int height,
                     InternalFormat internalFormat,
                     DataFormat dataFormat,
                     DataType dataType,
                     ByteBuffer data) {

        this(width, height, null, null, internalFormat, dataFormat, dataType, data);
    }


    public Texture2D(int width,
                     int height,
                     InternalFormat internalFormat,
                     DataFormat dataFormat,
                     DataType dataType,
                     PackAlignment unpackAlignment,
                     ByteBuffer data) {

        this(width, height, null, null, internalFormat, dataFormat, dataType, unpackAlignment, data);
    }


    public Texture2D(int width,
                     int height,
                     Filter minFilter,
                     Filter magFilter,
                     InternalFormat internalFormat,
                     DataFormat dataFormat,
                     DataType dataType,
                     PackAlignment unpackAlignment,
                     ByteBuffer data) {

        if (minFilter == null) {
            minFilter = Filter.DEFAULT_MIN;
        }
        if (magFilter == null) {
            magFilter = Filter.DEFAULT_MAG;
        }

        this.width = width;
        this.height = height;

        this.minFilter = minFilter;
        this.magFilter = magFilter;

        glTexture = new GLTexture(Target.TEXTURE_2D, internalFormat);

        glTexture.bind();

        try {
            glTexture.setMinFilter(minFilter);
            glTexture.setMagFilter(magFilter);

            GLTexture.unpackAlignment(unpackAlignment);

            glTexture.texImage2D(0, width, height, dataFormat, dataType, data);

            if (minFilter.isMipmap() && internalFormat.isCompatibleWithGLGenerateMipmap()) {
                glTexture.generateMipmap();
            }

        } finally {
            glTexture.unbind();
        }
    }


    public Texture2D(GLTextureData data) {
        this(data, null, null);
    }


    public Texture2D(GLTextureData data, Filter minFilter, Filter magFilter) {
        if (minFilter == null) {
            minFilter = Filter.DEFAULT_MIN;
        }
        if (magFilter == null) {
            magFilter = Filter.DEFAULT_MAG;
        }

        this.width = data.getWidth();
        this.height = data.getHeight();

        this.minFilter = minFilter;
        this.magFilter = magFilter;

        glTexture = new GLTexture(Target.TEXTURE_2D, data.getInternalFormat());

        glTexture.bind();

        try {
            glTexture.setMinFilter(minFilter);
            glTexture.setMagFilter(magFilter);

            GLTexture.unpackAlignment(data.getPackAlignment());

            glTexture.texImage2D(0, width, height, data.getDataFormat(), data.getDataType(), data.getDataBuffer());

            if (minFilter.isMipmap() && data.getInternalFormat().isCompatibleWithGLGenerateMipmap()) {
                glTexture.generateMipmap();
            }
        } finally {
            glTexture.unbind();
        }
    }


    public int getWidth() {
        return width;
    }


    public int getHeight() {
        return height;
    }

}
