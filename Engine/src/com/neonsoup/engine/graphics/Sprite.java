package com.neonsoup.engine.graphics;

import java.nio.FloatBuffer;
import java.util.Arrays;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.graphics.gl.GLBuffer;
import com.neonsoup.engine.graphics.gl.GLBuffer.MapAccess;
import com.neonsoup.engine.graphics.gl.GLBuffer.Target;
import com.neonsoup.engine.graphics.gl.GLBuffer.Usage;
import com.neonsoup.engine.graphics.gl.GLMesh;
import com.neonsoup.engine.graphics.gl.GLMesh.Mode;
import com.neonsoup.engine.graphics.gl.GLVertexAttributeData;
import com.neonsoup.engine.graphics.gl.GLVertexAttributeSignature;
import com.neonsoup.engine.graphics.gl.GLVertexAttributeSignature.Type;
import com.neonsoup.engine.graphics.material.Material;
import com.neonsoup.engine.graphics.material.MaterialProperties;
import com.neonsoup.engine.graphics.material.MaterialTextureProperties;
import com.neonsoup.engine.res.ResourceHandle;
import com.neonsoup.engine.scene.Scene;
import com.neonsoup.engine.scene.SceneNode;

public class Sprite extends AbstractDisposable {

    public static final String POSITION_ATTRIBUTE_NAME = "in_Position";
    public static final String DIFFUSE_TEXTURE_COORD_ATTRIBUTE_NAME = "in_DiffuseTexCoord";

    public static final String DIFFUSE_TEXTURE_UNIFORM_NAME = "u_DiffuseTexture";

    public static final GLVertexAttributeSignature POSITION_ATTRIB_SIGNATURE;

    public static final GLVertexAttributeSignature DIFFUSE_TEXTURE_COORD_ATTRIB_SIGNATURE;
    static {
        POSITION_ATTRIB_SIGNATURE = new GLVertexAttributeSignature(POSITION_ATTRIBUTE_NAME, Type.FLOAT, 2);

        DIFFUSE_TEXTURE_COORD_ATTRIB_SIGNATURE = new GLVertexAttributeSignature(DIFFUSE_TEXTURE_COORD_ATTRIBUTE_NAME,
                                                                                Type.FLOAT,
                                                                                2);
    }

    protected int techniqueBits;

    protected final GLVertexAttributeSignature[] otherAttribs;

    protected final ResourceHandle<Material> materialHandle;
    protected final Material material;

    protected final GLMesh mesh;
    protected final MaterialMesh matMesh;

    protected final GLVertexAttributeData posData;
    protected final GLVertexAttributeData diffuseTexCoordData;

    protected final ResourceHandle<Texture2D> textureHandle;

    protected final float width, height;


    public Sprite(GLVertexAttributeSignature[] otherAttribs,
                  ResourceHandle<Material> materialHandle,
                  MaterialProperties[] materialProperties,
                  ResourceHandle<Texture2D> textureHandle,
                  float width,
                  float height) {

        this(GLMesh.ProvokingVertex.FIRST,
             otherAttribs,
             materialHandle,
             materialProperties,
             textureHandle,
             width,
             height);
    }


    public Sprite(GLMesh.ProvokingVertex provokingVertex,
                  GLVertexAttributeSignature[] otherAttribs,
                  ResourceHandle<Material> materialHandle,
                  MaterialProperties[] materialProperties,
                  ResourceHandle<Texture2D> textureHandle,
                  float width,
                  float height) {

        this.otherAttribs = Arrays.copyOf(otherAttribs, otherAttribs.length);

        this.materialHandle = materialHandle.copy();
        this.material = this.materialHandle.getResourceObject();

        this.techniqueBits = material.getTechniqueBits();

        if (textureHandle != null) {
            this.textureHandle = textureHandle.copy();
        } else {
            this.textureHandle = null;
        }

        {
            int attribCount = otherAttribs.length + 1;

            if (textureHandle != null) {
                attribCount++;
            }

            GLVertexAttributeData[] attribData = new GLVertexAttributeData[attribCount];

            int attribIdx = 0;

            {
                GLBuffer posBuffer = new GLBuffer(POSITION_ATTRIB_SIGNATURE.numComponents
                                                  * POSITION_ATTRIB_SIGNATURE.type.elementSize
                                                  * 4,
                                                  Target.ARRAY_BUFFER,
                                                  Usage.STATIC_DRAW);

                posBuffer.bind();

                FloatBuffer posBufMap = posBuffer.mapBuffer(MapAccess.WRITE_ONLY).asFloatBuffer();

                posBufMap.put(0.0f);
                posBufMap.put(0.0f);

                posBufMap.put(width);
                posBufMap.put(0.0f);

                posBufMap.put(0.0f);
                posBufMap.put(height);

                posBufMap.put(width);
                posBufMap.put(height);

                posBufMap.rewind();

                posBuffer.unmapBuffer();
                posBuffer.unbind();

                posData = new GLVertexAttributeData(posBuffer, POSITION_ATTRIB_SIGNATURE);

                attribData[attribIdx++] = posData;
            }

            if (textureHandle != null) {
                GLBuffer diffuseTexCoordBuffer = new GLBuffer(DIFFUSE_TEXTURE_COORD_ATTRIB_SIGNATURE.numComponents
                                                              * DIFFUSE_TEXTURE_COORD_ATTRIB_SIGNATURE.type.elementSize
                                                              * 4,
                                                              Target.ARRAY_BUFFER,
                                                              Usage.DYNAMIC_DRAW);

                diffuseTexCoordBuffer.bind();

                FloatBuffer diffuseTexCoordBufferMap = diffuseTexCoordBuffer.mapBuffer(MapAccess.WRITE_ONLY)
                        .asFloatBuffer();

                diffuseTexCoordBufferMap.put(0.0f);
                diffuseTexCoordBufferMap.put(0.0f);

                diffuseTexCoordBufferMap.put(1.0f);
                diffuseTexCoordBufferMap.put(0.0f);

                diffuseTexCoordBufferMap.put(0.0f);
                diffuseTexCoordBufferMap.put(1.0f);

                diffuseTexCoordBufferMap.put(1.0f);
                diffuseTexCoordBufferMap.put(1.0f);

                diffuseTexCoordBufferMap.rewind();

                diffuseTexCoordBuffer.unmapBuffer();
                diffuseTexCoordBuffer.unbind();

                diffuseTexCoordData = new GLVertexAttributeData(diffuseTexCoordBuffer,
                                                                DIFFUSE_TEXTURE_COORD_ATTRIB_SIGNATURE);
                attribData[attribIdx++] = diffuseTexCoordData;
            } else {
                diffuseTexCoordData = null;
            }

            for (int i = 0; i < otherAttribs.length; i++) {
                GLVertexAttributeSignature otherAttrib = otherAttribs[i];

                GLBuffer buffer = new GLBuffer(otherAttrib.numComponents * otherAttrib.type.elementSize * 4,
                                               Target.ARRAY_BUFFER,
                                               Usage.DYNAMIC_DRAW);

                attribData[i + attribIdx] = new GLVertexAttributeData(buffer, otherAttrib);
            }

            this.mesh = new GLMesh(Mode.TRIANGLE_STRIP, provokingVertex, 4, attribData, null);

            int matPropCount = materialProperties.length;

            if (textureHandle != null) {
                matPropCount++;
            }

            MaterialProperties[] newMaterialProperties = Arrays.copyOf(materialProperties, matPropCount);

            int matPropIdx = materialProperties.length;

            if (textureHandle != null) {
                newMaterialProperties[matPropIdx++] = new MaterialTextureProperties(material,
                                                                                    techniqueBits,
                                                                                    DIFFUSE_TEXTURE_UNIFORM_NAME,
                                                                                    textureHandle.getResourceObject(),
                                                                                    0);
            }

            this.matMesh = new MaterialMesh(mesh, material, newMaterialProperties);
        }

        this.width = width;
        this.height = height;
    }


    public float getWidth() {
        return width;
    }


    public float getHeight() {
        return height;
    }


    public GLMesh getMesh() {
        return mesh;
    }


    public Material getMaterial() {
        return material;
    }


    public void setDiffuseTextureCoords(float minU, float minV, float maxU, float maxV) {
        GLBuffer diffuseTexCoordBuffer = this.diffuseTexCoordData.getBuffer();

        diffuseTexCoordBuffer.bind();

        FloatBuffer diffuseTexCoordBufferMap = diffuseTexCoordBuffer.mapBuffer(MapAccess.WRITE_ONLY).asFloatBuffer();

        diffuseTexCoordBufferMap.put(minU);
        diffuseTexCoordBufferMap.put(minV);

        diffuseTexCoordBufferMap.put(maxU);
        diffuseTexCoordBufferMap.put(minV);

        diffuseTexCoordBufferMap.put(minU);
        diffuseTexCoordBufferMap.put(maxV);

        diffuseTexCoordBufferMap.put(maxU);
        diffuseTexCoordBufferMap.put(maxV);

        diffuseTexCoordBufferMap.rewind();

        diffuseTexCoordBuffer.unmapBuffer();
        diffuseTexCoordBuffer.unbind();
    }


    public void draw(SceneRenderer renderer, RenderTechnique technique, Scene scene, SceneNode sceneNode) {
        matMesh.draw(renderer, technique, scene, sceneNode);
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        materialHandle.release();

        if (textureHandle != null) {
            textureHandle.release();
        }

        matMesh.dispose();

        disposed = true;
    }

}
