package com.neonsoup.engine.graphics;

import com.neonsoup.engine.scene.SceneNodeFilter;

public class RenderPass {

    private SceneNodeFilter nodeFilter;


    public RenderPass() {
    }


    public boolean hasFilter() {
        return nodeFilter != null;
    }


    public void setNodeFilter(SceneNodeFilter filter) {
        this.nodeFilter = filter;
    }


    public SceneNodeFilter getNodeFilter() {
        return nodeFilter;
    }

}
