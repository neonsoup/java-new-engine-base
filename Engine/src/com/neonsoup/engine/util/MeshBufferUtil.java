package com.neonsoup.engine.util;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public final class MeshBufferUtil {

    private MeshBufferUtil() {
    }


    public static void vertex3f(FloatBuffer buf, float x, float y, float z) {
        buf.put(x);
        buf.put(y);
        buf.put(z);
    }


    public static void quad2f(FloatBuffer buf, float x0, float y0, float x1, float y1) {
        buf.put(x0);
        buf.put(y0);

        buf.put(x1);
        buf.put(y0);

        buf.put(x0);
        buf.put(y1);

        buf.put(x1);
        buf.put(y1);
    }


    public static void triangleIndex1i(IntBuffer buf, int i0, int i1, int i2) {
        buf.put(i0);
        buf.put(i1);
        buf.put(i2);
    }


    public static void triangleIndex1i(ByteBuffer buf, int i0, int i1, int i2) {
        buf.put((byte) i0);
        buf.put((byte) i1);
        buf.put((byte) i2);
    }

}
