package com.neonsoup.engine.util;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.CachedDisposable;
import com.neonsoup.engine.Keyable;

public class NullCache<T extends CachedDisposable & Keyable> extends AbstractDisposable implements Cache<T> {

    public NullCache() {
    }


    @Override
    public void addUses(Object key, int uses) {
        ensureUndisposed();
    }


    @Override
    public void cacheObject(T res) {
    }


    @Override
    public T popEntry(Object key) {
        ensureUndisposed();

        return null;
    }

}
