package com.neonsoup.engine.util;

import com.neonsoup.engine.CachedDisposable;
import com.neonsoup.engine.Disposable;
import com.neonsoup.engine.Keyable;

public interface Cache<T extends CachedDisposable & Keyable> extends Disposable {

    void addUses(Object key, int uses);


    void cacheObject(T res);


    T popEntry(Object key);

}
