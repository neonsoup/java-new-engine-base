package com.neonsoup.engine.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import com.neonsoup.engine.AbstractDisposable;
import com.neonsoup.engine.CachedDisposable;
import com.neonsoup.engine.Keyable;

import gnu.trove.impl.Constants;
import gnu.trove.map.TObjectIntMap;
import gnu.trove.map.hash.TObjectIntHashMap;

public class UseCountCache<T extends CachedDisposable & Keyable> extends AbstractDisposable implements Cache<T> {

    protected static final int DEFAULT_QUEUE_CAPACITY = 16;


    protected final class QueueRecord {

        protected final T cachedObject;

        protected int uses;


        public QueueRecord(T obj, int uses) {
            this.cachedObject = obj;
            this.uses = uses;
        }

    }


    protected final int capacity;

    protected final ArrayList<QueueRecord> queue;

    protected final Map<Object, QueueRecord> queueRecordsMap;

    protected final TObjectIntMap<Object> uses = new TObjectIntHashMap<>(Constants.DEFAULT_CAPACITY,
                                                                         Constants.DEFAULT_LOAD_FACTOR,
                                                                         0);

    protected final Comparator<QueueRecord> comparator = new Comparator<QueueRecord>() {

        @Override
        public int compare(QueueRecord o1, QueueRecord o2) {
            return Integer.compare(o1.uses, o2.uses);
        }

    };


    public UseCountCache(int capacity) {
        this.capacity = capacity;

        this.queueRecordsMap = new HashMap<>(capacity);
        this.queue = new ArrayList<>(capacity);
    }


    @Override
    public void cacheObject(T obj) {
        ensureUndisposed();

        Object key = obj.getKey();

        int oldUses = uses.get(key);

        if (queue.isEmpty()) {
            if (capacity != 0) {
                QueueRecord newRec = new QueueRecord(obj, oldUses);

                queue.add(newRec);
                queueRecordsMap.put(key, newRec);
            }
        } else {
            QueueRecord lastQueueRecord = queue.get(queue.size() - 1);

            if ((oldUses > lastQueueRecord.uses || lastQueueRecord.cachedObject == obj)) {
                if (lastQueueRecord.cachedObject == obj) {
                    lastQueueRecord.uses = oldUses;
                } else {
                    queueRecordsMap.get(key).uses = oldUses;
                }

                Collections.sort(queue, comparator);
            } else if (queue.size() < capacity || lastQueueRecord.uses < oldUses) {
                if (!insertToQueue(key, obj, oldUses)) {
                    obj.dispose();
                }
            }
        }

        uses.put(key, oldUses);
    }


    @Override
    public void addUses(Object key, int addUses) {
        ensureUndisposed();

        int oldUses = uses.get(key);
        int newUses = oldUses + addUses;

        uses.put(key, newUses);
    }


    protected boolean insertToQueue(Object key, T obj, int newUses) {
        int i;
        for (i = 0; i < queue.size(); i++) {
            if (queue.get(i).uses < newUses) {
                break;
            }
        }

        if (i >= capacity) {
            return false;
        }

        if (queue.size() == capacity) {
            T oldObj = queue.remove(capacity - 1).cachedObject;

            queueRecordsMap.remove(oldObj.getKey());

            oldObj.dispose();
        }

        QueueRecord newRec = new QueueRecord(obj, newUses);

        queue.add(i, newRec);
        queueRecordsMap.put(obj.getKey(), newRec);

        return true;
    }


    @Override
    public T popEntry(Object key) {
        ensureUndisposed();

        QueueRecord record = queueRecordsMap.remove(key);

        if (record != null) {
            queue.remove(record);
            return (T) record.cachedObject;
        }

        return null;
    }


    @Override
    public void dispose() {
        if (disposed) {
            return;
        }

        for (QueueRecord record : queue) {
            record.cachedObject.dispose();
        }

        queue.clear();
        queueRecordsMap.clear();
        uses.clear();

        disposed = true;
    }

}
